<?php
class Order_mapper {
    
    private $input;
    const PREFIX_CLIENT     = 'client_';
    const PREFIX_AGENT      = 'agent_';
    const PREFIX_INSPECTOR  = 'inspector_';
    
    const SOURCE_POST = 'post';
    const SOURCE_GET  = 'get';
    
    public function __construct( $input ){
        if( $input ){
            $this->input = $input['input'];
        }
    }
    /**
    * Marshals object of type $class_name from input(GET, POST), if namespace is present, 
    * only Input elements with namespace prefix will be evaluated. Validatio is implemented 
    * in Object setters and RECORD::validate()
    * 
    * @param input  $input
    * @param object $class_name
    * @param string $source POST/GET
    * @param string $namespace
    */
    public function create_object_from_input( $class_name, $source = self::SOURCE_POST, $namespace = '' ){
        
        $properties = get_class_vars( $class_name );
        $calls      = get_class_methods( $class_name );
        
        $object = null;
        if( $properties ){
            
            $object = new $class_name;
            
            foreach( $properties as $property=>$value ){
                //Remove hardcode
                if( $property == 'agent') continue;
                
                if( ($value = $this->input->$source( $namespace . $property, TRUE )) !== FALSE ){
                    $call = 'set_'.$property;
                    if( in_array( $call, $calls )){
                        $object->$call( $value );           
                    }                     
                }
            }
        }

        return $object;
    } 
    public function create_object_array_from_input( $class_name, $source = self::SOURCE_POST, $prefix_pattern = '<name>_<index>',  $min_index = 0, $max_index = 20 ){

        $properties = get_class_vars( $class_name );
        $calls      = get_class_methods( $class_name );
        
        $objects = array();
        if( $properties ){
            //Lets go from min_index to max_index to try and find objects
            for( $i=$min_index; $i<=$max_index; $i++ ){
                
                $object = new $class_name;           
                foreach( $properties as $property=>$value ){
                    //We create input property from prefix pattern <name> is replaced by $name, <index> by $index i.e price_1, price_2
                    $input_property = preg_replace( array('/<name>/','/<index>/'), array( $property, $i ), $prefix_pattern );            
                    $value = $this->input->$source( $input_property, TRUE );
                    
                    if( $value ){
                        $call = 'set_'.$property;
                        if( in_array( $call, $calls )){
                            $object->$call( $value );           
                        }                     
                    }
                }
                $objects[] = $object;                
            }
        }
        return $objects;               
    }
    public function create_object_array_from_input_values( $class_name, $source = self::SOURCE_POST, $prefix_pattern = '<name>_<value>'){

        $properties = get_class_vars( $class_name );
        $calls      = get_class_methods( $class_name );
        
        $objects = array();
        if( $properties ){
            //Lets go from min_index to max_index to try and find objects
            foreach( $this->input->$source() as $name=>$value ){
                
                $object = new $class_name;
                $empty  = true;           
                foreach( $properties as $property=>$value ){
                    
                    $temp_pattern = $prefix_pattern;
                    $temp_pattern = preg_replace( '/<name>/',  $property , $temp_pattern );
                    if( preg_match( $temp_pattern, $name, $match )){
                        $call = 'set_'.$property;
                        if( in_array( $call, $calls )){
                            $object->$call( $match['value'] );
                            $empty = false;           
                        }                         
                    }

                }
                if( !$empty ){
                    $objects[] = $object;    
                }
                                
            }
        }
        return $objects;               
    }
    public function create_object_array_from_input_with_array( $class_name, $source = self::SOURCE_POST, $prefix_pattern = '<name>_<value>'){

        $properties = get_class_vars( $class_name );
        $calls      = get_class_methods( $class_name );
        
        $objects = array();
        if( $properties ){
            //Lets go from min_index to max_index to try and find objects
            foreach( $this->input->$source() as $name=>$value ){
                
                $object = new $class_name;
                $empty  = true;           
                foreach( $properties as $property=>$value ){
                    
                    $temp_pattern = $prefix_pattern;
                    $temp_pattern = preg_replace( '/<name>/',  $property , $temp_pattern );
                    if( preg_match( $temp_pattern, $name, $match )){
                        $call = 'set_'.$property;
                        if( in_array( $call, $calls )){
                            $object->$call( $match['value'] );
                            $empty = false;           
                        }                         
                    }

                }
                if( !$empty ){
                    $objects[] = $object;    
                }
                                
            }
        }
        return $objects;               
    }          
}  
?>
