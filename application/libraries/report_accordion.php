<?php
class Report_accordion extends Accordion{
    
    public function get_category_container( Categories $categories, $category_id, $category_index ){ 
        /**
        * @var Category
        */
        $category = $categories->get_category( $category_id, $category_index);
        $subcategories = '';
        if( $general_desc = $category->get_general_description_subcategory() ){
            $subcategories .= $this->get_subcategory_container( $general_desc, $category_id, $category_index );    
        }
        foreach( $category->_subcategories as $index=>$subcategory ){
            if( $general_desc && $subcategory->id == $general_desc->id ){continue;}
            $subcategories .= $this->get_subcategory_container( $subcategory, $category_id, $category_index );
        }
        
        $subcategories .= $this->get_add_new_subcategory( array(
            'category_id' => $category_id,
            'category_index' => $category_index,        
        ));

        
        return $subcategories;         
    }
    
    public function get_subcategory_container( Subcategory $subcategory, $category_id, $category_index){ 
        $ancestor_ids = array('category_id'=>$category_id, 'category_index'=>$category_index);
        return
        '<div class="subcategory_container" id="subcategory_accordion_'.$subcategory->id.'">                        
            <div class="subcategory_content">
                <fieldset class="control_fieldset">
                    <legend class="subcategory_label status_legend"><i class="icon_toggle icon-minus icon-white pull-left"></i>'.$this->clean($subcategory->name).'</legend>
                        <div class="subdiv">
                            '.$this->get_subcategory_ratings( $subcategory, $ancestor_ids).'
                            <div class="category_values">
                             <fieldset  class="control_fieldset">
                                '.$this->get_descriptions_section($subcategory, $ancestor_ids).'
                            </fieldset>
                            </div>
                            '.$this->get_additional_content($subcategory, $ancestor_ids).'
                        </div>
                </fieldset>
            </div>
        </div>';
    }
    
    public function get_add_new_subcategory( $ancestor_ids ){
       return 
        '<fieldset class="subcategory_container control_fieldset">
            <legend class="subcategory_label status_legend"><i id="icon_add_sub" class="icon_toggle icon-minus icon-white pull-left"></i>New Section:</legend>   
            <div class="add_description form-inline">                    
                <label>Subacategory Name:</label><input type="text" name="excluded" value=""><a id="btn_add_subcategory" '.$this->create_data_string(self::TYPE_SUBCATEGORY, $ancestor_ids).' class="btn">Add</a>                
            </div>
            <a class="btn btn_add_desc">New subcategory</a>               
        </fieldset>';        
    }
    function get_subcategory_ratings( Subcategory $subcategory, $ancestor_ids ){
        $category_id = $ancestor_ids['category_id'];
        $category_index = $ancestor_ids['category_index'];
        $ancestor_ids['subcategory_id'] = $subcategory->id;
        
        $array_ratings = array(
            Subcategory::RATING_GOOD => 'Good',
            Subcategory::RATING_FAIR => 'Fair',
            Subcategory::RATING_POOR => 'Poor',
        );
        
        foreach( $array_ratings as $rating_key=>$rating){
            $labels[] = '<label class="checkbox"><input type="checkbox" class="subcategory_rating" id="rating_'.$rating_key.'"  '.(isset($subcategory->_rating) && $subcategory->_rating == $rating_key ? ' checked="checked" ':'').'  name="'.$this->get_rating_form_name($rating_key, $ancestor_ids).'" >'.$rating.'</label>';
        }
        
        return 
        '<div class="rating form-inline">
            <fieldset class="control_fieldset">
                <legend class="legend_rating status_legend">Status</legend>
                <div>
                    '.implode('', $labels).'
                    <label class="checkbox"><input class="excluded_toggle subcategory_rating" placeholder="Why did you exclude this?" type="checkbox" id="rating_na" '.(isset($subcategory->rating) && $subcategory->rating == Subcategory::RATING_NA ? ' checked="checked" ':'').' name="'.$this->get_rating_form_name(Subcategory::RATING_NA, $ancestor_ids).'" >'.'N/A'.'</label><input type="text" name="excluded" value="" class="excluded">
                </div>                                                
            </fieldset>                
        </div>'; 
    }

    function get_rating_form_name($rating_key, $ancestor_ids){
        $category_id = $ancestor_ids['category_id'];
        $category_index = $ancestor_ids['category_index'];
        $subcategory_id = $ancestor_ids['subcategory_id'];    
        return 'categories['.$category_id.']['.$category_index.']['.$subcategory_id.'][rating]['.$rating_key.']';
    }

    function get_form_name_prefix( $ancestor_ids ){
        $category_id = $ancestor_ids['category_id'];
        $category_index = $ancestor_ids['category_index'];
        $subcategory_id = $ancestor_ids['subcategory_id']; 
        return 'categories['.$category_id.']['.$category_index.']['.$subcategory_id.']';
    }

    function get_descriptions_section(Subcategory $subcategory, $ancestor_ids ){ 
        $ancestor_ids['subcategory_id'] = $subcategory->id;
        $value_accordions = '';
        foreach($subcategory->_values as $value_id => $value ){
            $value_accordions .= $this->get_value_accordion( 
                $value,
                $ancestor_ids,
                is_array($subcategory->_value_ids) ? in_array( $value_id, $subcategory->_value_ids): false
            );
        }
        $value_accordions .= $this->get_new_element_div(self::TYPE_VALUE, $ancestor_ids);
        return $value_accordions;    
    }

    function get_additional_content( Subcategory $subcategory, $ancestor_ids ){
        $ancestor_ids['subcategory_id'] = $subcategory->id;
        return 
        '<div class="content_additional">
            <div class="buttons">                
                <a class="btn_bold btnnotes btn input-small">Notes</a>
                <a class="btn_bold btn input-small picture_button" id="picture_'.$subcategory->id.'">Pictures</a>
            </div>
            <div class="notes">
                <textarea cols="10" rows="5" name="'.$this->get_form_name_prefix($ancestor_ids).'[notes]">'.$subcategory->_comments.'</textarea>
            </div>
            <div class="pictures">
            </div>                                
        </div>';    
    }
    
    function clean( $string ){
        return str_replace('Good, Fair, Poor, None, N/A', '', $string);
    }        
    
}
