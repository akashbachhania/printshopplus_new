<?php
class Report_marshaller{
    
    private $report_model;
    
    public function set_report_model($report_model){
        $this->report_model = $report_model;
    }
    
    public function marshall_category(Report &$report, $submited_categories){
        $is_update = false;   
        if( $is_update = $this->is_category_update($submited_categories) ){
            $category_id = reset(array_keys($submited_categories));
            $index =  reset(array_keys($submited_categories[$category_id]));
            if( !$report->is_category_set($category_id, $index )){
                $report->add_category(
                    $this->report_model->get_category_for_init($category_id, $index ),
                    $index
                );
            }
            foreach( $submited_categories[$category_id][$index] as $subcategory_id=>$subcategories ){
                if( $report_subcategory = $report->get_subcategory($category_id, $index, $subcategory_id )){
                    foreach( $subcategories as $field=>$values ){
                        switch( $field ){
                            case 'rating':
                                $report_subcategory->add_rating( reset(array_keys($values)));
                                break;
                            case 'value_ids':
                                $report_subcategory = $this->set_subcategory_values($report_subcategory, $values);
                                break;
                            case 'notes':
                                $report_subcategory->add_comments($values);
                                break;
                        }
                    }                    
                }
            }                                   
       }
       return $is_update;
    }
    
    protected function set_subcategory_values( Subcategory $report_subcategory, $values ){
        $selected_values = array();
        foreach( $values as $id=>$value){
            $selected_values[] = $id;
            if( ($report_value = $report_subcategory->get_value($id)) &&
                isset($values[$id]['concern_ids']) && 
                is_array($values[$id]['concern_ids'])
            ){
                $report_value->add_selected_concerns(array_keys($values[$id]['concern_ids'])); 
            }                                    
        }
        $report_subcategory->add_selected_values($selected_values); 
        return $report_subcategory;  
    }
    
    protected function is_category_update($categories){   
        $changed = false;
        if( is_array($categories)){
            foreach($categories as $category_id=>$data){
                foreach( $data as $index=> $subcategories ){
                    foreach($subcategories as  $values){
                        foreach( $values as $field=>$value){
                            if(in_array($field, array('rating','value_ids'))){
                                $changed = true;
                                break;
                            }
                            if( $field == 'notes' && $value != ''){
                                $changed = true;
                                break;
                            }                    
                        }
                    }                
                }
            }        
        }
        return $changed;
    }    
}
