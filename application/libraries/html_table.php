<?php
class Html_table{
    
    private $table;
    private $rows;
    private $class;
    public function __construct(){
        $this->rows = array();
        $this->table = '';
        $this->class = 'table table-bordered';
        
    }
    public function add_row( array $columns, $attributes = array(), $tag = 'td'){
        $row = '';
		
        foreach( $columns as $column ){
            if( is_array( $column )){
                $attr_str = $this->get_attributes( $column['attributes']);
                $row .="<$tag $attr_str>".$column['data']."</$tag>";
            } else {
                $row .="<$tag>$column</$tag>";    
            }
            
        }
        $attr_str = $this->get_attributes( $attributes);
        
        $row = "<tr $attr_str>$row</tr>";
        if( $tag == 'td'){
            $this->rows[] = $row;    
        }
        return $row;
        
    }
	public function add_order_row( array $columns, $attributes = array(), $tag = 'td'){
        $row = '';
        foreach( $columns as $column ){
            if( is_array( $column )){
                $attr_str = $this->get_attributes( $column['attributes']);	
                $row .="<$tag $attr_str>".$column['data']."</$tag>";
            } else {
                $row .="<$tag>$column</$tag>";    
            }
            
        }
        $attr_str = $this->get_attributes( $attributes);
        
        $row = "<tr $attr_str>$row</tr>";
        if( $tag == 'td'){
            $this->rows[] = $row;    
        }
        return $row;
        
    }
    public function set_heading( array $columns, array $attributes = array() ){
        $this->heading_row = $this->add_row( $columns, $attributes, 'th');
    }
    public function generate(){
        $html =
            "<table ".($this->class ? 'class="'.$this->class.'"' : '').">".
                ($this->heading_row ?"<thead>$this->heading_row</thead>":'').
                "<tbody>".implode('',$this->rows)."</tbody>".
            "</table>";
            
        return $html;
    }
	
	 public function generate_order_table(){
        $html =
            "<table ".($this->class ? 'class="'.$this->class.'"' : '').">".
                ($this->heading_row ?"<thead>$this->heading_row</thead>":'').
                "<tbody>".implode('',$this->rows)."</tbody>".
            "</table>";
            
        return $html;
    }
	
    private function get_attributes( $attributes ){
        $_attrs = array();
        foreach ($attributes as $key=>$value) {
            $_attrs[] = $key.'="'.$value.'"';
        }
        $attr_str = implode(' ', $_attrs);
        return $attr_str;        
    }
    public function clear(){
        $this->table = '';
        $this->rows = array();
    }
}  
?>
