<?php
class Authorize_payment{
    const STATUS_FAILED  = 'Failed';
    const STATUS_SUCCESS = 'Success';
    
    public $request  = 'No request sent';
    public $response = 'No response';
     
    public function send_payment_request( $amount, $cc_num, $cc_date, $order_id, $merchant, $transaction, $url, $transaction_type = 'authCaptureTransaction' ){
        $response = array();
        $response['status'] = self::STATUS_FAILED;
        $response['message'] = 'Request has not been sent to Authorize.net';
          
        if( $merchant && $transaction && $url ){
            $request = 
            '<?xml version="1.0" encoding="utf-8"?>
            <createTransactionRequest xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
                <merchantAuthentication>
                    <name>'.$merchant.'</name>
                    <transactionKey>'.$transaction.'</transactionKey>
                </merchantAuthentication>
                <transactionRequest>
                    <transactionType>'.$transaction_type.'</transactionType>
                    <amount>'.$amount.'</amount>
                    <payment>
                        <creditCard>
                            <cardNumber>'.$cc_num.'</cardNumber>
                            <expirationDate>'.$cc_date.'</expirationDate>
                        </creditCard>
                    </payment>
                </transactionRequest>
            </createTransactionRequest>';
            
            $xml_response = $this->curl( $request, $url );
             
            try{
                $xml = @new SimpleXMLElement($xml_response);
                $response_code = (string)$xml->transactionResponse->responseCode;
                if($response_code == '1'){
                    $response['status']  = self::STATUS_SUCCESS;
                    $response['message'] = $xml->transactionResponse->messages->message->description .'(AuthCode:'.(string)$xml->transactionResponse->authCode.', transID:'.(string)$xml->transactionResponse->transId.')';
                } else {
                    $response['status']  = self::STATUS_FAILED;
                    
                    if(isset($xml->transactionResponse->messages)){
                        $response['message'] = (string)$xml->transactionResponse->messages->message->code . ':'.(string)$xml->transactionResponse->messages->message->text;    
                    } else if( isset($xml->transactionResponse->errors) ) {
                        $response['message'] = (string)$xml->transactionResponse->errors->error->errorCode . ':'.(string)$xml->transactionResponse->errors->error->errorText;
                    } else if( isset($xml->messages->message ) ){
                        $response['message'] = (string)$xml->messages->message->text;
                    } else {
                        $response['message'] = 'Transaction failed';
                    }
                     
                }                
            } catch( Exception $e ){
                $response['message'] = 'There was error while proceesing response from paymnent gateway, please check merchant interface backoffice';
                $response['status']  = self::STATUS_FAILED;
            }
 
                         
        }
        
        return $response;
    }
    /**
    <createTransactionResponse xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
       <messages>
          <resultCode>Ok</resultCode>
          <message>
             <code>I00001</code>
             <text>Successful.</text>
          </message>
       </messages>
       <transactionResponse>
          <responseCode>1</responseCode>
          <authCode>3DRNCE</authCode>
          <avsResultCode>Y</avsResultCode>
          <cvvResultCode/>
          <cavvResultCode>2</cavvResultCode>
          <transId>2177698954</transId>
          <refTransID/>
          <transHash>C018095242A7F7E3FA6816C8FE0438A0</transHash>
          <testRequest>0</testRequest>
          <accountNumber>XXXX0015</accountNumber>
          <accountType>MasterCard</accountType>
          <messages>
             <message>
                <code>1</code>
                <description>This transaction has been approved.</description>
             </message>
          </messages>
       </transactionResponse>
    </createTransactionResponse>
    * @param mixed $request
    */
    private function curl( $xml, $url ){
 

        $headers = array( 'Content-Type: text/xml' );
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_VERBOSE, TRUE);
        curl_setopt($curl, CURLOPT_URL, $url );
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers );
        $result = curl_exec ($curl);

        if(curl_error($curl)){
            $result = curl_error($curl); 
        }
        curl_close ($curl);

        $this->request = preg_replace('!cardNumber>\d+</cardNumber>!','cardNumber>XXXX</cardNumber>', $xml);
        $this->response = $result;
                
        return $result;                            
    }
}
