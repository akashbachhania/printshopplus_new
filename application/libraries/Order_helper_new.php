<?php
class Order_helper_new{       
    
    private $cached = array();
    
    const ELEMENT_CHECKBOX = 'checkbox';
    const ELEMENT_DROPDOWN = 'dropdown';
    const ELEMENT_TEXTAREA = 'textarea';
    const ELEMENT_TEXT     = 'text';
    const ELEMENT_SUBMIT     = 'submit';
    const INLINE_END = 'inline';
    
    const FORM_INLINE      = 'inline';
    const FORM_SEARCH      = 'search';
    const FORM_HORIZONTAL  = 'horizontal';
    
    /**
    * Add new checkbox id pattern
    */
    const ADD_NEW_CHECKBOX = 'add_new_item';
    private $table_name_override = array(
    
    );
    private $name_override = array(
        'agent::name' => 'Company',
        'report_due' => 'Order due date'
    );
    /**
    * Fields below are not part of form
    */
    private $ignore = array(
        'table','datetime', 'errors', 'user_id', 'required', 'active'
    );
    /**
    * These fields are part of the form, but are hidden
    */
    private $hidden = array(
        'id','type','company_id','person_type'
    );
    
    private $label_override = array(
        'inspector_id' => 'Inspector',
        'report_due' => 'Order due date' ,
        'po_text' => 'PO' 
    );
    /**
    * Create DIV element with input fiels for this Record object or its subset
    * 
    * @param Record $object
    * @param array  $attributes
    * @param string $type
    * @param array  $special
    * @param string $id_prefix
    */
      public function get_object_form( $object, $attributes = array(), $type = null, $special = array(), $id_prefixes = null, $overrides = array() ){
          
        $string  = '';        
        $counter = 0;
        
        $default_class = $type == self::FORM_INLINE ? 'input-small' : 'input-large';
		
		if($type == 'largeOrderNotes' || $type == 'emailcss' )
		{
			$default_class = 'input-largeOrderNotes';
		}
        
		
         
        if( $attributes ){
            $fields = $attributes;
        } else {
            $fields = array_diff( array_keys(get_object_vars( $object )), $this->ignore );
        }
                                                                                                              
        foreach( $fields as $name ){
             
            $value = $this->get_object_value( $object, $name );
            
            if( is_array ( $value )) {
                $string.= $this->get_array_field( $name, $value, $object, $special, $id_prefixes, $type, $overrides, $default_class );
            }  else {
                //Some fiels are hidden (ids, etc )
                //Display flag overrides this
                $input_type = 'text';
                if( in_array( preg_replace('/.+::/','',$name), $this->hidden ) && !isset( $special[$name]['display'])){
                    $input_type = 'hidden';
                }            
                //prefix id with param prefix or class name
                
                $id_prefix = '';
                if( $id_prefixes ){
                     
                    $object_name = $this->get_object_name( $object, $name );
                    
                    if( is_array( $id_prefixes) && isset( $id_prefixes[$object_name])){
                       $id_prefix = $id_prefixes[$object_name];  
                    } else if( is_string( $id_prefixes )){
                       $id_prefix = $id_prefixes; 
                    }
                }
                
                $id_name = preg_replace('/.+::/','',$name);
                if( isset($id_prefix) && $id_prefix ){
                    $id = $id_prefix . '_' .$id_name;    
                } else{
                    if( get_class( $object ) == 'Item'){
                        $id = $id_name.'_'.$counter++;    
                    } else {
                        $id = $id_name;
                    }
                }
                
                $control = $this->get_control( $id, $name,$value, $object, $special, $default_class );
                /**
                <span class="add-on">$</span>
                <input id="appendedPrependedInput" class="span2" type="text" size="16">
                <span class="add-on">.00</span>             
                
                */
                if( $special && isset( $special[ $name ]['prepend']) ){
                    $control = '<div class="input-group-btn"><span class="btn add-on">'.$special[ $name ]['prepend'].'</span></div> '. $control;             
                }
                /**
                * Is this element displayed inline with some other element?
                */
                if( $special && isset( $special[ $name ]['inline'])){
                    $inline = $special[ $name ]['inline'];
                }
                /**
                * Second inline element found, will be merged with first
                */
                if( isset($inline) && $name == $inline ){
                    $inline = self::INLINE_END;
                }
                /**
                * If this field is wrong
                */
                $error = '';
                if( in_array( $name, array_keys($object->errors) )){
                    $error = ' error';
                }                 
                
                if( isset( $overrides[ $name ]) ){
                    $name = $overrides[ $name ];
                } else if( isset( $this->name_override[ $name ] )){
                    $name = $this->name_override[ $name ];
                }          
                //Hidden elements dont have label 
               
                $label   = $input_type == 'hidden' ? '' : ( isset( $this->label_override[$name]) ? $this->label_override[$name] : ucfirst(preg_replace('/.+::/','',  str_replace('_',' ', $name))));
                /**
                * Is this inline form or horizontal ( classic ) form ?
                */
                switch( $type ){
                    case self::FORM_INLINE:
                        $form_class = 'form-inline';
                        $string .= '<label class="control-label '.$error.'" for="'.$id.'">'.$label.'</label>
                                    '.$control.'';    
                    break;
                    case self::FORM_SEARCH:
                    break;
                    default:
                        if( isset($inline) && $inline ){
                            if( $inline == self::INLINE_END) {
                                
                                $id = $inline_data['id'];
                                $label = $inline_data['label'];
                                $error = $inline_data['error'];
                                $control = $inline_data['control'] . $control;                                
                                $inline_data = array();
                                $inline = false;                                                         
                            } else {
                                $inline_data = array();
                                $inline_data['id'] = $id;
                                $inline_data['label'] = $label;
                                $inline_data['error'] = $error;
                                $inline_data['control'] = $control;                            
                            }                            
                        }

                        if( !isset($inline) || !$inline ){
                            $form_class = 'form-horizontal';
                            $string .= '<div class="form-group'.$error.'">
                                            <label class="control-label col-lg-4" for="'.$id.'" style="font-size:18px; font-weight:300;">'.$label.'</label>                            
                                            <div class="controls input-group add-on col-md-5">                           
                                                '.$control.'                                         
                                            </div>
                                       </div>';                            
                        }                            
                        break;                
                }                
            }
            
             

     
                             
        }

        
        if( $special && isset( $special['__form']['hidden'] )){
            foreach( $special['__form']['hidden'] as $field=>$value){
                $string .= '<input type="hidden" id="'.$field.'" name="'.$field.'" value="'.$value.'">';
            }
        }
        
        $legend = '';
        if( $special && isset( $special['__legend'])){
            $legend = '<legend>' .$special['__legend'].'</legend>';
        }
        $element = 'div';
        $submit  = '';
        $attributes  = '';  
        if( $special && isset( $special['__form'])){
            $element = 'form';
            $attributes = ' action="'.$special['__form']['action'].'" method="post" ';
            $submit  = '<input class="btn" type="submit" value="'.$special['__form']['submit_value'].'" name="'.(isset($special['__form']['submit_name']) ?  $special['__form']['submit_name'] : 'Save') .'"/>';
            $submit .= '<input class="btn" type="submit" value="'.( $object->id ? 'Close' : 'Cancel').'" name="btn_cancel"/>';
            if( isset($special['__form']['additional_buttons'])){
                foreach( $special['__form']['additional_buttons'] as $button_name=>$definition ){
                        switch( $definition['type'] ){
                            case self::ELEMENT_SUBMIT:
                                $submit .= '<input class="btn '.$definition['class'].'" type="submit" value="'.$button_name.'" name="'.$definition['name'].'" name="'.$definition['id'] .'"/>';
                            break;
                        }
                        
                }
            }
        }
        /**
        * Form class
        */
        switch( $type ){
            case self::FORM_INLINE:
                $form_class = 'form-inline';  
            break;
            case self::FORM_SEARCH:
                $form_class = 'form-inline'; 
            break;
            default:
                $form_class = 'form-horizontal';              
            break;                
        }         
        /**
        * Return form div
        */
        $return = 
        '<'.$element.' '.$attributes.' class="'.$form_class.'">            
            
             '.$legend.$string.'';
        
        if($type == 'emailcss')
		{
			$return .= '<div class="'.$type.'">
			    <input type="button"  value="Send" name="email" />
			</div>';
		}     
             
             
        $return = $return.'
            '.$submit.'
        </'.$element.'>';
              
        return $return;    
    }
	
    private function get_array_field( $name, $objects, $record, $special, $id_prefixes, $type,  $override = array(), $default_class ){
          
        $string = '';
        foreach( $objects as $index=>$object ){
            //Some fiels are hidden (ids, etc )
            $input_type = 'text';
            if( in_array( preg_replace('/.+::/','',$name), $this->hidden )){
                $input_type = 'hidden';
            }            
            //prefix id with param prefix or class name
            
            if( $id_prefixes ){
                $object_name = $this->get_object_name( $object, $name );                
                if( is_array( $id_prefixes) && isset( $id_prefixes[$object_name])){
                   $id_prefix = $id_prefixes[$object_name];  
                } else if( is_string( $id_prefixes )){
                   $id_prefix = $id_prefixes; 
                }
            }            
            if( isset( $special[$name]['id'])){ 
                $id = $this->get_id_value( $object, $special[$name]['id']);                                     
            }  else {
                $id_name = preg_replace('/.+::/','',$name);
                if( isset($id_prefix) ){
                    $id = $id_prefix . '_' .$id_name;    
                } else{
                    if( get_class( $object ) == 'Item'){
                        $id = $id_name.'_'.$counter++;    
                    } else {
                        $id = $id_name;
                    }
                }                
            }
            /**
            * If this field is wrong
            */
            $error = '';
            if( in_array( $name, array_keys($record->errors) )){
                $error = ' error';
            }
                        
            switch($special[$name]['element']){
                case self::ELEMENT_CHECKBOX:
                    $value = in_array($object->id, $special[$name]['selected']) ? true : false;
                break;
                case self::ELEMENT_DROPDOWN:
                    $control = form_dropdown( $id, 
                                              $special[ $name ][ 'options' ],
                                              $special[ $name ][ 'selected' ], 
                                              ' id="'.$id.'" class="'.$class.'"' );
                break;
                case self::ELEMENT_TEXTAREA:  
                    $control = '<textarea class="'.$class.'" id="'.$id.'" name="'.$id.'" rows="'.(isset( $special[ $name ][ 'rows' ]) ? $special[ $name ][ 'rows' ] : 3) .'">'.$value.'</textarea>';
                break;
                default:
                    $control = '<input '.$date.' type="text" class="'.$class.'" id="'.$id.'" name="'.$id.'" value="'.$value.'"/>';
                break;    
            }
            
            $control = $this->get_control( $id, $name, $value, $object, $special, $default_class );
            /**
            <span class="add-on">$</span>
            <input id="appendedPrependedInput" class="span2" type="text" size="16">
            <span class="add-on">.00</span>             
            
            */
            if( $special && isset( $special[ $name ]['prepend']) ){
                $control = '<div class="input-prepend"><span class="add-on">'.$special[ $name ]['prepend'].'</span>' . $control .'</div> ';             
            }
            /**
            * Is this element displayed inline with some other element?
            */
            if( $special && isset( $special[ $name ]['inline'])){
                $inline = $special[ $name ]['inline'];
            }
            /**
            * Second inline element found, will be merged with first
            */
            if( isset($inline) && $name == $inline ){
                $inline = self::INLINE_END;
            }
            
            
            if( isset( $this->name_override[ $name ] )){
                $name = $this->name_override[ $name ];
            } else if( isset( $overrides[ $name ]) ){
                $name = $overrides[ $name ];
            }
            //Control label
            $label = '';
            if( (isset( $special[$name]['group_label']) && $index == 0) || !isset( $special[$name]['label'])){
                //Hidden elements dont have label 
                $label = '';
                if( $input_type != 'hidden'){
                    if( isset($special[$name]['group_label'])){
                        $label = $special[$name]['group_label'];
                    } else {
                        if( isset( $this->label_override[$name]) ){
                            $label = $this->label_override[$name]; 
                        } else {
                            $label = ucfirst(preg_replace('/.+::/','',  str_replace('_',' ', $name)));
                        }
                    }
                }
            }
            /**
            * If this field is wrong
            */
            $error = '';
            if( in_array( $name, array_keys($record->errors) )){
                $error = ' error';
            }             
            /**
            * Is this inline form or horizontal ( classic ) form ?
            */
            switch( $type ){
                case self::FORM_INLINE:
                    $form_class = 'form-inline';
                    $string .= '<label class="control-label '.$error.'" for="'.$id.'">'.$label.'</label>
                                '.$control.'';    
                break;
                case self::FORM_SEARCH:
                break;
                default:
                    if( isset($inline) && $inline ){
                        if( $inline == self::INLINE_END) {
                            
                            $id = $inline_data['id'];
                            $label = $inline_data['label'];
                            $error = $inline_data['error'];
                            $control = $inline_data['control'] . $control;                                
                            $inline_data = array();
                            $inline = false;                                                         
                        } else {
                            $inline_data = array();
                            $inline_data['id'] = $id;
                            $inline_data['label'] = $label;
                            $inline_data['error'] = $error;
                            $inline_data['control'] = $control;                            
                        }                            
                    }

                    if( !isset($inline) || !$inline ){
                        $form_class = 'form-horizontal';
                        $string .= '<div class="control-group'.$error.'">
                                        <label class="control-label" for="'.$id.'">'.$label.'</label>                            
                                        <div class="controls">                           
                                            '.$control.'                                         
                                        </div>
                                   </div>';                            
                    }                            
                    break;                
            }            
        }
        return $string;
    }
    private function get_control( $id, $name, $value, $object, $special, $default_class ){
        $date = ''; 
        if( isset( $special[ $name ]['date'] ) && $special[ $name ]['date'] === true){
            $date = ' data-datepicker="datepicker" ';    
        }                    
        /**
        * Use Default class or one supplied in $special array
        */
        $class = $default_class;
        if( isset( $special[ $name ]['class'] ) && $special[ $name ]['class']){
            $class = $special[ $name ]['class'];     
        }                         
        $control = null;
        
        if( $special && isset( $special[ $name ]['element'] ) &&   $special[ $name ]['element'] ){
                
                switch( $special[ $name ][ 'element' ]  ){
                    case self::ELEMENT_CHECKBOX:
                        if( preg_match('!'.self::ADD_NEW_CHECKBOX.'!', $id)){ 
                            $control = '<div class="stop new_type_div form-horizontal" style="display:none;">
                                                <div class="control-group">                                                                                    
                                                     <label>Name:</label><input type="text" class="new_name" id="'.$object->type.'" name="'.$object->name.'">
                                                </div>
                                                <div class="control-group">
                                                       <label>Description:</label> <textarea class="new_description" name="description"></textarea>
                                                </div>
                                        </div>
                                        <a class="add_new_type btn">Add</a>
                                        <a class="hide_all btn" id="hide_'.$object->name.'">Hide</a>
                                        <a class="delete_all btn" id="delete_'.$object->name.'">Delete</a>
                                        ';
                        } else {
                            if( $object->id == null ){ //Place holder in group
                                $control  = '<label></label>';
                            } else {
                                $control  = '<input id="'.$id.'" name="'.$id.'" value="'.$object->id.'" type="checkbox" '.($value ? 'checked="yes"' : '').'>'; 
                            }
                                                                                                       
                            $description = '';
                            if( isset($special[$name]['description'])){
                                $description = '<span class="help-block">'.$this->get_id_value($object, $special[$name]['description']).'</span>';
                            }
                            if( isset($special[$name]['label'])){
                                $control = '<label class="checkbox">'.$control.$this->get_id_value($object, $special[$name]['label']).$description.'</label>';
                            }                            
                        }

                    break;
                    case self::ELEMENT_DROPDOWN:
                        $control = form_dropdown( $id, 
                                                  $special[ $name ][ 'options' ],
                                                  $special[ $name ][ 'selected' ], 
                                                  ' id="'.$id.'" class="'.$class.'"' );
                    break;
                    case self::ELEMENT_TEXTAREA:  
                        $control = '<textarea class="'.$class.'" id="'.$id.'" name="'.$id.'" rows="'.(isset( $special[ $name ][ 'rows' ]) ? $special[ $name ][ 'rows' ] : 3) .'">'.$value.'</textarea>';
                    break;
                    default:
                        $control = '<input '.$date.' type="text" class="'.$class.'" id="'.$id.'" name="'.$id.'" value="'.$value.'"/>';
                    break;    
                }

        }  
       
        if( !$control ){
            //Some fiels are hidden (ids, etc )
            $input_type = 'text';
            if( in_array( preg_replace('/.+::/','',$name), $this->hidden )){
                $input_type = 'hidden';
            }             
            $control = '<input '.$date.' type="'.$input_type.'" class="form-control '.$class.'" id="'.$id.'" name="'.$id.'" value="'.$value.'"/>';    
        }
        return $control;       
    }    
    public function get_text_area( $id, $name, $value, $class = 'default' ){
        $default_class = $class === 'default' ? 'input-xlarge': $class;
        return  '<textarea class="'.$default_class.'" id="'.$id.'" rows="1" name="'.$id.'" value="'.$value.'"></textarea>';    
        
    }
    public function get_text_field( $id, $name, $value, $class = 'default' ){
        $default_class = $class === 'default' ? 'input-xlarge': $class;
        return  '<input type="text" class="form-control '.$default_class.'" id="'.$id.'" name="'.$id.'" value="'.$value.'"/>';         
    }
	
	public function get_order_tables( array $orders, array $statuslist, array $operatorlist, array $fields = array() , $table, $overrides = array() , $row_href = true ){
        
        $this->table_name_override = $overrides;
        $table->clear();
        
        if( !$fields ){
            $fields = array_diff( get_class_vars( Order ), $this->ignore )  ;
        }
        
        foreach( $fields as $field ){
        	
            $headings[] = isset($this->table_name_override[$field]) ? $this->table_name_override[$field] :ucfirst( str_replace('_',' ', $field ));
        }
        $table->set_heading( $headings );

        foreach( $orders as $order ){
            $row = array();
            foreach( $fields as $field ){

                if( preg_match('/(?P<object>.+)::(?P<field>.+)/', $field, $matches )){
                    $object = $matches['object'];
                    $field  = $matches['field'];
                    $value = $order->$object->$field;
                } else {
                    $value = $order->$field;    
                } 
				
				if($field=='edit') {					
					$value = '<a href="'.site_url('orders/show_order/' . $order->id).'"><img id="edit" name="edit" src="'. site_url('application/views/assets/img/edit.png') .'" title="edit" /></a>';				
				}
				
				if ($field=='status') {
						
					$fieldcontrol='<select onchange="javascript:ChangeJobStatus('.$order->id .')" id="status_'. $order->id .'">';
			        foreach( $statuslist as $rows){
			        	if($rows->name == $order->status) {
			        		$fieldcontrol.='<option value="'. $rows->id.'" selected="selected">'. $rows->name .'</option>';
			        	} else {
			        		$fieldcontrol.='<option value="'. $rows->id.'">'. $rows->name .'</option>';
			        	}			        	
			        }					   
					$fieldcontrol.='</select>';
					$value = $fieldcontrol;
				}
				
				if ($field=='Operator') {
					
					$fieldcontrol='<select onchange="javascript:ChangeOperator('.$order->id .')" id="operator_'. $order->id .'">';
					$fieldcontrol.='<option value="">-Select-</option>';
					$fieldcontrol.='<option value="_8">-Add/Edit-</option>';		
					
							
			        foreach( $operatorlist as $rows){
			        	if($rows->id == $order->operator_id) {
			        		$fieldcontrol.='<option value="'. $rows->id.'" selected="selected">'. $rows->name .'</option>';
			        	} else {
			        		$fieldcontrol.='<option value="'. $rows->id.'">'. $rows->name .'</option>';
			        	}			        	
			        }					   
					$fieldcontrol.='</select>';
					$value = $fieldcontrol;
				}
                                
                                if ($field=='company_id') {
                                    $value ='<a target="_blank" href="'.  base_url().'clients/index/id/'.$order->agent_id.'">'. $order->company_id .'</a>';
                                }
				
                if( in_array( $field, array('report_due','order_date'))){
                    $value = $this->get_us_date_format( $value );
                }
                if( $row_href ) {
                    //$row[] = '<a href="'.site_url('orders/show_order/' . $order->id).'">'.$value.'</a>';    
                    $row[] = $value;					
                } else {
                    $row[] = $value;
                }
                  
            }
            
            $attr = array();
            switch( $order->status ){
                case Order::ORDER_STATUS_REPORT_STARTED:
                    $attr = array('class'=>"order_report_started");
                break;
                case Order::ORDER_STATUS_REPORT_COMPLETED:
                    $attr = array('class'=>"order_report_completed");
                break;
            }
			
            $table->add_order_row( $row, $attr );    
            
        }
        
        return $table->generate_order_table();             
    }
	
	
    public function get_order_table( array $orders, array $fields = array() , $table, $overrides = array() , $row_href = true ){
        
        $this->table_name_override = $overrides;
        $table->clear();
        
        if( !$fields ){
            $fields = array_diff( get_class_vars( Order ), $this->ignore )  ;
        }
        
        foreach( $fields as $field ){
        	
            $headings[] = isset($this->table_name_override[$field]) ? $this->table_name_override[$field] :ucfirst( str_replace('_',' ', $field ));
        }
        $table->set_heading( $headings );

        foreach( $orders as $order ){
            $row = array();
            foreach( $fields as $field ){

                if( preg_match('/(?P<object>.+)::(?P<field>.+)/', $field, $matches )){
                    $object = $matches['object'];
                    $field  = $matches['field'];
                    $value = $order->$object->$field;
                } else {
                    $value = $order->$field;    
                } 
				
				
                if( in_array( $field, array('report_due','order_date'))){
                    $value = $this->get_us_date_format( $value );
                }
                if( $row_href ) {
                    $row[] = '<a href="'.site_url('orders/show_order/' . $order->id).'">'.$value.'</a>';    
                    //$row[] = $value;					
                } else {
                    $row[] = $value;
                }
                  
            }
            
            $attr = array();
            switch( $order->status ){
                case Order::ORDER_STATUS_REPORT_STARTED:
                    $attr = array('class'=>"order_report_started");
                break;
                case Order::ORDER_STATUS_REPORT_COMPLETED:
                    $attr = array('class'=>"order_report_completed");
                break;
            }
			
            $table->add_order_row( $row, $attr );    
            
        }
        
        return $table->generate_order_table();             
    }
    public function get_clients_table( array $orders, array $fields = array() , $table, $url = '' ){
        
        $table->clear();
        
        if( !$fields ){
            $fields = array_diff( get_class_vars( Clients ), $this->ignore )  ;
        }
        
        foreach( $fields as $field ){
            if( $field != 'sid' ){
                $headings[] = isset($this->table_name_override[$field]) ? $this->table_name_override[$field] :ucfirst( str_replace('_',' ', $field ));
            }
        }
        $table->set_heading( $headings );

        foreach( $orders as $order ){
            $row = array();
            foreach( $fields as $field ){
                if( preg_match('/(?P<object>.+)::(?P<field>.+)/', $field, $matches )){
                    $object = $matches['object'];
                    $field  = $matches['field'];
                    $row[] = '<a href="'.site_url('clients/show_client/' . $order->id).'">'.$order->$object->$field.'</a>';
                } else {
                    if( $field == 'sid'){
                        $row[] = '<input type="hidden" name="client_id" value="'.$order->id.'">';     
                    } else {
                        //$row[] = '<a href="'.site_url('clients/show_client/' . $order->id).'">'.$order->$field.'</a>';     
                        $row[] =  '<span value="'.$order->$field.'">'.$order->$field.'</span>';     
                    }
                       
                }   
            }
            $table->add_row( $row );    
        }
        
        return $table->generate();             
    }
     public function generate_table( array $records, array $fields = array(), $table, array $special = array()){ 
        $table->clear();
        if( !$fields ){
             $fields = array_diff( array_keys(get_class_vars( get_class( $records['0']) )), $this->ignore )  ;
        }
        
        foreach( $fields as $field ){
        
        			$headings[] = !empty( $special[ $field ]['name']) ? $special[ $field ]['name'] :ucfirst( str_replace('_',' ', $field ));	
        
                
        }  
        $table->set_heading( $headings );        
        foreach( $records as $record ){
            $row = array();
            foreach( $fields as $field ){
                //Get value of this object field
                $value = '<span>'.$this->get_object_value( $record, $field ).'</span>';
                //Value override?
                if( isset($special[$field]['values'][$value])){
                    $value = $special[$field]['values'][$value];
                }
                /**
                * HREF is on the row level
                */
                if( isset( $special['__href']) && !empty($special['__href']) ){
                    $row[] = $this->get_href(  $record, $field, $value, $special['__href'] );
                } else if ( isset($special[ $field ]['href']) && !empty($special[ $field ]['href']) ){
                    $row[] = $this->get_href(  $record, $field, $value, $special[ $field ] );                     
                } else {
                    $row[] = $value;    
                }                                
            }
            $table->add_row( $row );
        }
        $this->cached = array();
        return $table->generate();
    }
    private function get_object_value( $record, $field ){
         
        if( preg_match('/(?P<object>.+)::(?P<field>.+)/', $field, $matches )){
            $object   = $matches['object'];
            $o_field  = $matches['field'];
            $value = $record->$object->$o_field;  
        } else {
            $value = $record->$field;  
        }
        
        if( in_array( $field, array('inspection_date','order_date','timeoff_date_start','timeoff_date_end'))){
            $value = $this->get_us_date_format( $value );
        }
        
        return $value;        
    }
    private function get_object_name( $record, $field ){
        
        $object = null;
        if( preg_match('/(?P<object>.+)::(?P<field>.+)/', $field, $matches )){
            $object   = $matches['object'];  
        }          
        return $object;         
    }
    public function get_us_date_format( $date ){
        if( preg_match('!(?P<year>\d\d\d\d)-(?P<month>\d\d)-(?P<day>\d\d)\s*(?P<rest>.*)!', $date, $matches ) ){
            $date = $matches['month'].'-'.$matches['day'].'-'.$matches['year'].' '.( isset($matches['rest']) ?' '.$matches['rest']: '' );
        }
        return $date;
    }
    protected function get_href( $record, $field, $value,  $special ){
        $href = $special['href'];
           
        if( preg_match_all('!<(?P<fields>.+?)>!', $href, $matches )){
            foreach( $matches['fields'] as $replace_field ){
                $href = preg_replace( "!<$replace_field>!", $this->get_object_value( $record, $replace_field ), $href);
            }
        }
        $id = null;
        if( isset($special['id']) ){
            if( preg_match_all('!<(?P<fields>.+?)>!', $special['id'], $matches )){
                foreach( $matches['fields'] as $replace_field ){
                    $id = preg_replace( "!<$replace_field>!", $this->get_object_value( $record, $replace_field ), $special['id']);
                    break;
                }
            }
            if( $id ){
                $id = ' id="'.$id.'" ';
            }
        }
        return '<a '.$id.' href="'.$href.'" target="_blank">'.$value.'</a>';
        return $value;        
    }
    private function get_id_value( $record, $id_pattern ){
        $id = null;
        if( preg_match_all('!<(?P<fields>.+?)>!', $id_pattern , $matches )){
            foreach( $matches['fields'] as $replace_field ){
                $id = preg_replace( "!<$replace_field>!", $this->get_object_value( $record, $replace_field ), $id_pattern );
                break;
            }
        }
        return $id;        
    }
}
?>
