<?php
class Waybill_pdf{
    
    
    
    
   public function __construct(){ 
       require_once('application/libraries/MPDF54/mpdf.php');
       
   }
   public function create_pdf( Order $order, Order_helper $order_helper, $file = false, $invoice_number ){
        
        $this->order_helper = $order_helper;
         
        $mpdf=new mPDF('win-1252','A4','','arial',20,15,48,25,10,10);
        $mpdf->useOnlyCoreFonts = true;    // false is default
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle( $order->company->name . '-Order:' . $order->company_order_id );
        $mpdf->SetAuthor($order->company->name);
        $mpdf->SetWatermarkText("Paid");
        $mpdf->showWatermarkText = false;
        $mpdf->watermark_font = 'DejaVuSansCondensed';
        $mpdf->watermarkTextAlpha = 0.1;
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->keep_table_proportions = true;
        $mpdf->showImageErrors = true;
        $path = parse_url( site_url());
        $path = str_replace('index.php','',$path['path']); 
        $path = $_SERVER['DOCUMENT_ROOT'] .   $path  . 'application/views/assets/img/';
        
        $filename = $order->company_id.'_logo.jpg'; 
         
        $img = '';
        if( file_exists( $path.$filename)){
            $img =  str_replace('index.php','',site_url() .'application/views/assets/img/' . $filename);
             
        }
        
       
        setlocale(LC_MONETARY, 'en_US');
        
        $ci =& get_instance();
        $ci->load->model('order_model');
        $getJobDetail = $ci->order_model->orderJobDetails($order->id);
        $getShirtDetail = $ci->order_model->orderShirtJobDetails($order->id);
        $jobDetail = '';
        if($getJobDetail) {
            foreach($getJobDetail as $detail) {
                $jobDetail .= $this->displayJobDetail('Job Name',$detail->job_name);
                $jobDetail .= $this->displayJobDetail('Stock',$detail->stock_name);
                $jobDetail .= $this->displayJobDetail('Colors',$detail->color_name);
                $jobDetail .= $this->displayJobDetail('Size',$detail->size_name);
                $jobDetail .= $this->displayJobDetail('Quantity',$detail->quantity);
                $jobDetail .= $this->displayJobDetail('Finishing',$detail->finishing_name);
                $jobDetail .= $this->displayJobDetail('Coating',$detail->coat_name);
                $jobDetail .= $this->displayJobDetail('Others',$detail->others);
                $jobDetail .= $this->displayJobDetail(' Additional notes',$detail->additional_notes);
            }
        }
        
        if($getShirtDetail) {
            foreach($getShirtDetail as $detail) {
                $jobDetail .= $this->displayJobDetail('Job Name',$detail->job_name);
                $jobDetail .= $this->displayJobDetail('Type',$detail->type_name);
                $jobDetail .= $this->displayJobDetail('Front Color',$detail->fcolor_name);
                $jobDetail .= $this->displayJobDetail('Back Color',$detail->bcolor_name);
                
                $sizeQTy = unserialize($detail->quantity_size);
                $sizeQuantity = array();
                if(is_array($sizeQTy)) {
                    $i = 1;                    
                    foreach ($sizeQTy as $size) {
                        $sizeObject = new ShirtSize($size['size']);
                        $jobsResult = $ci->order_model->get_object( $sizeObject,true );
                        $jobDetail .= $this->displayJobDetail('Size #'.$i,@$jobsResult->name);
                        $jobDetail .= $this->displayJobDetail('Quantity #'.$i,@$size['quantity']);
                        $i++;
                    }
                }
                
                $jobDetail .= $this->displayJobDetail('Others',$detail->others);
                $jobDetail .= $this->displayJobDetail(' Additional notes',$detail->additional_notes);
            }
        }
 
         
        //$invoice_number = '1'.str_pad($invoice_number, 4, "0", STR_PAD_LEFT);  
        $order_date = $this->get_us_date( $order->order_date);
        $inspection_date = $this->get_us_date( $order->inspection_date);
        $inspection_time = $this->get_times( $order->inspection_time);
        
        $shippingInfo = '';
        $address = $order->shipping_address . ' ' . $order->shipping_address_2;
        $city_state_zip = $order->shipping_city . ', ' . $order->shipping_state . ', ' .$order->shipping_zip;
        $shippingInfo .= $this->displayShippingDetail('Name',$order->shipping_name);
        $shippingInfo .= $this->displayShippingDetail('Contact',$order->shipping_contact);
        $shippingInfo .= $this->displayShippingDetail('Address',$address);
        $shippingInfo .= $this->displayShippingDetail('City,State,Zip',$city_state_zip);
        $shippingInfo .= $this->displayShippingDetail('Phone',$order->shipping_phone_1);
        
        $b__oxes = explode(' / ', $order->boxes);
        $d__eliver = explode(' / ', $order->deliver);
        $b_d = '';
        $i = 0;
        if( count($b__oxes) >= count($d__eliver)) {   
            foreach ($b__oxes as $keys => $values) {                                    
                    
             $b_d .= $this->displayShippingDetail( 'BOXES',$values );    

             $b_d .= $this->displayShippingDetail('DELIVER/WILL CALL',$d__eliver[$i]);

              $i++; 
            }
        }
        elseif( count($b__oxes) < count($d__eliver)) {   
            foreach ($d__eliver as $keys => $values) {                                    
                    
             $b_d .= $this->displayShippingDetail( 'BOXES',$values );    

             $b_d .= $this->displayShippingDetail('DELIVER/WILL CALL',$b__oxes[$i]);

              $i++; 
            }
        }        

$html = '<html>
    <head>
        <style>
            body {font-family: sans-serif;
                font-size: 10pt;
            }
            p {    margin: 0pt;
            }
            td { vertical-align: top; }  
            .items td {
                border-left: 0.1mm solid #000000;
                border-right: 0.1mm solid #000000;
                color: #000000
            }
            table thead td {  
                text-align: center;
                border: 0.1mm solid #000000;
                background-gradient: linear #FFFFFF #EEEEEE 0.5 0.5 1 1 1.2;
                font-size: 11pt;
            }
            .items td.blanktotal {
                background-color: #FFFFFF;
                border: 0mm none #000000;
                border-top: 0.1mm solid #000000;
                border-right: 0.1mm solid #000000;
            }
            .items td.totals {
                text-align: right;
                border: 0.1mm solid #000000;
            }
            .items{
                border-bottom: 0.1mm solid #000000;
            }
             
            .gradient {  }
        </style>
    </head>
    <body>
        

        <!--mpdf
        <htmlpageheader name="myheader">
        <table width="100%"><tr>
        '. ($img ?'<td><img src="'.$img.'"/></td>' : '') .'
        <td width="50%" style="color:#000000;"><span style="font-weight: bold; font-size: 14pt;">'.$order->company->name.'</span><br />'.$order->company->address.'<br />'.$order->company->city.','.$order->company->state.','. $order->company->zip. '<br />'.$order->company->phone_1.'<br />'. $order->company->email .'<br /></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td style="font-weight: bold; font-size: 12pt;">Invoice# '.$invoice_number.'</td>
        </tr></table>

        </htmlpageheader>

        <htmlpagefooter name="myfooter">
        <div style="border-top: 1px solid #000000; font-size: 9pt; text-align: center; padding-top: 3mm; ">
        Page {PAGENO} of {nb}
        </div>
        </htmlpagefooter>

        <sethtmlpageheader name="myheader" value="on" show-this-page="1" />
        <sethtmlpagefooter name="myfooter" value="on" />
        mpdf-->

         
        <!-- ORDER HEADER -->
        <table width="100%" style="font-family: sans; border-collapse: collapse;" cellpadding="10">
            <thead>
                <tr>
                    <td width="100">ORDER DATE</td>
                    <td width="100">SALES REP</td>
                    <td width="100">PO</td>
                </tr>
            </thead>        
            <tr>
                <td align="center" style="border: 0.1mm solid #888888;"><span style="font-weight: bold;font-size: 8pt; color: #555555; font-family: sans;">'.$order_date.'</span></td>
                <td align="center" style="border: 0.1mm solid #888888;"><span style="font-weight: bold;font-size: 8pt; color: #555555; font-family: sans;">'.$order->inspector->name.'</span></td>
                <td align="center" style="border: 0.1mm solid #888888;"><span style="font-weight: bold;font-size: 8pt; color: #555555; font-family: sans;">'.$order->po_text.'</span></td>
            </tr>
        </table>
                
        <!-- CLIENT DETAILS, SHIPPING DETAILS  -->
        
  
        <div width="48%" style="float:left; margin-right: 2;"> 
            <table class="items" width="100%" style="font-family: sans; margin-top:10; border-collapse: collapse;" cellpadding="6">
                <thead>
                    <tr>
                        <td colspan="2">CLIENT DETAILS</td>
                    </tr>
                </thead>                           
                '.$this->get_fields( $order->client, array('name','address','city','phone_1','phone_2','email','fax')).'                                                                                                                                                                      
            </table>
                                              
       </div>
       <div width="48%" style="float:right; margin-left: 2;">
       <table class="items" width="100%" style="font-family: sans; margin-top:10; border-collapse: collapse;" cellpadding="6">
            <thead>
                <tr>
                    <td colspan="2">SHIPPING DETAILS</td>
                </tr>
            </thead>  
            
              '.$shippingInfo.'
        </table>                
       </div>

       <!-- ORDER DETAILS, SHIPPING DETAILS  -->
       

       <div width="48%" style="float:left; margin-right: 2;margin-left:52%"> 
            <table class="items" width="100%" style="font-family: sans; margin-top:10; border-collapse: collapse;" cellpadding="6">
                <thead>
                    <tr>
                        <td colspan="2">SHIPPING</td>
                    </tr>
                </thead>                           
                '.$b_d .'                                                                                                                                                                      
            </table>
                                              
       </div>
       <div width="48%" style="float:right; margin-left: 2;">
       <table class="items" width="100%" style="font-family: sans; margin-top:10; border-collapse: collapse;" cellpadding="6">
            <thead>
                <tr>
                    <td colspan="2">ORDER DETAILS</td>
                </tr>
            </thead>  
            
              '.$jobDetail.'
        </table>                
       </div>



    </body>
</html>';        
         
        $mpdf->WriteHTML( $html );
        if( $file ){
            $mpdf->Output( $file, 'F');    
        } else {
            $mpdf->Output();    
        }
                    
   }
   
   private function get_fields( Record $order, array $fields ){
        
        foreach( $fields as $field ) {
            
            if( $field == 'buyer' && $order->$field == null ){
                $order->set_buyer( $order->agent->name );
            }
            if( isset( $order->$field ) && ($value = $order->$field )!= null ){
                if( $field == 'city'){
                    
                    $title = 'City';
                    if( $order->state){
                        $title .= ',State';
                        $value .= ','.$order->state;
                    }
                    if( $order->zip){
                        $title .= ',Zip';
                        $value .= ','.$order->zip;
                    }
                    $rows   .= '<tr>
                                 <td class="td_details" align="right" width="30%"><span style="font-size: 8pt; color: #555555; font-family: helvetica;">'.$title.'</td>
                                 <td class="td_details" align="left"  width="70%"><span style="font-weight: bold; font-size: 8pt; color: #555555; font-family: sans;">'.$value.'</td>
                                </tr>';
                } else {
                    
                    $CI =& get_instance();
                                        
                    if( $field == 'spa'){
                        $value = $order->spa ? 'Yes' : 'No';
                    }elseif( $field == 'pool'){
                        $value = $order->pool ? 'Yes' : 'No';
                    }elseif( $field == 'estimated_inspection_time'){
                        $value = $order->$field . ' hrs';
                    }elseif( $field == 'notes_order'){
                        $title = 'Order notes';
                    }
                    
                    $title = ucfirst( str_replace('_',' ', $field ));
                    $rows .= '<tr>
                                <td class="td_details" align="right" width="30%" ><span style="font-size: 8pt; color: #555555; font-family: arial;">'.$title.'</td>
                                <td class="td_details" align="left"  width="70%" ><span style="font-weight: bold; font-size: 8pt; color: #555555; font-family: sans;">'.$value.'</td>
                             </tr>';                    
                }

            }
        }
        return $rows;
   }
   private function get_items( Order $order ){
       $rows = '';    
       if( $order->items ){
           foreach( $order->items as $item ){ 
               $rows .= '<tr>
                            <td style="border: 0.1mm solid #000000;" align="center">'.$item->name.'</td>
                            <td style="border: 0.1mm solid #000000;" align="center">'.$item->description.'</td>
                            <td style="border: 0.1mm solid #000000;" align="right">'. $this->money_format('%i',$item->price).'</td>
                        </tr>';
           }           
       }
       return $rows;
   }
   private function get_us_date( $date ){
        if( preg_match('/(\d\d\d\d)-(\d\d)-(\d\d)/', $date, $match )){
            $date = $match['2'].'/'.$match['3'].'/'.$match['1'];
        }    
        return $date;    
   }
   public function get_times( $time ){
        $array = array( 
          '07:00' => '7:00 AM',
          '07:30' => '7:30 AM',
          '08:00' => '8:00 AM',
          '08:30' => '8:30 AM',
          '09:00' => '9:00 AM',
          '09:30' => '9:30 AM',
          '10:00' => '10:00 AM',
          '10:30' => '10:30 AM',
          '11:00' => '11:00 AM',
          '11:30' => '11:30 AM',
          '12:00' => '12:00 PM',
          '12:30' => '12:30 PM',
          '13:00' => '1:00 PM',
          '13:30' => '1:30 PM',
          '14:00' => '2:00 PM',
          '14:30' => '2:30 PM',
          '15:00' => '3:00 PM',
          '15:30' => '3:30 PM',
          '16:00' => '4:00 PM',
          '16:30' => '4:30 PM',
          '17:00' => '5:00 PM',
          '17:30' => '5:30 PM',
          '18:00' => '6:00 PM',
          '18:30' => '6:30 PM',
          '19:00' => '7:00 PM',
          '19:30' => '7:30 PM',
          '20:00' => '8:00 PM'
        );
        
        return $array[$time];
    }
    function displayJobDetail($title,$value) {
        return '<tr>
                                <td class="td_details" align="right" width="30%" ><span style="font-size: 8pt; color: #555555; font-family: arial;">'.$title.'</td>
                                <td class="td_details" align="left"  width="70%" ><span style="font-weight: bold; font-size: 8pt; color: #555555; font-family: sans;">'.$value.'</td>
                             </tr>';
    }
    function displayShippingDetail($title,$value) {
        return '<tr>
                                <td class="td_details" align="right" width="30%" ><span style="font-size: 8pt; color: #555555; font-family: arial;">'.$title.'</td>
                                <td class="td_details" align="left"  width="70%" ><span style="font-weight: bold; font-size: 8pt; color: #555555; font-family: sans;">'.$value.'</td>
                             </tr>';
    }
function money_format($format, $number)
{
    $regex  = '/%((?:[\^!\-]|\+|\(|\=.)*)([0-9]+)?'.
              '(?:#([0-9]+))?(?:\.([0-9]+))?([in%])/';
    if (setlocale(LC_MONETARY, 0) == 'C') {
        setlocale(LC_MONETARY, '');
    }
    $locale = localeconv();
    preg_match_all($regex, $format, $matches, PREG_SET_ORDER);
    foreach ($matches as $fmatch) {
        $value = floatval($number);
        $flags = array(
            'fillchar'  => preg_match('/\=(.)/', $fmatch[1], $match) ?
                           $match[1] : ' ',
            'nogroup'   => preg_match('/\^/', $fmatch[1]) > 0,
            'usesignal' => preg_match('/\+|\(/', $fmatch[1], $match) ?
                           $match[0] : '+',
            'nosimbol'  => preg_match('/\!/', $fmatch[1]) > 0,
            'isleft'    => preg_match('/\-/', $fmatch[1]) > 0
        );
        $width      = trim($fmatch[2]) ? (int)$fmatch[2] : 0;
        $left       = trim($fmatch[3]) ? (int)$fmatch[3] : 0;
        $right      = trim($fmatch[4]) ? (int)$fmatch[4] : $locale['int_frac_digits'];
        $conversion = $fmatch[5];

        $positive = true;
        if ($value < 0) {
            $positive = false;
            $value  *= -1;
        }
        $letter = $positive ? 'p' : 'n';

        $prefix = $suffix = $cprefix = $csuffix = $signal = '';

        $signal = $positive ? $locale['positive_sign'] : $locale['negative_sign'];
        switch (true) {
            case $locale["{$letter}_sign_posn"] == 1 && $flags['usesignal'] == '+':
                $prefix = $signal;
                break;
            case $locale["{$letter}_sign_posn"] == 2 && $flags['usesignal'] == '+':
                $suffix = $signal;
                break;
            case $locale["{$letter}_sign_posn"] == 3 && $flags['usesignal'] == '+':
                $cprefix = $signal;
                break;
            case $locale["{$letter}_sign_posn"] == 4 && $flags['usesignal'] == '+':
                $csuffix = $signal;
                break;
            case $flags['usesignal'] == '(':
            case $locale["{$letter}_sign_posn"] == 0:
                $prefix = '(';
                $suffix = ')';
                break;
        }
        if (!$flags['nosimbol']) {
            $currency = $cprefix .
                        ($locale['currency_symbol']) .
                        $csuffix;
        } else {
            $currency = '';
        }
        $space  = $locale["{$letter}_sep_by_space"] ? ' ' : '';
          
        $value = number_format($value, $right, $locale['mon_decimal_point'],
                 $flags['nogroup'] ? '' : $locale['mon_thousands_sep']);
        $value = @explode($locale['mon_decimal_point'], $value);

        $n = strlen($prefix) + strlen($currency) + strlen($value[0]);
        if ($left > 0 && $left > $n) {
            $value[0] = str_repeat($flags['fillchar'], $left - $n) . $value[0];
        }
        $value = implode($locale['mon_decimal_point'], $value);
        if ($locale["{$letter}_cs_precedes"]) {
            $value = $prefix . $currency . $space . $value . $suffix;
        } else {
            $value = $prefix . $value . $space . $currency . $suffix;
        }
        if ($width > 0) {
            $value = str_pad($value, $width, $flags['fillchar'], $flags['isleft'] ?
                     STR_PAD_RIGHT : STR_PAD_LEFT);
        }

        $format = str_replace($fmatch[0], $value, $format);
    }
    return $format;
}        
}  
?>
