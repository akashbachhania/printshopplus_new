<?php
class Run_pdf{
   public function __construct(){ 
       require_once('application/libraries/MPDF54/mpdf.php');
       
   }
   public function create_pdf( Run $run){
         
        $this->order_helper = $order_helper;
         
        $mpdf=new mPDF('win-1252','A4','','verdana',20,15,10,25,10,10);
        $mpdf->useOnlyCoreFonts = true;    // false is default
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle( $run["run"]["id"] . '-run:' );
        //$mpdf->SetAuthor($order->company->name);
        //$mpdf->SetWatermarkText("Paid");
        $mpdf->showWatermarkText = false;
        $mpdf->watermark_font = 'DejaVuSansCondensed';
        $mpdf->watermarkTextAlpha = 0.1;
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->keep_table_proportions = true;
        $mpdf->showImageErrors = true;
        $path = parse_url( site_url());
        
        setlocale(LC_MONETARY, 'en_US');
		//print_r($run["orders"]);exit;
		$html = '<html>
    <head>
        <style>
            body {font-family: verdana;
                font-size: 10pt;
            }
            p {    margin: 0pt;
            }
            td { vertical-align: top; } 
			.maintable td{
				border:1px solid black;
				height:30px;
			}
			table.table {
				clear: both;
				margin-bottom: 6px !important;
			}
        </style>
    </head>
    <body>			
			<table width="100%" class="maintable">
			<tr style="background-color:#ccc;color:#000;"><td colspan="4">Run#'.$run["run"]["id"].'</td></tr>
			  <tr>
				<td width="25%" align="right">Size</td>
				<td>'.$run["run"]["size"].'</td>
				<td width="25%" align="right">Paperstock</td>
				<td>'.$run["run"]["paperstock"].'</td>
			</tr>					
			<tr>
				<td align="right">Colors</td>
				<td>'.$run["run"]["colors"].'</td>	
				<td align="right">Quantity</td>
				<td>'.$run["run"]["quantity"].'</td>
			  </tr>
			  <tr>				
				<td align="right">Coating</td>
				<td>'.$run["run"]["coating"].'</td>			
				<td align="right">Due Date</td>
				<td>'.$this->get_us_date($run["run"]["due_date"]).'</td>    
			  </tr>
			  <tr>			
				<td align="right">Proofs</td>
				<td>'.$run["run"]["proofs"].'</td>
				<td></td>
				<td></td>
			  </tr>			  
			  <tr>
				<td align="right">Additional Info/Comments</td>
				<td colspan="3">'.$run["run"]["additional_info"].'</td>
			  </tr>			  
			  <tr>
				<td colspan="4">'.$this->generate_order_details($run["orders"]).'
			  </tr>	
			  <tr>
				<td valign="top" colspan="4">
					<img height="400" src="'.site_url("application/run_orders_files/".$run["run"]["upload_image"]).'" />
				</td>
			  </tr>
			</table>
			<div>1. Approve color proof(before plates): ___________(front)___________(back)</div><br />
			<div>2. Check colors: ___________(front)___________(back)</div><br />
			<div>3. Check registration: ___________(front)___________(back)</div><br />
			<div>4. Check crop marks: ___________(front)___________(back)</div><br />
			<div>5. Check quality: ___________(front)___________(back)</div><br /><br /><br />
			<div>Supervisor signature: ____________________________________________</div><br />
			<div>Date: _______________</div><br />
	</body>
	</html>';
        $mpdf->WriteHTML($html);
        if( $file ){
            $mpdf->Output( $file, 'F');    
        } else {
            $mpdf->Output();    
        }
                    
   }
   
   private function generate_order_details(Order $orders){
   //print_r($orders);exit;
		$row='
		<table width="100%">
		<tr style="background-color:#ccc;color:#000;">
			<th>Invoice</th>
			<th>Job Name</th>
			<th>Description</th>
			<th>Date</th>
			<th>Due Date</th>
			<th>Rep</th>
		</tr>';
		foreach($orders as $order){
			$row.='<tr>
				<td align="center">'.$order->id.'</td>
				<td align="center">'.$order->job_name.'</td>
				<td align="center">'.$order->order_notes.'</td>
				<td align="center">'.$this->get_us_date($order->order_date).'</td>				
				<td align="center">'.$this->get_us_date($order->report_due).'</td>
				<td align="center">'.$order->sales_rep.'</td>
				</tr>';
		}
		$row .= '</table>';
		return $row;
   }
   private function get_us_date( $date ){
        if( preg_match('/(\d\d\d\d)-(\d\d)-(\d\d)/', $date, $match )){
            $date = $match['2'].'/'.$match['3'].'/'.$match['1'];
        }    
        return $date;    
   }
}  
?>