<?php
class Report_pdf{
    
    public function __construct(){
        require_once('application/libraries/MPDF54/mpdf.php');
    }
    
    public function create_report(Report $report, $file = false){
        $this->mpdf = $this->initialize_engine( $report ); 
        $this->report = $report; 
        $this->pdf($report, $file);       
    }
    
    private function initialize_engine( Report $report){
        $mpdf=new mPDF('win-1252','A4','','arial',20,15,48,25,10,10);
        $mpdf->useOnlyCoreFonts = true;  // false is default
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle( $report->_order->company->name . '-Order:' . $order->id );
        $mpdf->SetAuthor($report->_order->company->name);
        $mpdf->SetWatermarkText("Paid");
        $mpdf->showWatermarkText = false;
        $mpdf->watermark_font = 'DejaVuSansCondensed';
        $mpdf->watermarkTextAlpha = 0.1;
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->keep_table_proportions = true;
        $mpdf->showImageErrors = true; 
        return $mpdf;       
    }
    
    protected function get_path(){
        $path = parse_url( site_url());
        $path = str_replace('index.php','',$path['path']); 
        $path = $_SERVER['DOCUMENT_ROOT'] .   $path  . 'application/views/assets/img/';        
    }
    
    protected function pdf( Report $report, $file = false){
        $html = '<html>
            '.$this->get_head().'
            <body>
                <!--mpdf
                    '.$this->get_page_header($report).'
                    '.$this->get_page_footer().'
                mpdf-->
                '.$this->get_preface($report).'
                '.$this->get_report_details($report).'
                '.$this->get_additional_information($report).'
                '.$this->get_categories($report).'
            </body>
        </html>';        
        
        file_put_contents('c:\\html.html',$html);
        
        $this->mpdf->WriteHTML( $html );
        if( $file ){
            $this->mpdf->Output( $file, 'F');    
        } else {
            $this->mpdf->Output();    
        }         
    }
    
    private function get_preface( Report $report ){
         
        $html = '<bookmark content="Preface" level="0">
                 <div class="div_first_page">'.$report->_address.'</div>
                 <div class="div_subcategory" width="100%">
                    <table class="table_pictures" width="100%" >
                       <tbody>
                         '.$this->get_picture_rows($report->_pictures[0]).'
                       </tbody>
                    </table>                    
                 </div>                 
                 <span></span>';
        return $html;
    }
    
    private function get_report_details(Report $report ){
        $html =  '<bookmark content="Your report details" level="0">
                  <div class="category_div" width="40%"><span class="span_title">Your report details</span></div>
                  <div class="category_container">
                      <table class="table_details">
                        <tbody>'
                            .$this->get_report_details_table($report).
                        '</tbody>
                      </table>
                  </div>
                  ';
        return $html;                
    }
    
    private function get_report_details_table(Report $report){ 
        $fields = array(
            'Report #' => $report->_report_number,
            'Type of Property' => $report->_property_type,
            'Address' => $report->_address,
            'Inspector name' => $report->_order->inspector->name,
            'Date of inspection' => $report->_order->inspection_date,
            'Buyer' => $report->_order->client->name,
            'Agent' => $report->_order->agent->name,
            'Year built' => $report->estimated_age,
            'Attendees' => $report->attendees,
            'Property status' =>$report->property_status
        );
        foreach( $fields as $field=>$value){
            if( $value !== null ){
                $rows .= '<tr><td class="td_field"><b>'.$field.':</b></td><td class="td_value">'.$value.'</td></tr>';                            
            }
        }
        return $rows;
    }
    
    private function get_additional_information(Report $report){
        foreach( $report->_general_disclosures as $disclosure ){
            if( is_numeric($disclosure->id)){
                $list[] = '<li>'.$disclosure->description.'</li>';   
            }
        }
        foreach( $report->_types_selected as $disclosure ){
            if( is_numeric($disclosure->id)){
                $list[] = '<li>'.$disclosure->description.'</li>';    
            }
        } 
        $html = '';
        if($list){
            $html =   
                '<bookmark content="General Disclosures" level="0">
                 <div class="category_div" width="40%" style="margin-top: 20mm"><span class="span_title">General Disclosures</span></div>
                 <div class="category_container">
                    <div class="div_disclosure">
                         <ul>'.implode('',$list).'</ul>
                    </div>
                </div>';            
        } 
        
        if($report->comments){
            $html .=
                '<bookmark content="Additional Disclosures" level="0">
                <div class="category_div"  width="40%" style="margin-top: 20mm"><span class="span_title">Additional Disclosures</span></div>
                <div class="category_container">
                    <div class="div_disclosures">'.$report->comments.'</div>
                </div>
                <pagebreak />';            
        }
                    
        return '<pagebreak/>'.$html;
    }
    
    private function get_categories( Report $report ){
        $html = '';
        $categories = $report->get_categories();
        foreach( $categories->categories as $category_id=>$same_category_group ){
            $multi = count($same_category_group) > 1 ? true : false;
            foreach( $same_category_group as $index=>$category ){                    
                if( $category->validate()){                    
                    $html.=$this->get_category_section( $category );
                    $html.='<pagebreak />';
                }   
            }
        }
        return $html;
    }
    
    private function get_category_section(Category $category ){
        $html = '';
        foreach( $category->_subcategories as $subcategory_id=>$subcategory ){
            if( $subcategory->validate()){
                $html .= $this->get_subcategory_section( 
                    $subcategory,
                    isset($this->report->_pictures[$subcategory_id]) ? $this->report->_pictures[$subcategory_id]: array()
                );
                //$html.='<pagebreak />';    
            }
        }
        return '<bookmark content="'.$category->name.'" level="0">
                <div class="category_div">                
                    <span class="span_title">'.$category->name.'</span>
                </div>        
                <div class="category_container">  
                    '.$html.'
                </div>';
    }
    
    private function get_subcategory_section( $subcategory, $pictures ){
        $html  = '<div class="div_subcategory" width="100%">
                    <div class="div_subcategory_container">
                        '.$this->get_subcategory_div($subcategory).'
                    </div>
                    <table class="table_pictures" width="100%" >
                       <tbody>
                         '.$this->get_picture_rows($pictures).'
                       </tbody>
                    </table>                    
                  </div>';
        return $html;
    }
    
    private function get_subcategory_div( Subcategory $subcategory){
        $html .= 
        '<div>
            <bookmark content="'.$subcategory->name.'" level="1">
            <table class="tr_subcategory" width="100%">
               <tbody>
                <tr><td class="td_subcategory" width="70%">'.$this->clean($subcategory->name).'</td><td width="30%" class="td_rating">Condition:&nbsp;'.$subcategory->_rating.'</td></tr>
               </tbody>
            </table> 
            <div class="div_description">
              '.$this->get_values_section( $subcategory ).'
              '.$this->get_notes_section( $subcategory ).'
            </div>
        </div>';
        
        return $html;
    }
    
    private function clean( $string ){
        return str_replace('Good, Fair, Poor, None, N/A', '', $string);
    }
    private function get_values_section( Subcategory $subcategory ){
        $html = array();
        foreach( $subcategory->_value_ids as $id){
            $concerns = array();
            foreach( $subcategory->_values[$id]->_concerns as $concern){
                
                if( $concern->validate() && in_array($concern->id, $subcategory->_values[$id]->_concern_ids) ){
                    $concerns[] = '<li>'.$concern->name.'</li>';
                }
            }
            $html[] = '<li>'.$subcategory->_values[$id]->name.( $concerns ? '<ul>'.implode(' ', $concerns).'</ul>':'').'</li>';
        }
        return '<div><ul>'.implode('. ', $html).'</ul></div>';
    }
    
    private function get_notes_section( Subcategory $subcategory ){
        return $subcategory->comments ? '<span class="span_notes">Notes:&nbsp;</span>'.$subcategory->comments : '';        
    }
    
    private function get_picture_rows( $pictures ){
        $row = array();
        $rows = '';
        if( is_array($pictures)){
            foreach( $pictures as $picture ){
                if(count($row)<2){
                    $row[] = '<td class="td_image" width="50%"><img src="'.$picture->path.'"></td>';
                } else {
                    $rows .= '<tr>'.implode('',$row).'</tr>';
                    $row = array();
                }                      
            } 
            if($row){
                $rows .= '<tr>'.implode('',$row).'</tr>'; 
            }            
        }
        return $rows;
    }
    
    private function get_head(){
        return 
            '<head>
                    <style>
                        @page header{
                            margin-top: 20mm;
                            page-break-before: right;
                        }
                        @page noheader {
                            odd-header-name:  _blank;
                            even-header-name: _blank;
                            odd-footer-name:  _blank;
                            even-footer-name: _blank;
                            margin-top: 5mm;
                        }
                        .div_first_page{
                            color: #78B43B;
                            float: none !important;
                            font-size: 16pt;
                            font-weight: bold;
                            margin: 0 auto;
                            padding: 5px;
                            text-align: center;
                            width: 80%   
                        }
                        .div_header{
                            background-gradient: linear #FFFFFF #EBE9E9 0 0.5 0 0;
                        }
                        .table_details{
                            width:50%;
                            margin-left: auto;
                            margin-right: auto;
                        }
                        .td_field{
                        
                        }
                        .td_value{
                        
                        }
                        .div_title_details{
                            width: 40%;
                            color: #FFFFFF;
                            margin-left: auto;
                            margin-right: auto;
                            border-top-left-radius: 3mm;
                            border-top-right-radius: 3mm;
                            background-gradient: linear #99CD5A #6CA628 0 1 0 0.5;
                            text-align: center;
                            font-size: 14pt;                                                        
                        }
                        .div_disclosures_title{
                            width: 40%;
                            color: #FFFFFF;
                            margin-left: auto;
                            margin-right: auto;
                            border-top-left-radius: 3mm;
                            border-top-right-radius: 3mm;
                            background-gradient: linear #99CD5A #6CA628 0 1 0 0.5;
                            text-align: center;
                            font-size: 14pt;                                                        
                        }
                        .div_disclosures{
                            padding: 5px;
                        }
                        .div_disclosure{
                            width:50%;
                            margin-left: auto;
                            margin-right: auto;
                        }
                        .div_comments{
                            width:70%
                            margin-left: auto;
                            margin-right: auto;
                        }                    
                        .category_container{
                            border: 0.8mm solid  #DEDCDC;
                        }                                                
                        .category_div{
                            text-align: center;
                            font-size: 14pt;
                            page: noheader;   
                        }
                        .category_div{
                            color: #FFFFFF;
                            margin-left: auto;
                            margin-right: auto;
                            width: 20%;
                            border-top-left-radius: 3mm;
                            border-top-right-radius: 3mm;
                            background-gradient: linear #99CD5A #6CA628 0 1 0 0.5;                           
                        }
                        .table_pictures{
                           /*margin-top: -7mm;                           */
                        } 
                        .td_image{
                    
                            padding: 5mm;
                        }
                        .tr_subcategory{
                            border-bottom: 0.1mm solid #DEDCDC;
                            vertical-align: middle;
                            height: 5mm;
                            line-height: 0.5mm; 
                        }
                        .div_subcategory{
                            border-top: 0.1mm solid #DEDCDC;
                              
                        }
                        .div_subcategory_container{                            
                              
                        }
                        .td_subcategory{
                            padding-left: 4mm;
                            padding-bottom: 4mm;
                            width: 80%;
                            text-align: left;
                            color: #1A6FC9;
                            font-weight: bold;
                            font-size: 16pt;
                        }
                        .td_rating{
                            padding-right: 5mm;
                            padding-bottom:4mm;
                            width: 20%;
                            text-align: right;
                            font-size: 16pt;
                            color: #78B43B;
                            font-weight: bold;
                        }
                        .div_description{
                            padding-left: 5mm;
                            padding-right: 5mm;
                            padding-bottom: 5mm;
                        }
                        ul{
                            padding-left: 1mm;
                            padding-right: 1mm;                         
                        }
                        .span_notes{
                            font-weight: bold;
                            color: #1A6FC9;
                            margin-left: 1mm;
                            margin-right: 1mm;                        
                        }
                        .div_footer{
                            font-size: 9pt;
                            text-align: center;
                            padding-top: 3mm;
                        }
                    </style>
             </head>';
    }
    
    private function get_page_header(Report $report){ 
        $html =  
            '<htmlpageheader name="myheader">
                <div class="div_header">
                    <table width="100%">
                        <tr>
                            '.($report->_company_logo ?'<td><img src="'.$report->_company_logo.'"/></td>' : '').'
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td width="auto" style="color:#000000;float:right; text-align:right;"><span style="font-weight: bold; font-size: 14pt;">'.$report->_order->company->name.'</span><br />'.$report->_order->company->address.'<br />'.$report->_order->company->city.','.$report->_order->company->state.','. $report->_order->company->zip. '<br />'.$report->_order->company->phone_1.'<br />'. $report->_order->company->email .'<br /></td>                            
                        </tr>
                    </table>
                </div>
            </htmlpageheader>';            
        return $html;
    }
    
    public function get_page_footer(){
        return 
            '<htmlpagefooter name="myfooter">
                    <div class="div_footer">
                    Page {PAGENO} of {nb}
                    </div>
            </htmlpagefooter>
            <sethtmlpageheader name="myheader" value="on" show-this-page="1" />
            <sethtmlpagefooter name="myfooter" value="on" />';        
    }
}  
?>
