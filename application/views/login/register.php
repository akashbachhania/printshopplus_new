<!DOCTYPE html>
<html>
    <head>
        <title>Registration :: PrintShopPlus</title>
        <?php $this->load->view('login/login_header');?>
    </head>
    <body>        
        <div class="container">
          <section id="gridSystem">
              <div>
                  <div class="span6 offset2 well">
                    <form class="form-horizontal" action="" method="post">
                    <fieldset>
                        <legend style="margin-bottom: 20px;"><img src="http://www.printshopplus.net/live/application/views/assets/img/homeinspector.logo.png" alt="PrintShopPlus logo"></legend>
                    <?php if(validation_errors() !='') { ?>
                        <div class="alert alert-danger"><strong>Error!</strong> <?php echo validation_errors(); ?> </div>
                    <?php }?>
                    <?php if($error !='') { ?>
                        <div class="alert alert-danger"><strong>Error!</strong> <?php echo $error; ?> </div>
                    <?php }?>
                    <?php $message = $this->session->userdata('frontMsg'); ?>
                    <?php if($message !='') { 
                        $this->session->set_userdata('frontMsg','');
                        ?>
                      <div class="alert alert-success">
                          <strong>Success!</strong> <?php echo $message; ?>
                      </div>
                    <?php }?>  
                    <div class="control-group">
                        <label class="control-label" for="company_name">Company Name</label>
                        <div class="controls">
                            <input type="text" class="input-large" name="company_name" id="company_name" value="<?php echo (isset($_POST['company_name'])) ? $_POST['company_name'] : '';?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="your_full_name">Your Full Name</label>
                        <div class="controls">
                            <input type="text" class="input-large" name="your_full_name" id="your_full_name" value="<?php echo (isset($_POST['your_full_name'])) ? $_POST['your_full_name'] : '';?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="email_text">Email</label>
                        <div class="controls">
                            <input type="text" class="input-large" name="email_text" id="email_text" value="<?php echo (isset($_POST['email_text'])) ? $_POST['email_text'] : $email;?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="address_text">Address</label>
                        <div class="controls">
                            <input type="text" class="input-large" name="address_text" id="address_text" value="<?php echo (isset($_POST['address_text'])) ? $_POST['address_text'] : '';?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="city_text">City</label>
                        <div class="controls">
                            <input type="text" class="input-large" name="city_text" id="city_text" value="<?php echo (isset($_POST['city_text'])) ? $_POST['city_text'] : '';?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="zip_text">Zip</label>
                        <div class="controls">
                            <input type="text" class="input-large" name="zip_text" id="zip_text" value="<?php echo (isset($_POST['zip_text'])) ? $_POST['zip_text'] : '';?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="state_text">State</label>
                        <div class="controls">
                            <select name="state_text" class="input-large">
                            <?php 
                                foreach($states as $key => $value) {
                                    ?><option value="<?php echo $key;?>"><?php echo $value;?></option><?php
                                }
                            ?>
                            </select>
                            
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="phone_text">Phone</label>
                        <div class="controls">
                            <input type="text" class="input-large" name="phone_text" id="phone_text" value="<?php echo (isset($_POST['phone_text'])) ? $_POST['phone_text'] : '';?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="password">Password</label>
                        <div class="controls">
                            <input type="password" class="input-large" name="password" id="password">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="re_password">Retype Password</label>
                        <div class="controls">
                            <input type="password" class="input-large" name="re_password" id="re_password">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="input02"></label>
                        <div class="controls">
                            <input type="submit" class="btn span2" name="submit" id="submit" value="Register">
                        </div>
                    </div>                                                           
                </fieldset>
            </form>            
                  </div>          
              </div>
          </section>
        </div>
    </body>
</html>