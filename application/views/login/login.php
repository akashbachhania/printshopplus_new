<!DOCTYPE html>
<html>
    <head>
        <title>Print Shop Plus</title>
        <?php $this->load->view('login/login_header');?>
    </head>
    <body style="background-color:#a4d1e8;">        
        <div class="container">
          <section id="gridSystem">
              <div>
                  <div class="span6 offset2" style="border: 10px solid rgba(0, 0, 0, 0.05); background-color:#477fa9;">
                    <form class="form-horizontal" action="<?php echo site_url('login')?>" method="post">
                        <fieldset>
                        <img src="http://www.printshopplus.net/live/application/views/assets/img/homeinspector.logo.png" alt="PrintShopPlus logo">
<div class="control-group">
              <label class="control-label" for="input01"><span style="color:#FFF;">Username</span></label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge" name="username"id="input01">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="input02"><span style="color:#FFF;">Password</span></label>
                                <div class="controls">
                                    <input type="password" class="input-xlarge" name="password" id="input02">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="input02"></label>
                                <div class="controls">
                                    <input type="submit" class="btn span2" name="submit" id="submit" value="Ok"><br/><br/>
                                    <a href="<?= base_url() ?>login/forget_pwd" style="color:#fff;text-decoration:none;">I forgot my password</a>
                                </div>
                            </div>                                                           
                        </fieldset>
                    </form>          
                  </div>          
              </div>
          </section>
        </div>
    </body>
</html>            