
<?php
if ($saved !== null) {
    if ($saved === true) {
        $saved = 'alert alert-success';
        $msg = '<strong>Success: </strong>Your order has been saved';
    } else if ($saved === false) {
        $saved = 'alert alert-error';
        $msg = '<strong>Error: </strong>Your order has not been saved (Please correct fields with red border)';
    } else if ($saved === 'update') {
        $saved = 'alert alert-info';
        $msg = '<strong>Success: </strong>Your order has been updated successfully!';
    } else if ($saved === 'inspector_taken') {
        $saved = 'alert alert-error';
        $msg = '<strong>Error: </strong>I\'m sorry, ' . $order->inspector->name . ' is already booked for that time.  Please select another time or another inspector!';
    } else if ($saved === 'email-sent') {
        $saved = 'alert alert-success';
        $msg = '<strong>Success: </strong>Emails have been sent';
    } else if ($saved === Authorize_payment::STATUS_FAILED) {
        $saved = 'alert alert-error';
        if ($order->errors) {
            $msg = '<strong>Error: </strong>' . implode($order->errors);
            $order->errors = array();
        } else {
            $msg = '<strong>Error: </strong>Transaction could not be performed at this time';
        }
    } else if ($saved === Authorize_payment::STATUS_SUCCESS) {
        $saved = 'alert alert-success';
        $msg = '<strong>Success: </strong>Credit card has been charged';
        $order->errors = array();
    }
} else {
    $saved = '';
    $msg = '';
}
$orderid = $this->uri->segment(3);
if ($this->uri->segment(2) == 'delete') {
    $orderid = "";
}

if (is_array($errors) and count($errors) > 0) {
    $saved = 'alert alert-error';
    $msg = ($msg != '') ? $msg . '<br />' : '<strong>Error: </strong><br />';
    foreach ($errors as $key => $error) {
        if (is_array($error) and count($error) > 0) {
            foreach ($error as $newK => $newValue)
                $msg .= ucfirst($key) . '  ' . ucfirst($newK) . ':  ' . $newValue . '<br />';
        } else {
            $msg .= ucfirst($key) . ' ' . $error . '<br />';
        }
    }
}
$posturl = isset($orderid) ? site_url('orders/new_order/' . $orderid) : site_url('orders/new_order');
$orderJob = new Ordersjob();

$user_order_job = ($this->session->userdata('user_order') != '') ? $this->session->userdata('user_order') : rand(40000,99999);
if($order->id > 0) {
    $orderIdForJob = $order->id;
} else {
    $orderIdForJob = $user_order_job;
    $this->session->set_userdata('user_order',$user_order_job);
}

?>      
<style>
    .addJobBtn {
        width: 100%;
        text-align: center;
        margin: 0 auto;
        padding-bottom: 10px;
        padding-top: 10px;
    }
    #orders_job_section {
        margin-top: 10px;
        padding-left: 20px;
    }
    #notes_date {
        width: 150px;
    }
</style>
<script type="text/javascript">
    <?php if ($orderid > 0) { ?>
        function reorder() {
            if (confirm('Are you sure you want to create a new order with these same details?')) {
                alert('<?php echo base_url() ?>orders/duplicate_order/<?php echo $orderid; ?>');
                document.location.href = '<?php echo base_url() ?>orders/duplicate_order/<?php echo $orderid; ?>';
            }

        }
        <?php } ?>
        function showupload()
        {
            if (document.getElementById("uploadon").value == 1) {
                new vpb_multiple_file_uploader
                ({
                    vpb_form_id: "form_order",
                    autoSubmit: true,
                                    vpb_server_url: "<?php echo base_url() . 'application/views/orders/vpb_uploader.php' ?>" // PHP file for uploading the browsed filese as wish.
                                });
                document.getElementById("uploadon").value = 2;
            }
        }
        function checkform(formid) {
            if($('#order__notes').val() != ''){
                var a = '<?= (isset($order->order_notes) && ($order->order_notes != "")) ? $order->order_notes . " / " : "" ?>';
                $('#order_notes').attr('value',a + $('#order__notes').val());    
            }
            if($('#payment__n').val() != ''){
                var b = '<?= (isset($order->payment_n) && ($order->payment_n != "")) ? $order->payment_n . " / " :"" ?>';
                $('#payment_n').attr('value',b + $('#payment__n').val());
            }            
            
            saveJobSpecs();
            alert('Checking job to save.');
            if (formid == 1) {
                // alert($('#order_notes').val());
                // alert($('#payment_n').val());
                // alert("<?php echo $posturl; ?>");
                document.getElementById('form_order').action = "<?php echo $posturl; ?>";
                        
                        //document.getElementById('form_order').submit();
                    } else if (formid == 3) {

                        var order_id = $("input[name=order_id]").val();
                        var dataString = $("#form_order").serialize();

                        $.ajax({
                            type: "POST",
                            url: "<?php echo site_url('orders/save_order_pdf'); ?>/" + order_id,
                            data: dataString,
                            cache: false,
                            beforeSend: function ()
                            {
                            },
                            success: function (response)
                            {
                                var print_url = '';
                                var ret_val = jQuery.parseJSON(response);
                                if (ret_val["save_order"] == "updated") {
                                    print_url = "<?php echo site_url('orders/print_order_pdf'); ?>/" + order_id;
                                    window.open(print_url);
                                    window.location.href = "<?php echo site_url('orders/show_order'); ?>/" + order_id;
                                }
                                else {
                                    print_url = "<?php echo site_url('orders/print_order_pdf'); ?>/" + ret_val["save_order"];
                                    window.open(print_url);
                                    window.location.href = "<?php echo site_url('orders/show_order'); ?>/" + ret_val["save_order"];
                                }
                            }
                        });
}
else if (formid == 2) {
    var validate = true;
    if (document.getElementById("from").value == "") {
        alert("Please enter from id");
        document.getElementById("from").focus();
        validate = false;
        return false;
    } else {
        var email = document.getElementById("from").value;
        if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))) {
            alert("Invalid E-mail address! Please re-enter.");
            document.getElementById("from").focus();
            validate = false;
            return false;
        }
    }
    if (document.getElementById("to").value == "") {
        alert("Please enter to id");
        document.getElementById("to").focus();
        validate = false;
        return false;
    } else {
        var email = document.getElementById("to").value;
        if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))) {
            alert("Invalid E-mail address! Please re-enter.");
            document.getElementById("to").focus();
            validate = false;
            return false;
        }
    }
    if (document.getElementById("subject").value == "") {
        alert("Please enter subject");
        document.getElementById("subject").focus();
        validate = false;
        return false;
    }
    if (document.getElementById("message").value == "") {
        alert("Please enter message");
        document.getElementById("message").focus();
        validate = false;
        return false;
    }
    if (validate) {
        document.getElementById("ajax_url").value = "<?php echo base_url() . 'application/views/orders/vpb_uploader.php' ?>";
        if (document.getElementById("uploadon").value == 1) {
            new vpb_multiple_file_uploader
            ({
                vpb_form_id: "form_order",
                autoSubmit: true,
                                            vpb_server_url: document.getElementById("ajax_url").value // PHP file for uploading the browsed filese as wish.
                                        });
        }
    }
}
}

$(document).ready(function () {
    calculate();
    $('.accordion').collapse();
    $('#div_order_inspection fieldset , #accordion').css('height', 'auto');
    $('#addJobBtn').click(function() {

        var order_type = $('#order_type').val(); 
        var post_data = '';
        if(order_type == '1') {
            var jobName = $('#job_name').val();
            if(jobName == '') {
                $('#job_name').focus();
                return false;
            }
            post_data = $('#print_order').find("select, textarea, input").serialize();
        } else if(order_type == '2') {
            var jobName = $('#shirt_job_name').val();
            if(jobName == '') {
                $('#shirt_job_name').focus();
                return false;
            }
            post_data = $('#shirt_order').find("select, textarea, input").serialize();                                
        }
        var combine_data = $('#combine_fields').find("select, textarea, input").serialize();

        $.ajax({
            type: "POST",
            url: "<?php echo site_url('orders/submit_job'); ?>/<?php echo $orderIdForJob;?>?order_type="+order_type,
            data: post_data + '&' +combine_data,
            cache: false,
            success: function (response) {
                if($('#job_id').val() != '') {
                    $('#job_detail_'+$('#job_id').val()).remove();
                }
                if($('#shirt_job_id').val() != '') {
                    $('#job_shirt_'+$('#shirt_job_id').val()).remove();
                }                                    
                $('#accordion').append(response);                                    
                $('#cancelJobBtn').click();

            }
        });

    });


$('#cancelJobBtn').click(function() {
    $('#addJobBtn').val('Add Job Specs');
    $('#div_order_inspection').trigger('reset');
    $('#job_name').val('');
    $('#others').val('');
    $('#additional_notes').val('');
    $('#job_id').val('');
    selectJobDetail('stock','');
    selectJobDetail('colors','');
    selectJobDetail('size','');
    selectJobDetail('quantity','');
    selectJobDetail('finishing','');
    selectJobDetail('coating','');
    selectJobDetail('department','');

    $('#shirt_qty_size_more').html('');
    $('#shirt_job_name').val('');
    $('#shirt_job_id').val('');
    $('#shirt_quantity').val('');
    selectJobDetail('shirt_type','');
    selectJobDetail('shirt_front_color','');
    selectJobDetail('shirt_back_color','');
    selectJobDetail('shirt_size','');                            
});

$('#order_type').change(function() {
   var order_type = $(this).val();
   if(order_type == 1) {
       $('#print_order').show();
       $('#shirt_order').hide();
   } else {
       $('#shirt_order').show();
       $('#print_order').hide();
   }
});

});
function editProductionDetail(jobId) {
    $('#addJobBtn').val('Edit Job Specs');
    $.ajax({
        type: "POST",
        url: "<?php echo site_url('orders/getJobInfo'); ?>",
        data: 'order_id=<?php echo $orderIdForJob;?>&job_type=1&job_id='+jobId,
        cache: false,
        success: function (response)
        {
            object = JSON.parse(response);
            if(object.succcess) {
                $('#job_name').val(object.info.job_name);
                $('#others').val(object.info.others);
                $('#additional_notes').val(object.info.additional_notes);
                $('#job_id').val(jobId);
                selectJobDetail('stock',object.info.stock);
                selectJobDetail('colors',object.info.colors);
                selectJobDetail('size',object.info.size);
                selectJobDetail('quantity',object.info.quantity);
                selectJobDetail('finishing',object.info.finishing);
                selectJobDetail('coating',object.info.coating);
                selectJobDetail('department',object.info.department);     
            }
        }
    });
}
function delBtn(obj){
    if( confirm('Do you really want to delete this note?') ){
        $(obj).parent().parent().css('display','none');
        var note = $(obj).parent().parent().find('#n_n').text();

        var main_note = $(obj).parent().parent().parent().find('#order_notes').val().replace(note + ' / ','').replace(' / ' + note,'').replace(note,'').split('/').join('/');
        
        $('#order_notes').val(main_note);
    }
}
function delBtnC(obj){
    if( confirm('Do you really want to delete this note?') ){
        $(obj).parent().parent().css('display','none');
        
        var note = $(obj).parent().parent().find('#c_n').text();

        var main_note = $(obj).parent().parent().parent().find('#payment_n').val().replace(note + ' / ','').replace(' / ' + note,'').replace(note,'').split('/').join('/');
        
        $('#payment_n').val(main_note);
    }
}
function editShirtDetail(jobId) {
    $('#addJobBtn').val('Edit Job Specs');                    
    $.ajax({
        type: "POST",
        url: "<?php echo site_url('orders/getJobInfo'); ?>",
        data: 'order_id=<?php echo $orderIdForJob;?>&job_type=2&job_id='+jobId,
        cache: false,
        success: function (response)
        {
            object = JSON.parse(response);
            if(object.succcess) {
                selectJobDetail('order_type',2);
                $('#shirt_job_name').val(object.info.job_name);
                $('#shirt_job_id').val(jobId);
                $('#others').val(object.info.others);
                $('#additional_notes').val(object.info.additional_notes);
                selectJobDetail('shirt_type',object.info.type);
                selectJobDetail('shirt_front_color',object.info.front_color);
                selectJobDetail('shirt_back_color',object.info.back_color);     
                selectJobDetail('shirt_size',object.info.size);
                selectJobDetail('department',object.info.department);   
                $('#shirt_quantity').val(object.info.quantity);  
                $( object.info.size_quantity ).each(function( i, elem ) {
                    var newSizeQty = $('#shirt_qty_size').html();
                    newSizeQty = newSizeQty.replace('id="shirt_size"', 'id="shirt_size_'+i+'"'); 
                    newSizeQty = newSizeQty.replace('value="'+elem.size+'"', 'value="'+elem.size+'" selected'); 
                    newSizeQty = newSizeQty.replace('id="shirt_quantity"', 'value="'+elem.quantity+'"'); 
                    $('#shirt_qty_size_more').append(newSizeQty);

                });
            }
        }
    });
}
function deleteProductionDetail(jobId) {
    $.ajax({
        type: "POST",
        url: "<?php echo site_url('orders/delJobInfo'); ?>",
        data: 'order_id=<?php echo $orderIdForJob;?>&job_type=1&job_id='+jobId,
        cache: false,
        success: function (response) {
            object = JSON.parse(response);
            if(object.succcess) {
                $('#job_detail_'+jobId).remove();
            }
        }
    });
}
function deleteShirtDetail(jobId) {
    $.ajax({
        type: "POST",
        url: "<?php echo site_url('orders/delJobInfo'); ?>",
        data: 'order_id=<?php echo $orderIdForJob;?>&job_type=2&job_id='+jobId,
        cache: false,
        success: function (response) {
            object = JSON.parse(response);
            if(object.succcess) {
                $('#job_shirt_'+jobId).remove();
            }
        }
    });
}
function selectJobDetail(objectSelector,value) {
    if(value > 0)  {
        $('#'+objectSelector).val(value).change();
    } else {
        $('#'+objectSelector).val('').change();
    }
}
function addMoreSizeQty() { 
    $('#shirt_qty_size_more').append($('#shirt_qty_size').html());
}
function saveJobSpecs() {
 var jobName = $('#job_name').val();
 var jobShirtName = $('#shirt_job_name').val();
 if(jobName !='' || jobShirtName !='')
    $('#addJobBtn').click();
}

</script>

<?php $this->load->view('include/header'); ?>

<form id="form_order" action="javascript:void(0);" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-lg-12" id="div_order_details">
            <div class="panel panel-default panel-block">
                <div class="panel-heading">
                    <h4><?= $orderid == null ? 'Order details' : 'Edit Order # ' . $order->company_order_id ?></h4>
                </div>
                <div class="list-group">
                    <div class="list-group-item o_d" id="input-fields">
                        <?php if ($saved) { ?>
                        <div id="div_order_added" class="<?php echo $saved ?>">
                            <!--<a class="close" data-dismiss="alert">x</a>-->
                            <?php echo $msg ?>
                        </div>
                        <?php } ?>

                        <?php
                        echo $this->order_helper->get_object_form(
                            $order, array('order_date', 'report_due', 'po_text', 'sales_rep', 'terms'), Order_helper::FORM_INLINE, array(
                                'order_date' => array(
                                    'date' => true,
                                    ),
                                'report_due' => array(
                                    'date' => true,
                                    ),
                                'po_text' => array(
                                    ),
                                'sales_rep' =>
                                array('element' => Order_helper::ELEMENT_DROPDOWN,
                                    'options' => $salesrep,
                                    'selected' => $order->sales_rep,
                                    ),
                                'terms' =>
                                array('element' => Order_helper::ELEMENT_DROPDOWN,
                                    'options' => $terms,
                                    'selected' => $order->terms,
                                    ),
                                '__legend' => ''
                                ), null, array('order_date' => 'Date', 'report_due' => 'Due', 'sales_rep' => 'Rep','status' => 'Department ')
);
?>

</div>
</div>
</div>
</div>
</div>

<div class="row" id ="div_client_details">
    <div class="col-lg-6" id="div_order_client">
        <div class="panel panel-default panel-block">
            <div class="panel-heading">
                <h4>Client details</h4>
                
            </div>
            <div class="list-group">
                <div class="list-group-item" id="input-fields">

                    <?php
                    echo $this->order_helper->get_object_form($order, array('company', 'client::name', 'client::contact', 'client::address', 'client::address_2', 'client::state', 'client::city', 'client::zip', 'client::phone_1', 'client::phone_2', 'client::email','client::email_2','client::email_3','client::notes', 'client::referred_by', 'shipping_method', 'client::id',), null, array(
                        'company' => array(
                            'element' => Order_helper::ELEMENT_DROPDOWN,
                            'options' => $companylist,
                            'selected' => $order->client->id),
                        'client::notes' => array( 'element'  => Order_helper::ELEMENT_TEXTAREA),  
                        'client::state' => array(
                            'element' => Order_helper::ELEMENT_DROPDOWN,
                            'options' => $us_states,
                            'selected' => $order->client->state),
                        'shipping_method' => array(
                            'element' => Order_helper::ELEMENT_DROPDOWN,
                            'options' => $shipping_method,
                            'selected' => $order->shipping_method),
                        '__legend' => ''), array('client' => 'client'), array('client::name' => 'name')
                    );
                    ?>

                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-6" id="div_order_inspection">
        <div class="panel panel-default panel-block o_d_p">
            <div class="panel-heading">
                <h4>Production details</h4>
            </div>
            <div class="list-group">
                <div class="list-group-item" id="input-fields">

                    <div id="orders_job_section">
                        <div class="panel-group" id="accordion">
                            <?php 
                            $jobs = new Ordersjob(null,$orderIdForJob);
                            $jobsResult = $this->order_model->get_object( $jobs );
                            if($jobsResult) {
                                $getJobDetail = $this->order_model->orderJobDetails($orderIdForJob);
                                foreach($getJobDetail as $job) { ?>

                                <div class="panel panel-default" id="job_detail_<?php echo $job->job_id;?>">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#accordion_<?php echo $job->job_id?>">
                                                <?php echo $job->job_name?>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="accordion_<?php echo $job->job_id?>" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <b>Stock:</b> <?php echo $job->stock_name?><br />
                                            <b>Color:</b> <?php echo $job->color_name?><br />
                                            <b>Size:</b> <?php echo $job->size_name?><br />
                                            <b>Quantity:</b> <?php echo $job->quantity_name?><br />
                                            <b>Finishing:</b> <?php echo $job->finishing_name?><br />
                                            <b>Coating:</b> <?php echo $job->coat_name?><br />
                                            <b>Department:</b> <?php echo $job->department_name?><br />
                                            <b>Others:</b> <?php echo $job->others?><br />
                                            <b>Additional notes:</b> <?php echo $job->additional_notes?><br />
                                            <a href="javascript:void(0)" onclick="editProductionDetail(<?php echo $job->job_id?>)">Edit</a>
                                            | <a href="javascript:void(0)" onclick="deleteProductionDetail(<?php echo $job->job_id?>)">Delete</a>
                                        </div>
                                    </div>
                                </div>


                                <?php } } ?>
                                <?php 
                                $jobs = new Ordersshirt(null,$orderIdForJob);
                                $jobsResult = $this->order_model->get_object( $jobs );
                                if($jobsResult) {
                                    $getJobDetail = $this->order_model->orderShirtJobDetails($orderIdForJob);                            
                                    foreach($getJobDetail as $job) { ?>

                                    <div class="panel panel-default" id="job_shirt_<?php echo $job->job_id;?>">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#demo-accordion" href="#accordion_shirt_<?php echo $job->job_id?>">
                                                    <?php echo $job->job_name?>
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="accordion_shirt_<?php echo $job->job_id?>" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <b>Type:</b> <?php echo $job->type_name;?><br />
                                                <b>Front Color:</b> <?php echo $job->fcolor_name;?><br />
                                                <b>Back Color:</b> <?php echo $job->bcolor_name;?><br />
                                                <?php 
                                                $sizeQTy = unserialize($job->quantity_size);
                                                $sizeQuantity = array();
                                                if(is_array($sizeQTy)) {
                                                    $i = 1;                    
                                                    foreach ($sizeQTy as $size) {
                                                        $sizeObject = new ShirtSize($size['size']);
                                                        $jobsResult = $this->order_model->get_object( $sizeObject,true );
                                                        ?>
                                                        <b>Size #<?php echo $i;?>:</b> <?php echo @$jobsResult->name;?><br />
                                                        <b>Quantity #<?php echo $i;?>:</b> <?php echo $size['quantity'];?><br />
                                                        <?php
                                                        $i++;
                                                    }
                                                }
                                                ?>
                                                <b>Department:</b> <?php echo $job->department_name;?><br />
                                                <b>Others:</b> <?php echo $job->others;?><br />  
                                                <b>Additional notes:</b> <?php echo $job->additional_notes;?><br />
                                                <a href="javascript:void(0)" onclick="editShirtDetail(<?php echo $job->job_id?>)">Edit</a>
                                                | <a href="javascript:void(0)" onclick="deleteShirtDetail(<?php echo $job->job_id?>)">Delete</a>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            } ?>


                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="stock" class="control-label col-lg-2">Order Type</label>                            
                        <div class="controls col-lg-6">   
                            <?php echo form_dropdown('order_type', array('1' => 'Print Order', '2' => 'Shirt Order') , '', 'class="input-large form-control" id="order_type"') ?>                                   
                        </div>
                    </div>

                    <div name="controls" id="print_order" style="padding-top: 0px!important;">
                        <div class="form-group">
                            <label for="job_name" class="control-label">Job name</label>                            
                            <div class="controls">                          

                                <input type="text" value="" name="job_name" id="job_name" class="input-large form-control">                                         
                                <input type="hidden" name="job_id" id="job_id" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="stock" class="control-label">Stock</label>                            
                            <div class="controls">   
                                <?php echo form_dropdown('stock', $stocks, '', 'class="input-large form-control" id="stock"') ?>                                   
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="colors" class="control-label">Colors</label>                            
                            <div class="controls"> 
                                <?php echo form_dropdown('colors', $colors, '', 'class="input-large form-control" id="colors"') ?>                                      
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="size" class="control-label">Size</label>                            
                            <div class="controls"> 
                                <?php echo form_dropdown('size', $sizes, '', 'class="input-large form-control" id="size"') ?>                                   
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="quantity" class="control-label">Quantity</label>                            
                            <div class="controls">
                                <?php //echo form_dropdown('quantity', $quantities, '', 'class="input-large" id="quantity"') ?>                                    
                                <input type="text" value="" name="quantity" id="quantity" class="input-large form-control">                                         
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="finishing" class="control-label">Finishing</label>                            
                            <div class="controls">      
                                <?php echo form_dropdown('finishing', $finishings, '', 'class="input-large form-control" id="finishing"') ?>                                    
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="coating" class="control-label">Coating</label>                            
                            <div class="controls">                           
                                <?php echo form_dropdown('coating', $coating, '', 'class="input-large form-control" id="coating"') ?>                                             
                            </div>
                        </div>  
                    </div>

                    <div id="shirt_order" name="controls" style="padding-top: 0px!important; display: none;">
                        <div class="form-group">
                            <label for="job_name" class="control-label">Job name</label>                            
                            <div class="controls">                          
                                <input type="hidden" name="shirt[job_id]" id="shirt_job_id" value="">
                                <input type="text" value="" name="shirt[job_name]" id="shirt_job_name" class="input-large form-control">                                         
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="shirt_type" class="control-label">Shirt Type</label>                            
                            <div class="controls">   
                                <?php echo form_dropdown('shirt[type]', $shirt_type, '', 'class="input-large form-control" id="shirt_type"') ?>                                   
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="shirt_front_color" class="control-label">Front Color</label>                            
                            <div class="controls">   
                                <?php echo form_dropdown('shirt[front_color]', $shirt_front_color, '', 'class="input-large form-control" id="shirt_front_color"') ?>                                   
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="shirt_back_color" class="control-label">Back Color</label>                            
                            <div class="controls">   
                                <?php echo form_dropdown('shirt[back_color]', $shirt_back_color, '', 'class="input-large form-control" id="shirt_back_color"') ?>                                   
                            </div>
                        </div>
                        <div id="shirt_qty_size">
                            <div class="form-group">
                                <label class="control-label">Size</label>                            
                                <div class="controls">   
                                    <?php echo form_dropdown('shirt[size][]', $shirt_size, '', 'class="input-large form-control" id="shirt_size"') ?>                                   
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Quantity</label>                            
                                <div class="controls">   
                                    <input type="text" name="shirt[quantity][]" class="input-large form-control" id="shirt_quantity">                                  
                                </div>
                            </div>
                        </div>
                        <div id="shirt_qty_size_more">

                        </div>
                        <div class="form-group">
                            <label for="stock" class="control-label">&nbsp;</label>                            
                            <div class="controls">   
                                <a href="javascript:void(0)" onclick="addMoreSizeQty();">Add Another</a>                         
                            </div>
                        </div>  
                    </div>

                    <div id="combine_fields" style="padding-top:15px;">
                        <div class="form-group">
                            <label for="department" class="control-label">Department</label>                            
                            <div class="controls">                           
                                <?php echo form_dropdown('department', $status, '', 'class="input-large form-control" id="department"') ?>                                             
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="others" class="control-label">Others</label>                            
                            <div class="controls">                           
                                <input type="text" value="" name="others" id="others" class="input-large form-control">                                         
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="additional_notes" class="control-label">Additional notes</label>                            
                            <div class="controls">                           
                                <textarea rows="6" name="additional_notes" id="additional_notes" class="input-large form-control"></textarea>                                         
                            </div>
                        </div>
                    </div>
                    <div class="addJobBtn">
                        <input type="button" name="addJobBtn" value="Add Job Specs" id="addJobBtn" class="btn btn-info">
                        <input type="button" name="cancelJobBtn" value="Cancel" id="cancelJobBtn" class="btn btn-info">
                    </div>


                </div>
            </div>
        </div>
    </div>

</div>


<div class="row">

    <div class="col-lg-12">

        <div class="panel panel-default panel-block">
            <div class="panel-heading">
                <h4>Order Notes</h4>
            </div>
            <div class="list-group" id="div_order_notes">
                <div class="list-group-item o_n" id="input-fields">
                    <div class="form-horizontal">            

                        <div name="controls" id="o_notes">
                            <div class="form-group row">
                                <label class="control-label col-lg-2" for="order_notes">Order notes</label>                            
                                <div class="controls col-lg-12">
                                    <textarea class="input-largeOrderNotes form-control" id="order__notes" name="order__notes" rows="6" value="<?= $order->order_notes; ?>"></textarea>                                         
                                    <input type="hidden" id="order_notes" name="order_notes" value="<?= $order->order_notes; ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <!-- <label class="control-label col-lg-1" for="notes_date"></label>                             -->
                                <!-- <div class="controls col-lg-2">                            -->
                                    <!-- <input data-datepicker="datepicker" type="hidden" class="form-control input-largeOrderNotes" id="notes_date" name="notes_date" value="<?= $order->notes_date; ?>" data-date-format="mm-dd-yyyy"> -->
                                    
                                <!-- </div> -->
                                <div class="col-lg-5">
                                    <input type="hidden" class="form-control" id='notes_date'  name='notes_date' value="<?= date('m-d-Y'); ?>">
                                    <input id="s_n" type="submit" class="btn btn-info" name="save_n" onclick="checkform(1);" value="Save Notes">
                                    <button id="c_n" class="btn btn-info">Clear Notes</button>  
                                </div>
                            </div>
                            <?php if( ($order->notes_date != '') && ($order->order_notes != '') ){ ?>
                                <?php $order_n_otes = explode(' / ', $order->order_notes); ?>
                                <?php foreach ($order_n_otes as $keys => $values) { ?>
                                    
                                    <div class="form-group row s_n">
                                        <div class="col-lg-2" style="border:1px solid #ddd;margin-left:10px;">
                                            <p id="n_d"><?= $order->notes_date; ?></p>    
                                        </div>
                                        <div class="col-lg-2" style="border:1px solid #ddd;margin-left:10px;">

                                            <p id="n_u_n"></p>
                                        </div>
                                        <div class="col-lg-6" style="border:1px solid #ddd;margin-left:10px;">
                                            <p id="n_n"><?= $values; ?></p>
                                        </div>
                                        <div class="col-lg-1">
                                            <button class="btn btn-info" id="del_btn" onclick="delBtn(this)">Delete</button>
                                        </div>
                                    </div>    

                                <?php } ?>
                                
                            <?php } ?>
                        </div>

                    </div>
                    <?php

                                // echo $this->order_helper->get_object_form($order, array('order_notes','notes_date'), 'largeOrderNotes', array(
                                //     'notes_date' => array(
                                //         'date' => true,
                                //     ),
                                //     'order_notes' =>
                                //     array('element' => Order_helper::ELEMENT_TEXTAREA,
                                //         'rows' => 6),
                                //     '__legend' => '')
                                // );
                    ?>

                </div>

            </div>
        </div>

    </div>

</div>

<div class="row" id="id_div_items">

    <div class="col-lg-12" id="div_order_email">

        <div class="panel panel-default panel-block">
            <div class="panel-heading">
                <h4>Send Proofs to customer</h4>
            </div>
            <div class="list-group" id="div_order_notes">
                <div class="list-group-item" id="input-fields">

                    <div name="controls">
                        <div class="form-group">
                            <div class="controls">
                               <!-- <iframe align="left" src="http://localhost/uploadfile/index2.html" scrolling="yes" height="200px" width="700px" ></iframe>-->
                           </div>
                       </div>

                       <div class="form-group row">
                        <label class="control-label col-lg-2" for="from">From</label>
                        <div class="controls col-lg-6">
                            <textarea class="emailcss form-control" id="from" name="from" rows="1"></textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="control-label col-lg-2" for="to">To</label>
                        <div class="controls col-lg-6">
                            <textarea class="emailcss form-control" id="to" name="to" rows="1"></textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="control-label col-lg-2" for="subject">Subject</label>
                        <div class="controls col-lg-6">
                            <textarea class="emailcss form-control" id="subject" name="subject" rows="1"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-lg-2" for="message">Message</label>
                        <div class="controls col-lg-12">
                            <textarea class="input-largeOrderNotes form-control" id="message" name="message" rows="6"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-lg-2" for="message">Upload Files</label>
                        <div class="controls col-lg-6">
                            <div class="vpb_browse_file_box"><input type="file" class="form-control" name="vasplus_multiple_files" id="vasplus_multiple_files" multiple="multiple" style="opacity:0;-moz-opacity:0;filter:alpha(opacity:0);z-index:9999;width:90px;padding:5px;cursor:default;" onclick="return showupload();" /></div>
                        </div>                                      
                    </div>
                    <div id="vpb_added_files_box" class="vpb_file_upload_main_wrapper">
                        <div id="mailprocess"></div>
                        <div id="vpb_file_system_displayer_header"> 
                            <div id="vpb_header_file_names"><div style="width:365px; float:left;">File Names</div><div style="width:90px; float:left;">Status</div></div>
                            <div id="vpb_header_file_size">Size</div>
                            <div id="vpb_header_file_last_date_modified">Last Modified</div><br clear="all" />
                        </div>
                        <input type="hidden" id="ajax_url" value="vpb_blue" />
                        <input type="hidden" id="uploadon" value="1" />
                        <input type="hidden" id="added_class" value="vpb_blue">
                        <span id="vpb_removed_files"></span>
                    </div>
                    <div class="form-group row" style="margin-top:10px;">
                        <div class="controls col-lg-3 col-lg-push-2">
                            <input id="save" class="btn btn-info" type="submit" name="btn_send_mail" value="Send" onclick="return checkform(2);"/>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
</div>


<div class="row" id="id_div_items">

    <div class="col-lg-12" id="div_order_email">

        <div class="panel panel-default panel-block">
            <div class="panel-heading">
                <h4>Charges</h4>
            </div>
            <div class="list-group" id="div_order_notes">
                <div class="list-group-item" id="input-fields">


                    <div name="controls" id="div_order_items">

                        <?php
                        $this->table->set_heading('Item', 'Description', 'Price', '');
                        $count = ($item_count = count($order->items)) ? $item_count : 3;
                        for ($i = 0; $i <= $count; $i++) {
                                        //Use items if available
                            $description = '';
                            $price = '';
                            $selected = '';
                            if (isset($order->items[$i])) {
                                $description = $order->items[$i]->description;
                                $price = $order->items[$i]->price;
                                $selected = $order->items[$i]->item_id;
                            }
                            /* Need to review this code, something wrong with this */
                            $this->table->add_row(
                                form_dropdown('item_item_id_' . $i, $items, $selected), 
                                $this->order_helper->get_text_field('item_description_' . $i, 'item_description_' . $i, $description, ''), 
                                $this->order_helper->get_text_field('item_price_' . $i, 'item_price_' . $i, $price, ''), 
                                '<i class="icon-remove-sign"></i>'
                                );
                            /* Need to review this code, something wrong with this */
                        }
                        echo $this->table->generate();
                        ?>
                        <a id="add_item_row" class="btn"><i class="icon-plus"></i>Add rows</a>
                    </div>

                </div>
            </div>
        </div>

    </div>

</div>


<div class="row" id="">
   <?php if ($payment_gateway) { ?>             
   <div class="col-lg-7" id="div_id_charge">

    <div class="panel panel-default panel-block">
        <div class="panel-heading">
            <h4>Charges</h4>
        </div>
        <div class="list-group" id="div_order_notes">
            <div class="list-group-item" id="input-fields">

                <div class="table-responsive">
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                               <td>
                                <div class="form-group row">
                                    <div class="col-lg-4">
                                        <input id="charge_amount" class="form-control" type="text" value="<?php echo $order->amount_due ?>" name="amount">
                                    </div>
                                    <div class="col-lg-6">
                                        <a href="#" class="btn btn-info" id="btn_charge" style="margin-top:4px;background:#4dac39;border-color:#4dac39;">Charge this amount</a>        
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <textarea rows="6" cols="6" id="payment__n" name="payment__n" class="form-control" value="<?= $order->payment_n; ?>"></textarea>
                                        <input type="hidden" id="payment_n" name="payment_n" value="<?= $order->payment_n; ?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-2">
                                        <input type="hidden" class="form-control" id='notes_charge_date'  name='notes_charge_date' value="<?= date('m-d-Y'); ?>">
                                        <input type="submit" name="charge_amount_submit" class="btn btn-info" value="Save Note" onclick="checkform(1)">
                                    </div>
                                    <div class="col-lg-2">
                                        <input id="reset_charge_amount" name="reset_charge_amount" class="btn btn-info" value="Clear Note">
                                    </div>
                                </div>
                            
                        <?php if( ($order->notes_charge_date != '') && ($order->payment_n != '') ){ ?>
                            <?php $p_ayment_n = explode(' / ', $order->payment_n); ?>
                            <?php foreach ($p_ayment_n as $keys => $values) { ?>
                                
                                        <div class="form-group row c_p_n">
                                            <div class="col-lg-3" style="border:1px solid #ddd;">
                                                <p id="c_d"><?= $order->notes_charge_date; ?></p>    
                                            </div>
                                            <div class="col-lg-3 c_u_n" style="border:1px solid #ddd;margin-left:5px;">

                                                <p id="c_u_n"></p>
                                            </div>
                                            <div class="col-lg-4" style="border:1px solid #ddd;margin-left:5px;">
                                                <p id="c_n"><?= $values; ?></p>
                                            </div>
                                            <div class="col-lg-1">
                                                <button class="btn btn-info" id="del_btn_c" style="padding:4px;" onclick="delBtnC(this)">Delete</button>
                                            </div>
                                        </div>
                                
                        <?php } } ?>
                        </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
<?php } ?>
<div class="col-lg-5" id="div_id_charge">

    <div class="panel panel-default panel-block">
        <div class="panel-heading">
            <h4>Calculation</h4>
        </div>
        <div class="list-group" id="div_order_notes">
            <div class="list-group-item" id="input-fields">
                <div name="controls" id="div_id_calculation" class="<?php echo!$payment_gateway ? 'offset7 ' : '' ?>span5">

                    <?php
                    echo $this->order_helper_new->get_object_form($order, array('subtotal', 'tax', 'tax_price', 'shipping_amount', 'total', 'paid', 'amount_due'), null, array(
                        'tax_type' => array(
                            'prepend' => '%',
                            'element' => Order_helper::ELEMENT_DROPDOWN,
                            'options' => $taxes,
                            'selected' => $order->tax_type),
                        'subtotal' => array(
                            'class' => 'input-small',
                            'prepend' => '$'
                            ),
                        'tax' => array(
                            'class' => 'input-small',
                            'prepend' => '%',
                            ),
                        'tax_price' => array(
                            'class' => 'input-small',
                            'prepend' => '$',
                            ),
                        'shipping_amount' => array(
                            'class' => 'input-small',
                            'prepend' => '$',
                            ),
                        'total' => array(
                            'class' => 'input-small',
                            'prepend' => '$',
                            ),
                        'paid' => array(
                            'class' => 'input-small paid',
                            'prepend' => '$',
                            ),
                        'amount_due' => array(
                            'class' => 'input-small ',
                            'prepend' => '$',
                            ),
                        '__legend' => ''
                        ));
                        ?>

                    </div>

                    <script type="text/javascript">
                                    //this code is for add other paid
                                    el = '<div class="form-group row">\
                                    <label class="control-label col-lg-2" for="paid">Payment</label>\
                                    <div class="controls col-lg-6">\
                                        <select name="department" style="width:100%">\
                                            <option value="" selected="selected"> -Select- </option>\
                                            <option value="1">Cash</option>\
                                            <option value="21">Card</option>\
                                            <option value="21">Check</option>\
                                            <option value="21">Paypal</option>\
                                        </select>\
                                    </div>\
                                </div><span id="more_section"></span><div class="controls"><a href="javascript:void(0)" id="add_other_paid">Add other paid</a></div>';
                                $("#paid").closest('.form-group').after(el);
                                $("#add_other_paid").click(function(event) {
                                    $("#more_section").before('<div class="form-group">\
                                        <label class="control-label col-lg-2" for="paid">Paid</label>\
                                        <div class="controls col-lg-6">\
                                            <div class="input-prepend"><span class="add-on">$</span><input type="text" class="input-small paid" name="" value="0.00"></div>\
                                        </div>\
                                    </div>\
                                    <div class="form-group">\
                                        <label class="control-label col-lg-2" for="paid">Payment</label>\
                                        <div class="controls col-lg-6">\
                                            <select name="department" style="width:100%">\
                                                <option value="" selected="selected"> -Select- </option>\
                                                <option value="1">Cash</option>\
                                                <option value="21">Card</option>\
                                                <option value="21">Check</option>\
                                                <option value="21">Paypal</option>\
                                            </select>\
                                        </div>\
                                    </div>');
                                            //handle paid when it change
                                            $('.paid').unbind("change").change(function(){
                                                if( isNaN( $(this).attr('value'))){

                                                    $(this).attr('value','0.00' );    

                                                } else {

                                                    $('#amount_due').attr('value', Number($('#amount_due').attr('value')) - Number($(this).attr('value')) );

                                                    if($('#amount_due').attr('value').indexOf('.') == -1 ){

                                                        $('#amount_due').attr('value', $('#amount_due').attr('value') + '.00' );      
                                                        

                                                    }

                                                }   

                                            });
                                        });





</script>


</div>
</div>
</div>
</div>

</div>

<br/><br/>
<div class="row">
 <div class="col-lg-3"></div>
 <div class="col-lg-6 form-inline" id="div_order_submit">    

    <input id="save"   class="btn btn-info" type="submit" name="btn_save_order"   value="Save" onclick="checkform(1);"/>

    <input id="btn_print_order"  class="btn btn-info" type="submit" name="btn_print_order"  value="Print" onclick="checkform(3);"/>

    <input id="email_order"  class="btn btn-info" type="submit" name="btn_email_order"  value="Email" onclick="checkform(1);"/>

    <input id="cancel" class="btn btn-info" type="submit" name="btn_cancel_order" value="Cancel" onclick="checkform(1);"/>

    <?php if ($user->company_id == 11) { ?>
    <input id="report" class="btn btn-info" type="submit" name="btn_report_order" value="Report" onclick="checkform(1);"/>
    <?php } ?>
    <input id="delete" class="btn btn-info" type="submit" name="btn_delete_order" value="Delete" onclick="checkform(1);"/>      

    <input id="back" class="btn btn-info" type="submit" name="btn_back" value="Back" onClick="history.go(-1);
    return true;" />   

    <?php
    if ($order->id > 0) {
        ?><input id="btn_reorder" class="btn btn-info" type="submit" name="btn_reorder" value="Reorder" onClick="return reorder();" />   <?php
    }
    ?>

</div>
</div>


<div class="modal fade" id="alert_modal">
    <div class="modal-dialog modal-md">
      <div class="modal-content">

        <div class="modal-header">
            <a class="close" data-dismiss="modal">�</a>
            <h3 id="h_modal_header">Modal header</h3>
        </div>
        <div class="modal-body">
            <div class="alert alert-block alert-error">
                <p id="p_modal_text">One fine body�</p>
            </div>

        </div>
        <div class="modal-footer">
            <a data-dismiss="modal" href="#" class="btn btn-info">Close</a>
        </div>
    </div>
</div>
</div>
<div class="modal fade" id="delete_modal">
    <div class="modal-dialog modal-md">
      <div class="modal-content">

        <div class="modal-header">
            <a class="close" data-dismiss="modal">�</a>
            <h3 id="h_delete_header"></h3>
        </div>
        <div class="modal-body">
            <div class="alert alert-block alert-error">
                <p id="p_delete_text">One fine body�</p>
            </div>

        </div>
        <div class="modal-footer">
            <a id="delete_cancel" data-dismiss="modal" href="#" class="btn btn-info">Cancel</a>
            <a id="delete_yes"   href="<?php echo site_url('orders/delete/' . $order->id) ?>" class="btn btn-info">Yes</a>
        </div>
    </div>
</div>
</div>   
<input type="hidden" name="id" value="<?php echo $order->id ?>"/>


</form>


<div class="modal fade" id="charge_modal">
 <div class="modal-dialog modal-md">
  <div class="modal-content">

    <div class="modal-header">
        <a class="close" data-dismiss="modal">x</a>
        <h3 id="h_modal_header">Charge Credit Card</h3>
    </div>

    <div class="modal-body">
        <div id="div_order_charged">
            <a class="close" data-dismiss="alert">�</a>
            <span></span>
        </div>                
        <form class="form-horizontal" id="form_cc" action="<?php echo site_url('orders/charge') ?>" method="post">            

            <div name="controls">
                <div class="form-group row">
                    <label class="control-label col-lg-3" for="subtotal">Charge Amount:</label>
                    <div class="controls col-lg-4">                                         
                        <div class="input-prepend">

                            <input id="amount" class="input-small form-control" type="text" value="<?php echo $order->amount_due ?>" name="amount">
                            <span  name="span_help"     class="help-inline hidden"></span>
                        </div>
                    </div>                                  
                </div>
                <div class="form-group row">
                    <label class="control-label col-lg-3" for="subtotal">CC Number:</label>                         
                    <div class="controls col-lg-4">                           
                        <input id="cc_num" class="input_charge form-control" type="text" value="" name="cc_num">
                        <span  name="span_help"     class="help-inline hidden"></span>                                           
                    </div>
                </div>                                
                <div class="form-group row">
                    <label class="control-label col-lg-3" for="subtotal">Expire Date:</label>                          
                    <div class="controls col-lg-4">                           
                        <input id="cc_date" class="input_charge form-control" type="text" value="" name="cc_date">
                        <span  name="span_help"     class="help-inline hidden"></span>                                          
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-lg-3" for="subtotal">Security code:</label>                          
                    <div class="controls col-lg-4">                           
                        <input id="cc_cvn" class="input_charge form-control" type="text" value="" name="cc_cvn">
                        <span  data-placement="top" rel="tooltip"  data-original-title="help help help" name="span_help"   class="help-inline hidden">Whats this?</span>                                          
                    </div>
                </div>                                
                <div class="form-group row">
                    <label class="control-label col-lg-3" for="subtotal">Billing address:</label>                          
                    <div class="controls col-lg-4">                           
                        <select id="select_fill_in"  class="input_charge form-control">
                            <option value="1">No billing address</option>
                            <option value="2">Copy from Agent</option>
                            <option value="3">Copy from Client</option>
                            <option value="4">Copy from Meet With</option>
                            <option value="5">Manual fill in</option>
                        </select>
                        <span  data-placement="top" rel="tooltip"  data-original-title="help help help" name="span_help"   class="help-inline hidden">Whats this?</span>                                          
                    </div>
                </div>                                 
                <div class="form-group fill_in row">
                    <label class="control-label col-lg-3" for="subtotal">First Name</label>                          
                    <div class="controls col-lg-4">                           
                        <input id="cc_date" class="input_charge form-control" type="text" value="" name="first_name">
                        <span  data-placement="top" rel="tooltip"  data-original-title="help help help" name="span_help"   class="help-inline hidden">Whats this?</span>                                          
                    </div>
                </div>
                <div class="form-group fill_in row">
                    <label class="control-label col-lg-3" for="subtotal">Last Name</label>                          
                    <div class="controls col-lg-4">                           
                        <input id="cc_date" class="input_charge form-control" type="text" value="" name="last_name">
                        <span  data-placement="top" rel="tooltip"  data-original-title="help help help" name="span_help"   class="help-inline hidden">Whats this?</span>                                          
                    </div>
                </div> 
                <div class="form-group fill_in row">
                    <label class="control-label  col-lg-3" for="subtotal">Address</label>                          
                    <div class="controls col-lg-4">                           
                        <input id="cc_date" class="input_charge form-control" type="text" value="" name="address">
                        <span  data-placement="top" rel="tooltip"  data-original-title="help help help" name="span_help"   class="help-inline hidden">Whats this?</span>                                          
                    </div>
                </div> 
                <div class="form-group fill_in row">
                    <label class="control-label col-lg-3" for="subtotal">City</label>                          
                    <div class="controls col-lg-4">                           
                        <input id="cc_date" class="input_charge form-control" type="text" value="" name="city">
                        <span  data-placement="top" rel="tooltip"  data-original-title="help help help" name="span_help"   class="help-inline hidden">Whats this?</span>                                          
                    </div>
                </div>
                <div class="form-group fill_in row">
                    <label class="control-label col-lg-3" for="subtotal">Zip</label>                          
                    <div class="controls col-lg-4">                           
                        <input id="cc_date" class="input_charge form-control" type="text" value="" name="zip">
                        <span  data-placement="top" rel="tooltip"  data-original-title="help help help" name="span_help"   class="help-inline hidden">Whats this?</span>                                          
                    </div>
                </div>
                <div class="form-group fill_in row">
                    <label class="control-label col-lg-3" for="subtotal">State</label>                          
                    <div class="controls col-lg-4">                           
                        <?php echo form_dropdown('state', $us_states, '', 'class="input_charge form-control"') ?>
                        <span  data-placement="top" rel="tooltip"  data-original-title="help help help" name="span_help"   class="help-inline hidden">Whats this?</span>                                          
                    </div>
                </div>
                <div class="form-group fill_in row">
                    <label class="control-label col-lg-3" for="subtotal">Country</label>                          
                    <div class="controls col-lg-4">                           
                        <input id="cc_date" class="input_charge form-control" type="text" value="" name="country">
                        <span  data-placement="top" rel="tooltip"  data-original-title="help help help" name="span_help"   class="help-inline hidden">Whats this?</span>                                          
                    </div>
                </div>                                                                                                                                                                                                                                                                                                                             
            </div>

            <input type="hidden" name="order_id" value="<?php echo $order->id ?>"/>

        </form>
    </div>

    <div class="modal-footer">
        <a href="#" class="btn btn-info" id="btn_charge_modal">Charge</a>
        <a data-dismiss="modal" href="#" class="btn btn-info">Close</a>
    </div>
</div>
</div>
</div>
<script>
    <?php if($order->notes_by > 0) { 

        $this->db->select('name');
        $this->db->from('dr_persons');
        $this->db->where('id',$order->notes_by);
        $order_by = $this->db->get();
        $order_by = $order_by->row();
        $order_by = $order_by->name;
        ?>
        $('#div_order_notes div[name=controls]').append('<div class="form-group">'+
            '<label for="notes_by" class="control-label"></label>'+                        
            '<div class="controls">'+                          
            '<p></p>'+                                         
            '</div>'+
            '</div>');
        <?php }?>
        $("p[id^='n_u_n']").each(function() {
            $(this).text('<?= $order_by ?>');
        });
        $("p[id^='c_u_n']").each(function() {
            $(this).text('<?= $order_by ?>');
        });
        // $('#notes_date').attr('data-date-format','mm-dd-yyyy');
    </script>



</section>

<script type="text/javascript">

    $(document).ready(function(){
        $('fieldset .form-group, #print_order .form-group,#combine_fields .form-group,#shirt_order .form-group').addClass('row');
        $('fieldset .form-group label, #print_order .form-group label,#combine_fields .form-group label,#shirt_order .form-group label').addClass('col-lg-2');
        $('fieldset .form-group .controls, #print_order .form-group .controls,#combine_fields .form-group .controls,#shirt_order .form-group .controls').addClass('col-lg-6');
        $('.form-horizontal input.btn').addClass('btn-info');
        $('.form-horizontal input.btn').css('margin-right','10px');
        $('textarea').addClass('form-control');
        $('.panel-heading').css('padding','10px');
        $('.o_n .row:nth-child(1) div.controls').removeClass('col-lg-6');
        $('.o_n .row:nth-child(1) div.controls').addClass('col-lg-12');
        $('.o_n .row:nth-child(2) label.control-label').removeClass('col-lg-2');
        $('.o_n .row:nth-child(2) label.control-label').addClass('col-lg-1');
    });

</script>
<style type="text/css">

    .o_n .row:nth-child(2) div.controls .datepicker{
        top: 39px!important;
        left: 14px!important;
    }
    @media (min-width: 1200px){
        .col-lg-3.c_u_n{
            width: 27% !important;
        }
    }

</style>


<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="border:none;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <!-- <h4 class="modal-title">Modal Header</h4> -->
      </div>
      <div class="modal-body" id="load_view">
        
      </div>
      
    </div>

  </div>
</div>