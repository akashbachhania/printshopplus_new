<?php
$overrides = array(
    'report_due' => 'Due date',
    'sales_rep' => 'Sales Rep',
    'order_notes' => 'Description',
    'client_id' => 'Order #',
    'company_id' => 'Company'
);

$source = $this->uri->segment(4);
?>
<?php $this->load->view('include/header'); ?>
    <div class="row">
        <div class="col-lg-12">
            <div id="top" class="panel panel-default panel-block panel-title-block">
                <div id="div_second_members"  class="span12">    
                    <form id="form_total_statistics" method="post" action="">
                        <div class="span12">
                            <div id="div_statistics_total" class="table-responsive">
                                <table class="table" id="statistics_table">
                                    <tbody class="row">
                                        <tr class="col-lg-4">
                                            <td style="background:#fff;">
                                                <div class="form form-inline" id="div_form_total_visits">
                                                    <input name="set_date" type="submit" class="btn btn-info" value="Set"/>
                                                    <input name="start_date" class="input-small" type="text" id="start_date" data-datepicker="datepicker" value="<?php echo $start_date ?>" style="width:100px;"/>-<input  class="input-small" id="end_date" name="end_date" type="text" data-datepicker="datepicker" value="<?php echo $end_date ?>"  style="width:100px;"/>
                                                    <input name="check_feed" class="input-small" type="hidden" id="check_feed" value="<?php echo strtotime($end_date) < strtotime(gmdate('m-d-Y')) ? 'once' : 'true' ?>"/>
                                                </div> 
                                            </td>
                                        </tr>
                                        <tr class="col-lg-8">
                                            <td>
                                                <table id="show_statistics">
                                                    <tr class="row">
                                                        <td class="col-lg-4"><b>Total Jobs:</b> <?php echo $allJobs?></td>
                                                        <td class="col-lg-4" style="background-color: transparent!important;"><b>Active Jobs:</b> <?php echo $activeJobs?></td>
                                                        <td class="col-lg-4" style="width:295px;"><b>Completed jobs:</b> <?php echo $completedJobs;?></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>    
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>       
            
    <div class="row">
        <div class="col-md-12">
            
            <div class="panel panel-default panel-block table-responsive" id="div_orders_listing">
                <div id="data-table" class="panel-heading datatable-heading">
                    <h4 class="section-title">Orders</h4>
                </div>

                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th><img title="Delete" src="<?php echo base_url(); ?>application/views/assets/img/trash.png" name="deleteall" id="deleteall"></th>
                            <!-- <th>Edit</th> -->
                            <th>Order #</th>
                            <th>Order date</th>
                            <th>Company</th>
                            <th>Description</th>
                            <th>Due date</th>
                            <th>Sales Rep</th>
                            <th>Status</th>
                            <th>Operator</th>
                        </tr>
                    </thead>
                </table>
                <button class="btn btn-info" id="generate_bulk_waybill" style="margin-top: 0px; margin-left: 57%;">Waybill</button>
                <button class="btn btn-info" id="generate_bulk_print" style="margin-top: 0px; margin-left: 1%;">Print</button>
            </div>
        </div>
    </div>
        </section>

<script type="text/javascript">
    
    $(document).ready(function(){
        $('#DataTables_Table_0 > thead > tr > th').addClass('hiasdasdde');

        $(document).on('click','#generate_bulk_waybill',function(){

            var temp_ids = new Array();

            $("#div_orders_listing input[type='checkbox']:checked").each(function(i,v){
                temp_ids.push($(v).val());
            });

            $.post(base_url+'/orders/generate_bulk_waybill?id='+temp_ids.join(','),function(res){
                var response = jQuery.parseJSON(res);

                $(response).each(function(i,c){
                    window.open(base_url+'/'+c);
                });
            });
        });

        $(document).on('click','#generate_bulk_print',function(){

            var temp_ids = new Array();

            $("#div_orders_listing input[type='checkbox']:checked").each(function(i,v){
                temp_ids.push($(v).val());
            });

            $.post(base_url+'/orders/generate_bulk_print?id='+temp_ids.join(','),function(res){
                var response = jQuery.parseJSON(res);

                $(response).each(function(i,c){
                    window.open(base_url+'/'+c);
                });
            });
        });
    });

</script>