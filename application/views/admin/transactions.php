<?php
   
?>
<div id="div_view_transactions" class="row div_header">
    <div id="div_transactions_table" class="span12 well">
        <legend>Orders</legend>
        <?php echo $this->order_helper->generate_table( $transactions, array(
                    'id',
                    'amount',
                    'message',
                    'status',
                    //'order_date',
                    'datetime',
                    ), 
                  $this->table,
                  array(
                    'status' => array(
                        'values' => array(
                            Authorize_payment::STATUS_FAILED => 'Failed',
                            Authorize_payment::STATUS_SUCCESS=> 'Success',                          
                        )
                    )
                  )
              
              );
        ?>
    </div>                
</div>
