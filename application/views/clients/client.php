<?php
//echo "<pre>";print_r($orders);die; 
if( $saved !== null){
    if( $saved === true ){
        $saved = 'alert alert-success';
        $msg   = '<strong>Success: </strong>Your order has been saved';                    
    } else if ( $saved === false ) {
        $saved = 'alert alert-error';
        $msg   = '<strong>Error: </strong>Your order has not been saved (Please correct fields with red border)';            
    } else if ( $saved === 'update') {
        $saved = 'alert alert-info';
        $msg   = '<strong>Success: </strong>Your order has been updated successfully!';             
    }
} else {
    $saved = '';                         
    $msg  = '';
}
/**
* We'll override names
*/
$overrides = array(
    'status_string::name' => 'Status',
    'term::name' => 'Terms',
    'agent::name' => 'Agent',
    'inspector::name' => 'Sales Rep',
    'inspection::name' => 'Inspection',
    'client::phone_1' => 'Phone',
    'company_order_id' => 'ID'
);
 
if( $orders && is_object( $orders['0'] ) && isset( $orders['0']->client )){
    $client = $orders['0']->client;    
}
$fields = array('name','contact' ,'address','city','state','zip','phone_1','phone_2','email','email_2','email_3', 'active','notes', 'id','company_id','person_type');
// $fields = array('name','contact' ,'address','city','state','zip','phone_1','phone_2','email','email_2','email_3', 'active','id','company_id','person_type');
$options = array(
    1 => 'Active Client',
    0 => 'Disabled Client'
)  
?>


<?php $this->load->view('include/header'); ?>    
    
    

    <div class="row">
        <div class="col-md-5">
            <div class="panel panel-default panel-block" id="d_iv_clients_table">
                <div id="data-table" class="panel-heading datatable-heading">
                    <h4 class="section-title">Clients</h4>
                </div>
                <?php   echo $this->order_helper->get_clients_table( $clients, array(
                                                                                    'id',
                                                                                    'name',
                                                                                    'amount_due',
                                                                                    ), 
                                                                                    $this->table);
                ?>
                <a id="btn_new_person" href="<?php echo site_url('clients/add_new/Person')?>" target="__blank" class="btn btn-info" style="margin:10px 0 10px 10px">Add new</a>
            </div>
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-4">
                    <a id="a_excel"   data-placement="top" rel="tooltip"  data-original-title="Export contacts to Excel" href="<?php echo site_url('clients/export')?>" target="_blank"></a>
                </div>
                
            </div>
        </div>

        <div class="col-md-7">
            <?php if( $saved ){ ?>
                <div id="div_order_added" class="<?php echo $saved ?>">
                        <a class="close" data-dismiss="alert">x</a>
                        <?php echo $msg ?>
                </div>
            <?php } ?>                    
            <?php 
                if( $selected_client ){ ?>
                    <div class="panel panel-default panel-block">
                        <div class="panel-heading datatable-heading">
                            <h4 class="section-title">Client details: <?= $selected_client->name; ?></h4>
                        </div>
                        <div class="list-group">
                            <div class="list-group-item">

                        <?php echo $this->order_helper_new->get_object_form( $selected_client, $fields, $this->table, 
                                array(
                                    'notes' => array('element'=> Order_helper::ELEMENT_TEXTAREA),
                                    'active' => array( 
                                        'element'  => Order_helper::ELEMENT_DROPDOWN, 
                                        'options'  => $options,
                                        'selected' => $selected_client->active ),
                                    'state'=> array('element'=> Order_helper::ELEMENT_DROPDOWN,
                                        'options'=>  $states,
                                        'selected'=> $selected_client->state ),    
                                    
                                    '__form'   => array(
                                        'action' => site_url('clients'), 
                                        'submit_value' => 'Save',
                                        'submit_name'  => 'save_client'
                                        )
                                    )
                            ); ?>
                            </div>
                        </div>
                    </div>
               <?php }
            
                unset($order_fields['id']);
                $order_fields = array('company_order_id') + $order_fields;
                
            ?>
            <div class="panel panel-default panel-block">
                <div id="data-table" class="panel-heading datatable-heading">
                    <h4 class="section-title">Client Orders</h4>
                </div>

                <table class="table table-bordered table-striped" id="tble2">
                    <thead>
                        <th>#</th>
                        <th>Job name</th>
                        <th>Sales Rep</th>
                        <th>Address</th>
                        <th>City</th>
                        <th>Paid</th>
                        <th>Due</th>
                    </thead>
                    <tbody>
                        <?php
                        if($orders) {
                            
                            foreach ($orders as $order) {
                                $getJobDetail = $this->order_model->orderJobDetails($order->id);
                                $getShirtDetail = $this->order_model->orderShirtJobDetails($order->id);
                                if($getJobDetail) {
                                    foreach($getJobDetail as $job) {
                                        $link = '<a href="'.base_url().'orders/show_order/'.$order->id.'">%s</a>';
                                ?>
                                    <tr>
                                        <td><?php echo sprintf($link, $order->company_order_id);?></td>
                                        <td><?php echo sprintf($link, $job->job_name);?></td>
                                        <td><?php echo sprintf($link, (isset($order->inspector->name)) ? $order->inspector->name : '');?></td>
                                        <td><?php echo sprintf($link, $order->address);?></td>
                                        <td><?php echo sprintf($link, $order->city);?></td>
                                        <td><?php echo sprintf($link, $order->paid);?></td>
                                        <td><?php echo sprintf($link, $order->amount_due);?></td>
                                    </tr>
                                <?php
                                    }
                                }
                                
                                if($getShirtDetail) {
                                    foreach($getShirtDetail as $job) {
                                        $link = '<a href="'.base_url().'orders/show_order/'.$order->id.'">%s</a>';
                                ?>
                                    <tr>
                                        <td><?php echo sprintf($link, $order->company_order_id);?></td>
                                        <td><?php echo sprintf($link, $job->job_name);?></td>
                                        <td><?php echo sprintf($link, (isset($order->inspector->name)) ? $order->inspector->name : '');?></td>
                                        <td><?php echo sprintf($link, $order->address);?></td>
                                        <td><?php echo sprintf($link, $order->city);?></td>
                                        <td><?php echo sprintf($link, $order->paid);?></td>
                                        <td><?php echo sprintf($link, $order->amount_due);?></td>
                                    </tr>
                                <?php
                                    }
                                }
                            }
                        }
                        ?>
                        
                    </tbody>
                </table>


            </div>
        </div>
    </div>

</section>
<script type="text/javascript">
    $(document).ready(function(){
        $('.col-md-5 table').attr('id','tableSortable');
        $('.form-group label').removeAttr('style');
        $('.form-group label').before('<div class="col-lg-1"></div>');
        $('.form-group label').removeClass('col-lg-4');
        $('.form-group label').addClass('col-lg-2');
    });
</script>