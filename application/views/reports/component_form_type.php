<?php if( isset($type) && $type instanceof Type ) {?>
<div class="control-group">
        <div class="controls">
        <label class="checkbox">
        <input id="type_id_<?php echo ($type->type == Type::TYPE_GENERAL_REMARKS ? 'general_':'utility_').$type->id?>" type="checkbox" value="<?php echo $type->id?>" name="type_id_<?php echo $type->id?>">
        <?php echo $type->name?>
        <span class="help-block"><?php echo $type->description?></span>         
        </label>
    </div>
</div>
<?php } ?>
