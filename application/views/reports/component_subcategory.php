<div id="accordition_<?php echo $category_id?>_<?php echo $subcategory->id?>" class="accordion">
      <div class="accordion-heading accordion_heading_subcat">
            <input type="checkbox" checked="checked" id="subcat_<?php echo $subcategory->id?>">
            <a href="#subcategory_<?php echo $subcategory->id?>" data-parent="#accordion2" data-toggle="collapse" class="accordion-toggle"><?php echo $subcategory->_name?></a>                                                   
      </div>
      <div class="accordion-body in collapse" id="subcategory_<?php echo $subcategory->id?>">
             <div class="accordion-inner accordion_values">
                 <fieldset class="control_fieldset">
                     <!-- Values loop start-->                        
                     <?php foreach($subcategory->_values as $value) {?>
                        <label class="checkbox"><input type="checkbox" name="categories[<?php echo $category_id ?>][<?php echo $subcategory->id ?>][value_ids][<?php echo $value->id ?>]" checked="checked" id="value_<?php echo $value->id?>"><?php echo $value->name?></label>                                
                     <?php }?>
                     <!-- Values loop end--> 
                    <span class="add_description form-inline"><input type="text" value="" name="new_description"><a class="btn">Ok</a></span>
                    <a class="btn btn_add_desc">Add Description</a>                                                     
                 </fieldset>  
             </div>
      </div>
</div>
