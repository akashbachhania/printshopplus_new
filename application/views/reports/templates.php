<?php
if( $status!== null){
    if( $status=== Reports::STATUS_SAVED ){
        $class= 'alert alert-success';
        $msg   = '<strong>Success: </strong>Your template has been saved';                    
    } else if ( $status=== Reports::STATUS_ERROR) {
        $class= 'alert alert-error';
        $msg   = '<strong>Error: </strong>Your template has not been saved';            
    } else if ( $status=== Reports::STATUS_UPDATED) {
        $class= 'alert alert-info';
        $msg   = '<strong>Success: </strong>Your template has been updated successfully!';             
    } 
} else {
    $status= '';                         
    $msg  = '';
    $class = '';
}  
?>
<div class="row">
    <?php if( isset($status) && in_array($status, array(Reports::STATUS_SAVED,Reports::STATUS_ERROR,Reports::STATUS_UPDATED)) ){ ?>
     <div id="div_status" class="<?php echo $class ?>">
         <a class="close" data-dismiss="alert">�</a>
         <?php echo $msg; ?>
     </div>
    <?php } ?>
</div>
<div class="row">
    <!-- List of property types, creation of a new property -->
    <div class="properties span3">
        <!-- <legend class="status_legend">Property types:</legend> -->
        <form id="form_submit_property" action="<?php echo site_url('reports/templates')?>" method="get">
            <div id="properties">
                <div class="span3 bs-docs-sidebar">
                    <ul class="nav nav-tabs nav-stacked">
                    <?php foreach($properties as $index=>$property ){ ?>
                        <li class="property"><a  class="<?php echo $property->id==$selected_property_id?'selected':''?>" name="a_property_id" value="<?php echo $property->id ?>" id="property_<?php echo $property->id?>"><?php echo strlen($property->name)>14 ? substr($property->name,0,14).'...': $property->name?><i class="icon-chevron-right pull-right"></i></a></li>                
                    <?php } ?>
                    </ul>                  
                </div>
            </div>
            <input id="submit_property_id" type="hidden" name="property_id" value="<?php echo $selected_property_id ?>">
        </form>
        <div id="div_template_new_property">
            <a class="btn span2" id="btn_new_property">Add new</a>
        </div>
        <!-- Creates new property -->
        <div id="div_new_property" class="form-horizontal pull-left span3" style="display:none;">
            <fieldset class="control_fieldset">
                <legend class="status_legend">Add new property</legend>
                <label>Property name</label>
                <input class="input-medium" type="text" id="property_name">
                <label>Property descriprion</label>
                <input class="input-medium" type="text" id="property_description">           
                <span class="help-block">Description of the property</span>
                <img id="property_image" src=""/>
                <a class="btn input-mini" style="display:none;" id="btn_add_property_button_image">Add image</a>
                <button id="btn_add_property"  type="submit" class="btn">Add</button>
                <div id="div_add_new_property_image" style="display:none;">
                </div>
            </fieldset>         
        </div>        
    </div>      
        <!-- Select template for property, create new template-->        
        <div class="first_row_template span4">
            <fieldset class="control_fieldset">
                <legend class="status_legend">Select Template</legend>
                <form id="form_show_template" class="form-inline" action="<?php site_url('reports/templates')?>" method="post">
                    <?php echo form_dropdown('select_template', $templates, $selected_template ? $selected_template->id :'', 'class="input-medium" id="select_template" ')?>
                    <a class="btn input-small" id="btn_new_template">New template</a>
                    <input type="hidden"  name="action" value="<?php echo Reports::ACTION_SHOW?>" value="true">
                </form>
                 <div id="div_new_template" class="form-horizontal" style="display:none;">
                    <fieldset class="control_fieldset">
                        <legend class="status_legend">Add new Template</legend>
                        <label>Template name</label>
                        <input type="text" value="" class="input-small" id="template_name" name="new_template">
                        <label>Template description</label>
                        <input type="text" value="" class="input-small" id="template_description" name="new_template">                        
                        <a class="btn input-mini" id="btn_add_template">Add new</a>                                    
                    </fieldset>         
                </div>                                                                
            </fieldset>
        </div>
        <div class="first_row_template categories span5">
                <!--Category select box, for adding to the new template-->         
                <div>
                    <fieldset class="control_fieldset">
                        <legend class="status_legend">Category</legend>
                        <form class="form-inline">
                        <?php echo form_dropdown('select_category', $categories,'', ' id="select_category" class="input-medium" ')?>
                        <a id="btn_add_to_template" class="btn input-mini">Add to</a>
                        <a id="btn_add_new_cat_subcat" class="btn input-mini">New</a>
                        </form>
                        <div id="div_new_cat_subcat" style="display:none;">
                            <div class="category_content new_category">
                                <fieldset class="control_fieldset">
                                    <legend class="status_legend">New Category:</legend>   
                                    <div class="form-inline">                    
                                        <label>Category Name:</label><input type="text" id="new_category_name" value=""><a id="btn_add_category" class="btn">Add</a>                 
                                    </div>               
                                </fieldset>
                            </div>                        
                            <div class="subcategory_content new_subcategory">
                                <fieldset class="control_fieldset">
                                    <legend class="status_legend">New Subcategory:</legend>   
                                    <div class="form-inline">                    
                                        <label>Subacategory Name:</label><input type="text" name="excluded" value=""><a id="btn_add_subcategory_template" class="btn">Add</a>                 
                                    </div>               
                                </fieldset>
                            </div>                        
                        </div>                                     
                    </fieldset>
                </div>
        </div>
        <div class="row">
        <!-- Template display area, contain 2 leve accordition. 1st level is category,second subcategory-->
        <div class="span9"> 
          <!-- Template form -->        
          <form id="form_submit_template" action="<?php echo site_url('reports/templates') ?>" method="post">
          
              <fieldset class="control_fieldset <?php echo !$selected_template ? 'hidden':'' ?>">
                  <legend class="status_legend"><?php echo $selected_template ? $selected_template->name: ''?></legend>
                  <input type="hidden" id="selected_property_id" name="property_id" value="<?php echo $selected_property_id ?>">
                  <input type="hidden" id="current_template_id" name="template_id" value="<?php echo $selected_template ? $selected_template->id :'' ?>">
              
                  <div id="accordition" class="accordion">
                    <!-- Load template component -->
                    <?php $this->load->view('reports/component_template', array('selected_template'=>$selected_template)) ?>
                  </div>
                  <button id="btn_delete_template" name="action" value="<?php echo Reports::ACTION_DELETE ?>" class="btn">Delete template</button>          
                  <a id="btn_undo" name="action" class="btn">Undo removal</a>          
              </fieldset>
          </form>             
        </div>
      </div>        
             
                 
    
    
    </div>
    <div class="row">

    </div>
</div>