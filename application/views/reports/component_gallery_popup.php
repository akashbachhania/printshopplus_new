<div class="modal hide fade image_modal" id="image_modal_<?php echo $subcategory_id?>" style="max-height:auto;">
    <div class="modal-header">
        <h3 id="h_modal_header"><?php echo $name ?></h3>
    </div>
    <div class="modal-body">                           
        <div class="galleria_container" id="div_galleria_<?php echo $subcategory_id?>">   
        <?php if($pictures) {?>
            <?php foreach($pictures as $src ){?>
                <a href="<?php echo $url.$src ?>"><img src="<?php echo  $url.'thumb_'.$src ?>"></a>
            <?php } ?>
        <?php }?>
        </div>
        <div id="image_upload_container_<?php echo $subcategory_id?>" style="display:none;">
            <div id="image_upload_modal_body_<?php echo $subcategory_id?>"> 
            </div>
            <a class="btn clear_upload" id="images_button_<?php echo $subcategory_id?>">Go back</a>
        </div>
        <input id="image_subcategory_<?php echo $subcategory_id?>" type="hidden" value="">
        <div style="display:none;">
            <img id="img_edit">
        </div>
    </div>
    <div class="modal-footer">
        <a class="btn add_image" >Add Image</a>
        <a class="btn edit_image" >Edit Image</a>
        <a class="btn delete_image" >Delete</a>
        <a data-dismiss="modal" href="#" class="btn">Close</a>
    </div>
</div>
<div class="modal hide fade image_upload_modal" id="image_upload_modal_<?php echo $subcategory_id?>" style="max-height:auto;">
    <div class="modal-header">
        <h3 id="h_modal_header">File Progress</h3>
    </div>
    <div class="modal-body" id="ximage_upload_modal_body_<?php echo $subcategory_id?>">                           
    </div>
    <div class="modal-footer">
        <a id="dismiss_image_modal_<?php echo $subcategory_id?>" data-dismiss="modal" href="#" class="btn">Close</a>
    </div>
</div>
