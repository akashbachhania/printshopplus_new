<?php
$cells = array();
$rows  = '';
foreach( $properties as $property ){
    if( count($rows)<3){
        $cells[] =
        '<div  class="span4 property_type">
            <a>'.($property->image_filename ? '<img class="img-polaroid" src="'.$path.$property->image_filename.'">' : ''). '</a>      
            <div id="div_'.$property->id.'" class="dropdown">
                <a class=" span2 dropdown-toggle" id="dLabel" role="button" data-toggle="dropdown" data-target="#">'.$property->name.'</a>
                '.get_dropdown( isset($templates[$property->id])? $templates[$property->id]:array(),$property->id).'                 
            </div> 
        </div>';
    } else {
        $rows .= '<div class="row">'.
                    implode('', $cells);
                 '</div>';
        $cells = array();
    }
}
if( $cells ){
    $rows .= '<div class="row">'.
                implode('', $cells);
             '</div>';    
}
function get_dropdown( $templates, $property_id ){
     
    $menu = '<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">';
    $menu .= '<li><a tabindex="-1" href="'.site_url('reports/templates?property_id='.$property_id).'">New Template</a></li>';
    $menu .= '<li class="divider"></li>';
    $menu .= '<li><a id="menu_delete_property_'.$property_id.'" href="#">Delete property</a></li>';
    $menu .= '<li class="divider"></li>';
    foreach( $templates as $template ){
        $menu .= '<li><a tabindex="-1" href="'.site_url('reports/new_report?property_id='.$template->property_id.'&template_id='.$template->id.'&action='.Reports::ACTION_SHOW).'">'.$template->name.'</a></li>';
    }
    $menu .= '</ul>';
    return $menu;
}
?>
<form id="form_order" action="<?php echo site_url('reports/new_report')?>" method="post">
    <div id="div_new_report_first" class="row">
        <?php if( isset($saved) ){ ?>
            <div id="div_order_added" class="<?php echo $saved ?>">
                    <a class="close" data-dismiss="alert">×</a>
                    <?php echo $msg ?>
            </div>
        <?php } ?>                 
        <div id="div_report_search" class="span12">          
            <!-- Creates new property -->
            <div id="div_new_property" class="form-inline" style="display:none;">
                <fieldset class="control_fieldset">
                    <legend class="status_legend">Add new property</legend>
                    <label>Property name:</label>
                    <input class="span2" type="text" id="property_name" placeholder="Type something…">
                    <label>Property descriprion:</label>
                    <input class="span2" type="text" id="property_description" placeholder="Type something…">           
                    <span class="help-block">Example block-level help text here.</span>
                    <button id="btn_add_property" type="submit" class="btn">Add</button>            
                </fieldset>         
            </div>                       
        </div>
    </div>
    <div>
       <!-- Property types -->
       <?php echo $rows ?>    
    </div>
</form>