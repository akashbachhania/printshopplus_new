<?php
    if( isset( $fluid_layout ) && $fluid_layout == true ){
        $class = 'row-fluid';
    } else {
        $class = 'row-fluid';
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Print Shop Plus</title>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'application/views/assets/style/bootstrap.css' ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'application/views/assets/style/reports.css' ?>"/>
        <?php if( isset( $responsive_css_enabled )) {?>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'application/views/assets/style/bootstrap-responsive.css' ?>"/>
        <?php }?>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'application/views/assets/style/docs.css' ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'application/views/assets/style/prettify.css' ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'application/views/assets/style/datepicker.css' ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'application/views/assets/js/uploader/fileuploader.css' ?>"/>
        
         <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/dark-hive/jquery-ui.css" id="theme"/>
        <!-- blueimp Gallery styles -->
		<link rel="stylesheet" href="http://blueimp.github.io/Gallery/css/blueimp-gallery.min.css"/>
        <!--  <link rel="stylesheet"  href="<?php echo base_url() . 'application/views/assets/style/demo.css' ?>" />-->
		<link rel="stylesheet" href="<?php echo base_url() . 'application/views/assets/style/jquery.fileupload.css' ?>" />
		<link rel="stylesheet"  href="<?php echo base_url() . 'application/views/assets/style/jquery.fileupload-ui.css' ?>" />
		<link rel="stylesheet"  href="<?php echo base_url() . 'application/views/assets/style/vpb_uploader.css' ?>" />
		<link rel="stylesheet"  href="<?php echo base_url() . 'application/views/assets/style/jquery.timepicker.css' ?>" />
		<link rel="stylesheet"  href="<?php echo base_url() . 'application/views/assets/style/jquery.fancybox.css?v=2.1.5' ?>" />
        
        <script src="<?php echo base_url() . 'application/views/assets/js/joblist.js' ?>" type="text/javascript"></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/jquery.js' ?>" type="text/javascript"></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/bootstrap-datepicker.js' ?>" type="text/javascript"></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/jquery.dataTables.js' ?>" type="text/javascript"></script>        
        <script src="<?php echo base_url() . 'application/views/assets/js/custom.js' ?>" type="text/javascript"></script>

        <script src="<?php echo base_url() . 'application/views/assets/js/quotes.js' ?>" type="text/javascript"></script>

        <script src="<?php echo base_url() . 'application/views/assets/js/bootstrap-tooltip.js' ?>" type="text/javascript"></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/bootstrap-popover.js' ?>" type="text/javascript"></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/bootstrap-modal.js' ?>" type="text/javascript"></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/bootstrap-collapse.js' ?>" type="text/javascript"></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/bootstrap.js' ?>" type="text/javascript"></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/estilos.js' ?>" type="text/javascript"></script>        
        <script src="<?php echo base_url() . 'application/views/assets/js/popup.js' ?>" type="text/javascript"></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/jquery.formatCurrency.all.js' ?>" type="text/javascript"></script>
		 <script src="<?php echo base_url() . 'application/views/assets/js/vpb_uploader.js' ?>" type="text/javascript"></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/uploader/fileuploader.js' ?>" type="text/javascript"></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/galleria/galleria-1.2.8.js' ?>" type="text/javascript"></script>
		<script src="<?php echo base_url() . 'application/views/assets/js/jquery.validate.js' ?>" type="text/javascript"></script>
		<script src="<?php echo base_url() . 'application/views/assets/js/jquery.fancybox.js?v=2.1.5' ?>" type="text/javascript"></script>
		<script src="<?php echo base_url() . 'application/views/assets/js/jquery.timepicker.js' ?>" type="text/javascript"></script>
        <script src="http://feather.aviary.com/js/feather.js" type="text/javascript"></script>
        
       
        
    </head>
    <body>
    <?php include_once("analyticstracking.php") ?>
        <?php if( !isset( $dont_display_header )) {?>
            <div id="div_header_top" class="navbar">
                <div class="navbar-inner">
                    <div id="div_header_container" class="container">                        
                        <div class="nav-collapse pull-left">
                            <ul class="nav">
                                <li><a class="" href="#"><?php echo $user->name ?></a></li>
                                <li class="divider-vertical"></li>
                                <li><a class="" href="<?php echo site_url('home/settings')?>" target="__blank">My Settings</a></li>
                                <li class="divider-vertical"></li>
                                <li><a class="" href="<?php echo site_url('login')?>">Sign Out</a></li>
                                <li class="divider-vertical"></li>
                                <li><a class="" href="<?php echo site_url('orders/new_order')?>">Support</a></li>                        
                            </ul>
                            <?php if( in_array( $user->person_type, array(Person::TYPE_ADMIN))){ ?>
                                <form class="navbar-form pull-left">
                                    <label id="label_admin_company" class="checkbox"></label> 
                                    <?php echo form_dropdown('admin_company_id', $companies, $user->company_id ? $user->company_id : '0', ' id="select_admin_view_companies" class="span2"'  ); ?>
                                </form>
                            <?php }?>                                  
                        </div>
                        <div id="div_logo"></div>
                    </div>
                 </div>
            </div>
            <div id="div_header_navbar" class="navbar">
                <div class="navbar-inner">
                    <div id="div_header_container_lower">
                        <div id="inner" class="nav-collapse pull-left"> 
                            <ul id="ul_header_submenu" class="nav">
                                <li><a id="a_home" href="<?php echo site_url('orders/view_orders/all/home')?>">Home</a></li>
                                
                                <?php if( in_array($user->person_type, array(Person::TYPE_ADMIN, Person::TYPE_USER )) ) {?>
                                    <li><a id="a_orders"  href="<?php echo site_url('orders/new_order')?>"> Orders</a></li>
                                    <li><a id="a_quotes"  href="<?php echo site_url('quotes/new_order')?>"> Quotes</a></li>
                                    <li><a id="a_clients" href="<?php echo site_url('clients')?>"> Clients</a></li>
                                    <?php if( $user->company_id == 11 ){ ?>                                                                            
                                    <li><a id="a_reports"   href="<?php echo site_url('reports')?>">Reports</a></li>
                                    <?php } ?>
                                    <li><a id="a_stats"   href="<?php echo site_url('stats')?>"> Stats</a></li>                                    
                                    <li><a id="a_inspectors"  href="<?php echo site_url('inspectors')?>"> Sales Rep</a></li>                                                         
                                <?php } ?>
                                
                                <?php if( in_array($user->person_type, array(Person::TYPE_ADMIN, Person::TYPE_USER, Person::TYPE_INSPECTOR )) ) {?>
                                    <li><a id="a_schedule"  href="<?php echo site_url('schedule')?>"> Schedule</a></li>
                                <?php } ?>
                                <?php if( in_array($user->person_type, array(Person::TYPE_ADMIN)) ) {?>                                    
                                    <li><a id="a_admin" href="<?php echo site_url('admin')?>">Admin</a></li>
                                    <li><a id="a_admin" href="<?php echo site_url('admin/transactions')?>">Transactions</a></li>                                                         
                                <?php } ?>                
                            </ul>
                        </div>
                    </div>
                </div>    
            </div>
        <?php } 
                
                $checkShow = explode('/', $_SERVER['REQUEST_URI']); 
                $showBar   = (in_array('orders', $checkShow)) ? true : false;
		if($this->uri->segment(2) !='add_new' and $showBar) {
        ?>
        
        
        <table width="100%" class="headersubnav">
			<tr>
				<td>			
					<div id="order_submenu" class="subnav">						
		                <div id="div_home_messages">
		                	<ul class="nav nav-tabs">   
		                		<?php
		                			
		                			$source = $this->uri->segment(4); 
									if($source == "") {
										$mycontroller = $this->uri->segment(2); 
										if($mycontroller =='new_order') {
										 	$source = 'orders';
										} else {
											$source = 'home';
										}
									
									}
									if($source == 'orders') { ?>
										<li><a class='abtn-primary' href="<?php echo site_url('orders/new_order/')?>"><i class=""></i> New Orders</a></li>
									<?php }
									echo "<li><a class='abtn-primary' href=". site_url('orders/view_orders/all/'.$source)."><i class=''></i> View All</a></li>";
			                		foreach($headermenuitems as $key => $value ){
			                			
			                			echo "<li><a  class='abtn-primary' href=".site_url("orders/view_orders/".$key."/".$source).">".$value."</a></li>";                			
									}
		   						?>         	
							</ul>
						</div>  		
					</div>		
		        </td>
			</tr>		
		</table>   
      
        
        <?php	
        }
        
        $checkShow = explode('/', $_SERVER['REQUEST_URI']); 
        $showBar   = (in_array('quotes', $checkShow)) ? true : false;
        if($this->uri->segment(2) !='add_new' and $showBar) {
            ?>
                
            <table width="100%" class="headersubnav">
			<tr>
				<td>			
					<div id="order_submenu" class="subnav">						
		                <div id="div_home_messages">
		                	<ul class="nav nav-tabs">   
		                		<?php
		                			
		                			$source = $this->uri->segment(4); 
									if($source == "") {
										$mycontroller = $this->uri->segment(2); 
										if($mycontroller =='new_order') {
										 	$source = 'quotes';
										} else {
											$source = 'home';
										}
									
									}
									if($source == 'quotes') { ?>
										<li><a class='abtn-primary' href="<?php echo site_url('quotes/new_order/')?>"><i class=""></i> New Quote</a></li>
									<?php }
									echo "<li><a class='abtn-primary' href=". site_url('quotes/view_orders/all/'.$source)."><i class=''></i> View All</a></li>";
			                		
		   						?>         	
							</ul>
						</div>  		
					</div>		
		        </td>
			</tr>		
		</table>      
                
            <?php
        }
        
        
        if( isset($reports )){
        ?>
        <div id="report_submenu" class="subnav">
            <ul class="nav nav-tabs"> 
                    <li><a class='abtn-primary' href="<?php echo site_url('reports')?>"><i class=""></i>New report</a></li>
                    <li><a class='abtn-primary' href="<?php echo site_url('reports/view_reports/'.Order::ORDER_STATUS_REPORT_STARTED)?>"><i class=""></i>View in progress</a></li>
                    <li><a class='abtn-primary' href="<?php echo site_url('reports/view_reports/all')?>"><i class=""></i>View all</a></li>
                    <li><a class='abtn-primary' href="<?php echo site_url('reports/templates')?>"><i class=""></i>Template settings</a></li>
            </ul>
        </div>
        <?php } ?>    
        <div style="width: 85%; margin: auto;min-height: 100%;">
        <div class="<?php echo $class ?> clearfix">
            <section id="gridSystem">