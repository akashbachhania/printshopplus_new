<?php
if(!isset( $object )){
    die('Access denied');
}
                       
if( isset( $saved ) && $saved !== null){
    if( $saved === true ){
        $saved = 'alert alert-success';
        $msg   = '<strong>Success</strong>';                    
    } else if ( $saved === false ) {
        $saved = 'alert alert-error';
        $msg   = '<strong>Error: </strong>Your query has not been saved (Please correct fields with red border)'; 
        
        if(isset($errors) and is_array($errors) and count($errors) > 0) {
            $msg = '<strong>Error: </strong><br />';
            foreach($errors as $key => $error) {
                $msg .= ucfirst($key).' '.$error.'<br />';
            }
        }
        
    } else if ( $saved === 'update') {
        $saved = 'alert alert-info';
        $msg   = '<strong>Success: </strong>Your query has been updated successfully!';             
    }
} else {
    $saved = '';                         
    $msg  = '';
}

$name = '';
$fields = array();
//echo get_class( $object );
switch( get_class( $object ) ){
    case 'Base_item':
        $name = 'Charge';
        $fields = array(
                       'name','description','price','active','id','company_id'
                    );         
    break;
    case 'Company':
        $name = 'New Company';
        $fields = array('name','address','city','state','zip','phone_1','phone_2','email','payment_gateway','merchant','transaction','active','time_zone','id');
        
    break;
    case 'Person':
        if( $object->person_type == Person::TYPE_CLIENT ){
            $name = 'Client';
            $fields = array(
               'name','address','city','state','zip','phone_1','phone_2','email','active','id','company_id','person_type','how_did_you_hear_about_us'
            );                    
        } else if ( $object->person_type == Person::TYPE_AGENT ){
            $name = 'Company';
            $fields = array(
               'name', 'contact', 'address','city','state','zip','phone_1','phone_2','email','email_2','email_3','active','notes','id', 'company_id','person_type'
            );            
        } else if( $object->person_type == Person::TYPE_USER ){
            $name = 'User';
            $fields = array(
               'name', 'address','city','state','zip','phone_1','phone_2','email','password','active','id','company_id', 'person_type'
            ); 
        } else if( $object->person_type == Person::TYPE_COMPANY ){
            $name = 'Company';
            $fields = array(
               'name', 'address','address_2','state','city','zip','phone_1','phone_2','email','referred_by','active','id','company_id', 'person_type'
            ); 
        }
        else {
            $name = 'Sales Rep';
            $fields = array(
               'name','address','city','state','zip','phone_1','phone_2','email','active','id','company_id','person_type'
            );            
        }
        
    break;
    case 'Type':
        $name = 'Type';
        $fields = array(
                       'name','active',
                    );         
    break;
    case 'SalesRep':
        $name = 'Sales Represative';
        $fields = array(
                       'name','active','id'
                    );         
    break;
    case 'Coating':
        $name = 'Coating';
        $fields = array(
                       'name','active','id','company_id'
                    );         
    break;
    case 'JobStatus':
        $name = 'Department';
        $fields = array(
                       'name','active','id'
                    );       
    break;

    case 'Stock':
        $name = 'Stock';
        $fields = array(
                       'name','active','id'
                    );
    break;
    case 'Colors':
        $name = 'Color';
        $fields = array(
                       'name','active','id'
                    );
    break;
    case 'Size':
        $name = 'Size';
        $fields = array(
                       'name','active','id'
                    );
    break;
    case 'Quantity':
        $name = 'Quantity';
        $fields = array(
                       'name','active','id'
                    );
    break;
    case 'Coatings':
        $name = 'Coating';
        $fields = array(
                       'name','active','id'
                    );
    break;
    case 'Finishing':
        $name = 'Finishing';
        $fields = array('name','active','id');
    break;
    case 'ShirtType':
        $name = 'Shirt Type';
        $fields = array('name','active','id');
    break;
    case 'ShirtFrontColor':
        $name = 'Shirt Front Color';
        $fields = array('name','active','id');
    break;
    case 'ShirtBackColor':
        $name = 'Shirt Back Color';
        $fields = array('name','active','id');
    break;
    case 'ShirtSize':
        $name = 'Shirt Size';
        $fields = array('name','active','id');   
    break;
    break;
    case 'ShippingMethod':
        $name = 'Shipping Method';
        $fields = array('name','charges','active', 'id');         
    break;
    case 'Terms':
        $name = 'Terms';
        $fields = array(
                       'name','active', 'id'
                    );         
    break;
    case 'OperatorList':
        $name = 'OperatorList';
        $fields = array(
                       'name','active', 'id'
                    );         
    break;  
    
    
}
$options = array(
    1 => 'Active',
    0 => 'inactive'
)
?>
 <?php $this->load->view('include/header'); ?>                              
    <div id="div_row_add_new" class="row">
            <div id="div_add_new" class="col-lg-12 span12">
            
                <?php if( $saved ){ ?>
                    <div id="div_order_added" class="<?php echo $saved ?>">
                            
                            <?php echo $msg ?>
                    </div>
                <?php } ?>             
         
                
                
                <?php if( isset( $objects )){ 
                        
                        $array = array();
                        $array['0'] = '-Select item-';   
                        foreach( $objects as $temp_object ){
                            $array[$temp_object->id] = $temp_object->name;
                        }
                     
                        $dropdown = form_dropdown('select_edit',$array, isset($selected_item)? $selected_item :'0');
                ?>
                        <div id="div_add_new_dropdown" class="form-horizontal well">
                            <fieldset>
                                <legend>Item to edit</legend>
                                <div name="controls">
                                    <div class="form-group row">
                                        <label class="control-label col-lg-3" for="type">Item</label>
                                        <div class="controls col-lg-5">
                                            <?php echo $dropdown; ?>
                                         </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                         
                <?php } ?>
                
                <?php  
                    $hidden = array(
                            'object_type' => get_class( $object ),                            
                    );
                    
                    $specials = array(
                                  'active' => array( 
                                        'element'  => Order_helper::ELEMENT_DROPDOWN, 
                                        'options'  => $options,
                                        'selected' => $object->active ),                     
                                    '__legend' => ''. $name,
                                    '__form'   => array(
                                        'action' => $action, 
                                        'submit_value' => 'Save',                                        
                                        'submit_name'  => 'save',
                                        'close_btn'   =>  'close',
                                        'hidden' => $hidden
                                        )
                    );
                                           
                    if( get_class( $object) == 'Person' ){
                        $hidden['person_type'] = $object->person_type;
                        $specials = $specials + array (
                                                    'state'=>
                                                    array('element'=> Order_helper::ELEMENT_DROPDOWN,
                                                          'options'=> $states,
                                                          'selected'=>$object->state ));
                        $specials = $specials + array (
                                                    'notes'=>
                                                    array('element'=> Order_helper::ELEMENT_TEXTAREA));
                        

                         
                    } else if( get_class( $object) == 'Company'){
                        $specials = $specials + array (
                                                'state'=>
                                                    array('element'=> Order_helper::ELEMENT_DROPDOWN,
                                                          'options'=> $states,
                                                          'selected'=>$object->state ),
                                                'time_zone'=>
                                                    array(
                                                          'element'=> Order_helper::ELEMENT_DROPDOWN,
                                                          'options'=> array('-07:00' => 'Pacific', '-05:00' => 'Central','-04:00' => 'Eastern'),
                                                          'selected'=> $object->time_zone                                                    
                                                    ),
                                                'payment_gateway'=>
                                                    array(
                                                          'element'=> Order_helper::ELEMENT_DROPDOWN,
                                                          'options'=> array('1' => 'Uses Authorize.net', '0' => 'No payment gateway'),
                                                          'selected'=>$object->payment_gateway                                                    
                                                    )                    
                       );
                    }
                    echo $this->order_helper->get_object_form( $object, $fields, $this->table, $specials );
                ?>
                <?php if( isset($select_name)){ ?>
                <input type="hidden" value="<?php echo $select_name ?>" id="select_refresh"/>
                <?php }?>
            </div>
     </div>
   </section>              

<script type="text/javascript">
    
    $(document).ready(function(){
        $('fieldset .form-group').addClass('row');
        $('fieldset .form-group label').addClass('col-lg-2');
        $('fieldset .form-group .controls').addClass('col-lg-6');
        $('.form-horizontal input.btn').addClass('btn-info');
        $('.form-horizontal input.btn').css('margin-right','10px');
        $('#c_btn').css('display','none');
    });

</script>
<script>
    $('input[name="btn_cancel"], button#c_btn').click(function(event) {
        event.preventDefault();
       $('#myModal').modal('hide');
    });
    $('select[name="select_edit"]').attr('onchange','r_fresh()');
</script>