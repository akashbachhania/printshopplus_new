<?php
$input_array = $user->person_type == Person::TYPE_INSPECTOR ? array('report_due') : array('report_due'); //, 'inspector_id'

$headers = array();

foreach( $dates as $index=>$date ){
    $headers[] = '<div>'.$this->schedule_model->get_us_date_format($date).'</div><div class="schedule_day">'.$dows[$index].'</div>'; 
}

$this->table->set_heading( $headers );

$grid = array();

foreach($dates as $date) {
    $table = '';
    
    if(count($orders) > 0) {
        $table = '<table class="innder_table_schedual">';
        foreach( $orders as $order ){
        
            //Dont display pending orders
            
            if( $order->status == Order::ORDER_STATUS_PENDING ) {
                continue;
            }
            $dateCheck = date('m-d-Y',  strtotime($date));
            
            if( $order->report_due == $dateCheck  ){ //&& ( $inspection_time <= $standard_hour && $standard_hour <= $end_time)  
                $jobs = new Ordersjob(null,$order->id);                 
                $jobs = $this->order_model->get_object( $jobs);
                if($jobs) {
                    foreach ($jobs as $job) {
                        $table .= '<tr><td><a '.get_popover_data( $order , $job ).'href="'.site_url('orders/show_order/'. $order->id).'" >'.$job->job_name.'</a></td></tr>';  
                    }
                }
                
                $jobs = new Ordersshirt(null,$order->id);                 
                $jobs = $this->order_model->get_object( $jobs);
                if($jobs) {
                    foreach ($jobs as $job) {
                        $table .= '<tr><td><a '.get_popover_data( $order , $job ).'href="'.site_url('orders/show_order/'. $order->id).'" >'.$job->job_name.'</a></td></tr>';  
                    }
                }
                
                
            }   
        }
        $table .='</table>';
    }
        
        //$grid[] = '<b>'.$this->schedule_model->get_us_date_format($date).'<br/>'.date('l',  strtotime($date)).'</b>';
        $grid[] = $table;
        
}

$this->table->add_row($grid);
 
/*
foreach( $hours as $standard_hour=>$hour ){
    foreach( $dates as $date ){
        $hour_date = array();
    
        foreach( $orders as $order ){
        
            //Dont display pending orders
            
            if( $order->status == Order::ORDER_STATUS_PENDING ) {
                continue;
            }
            $dateCheck = date('m-d-Y',  strtotime($date));
            //$end_time   = get_end_time( $order );
            //$inspection_time = substr($order->inspection_time,0,5);
            //$standard_hour   = (substr( $standard_hour, 1,1)==':') ? '0' . $standard_hour : $standard_hour;
            
            if( $order->report_due == $dateCheck  ){ //&& ( $inspection_time <= $standard_hour && $standard_hour <= $end_time) 
                
                $hour_date[] = '<a '.get_popover_data( $order ).'href="'.site_url('orders/show_order/'. $order->id).'" >'.$order->meet_with .'</a>';        
            }   
        }
        
        foreach( $inspectors as $inspector ){
            if( $input_order->inspector_id) {
                if( $input_order->inspector_id == $inspector->id ){
                    if( is_inside_timeoff( $inspector, $date, $standard_hour )){
                        $hour_date[] = '<a class="schedule_timeoff" href="#">Timeoff:'.$inspector->name.'</a>';    
                    }
                }               
            } else {
                if( is_inside_timeoff( $inspector, $date, $standard_hour )){
                    $hour_date[] = '<a class="schedule_timeoff" href="#">Timeoff:'.$inspector->name.'</a>';    
                }                
            }

        }
        
        $grid[$hour][$date] = $hour_date ? implode(' , ', $hour_date) : '';    
        
    }
    $this->table->add_row($grid[ $hour ]);//$this->table->add_row( array_merge( array($hour),$grid[ $hour ]));
}
 * 
 */

 


function get_popover_data( Order $order, Ordersjob $job ){
    
    $data  = '';
    $data .= "<strong>Company Name: </strong>".$order->client->name.'<br/>';
    $data .= "<strong>Job Name: </strong>".$job->job_name."<br/>";
    $data .= "<strong>Sales Rep: </strong>".$order->inspector->name.'<br/>';    
    if( $order->notes_order ){
        $data .="<strong>Notes: </strong> $order->notes_order";
    }
    if( isset($job->additional_notes) ){
        $data .="<strong>Additional Notes: </strong> $job->additional_notes";
    }
    return 'data-content="'.$data.'" rel="popover" data-trigger="hover" data-original-title="Inspection summary"';
}
function get_am_pm( $hour ){
    $return = $hour;
    if( preg_match('/(?P<hour>\d+):(?P<minutes>\d\d)/', $hour, $match )){
        if( intval( $match['hour']) > 12 ){
            $suffix = 'PM';
            $hour   = intval( $match['hour']) - 12;
        } else {
            $suffix = 'AM';
            $hour  = intval($match['hour']);
        }
        $return = $hour . ':' .$match['minutes']. ' '.$suffix;
    }
    return $return; 
}
function get_end_time( Order $order ){
    $hour   = intval(substr( $order->inspection_time, 0, 2));
    $minute = intval(substr( $order->inspection_time, 3, 2));
    return ( date( 'H:i', (mktime($hour, $minute)+ $order->estimated_inspection_time * 60*60) ));    
}
function is_inside_timeoff( Person $inspector, $date, $hour ){
    $inside = false;
    
    $start = date_parse($inspector->timeoff_date_start.''.$inspector->timeoff_time_start);
    $end   = date_parse($inspector->timeoff_date_end.''.$inspector->timeoff_time_end);
    $test  = date_parse($date .''.$hour);
    
    $start_time = @mktime($start['hour'],$start['minute'],0,$start['month'],$start['day'],$start['year']);
    $end_time   = @mktime($end['hour'],$end['minute'],0,$end['month'],$end['day'],$end['year']);
    $test_time  = @mktime($test['hour'],$test['minute'],0,$test['month'],$test['day'],$test['year']);
    
    if( $start_time<=$test_time && $test_time<=$end_time ){
        $inside = true;
    }
    return $inside;
}
?>

<?php $this->load->view('include/header'); ?>

    <div class="row">
        <div class="col-lg-12" id="div_schedule">
            <form id="form_schedule" action="<?php echo site_url('schedule')?>" method="post">    
                <div class="panel panel-default panel-block">
                    <div class="list-group">
                        <div class="list-group-item" id="input-fields">
                            <?php
            
                                $inspector_array = array();
                                $inspector_array[0] = '-Select Inspector-';
                                foreach( $inspectors as $inspector ){
                                    $inspector_array[ $inspector->id ] = $inspector->name;
                                }
                                
                                echo  $this->order_helper->get_object_form(
                                                                $input_order, 
                                                                $input_array, 
                                                                Order_helper::FORM_INLINE, 
                                                                array(
                                                                    'report_due'      => array(
                                                                        'date' => true,
                                                                    )/*,
                                                                    'inspector_id' => array(
                                                                        'element' => Order_helper::ELEMENT_DROPDOWN,
                                                                        'options' => $inspector_array,
                                                                        'selected'=> $input_order->inspector_id ? $input_order->inspector_id : '0',
                                                                    ), */
                                                                     
                                                               )
                                                         );
                            ?>           
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            
            <div class="panel panel-default panel-block">
                <div class="list-group">
                    <div class="list-group-item" id="responsive-bordered-table">
                        <div class="form-group">
                            <!--Basic Table-->
                            <div class="table-responsive">
                                <?php                          
                                    echo $this->table->generate();

                                ?>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</section>