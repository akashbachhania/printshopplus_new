
<?php
if( $saved !== null){
    if( $saved === true ){         
        $saved = 'alert alert-success';
        $msg   = '<strong>Success: </strong>Your quote has been saved';                    
    } else if ( $saved === false ) {
        $saved = 'alert alert-error';
        $msg   = '<strong>Error: </strong>Your quote has not been saved (Please correct fields with red border)';            
    } else if ( $saved === 'update') {
        $saved = 'alert alert-info';
        $msg   = '<strong>Success: </strong>Your quote has been updated successfully!';             
    } else if ( $saved === 'inspector_taken'){
        $saved = 'alert alert-error';
        $msg   = '<strong>Error: </strong>I\'m sorry, '.$order->inspector->name.' is already booked for that time.  Please select another time or another inspector!';             
    } else if( $saved === 'email-sent'){
        $saved = 'alert alert-success';
        $msg   = '<strong>Success: </strong>Emails have been sent';             
    } else if( $saved === Authorize_payment::STATUS_FAILED ){
        $saved = 'alert alert-error';
        if( $order->errors ) {
            $msg   = '<strong>Error: </strong>'.implode($order->errors);
            $order->errors = array();     
        } else {
            $msg   = '<strong>Error: </strong>Transaction could not be performed at this time'; 
        }           
    } else if( $saved === Authorize_payment::STATUS_SUCCESS ){
        $saved = 'alert alert-success';
        $msg   = '<strong>Success: </strong>Credit card has been charged';
        $order->errors = array();             
    }
} else {
    $saved = '';                         
    $msg  = '';
}
$orderid=$this->uri->segment(3);
if($this->uri->segment(2)== 'delete')
{
	$orderid="";
}

if(is_array($errors) and count($errors) > 0) {
    $saved = 'alert alert-error';
    $msg = ($msg !='') ? $msg.'<br />' : '<strong>Error: </strong><br />';
    foreach($errors as $key => $error) {
        if(is_array($error) and count($error) > 0) {
            foreach($error as $newK => $newValue)
                $msg .= ucfirst($key).'  '.ucfirst($newK).':  '.$newValue.'<br />';
        } else {
            $msg .= ucfirst($key).' '.$error.'<br />';
        }

    }
}

$posturl= isset($orderid)? site_url('quotes/new_order/'.$orderid) : site_url('quotes/new_order');
?>      
<script type="text/javascript">
    <?php if($orderid > 0) {?>
        function reorder() {
            if(confirm('Are you sure you want to create a new quote with these same details?')) {
                document.location.href = '<?php echo base_url()?>quotes/duplicate_order/<?php echo $orderid;?>';
            }

        }
        <?php }?>
        function showupload()
        {		
          if(document.getElementById("uploadon").value==1){
           new vpb_multiple_file_uploader
           ({
            vpb_form_id: "form_order",
            autoSubmit: true,
				vpb_server_url: "<?php echo base_url() . 'application/views/orders/vpb_uploader.php' ?>" // PHP file for uploading the browsed filese as wish.
			});
           document.getElementById("uploadon").value=2;
       }
   }
   function checkform(formid){
     if(formid==1){		
      document.getElementById('form_order').action = "<?php echo $posturl;?>";		
		//document.getElementById('form_order').submit();
	}else if(formid==3){
		
		var order_id = $("input[name=order_id]").val();
		var dataString = $("#form_order").serialize();
		
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('quotes/save_order_pdf');?>/"+order_id,
			data: dataString,
			cache: false,
			beforeSend: function() 
			{
			},
			success: function(response)
			{
				var print_url='';
				var ret_val  = jQuery.parseJSON(response);
				if(ret_val["save_order"] == "updated"){
					print_url = "<?php echo site_url('quotes/print_order_pdf');?>/"+order_id;
					window.open(print_url);
                    window.location.href="<?php echo site_url('quotes/show_order');?>/"+order_id;
                }
                else{
                 print_url = "<?php echo site_url('quotes/print_order_pdf');?>/"+ret_val["save_order"];
                 window.open(print_url);
                 window.location.href="<?php echo site_url('quotes/show_order');?>/"+ret_val["save_order"];
             }				
         }
     });
	}
	else if(formid==2){
		var validate=true;
		if(document.getElementById("from").value==""){
			alert("Please enter from id");
			document.getElementById("from").focus();
			validate=false;
			return false;
		}else{
			var email = document.getElementById("from").value;
			if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))){
				alert("Invalid E-mail address! Please re-enter.");
				document.getElementById("from").focus();
				validate=false;
				return false;	
			}
		}
		if(document.getElementById("to").value==""){
			alert("Please enter to id");
			document.getElementById("to").focus();
			validate=false;
			return false;
		}else{
			var email = document.getElementById("to").value;
			if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))){
				alert("Invalid E-mail address! Please re-enter.");
				document.getElementById("to").focus();
				validate=false;
				return false;	
			}
		}
		if(document.getElementById("subject").value==""){
			alert("Please enter subject");
			document.getElementById("subject").focus();
			validate=false;
			return false;
		}
		if(document.getElementById("message").value==""){
			alert("Please enter message");
			document.getElementById("message").focus();
			validate=false;
			return false;
		}				
		if(validate){
			document.getElementById("ajax_url").value="<?php echo base_url() . 'application/views/orders/vpb_uploader.php' ?>";
			if(document.getElementById("uploadon").value==1){				
				new vpb_multiple_file_uploader
				({
					vpb_form_id: "form_order",
					autoSubmit: true,
					vpb_server_url: document.getElementById("ajax_url").value // PHP file for uploading the browsed filese as wish.
				});
			}
		}
	}
}
</script>

<?php $this->load->view('include/header'); ?>
<div class="row">
  <div class="col-lg-12">
   <div id="top" class="panel panel-default panel-block panel-title-block">
    <div class="panel-heading">
        <h4><?= $orderid==null?'Quote details' : 'Edit Quote # '.$orderid ?></h4>
    </div>

    <form id="form_order" action="javascript:void(0);" method="post" enctype="multipart/form-data">
        <div id="div_second_members"  class="list-group">
            <div id="div_order_details" class="list-group-item quotes_container q_d" style="background:transparent;border:0;">
                <?php if( $saved ){ ?>
                <div id="div_order_added" class="<?php echo $saved ?>">
                    <!--<a class="close" data-dismiss="alert">x</a>-->
                    <?php echo $msg ?>
                </div>
                <?php } ?> 
                <?php
                echo  $this->order_helper->get_object_form(
                    $order, 
                    array('order_date','report_due','po_text','sales_rep','terms'), 
                    Order_helper::FORM_INLINE,
                    array(
                        'order_date' => array(
                            'date' => true,                                            
                            ),
                        'report_due' => array(
                            'date' => true,                                           
                            ),
                        'po_text' => array(

                            ),
                        'sales_rep' =>
                        array('element' => Order_helper::ELEMENT_DROPDOWN,
                           'options' => $salesrep,
                           'selected'=> $order->sales_rep,

                           ),                                                                               
                        'terms' => 
                        array('element'=> Order_helper::ELEMENT_DROPDOWN,
                          'options'=> $terms,
                          'selected'=>$order->terms,
                          ),

                        '__legend' =>  ''                                        
                        ),null,array('order_date' => 'Date','report_due' => 'Due','sales_rep' => 'Rep')
);
?>
</div>

</div>
</div>       

</div>
</div>         
<div class="row" id ="div_client_details">

    <div class="col-md-6">
        <div class="panel panel-default panel-block" id="div_order_client">
            <div class="panel-heading">
                <h4>Client details</h4>
            </div>
            <?php  echo $this->order_helper->get_object_form($order, array('company','client::name', 'client::contact' , 'client::address','client::address_2','client::state',    'client::city','client::zip','client::phone_1','client::phone_2','client::email','client::email_2','client::email_3','client::notes','client::referred_by','shipping_method','client::id',) ,null, 
                array(
                    'company' => array( 
                        'element'  => Order_helper::ELEMENT_DROPDOWN, 
                        'options'  => $companylist,
                        'selected' => $order->client->id),     
                    'client::notes' => array( 
                        'element'  => Order_helper::ELEMENT_TEXTAREA),  
                    'client::state' => array( 
                        'element'  => Order_helper::ELEMENT_DROPDOWN, 
                        'options'  => $us_states,
                        'selected' => $order->client->state ),                                        
                    'shipping_method' => array( 
                        'element'  => Order_helper::ELEMENT_DROPDOWN, 
                        'options'  => $shipping_method,
                        'selected' => $order->shipping_method ),       
                    '__legend' => ''),
                array('client'=>'client'),
                array('client::name'=>'name')
                );
                ?>
            </div>
        </div>
        <div class="col-md-6" id="div_order_inspection">
            <div class="panel panel-default panel-block q_d_p">

                <div class="panel-heading">
                    <h4>Production details</h4>
                </div>
                <?php echo $this->order_helper->get_object_form($order, array('job_name','stock','colors','size','quantity','finishing','coating','others','additional_notes'), null,
                   array(
                    'additional_notes' => 
                    array('element'=>Order_helper::ELEMENT_TEXTAREA,
                      'rows'=>6),                                                                
                    '__legend'=>'',
                    'stock' => array( 
                        'element'  => Order_helper::ELEMENT_DROPDOWN, 
                        'options'  => $stocks,
                        'selected' => $order->stock ),
                    'colors' => array( 
                        'element'  => Order_helper::ELEMENT_DROPDOWN, 
                        'options'  => $colors,
                        'selected' => $order->colors ),
                    'size' => array( 
                        'element'  => Order_helper::ELEMENT_DROPDOWN, 
                        'options'  => $sizes,
                        'selected' => $order->size ),
                      // 'quantity' => array( 
                      //       'element'  => Order_helper::ELEMENT_DROPDOWN, 
                      //       'options'  => $quantities,
                      //       'selected' => $order->quantity ),
                    'finishing' => array( 
                        'element'  => Order_helper::ELEMENT_DROPDOWN, 
                        'options'  => $finishings,
                        'selected' => $order->finishing ),
                    'coating' => array( 
                        'element'  => Order_helper::ELEMENT_DROPDOWN, 
                        'options'  => $coating,
                        'selected' => $order->coating )
                    )
); 
?>
</div>
</div>

</div>

<div class="row">
    <div class="col-md-12" id="div_order_notes">
        <div class="panel panel-default panel-block">

            <div class="panel-heading">
                <h4>Quote Notes</h4>
            </div>
            <div class="list-group" id="div_order_notes">
                <div class="list-group-item o_n" id="input-fields">
                    <div class="form-horizontal">            

                        <div name="controls" id="o_notes">
                            <div class="form-group row">
                                <label class="control-label col-lg-2" for="order_notes">Order notes</label>                            
                                <div class="controls col-lg-12">
                                    <textarea class="input-largeOrderNotes form-control" id="order_notes" name="order_notes" rows="6" value="<?= $order->order_notes; ?>"><?= $order->order_notes; ?></textarea>                                         
                                </div>
                            </div>
                            <div class="form-group row">
                                <!-- <label class="control-label col-lg-1" for="notes_date">Notes date</label>                             -->
                                <!-- <div class="controls col-lg-2">                            -->
                                    <!-- <input data-datepicker="datepicker" type="text" class="form-control input-largeOrderNotes" id="notes_date" name="notes_date" value="<?= $order->notes_date; ?>" data-date-format="mm-dd-yyyy"> -->
                                <!-- </div> -->
                                <div class="col-lg-5">
                                    <input type="hidden" class="form-control" id='notes_date'  name='notes_date' value="<?= date('m-d-Y'); ?>">
                                    <input id="s_n" type="submit" class="btn btn-info" name="save_n" onclick="checkform(1);" value="Save Notes">
                                    <button id="c_n" class="btn btn-info">Clear Notes</button>  
                                </div>
                            </div>
<!--                             <div class="form-group row s_n">
                                <div class="col-lg-2" style="border:1px solid #ddd;margin-left:10px;">
                                    <p id="n_d"><?= $order->notes_date; ?></p>    
                                </div>
                                <div class="col-lg-2" style="border:1px solid #ddd;margin-left:10px;">

                                    <p id="n_u_n"></p>
                                </div>
                                <div class="col-lg-6" style="border:1px solid #ddd;margin-left:10px;">
                                    <p id="n_n"><?= $order->order_notes; ?></p>
                                </div>
                            </div> -->
                        </div>

                    </div>
                </div>
            </div>
          </div>
      </div>
  </div>

  <div class="row">
    <div class="col-md-12" id="div_order_notes">
        <div class="panel panel-default panel-block">
            <div class="panel-heading">
                <h4>Send an email</h4>
            </div>
            <div class="list-group">
                <div class="list-group-item" id="input-fields-horizontal">
                    <div class="form-horizontal well">


                        <div class="form-group">
                            <div class="col-lg-1"></div>
                            <label for="input-horizontal" class="col-lg-2 control-label">From</label>
                            <div class="col-lg-6">
                                <textarea class="emailcss form-control" id="from" name="from" rows="1"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-1"></div>
                            <label for="input-horizontal-counter" class="col-lg-2 control-label">To</label>
                            <div class="col-lg-6">
                                <textarea class="emailcss form-control" id="to" name="to" rows="1"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-1"></div>
                            <label for="subject" class="col-lg-2 control-label">Subject</label>
                            <div class="col-lg-6">
                                <textarea class="emailcss form-control" id="subject" name="subject" rows="1"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-1"></div>
                            <label for="message" class="col-lg-2 control-label">Message</label>
                            <div class="col-lg-6">
                                <textarea class="emailcss form-control" id="message" name="message" rows="6"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-1"></div>
                            <label class="col-lg-2 control-label" for="message">Upload Files</label>
                            <div class="col-lg-6">
                                <div class="vpb_browse_file_box" style="background:#f2f2f2 url('<?=base_url()?>assets/images/browse_file_by_vasplus_programming_blog.png') no-repeat 12px 9px;"><input type="file" name="vasplus_multiple_files" id="vasplus_multiple_files" multiple="multiple" style="opacity:0;-moz-opacity:0;filter:alpha(opacity:0);z-index:9999;width:90px;padding:5px;cursor:default;" onclick="return showupload();" /></div>
                            </div>                                      
                        </div>
                        <div id="vpb_added_files_box" class="vpb_file_upload_main_wrapper">
                            <div id="mailprocess"></div>
                            <div id="vpb_file_system_displayer_header"> 
                                <div id="vpb_header_file_names"><div style="width:365px; float:left;">File Names</div><div style="width:90px; float:left;">Status</div></div>
                                <div id="vpb_header_file_size">Size</div>
                                <div id="vpb_header_file_last_date_modified">Last Modified</div><br clear="all" />
                            </div>
                            <input type="hidden" id="ajax_url" value="vpb_blue" />
                            <input type="hidden" id="uploadon" value="1" />
                            <input type="hidden" id="added_class" value="vpb_blue">
                            <span id="vpb_removed_files"></span>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-2"></div>
                            <div class="controls col-lg-2"><br/>
                                <input id="save"   class="btn btn-info" type="submit" name="btn_send_mail" value="Send" onclick="return checkform(2);"/>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="row"  id="id_div_items">
    <div class="col-md-12">
        <div class="panel panel-default panel-block">
            <div class="panel-heading">
                <h4>Charges</h4>
            </div>
            <div class="list-group">
                <div class="list-group-item" id="responsive-bordered-table">
                    <div class="form-group span12">

                        <!--Basic Table-->
                        <div class="table-responsive"  name="controls" id="div_order_items">

                            <?php 

                            $this->table->set_heading(
                                'Item',
                                'Description',
                                'Price',                
                                ''                
                                );

                            $count = ($item_count = count( $order->items )) ? $item_count : 3;


                            for( $i=0;$i<=$count;$i++){
                                        //Use items if available
                                $description = '';
                                $price       = '';
                                $selected    = '';
                                if( isset( $order->items[$i])){
                                    $description = $order->items[$i]->description;
                                    $price       = $order->items[$i]->price;
                                    $selected    = $order->items[$i]->item_id;
                                }
                                $this->table->add_row(
                                    form_dropdown( 'item_item_id_' .$i, $items, $selected ),
                                    $this->order_helper->get_text_field( 'item_description_' . $i, 'item_description_' .$i, $description, '' ),
                                    $this->order_helper->get_text_field( 'item_price_' . $i, 'item_price_' . $i, $price, '' ),
                                    '<i class="icon-remove-sign"></i>' 
                                    );
                            }
                            echo $this->table->generate();
                            ?>
                            <a id="add_item_row" class="btn btn-default"><i class="icon-plus"></i>Add rows</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <?php if( $payment_gateway ){?>
    <div class="col-lg-7">
        <div class="panel panel-default panel-block">
            <div class="panel-heading">
                <h4>Receive payment</h4>
            </div>
            <div class="list-group">
                <div class="list-group-item" id="responsive-bordered-table">
                    <div class="form-group" name="controls" id="div_id_charge">

                        <!--Basic Table-->
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="form-group row">
                                                <div class="col-lg-4">
                                                    <input id="charge_amount" class="form-control" type="text" value="<?php echo $order->amount_due ?>" name="amount">
                                                </div>                              
                                                <div class="col-lg-6">
                                                    <a href="#" class="btn btn-info" id="btn_charge" style="margin-top:4px;background:#4dac39;border-color:#4dac39;">Charge this amount</a>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <textarea rows="6" cols="6" id="payment_n" name="payment_n" class="form-control"><?= $order->payment_n; ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-2">
                                                    <input type="submit" name="charge_amount_submit" class="btn btn-info" value="Save Note" onclick="checkform(1)">
                                                </div>
                                                <div class="col-lg-2">
                                                    <input id="reset_charge_amount" name="reset_charge_amount" class="btn btn-info" value="Clear Note">
                                                </div>
                                            </div>

                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    <div class="col-lg-5">
        <div class="panel panel-default panel-block">
            <div class="panel-heading">
                <h4>Calculation</h4>
            </div>
            <div class="list-group">
                <div class="list-group-item" id="responsive-bordered-table">

                    <?php
                    echo $this->order_helper_new->get_object_form($order, array('subtotal', 'tax', 'shipping_amount', 'total' ,'paid', 'amount_due' ),null, 
                        array(
                          'tax_type' => array(
                            'prepend' => '%',
                            'element' => Order_helper::ELEMENT_DROPDOWN, 
                            'options' => $taxes ,
                            'selected' => $order->tax_type ),
                          'subtotal' => array(
                            'class' => 'input-small',
                            'prepend' => '$'
                            ),
                          'tax' => array(
                            'class' => 'input-small',
                            'prepend' => '%', 
                            ),
                          'shipping_amount' => array(
                            'class' => 'input-small',
                            'prepend' => '$', 
                            ),
                          'total' => array(
                            'class' => 'input-small',
                            'prepend' => '$', 
                            ),
                          'paid' => array(
                            'class' => 'input-small',
                            'prepend' => '$', 
                            ),
                          'amount_due' => array(
                            'class' => 'input-small',
                            'prepend' => '$', 
                            ),
                          '__legend'=> ''                                                                                                      
                          ));
                          ?>

                      </div>
                  </div>
              </div>
          </div>
      </div>
      <br/><br/>
      <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <input id="save" class="btn btn-info" type="submit" name="btn_save_order"   value="Save" onclick="checkform(1);"/>

            <input id="btn_print_order"  class="btn btn-info" type="submit" name="btn_print_order"  value="Print" onclick="checkform(3);"/>
            
            <input id="email_order"  class="btn btn-info" type="submit" name="btn_email_order"  value="Email" onclick="checkform(1);"/>
            
            <input id="cancel" class="btn btn-info" type="submit" name="btn_cancel_order" value="Cancel" onclick="checkform(1);"/>
            
            <?php if ( $user->company_id == 11 ) {?>
            <input id="report" class="btn btn-info" type="submit" name="btn_report_order" value="Report" onclick="checkform(1);"/>
            <?php } ?>
            <input id="delete" class="btn btn-info" type="submit" name="btn_delete_order" value="Delete" onclick="checkform(1);"/>      
            
            <input id="back" class="btn btn-info" type="submit" name="btn_back" value="Back" onClick="history.go(-1);return true;" />   
            
            <?php 
            if($order->id > 0) {
                ?><input id="btn_reorder" class="btn btn-info" type="submit" name="btn_reorder" value="Reorder" onClick="return reorder();" />   <?php
            }
            ?>

            <input id="make_order_live" class="btn btn-info" type="submit" name="btn_live" value="Make Order Live" onclick="checkform(1);" />
        </div>
        <div class="col-lg-3"></div>
    </div>
    <div class="modal fade" id="alert_modal">
        <div class="modal-dialog modal-md">
            <div class="modal-content">        

                <div class="modal-header">
                    <a class="close" data-dismiss="modal">�</a>
                    <h3 id="h_modal_header">Modal header</h3>
                </div>
                <div class="modal-body">
                    <div class="alert alert-block alert-error">
                        <p id="p_modal_text">One fine body�</p>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <a data-dismiss="modal" href="#" class="btn btn-info">Close</a>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="delete_modal">
        <div class="modal-dialog modal-md">
          <div class="modal-content">    
            <div class="modal-header">
                <a class="close" data-dismiss="modal">�</a>
                <h3 id="h_delete_header"></h3>
            </div>
            <div class="modal-body">
                <div class="alert alert-block alert-error">
                    <p id="p_delete_text">One fine body�</p>
                </div>
                
            </div>
            <div class="modal-footer">
                <a id="delete_cancel" data-dismiss="modal" href="#" class="btn btn-info">Cancel</a>
                <a id="delete_yes"   href="<?php echo site_url('quotes/delete/' . $order->id)?>" class="btn btn-info">Yes</a>
            </div>
        </div>
    </div>
</div>   
<input type="hidden" name="id" value="<?php echo $order->id?>"/>

</form>

<div class="modal fade" id="charge_modal">
    <div class="modal-dialog modal-md">
      <div class="modal-content">        
        <div class="modal-header">
            <a class="close" data-dismiss="modal">�</a>
            <h3 id="h_modal_header">Charge Credit Card</h3>
        </div>
        <div class="modal-body">
            <div id="div_order_charged">
                <a class="close" data-dismiss="alert">�</a>
                <span></span>
            </div>                
            <form class="form-horizontal" id="form_cc" action="<?php echo site_url('quotes/charge')?>" method="post">            
                <fieldset>
                 <div name="controls">
                    <div class="form-group row">
                        <label class="control-label col-lg-2" for="subtotal">Charge Amount:</label>
                        <div class="controls col-lg-4">                                         
                            <div class="input-prepend">
                                <span class="add-on">$</span>
                                <input id="amount" class="input-small" type="text" value="<?php echo $order->amount_due ?>" name="amount">
                                <span  name="span_help"     class="help-inline hidden"></span>
                            </div>
                        </div>                                  
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-lg-2" for="subtotal">CC Number:</label>                         
                        <div class="controls col-lg-4">                           
                         <input id="cc_num" class="input_charge form-control" type="text" value="" name="cc_num">
                         <span  name="span_help"     class="help-inline hidden"></span>                                           
                     </div>
                 </div>                                
                 <div class="form-group row">
                    <label class="control-label col-lg-2" for="subtotal">Expire Date:</label>                          
                    <div class="controls col-lg-4">                           
                     <input id="cc_date" class="input_charge form-control" type="text" value="" name="cc_date">
                     <span  name="span_help"     class="help-inline hidden"></span>                                          
                 </div>
             </div>
             <div class="form-group row">
                <label class="control-label col-lg-2" for="subtotal">Security code:</label>                          
                <div class="controls col-lg-4">                           
                 <input id="cc_cvn" class="input_charge form-control" type="text" value="" name="cc_cvn">
                 <span  data-placement="top" rel="tooltip"  data-original-title="help help help" name="span_help"   class="help-inline hidden">Whats this?</span>                                          
             </div>
         </div>                                
         <div class="form-group row">
            <label class="control-label col-lg-2" for="subtotal">Billing address:</label>                          
            <div class="controls col-lg-4">                           
             <select id="select_fill_in"  class="input_charge form-control">
                <option value="1">No billing address</option>
                <option value="2">Copy from Agent</option>
                <option value="3">Copy from Client</option>
                <option value="4">Copy from Meet With</option>
                <option value="5">Manual fill in</option>
            </select>
            <span  data-placement="top" rel="tooltip"  data-original-title="help help help" name="span_help"   class="help-inline hidden">Whats this?</span>                                          
        </div>
    </div>                                 
    <div class="form-group fill_in row">
        <label class="control-label col-lg-2" for="subtotal">First Name</label>                          
        <div class="controls col-lg-4">                           
         <input id="cc_date" class="input_charge form-control" type="text" value="" name="first_name">
         <span  data-placement="top" rel="tooltip"  data-original-title="help help help" name="span_help"   class="help-inline hidden">Whats this?</span>                                          
     </div>
 </div>
 <div class="form-group fill_in row">
    <label class="control-label col-lg-2" for="subtotal">Last Name</label>                          
    <div class="controls col-lg-4">                           
     <input id="cc_date" class="input_charge form-control" type="text" value="" name="last_name">
     <span  data-placement="top" rel="tooltip"  data-original-title="help help help" name="span_help"   class="help-inline hidden">Whats this?</span>                                          
 </div>
</div> 
<div class="form-group fill_in row">
    <label class="control-label col-lg-2" for="subtotal">Address</label>                          
    <div class="controls col-lg-4">                           
     <input id="cc_date" class="input_charge form-control" type="text" value="" name="address">
     <span  data-placement="top" rel="tooltip"  data-original-title="help help help" name="span_help"   class="help-inline hidden">Whats this?</span>                                          
 </div>
</div> 
<div class="form-group fill_in row">
    <label class="control-label col-lg-2" for="subtotal">City</label>                          
    <div class="controls col-lg-4">                           
     <input id="cc_date" class="input_charge form-control" type="text" value="" name="city">
     <span  data-placement="top" rel="tooltip"  data-original-title="help help help" name="span_help"   class="help-inline hidden">Whats this?</span>                                          
 </div>
</div>
<div class="form-group fill_in row">
    <label class="control-label col-lg-2" for="subtotal">Zip</label>                          
    <div class="controls col-lg-4">                           
     <input id="cc_date" class="input_charge form-control" type="text" value="" name="zip">
     <span  data-placement="top" rel="tooltip"  data-original-title="help help help" name="span_help"   class="help-inline hidden">Whats this?</span>                                          
 </div>
</div>
<div class="form-group fill_in row">
    <label class="control-label col-lg-2" for="subtotal">State</label>                          
    <div class="controls col-lg-4">                           
     <?php echo form_dropdown('state', $us_states, '', 'class="input_charge form-control"' )?>
     <span  data-placement="top" rel="tooltip"  data-original-title="help help help" name="span_help"   class="help-inline hidden">Whats this?</span>                                          
 </div>
</div>
<div class="form-group fill_in row">
    <label class="control-label col-lg-2" for="subtotal">Country</label>                          
    <div class="controls col-lg-4">                           
     <input id="cc_date" class="input_charge form-control" type="text" value="" name="country">
     <span  data-placement="top" rel="tooltip"  data-original-title="help help help" name="span_help"   class="help-inline hidden">Whats this?</span>                                          
 </div>
</div>                                                                                                                                                                                                                                                                                                                             
</div>
</fieldset>
<input type="hidden" name="order_id" value="<?php echo $order->id?>"/>

</form>
</div>
<div class="modal-footer">
    <a href="#" class="btn btn-info" id="btn_charge_modal">Charge</a>
    <a data-dismiss="modal" href="#" class="btn btn-info">Close</a>
</div>
</div>
</div>
</div>


</section>
<script type="text/javascript">
    $(document).ready(function(){
        $('form, .form-horizontal').addClass('well');
        $('.well').css('background','Transparent');
        $('.well').css('border','0');
        $('#div_order_client .form-group, #div_order_inspection .form-group').addClass('row');
        $('#div_order_client .form-group label, #div_order_inspection .form-group label').addClass('col-lg-4');
        $('#div_order_client .form-group .controls, #div_order_inspection .form-group .controls').addClass('col-lg-5');
        $('.panel-heading').css('padding','10px');
        $('legend').css('border','0');
        $('#form_order').removeClass('well');
    });
</script>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="border:none;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <!-- <h4 class="modal-title">Modal Header</h4> -->
      </div>
      <div class="modal-body" id="load_view">
        
      </div>
      
    </div>

  </div>
</div>