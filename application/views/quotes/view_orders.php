<?php
$overrides = array(
    'report_due'=> 'Due date',
    'sales_rep' => 'Sales Rep',
    'order_notes' => 'Description',
    'client_id' => 'Quote #',
    'company_id' => 'Company',
);  
?>           
         
		<?php
		$source = $this->uri->segment(4);
		if($source == 'orders') { 
                        echo "<div id='div_view_orders' class='row div_header'>";
        	} else {
			echo "<div id='div_home' class='row div_header'>";
		}
        ?>

<?php $this->load->view('include/header'); ?>

    <div class="row">

        <div class="col-lg-12">
            <div class="panel panel-default panel-block quotes_container table-responsive">
                <div id="data-table" class="panel-heading datatable-heading">
                    <h4 class="section-title">Quotes</h4>
                </div>
                <table cellspacing="0" width="100%" class="table table-bordered table-striped dataTable" id="tableSortable">
                    <thead>
                        <tr>
                            <th style="width: 50px;"><img title="Delete" src="<?php echo base_url();?>application/views/assets/img/trash.png" name="deleteall" id="deleteall"></th>
                            <th style="width: 50px;">Edit</th>
                            <th style="width: 50px;">Order #</th>
                            <th style="width: 80px;">Order date</th>
                            <th style="width: 150px;">Company</th>
                            <th style="width: 250px;">Description</th>
                            <th style="width: 80px;">Due date</th>
                            <th>Sales Rep</th>
                            <th>Status</th>
                            <th>Operator</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>

    </div>
</section>            	
                
                  