<?php
if ($saved !== null) {
    if ($saved === true) {
        $saved = 'alert alert-success';
        $msg = '<strong>Success: </strong>Your details have been saved';
    } else if ($saved === false) {
        $saved = 'alert alert-error';
        $msg = '<strong>Error: </strong>Your settings have not been(Please correct fields with red border)';
    }
} else {
    $saved = '';
    $msg = '';
}
$person_fields = array(
    'name', 'address', 'city', 'state', 'zip', 'active'
);
?>

<?php $this->load->view('include/header'); ?>

    <div class="row">

        <div class="col-lg-6">
            <?php if ($saved) { ?>
                <div id="div_order_added" class="<?php echo $saved ?>">
                    <a class="close" data-dismiss="alert">�</a>
                    <?php echo $msg ?>
                </div>
            <?php } ?>            
        </div>    

    </div>

    <div class="row">
        <div class="col-lg-6">
            

            <ul class="nav nav-tabs panel panel-default panel-block">
                <li class="active"><a href="#general_settings" id="general_settings_link" data-toggle="tab">General Settings</a></li>
                <?php if ($user->id == $primary_contact) { ?>

                <li><a href="#users" data-toggle="tab">Users</a></li>
                <li><a href="#departments" data-toggle="tab">Departments</a></li>

                <?php } ?>
                
            </ul>
            <form action="<?php echo site_url('home/settings') ?>" enctype="multipart/form-data" method="post">
                
                <div class="tab-content panel panel-default panel-block">

                    <div class="tab-pane list-group active" id="general_settings">

                        <div class="list-group-item">
                            <h4 class="section-title">Settings</h4>

                            <div class="form-group">
                                <label for="input01">Username</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge form-control" name="username"id="input01" value="<?php echo $user->email ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input02">Password</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge form-control" name="password" id="password"  value="<?php echo $user->password ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input02">Repeat password</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge form-control" name="password_2" id="password_repeat"  value="<?php echo $user->password ?>">
                                </div>
                            </div>
                            <?php if ($file)  ?>
                            <div class="form-group">
                                <label class="control-label" for="input04">Company logo</label>
                                <div class="controls">
                                    <img src="<?php echo $file; ?>">
                                </div>
                            </div>                                
                            <?php ?>
                            <div class="form-group">
                                <label class="control-label" for="input04"></label>
                                <div class="controls">
                                    <input type="file" class="btn btn-info" name="file" id="file">
                                </div>
                            </div>                                                             
                            <div class="form-group">
                                <label class="control-label" for="input02"></label>
                                <div class="controls">
                                    <input type="submit" class="btn  btn-info" name="save" id="submit" value="Ok">
                                    <input type="submit" class="btn btn-info" name="save" id="btn_window_close" value="Cancel">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input02"></label>
                                <div class="controls">
                                    
                                </div>
                            </div>
                        </div>
                    
                    </div>

                    <?php if ($user->id == $primary_contact) { ?>
                        <div class="tab-pane list-group" id="users">
                            <div class="list-group-item form-horizontal">
                                <h4 class="section-title preface-title">Users</h4>

                                <div name="controls">
                                    <?php
                                    if ($users) {
                                        $coordinators = array();
                                        foreach ($users as $index => &$user) {
                                            if ($user->person_type == Person::TYPE_USER) {
                                                $coordinators[] = $user;
                                            }
                                        }

                                        echo $this->order_helper->generate_table($coordinators, $person_fields, $this->table, array(
                                            '__href' => array(
                                                'href' => site_url('home/add_new/Person/<id>/show_user'),
                                                'id' => '<id>',
                                                'name' => 'person',
                                            )
                                                )
                                        );
                                    }
                                    ?>
                                </div>
                                <a class="btn btn-info" target="_blank" href="<?php echo site_url('home/add_new/Person/') ?>">New user</a>
                            </div>
                        </div>
                        
                        <div class="tab-pane list-group" id="departments">
                            <div class="list-group-item">
                                <h4 class="section-title preface-title">Departments</h4>
                                <div name="controls">
                                    <?php
                                    $departments_fields = array('name');
                                    if ($departments) {
                                        ?>
                                        <div name="controls">
                                            <table cellspacing="0" cellpadding="4" border="0" class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="sortable">
                                                    <?php foreach($departments as $key => $dept) {?>
                                                    <tr id="item-<?php echo $key;?>">
                                                        <td>
                                                            <span><?php echo $dept;?></span>
                                                        </td>
                                                    </tr>
                                                    <?php }?>
                                                    
                                                </tbody>
                                            </table>                                
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <script src="<?php echo base_url()?>application/views/assets/js/jquery-ui.js"></script>
                                        <script>
                                    $(function() {
                                      $('#sortable').sortable({
                                        axis: 'y',
                                        update: function (event, ui) {
                                            var data = $(this).sortable('serialize');

                                            // POST to server using $.post or $.ajax
                                            $.ajax({
                                                data: data,
                                                type: 'POST',
                                                url: '<?php echo base_url()?>home/update_order/'
                                            });
                                        }
                                    });
                                      $( "#sortable" ).disableSelection();
                                    });
                                    </script>
                                </div>
                                <a onclick='window.open("<?php echo base_url();?>orders/add_new?type=_5","Add new", "status=1,height=500,width=520,scrollbars=no,toolbar=no,location=no");' data-type="6" value="add_new" name="" class="btn btn-info" href="javascript:void(0);">New Department</a>
                            </div>
                        </div>
                    <?php } ?>
                </div>


            </form> 


        </div>
    </div>
</section>