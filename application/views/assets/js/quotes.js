jQuery(document).ready(function($) {
            var l = window.location;
            var base_url = l.protocol + "//" + l.host + "/" + l.pathname.split('/')[1];


            /**

            * QUOTES

            */
              if( String(window.location).match(/quotes/) ){

                //Total

                /**

                * Tax value holder field, used in calculation

                */

                $('#div_id_calculation').append('<input type="hidden" value="0" id="tax_type_percentage"/>');

                //Client/Agent/inspector - Drop down change

                $('#client_name, #name, #inspector_id').change(
        
                    function(){

                        
                        //Open pop up for creation of new person

                        if( $(this).attr('value') == '_1' ||

                            $(this).attr('value') == '_2'|| 

                            $(this).attr('value') == '_3' ){ //ddd//

                            window.open(String(window.location).match(/(.*)(?:home|orders|quotes|clients|stats|inspectors|schedule|admin)/).pop()+"orders/add_new?type=" + $(this).attr('value') ,"Add new", "status=1,height=720,width=475");

                        

                        //Update person form    

                        } else {

                             //Send AJAX request

                             $.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+"/orders/ajax_control", 

                                    { value: $(this).attr('value'), name: $(this).attr('name'), type: 'person'},

                                    //Fill in Person form with data

                                    function(data) {                            

                                         var ret_val  = jQuery.parseJSON( data );

                                         for( var key in ret_val ){

                                             $('#' + key).attr('value', ret_val[ key ]);

                                         }

                                    });                         

                        }

                    }

                );


                $('[id^="operator_"]').click(
                    function(){
                  
        
                                var parent = $(this);
    
                                //$.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+"orders/ajax_control",
                                    $.post(base_url+"/orders/ajax_control",  
        
                                    { id: 'operetor', type: 'refresh_dropdown' },
        
                                    function(data) {
        
                                        //alert(data);
                                        
                                        var ret_val  = jQuery.parseJSON( data );
        
                                        var ids      = new Array();                        
        
                                        parent.children().each(
        
                                            function(){
        
                                                ids.push( $(this).attr('value'));
        
                                            }
        
                                        );
        
                                        
        
                                        for( var key in ret_val ){
        
                                            if( jQuery.inArray(key ,ids) == -1){
        
                                                parent.children().removeAttr('selected');
        
                                                parent.append('<option selected="selected" value="'+key+'">'+ret_val[key]+'</option>');
        
                                                //parent.trigger('change');
                                                
        
                                            }
        
                                        }
        
                                    });                         
        
                            }
                );
               $('[id^="Coating_"]').click(
                    function(){
                  
        
                                var parent = $(this);
    
                                //$.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+"orders/ajax_control",
                                    $.post(base_url+"/orders/ajax_control",  
        
                                    { id: 'Coating', type: 'refresh_dropdown' },
        
                                    function(data) {
        
                                        //alert(data);
                                        
                                        var ret_val  = jQuery.parseJSON( data );
        
                                        var ids      = new Array();                        
        
                                        parent.children().each(
        
                                            function(){
        
                                                ids.push( $(this).attr('value'));
        
                                            }
        
                                        );
        
                                        
        
                                        for( var key in ret_val ){
        
                                            if( jQuery.inArray(key ,ids) == -1){
        
                                                parent.children().removeAttr('selected');
        
                                                parent.append('<option selected="selected" value="'+key+'">'+ret_val[key]+'</option>');
        
                                                //parent.trigger('change');
                                                
        
                                            }
        
                                        }
        
                                    });                         
        
                            }
                );

                $('#terms,#status,#shipping_method,#Coating,#sales_rep,#company,#stock,#colors,#size,#quantity,#finishing,#coating,#shirt_type,#shirt_front_color,#shirt_back_color,#shirt_size').click(

                    function(){

                        

                        var parent = $(this);
                        var parentId = parent.attr('id');

                        //$.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+"orders/ajax_control",
                            $.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|quotes|admin)/).pop()+"orders/ajax_control",  

                            { id: $(this).attr('id'), type: 'refresh_dropdown' },

                            function(data) {

                                //alert(data);
                                
                                var ret_val  = jQuery.parseJSON( data );

                                var ids      = new Array();                        

                                parent.children().each(

                                    function(){

                                        ids.push( $(this).attr('value'));

                                    }

                                );

                                

                                for( var key in ret_val ){

                                    if( jQuery.inArray(key ,ids) == -1){

                                        parent.children().removeAttr('selected');

                                        parent.append('<option selected="selected" value="'+key+'">'+ret_val[key]+'</option>');

                                        parent.trigger('change');

                                    } else {
                                       
                                        $('#'+parentId+' option[value='+key+']').text(ret_val[key]);

                                    }

                                }

                            });                         

                    }

                );

                
                

                //Item - Drop down change

                $('[name^="item_item_id_"]').change( item_change );                                    

                //Item price change, recalculation - TODO JS optimization

                $('[id^="item_price_"]').change( item_price_change );

                

                $('#tax_type').change(

                    function(){

                        

                        value = $(this).attr('value');

                        var regex = /new_tax/;

                        if( regex.test( value )){

                            window.open(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|quotes|admin)/).pop()+"orders/add_new?type=" + value,"Add new", "status=1,height=720,width=475");    

                        } else {

                            var tax_type = $(this).attr('value');

                            

                            if( Number(tax_type )>0){

                                $.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+"orders/ajax_control", 

                                    { type: 'tax', id: tax_type},                        

                                    function(data) {                            

                                        var hidden = $('#tax_type_percentage');

                                        var response = jQuery.parseJSON( data );

       

                                        hidden.attr('value', response['value']);

                                        $('#tax').trigger('change');

                                    });                             

                            }

        

                        }

                    }

                )
                
                // 
                            
                
                 $('#sales_rep').change(

                    function(){
                        
                        value = $(this).val();
                        
                        if(value=='_2')
                        {
                            $.ajax({
                                method: "GET",
                                url: String(window.location).match(/(.*)(?:home|orders|quotes|clients|stats|inspectors|schedule|admin)/).pop()+"orders/add_new",
                                data: { type: value }
                            })
                            .done(function( result ) {
                                $("#load_view").html(result);
                                $("#myModal").modal('show');
                               //alert( "Data Saved: " + msg );
                            });
                                // window.open(String(window.location).match(/(.*)(?:home|orders|quotes|clients|stats|inspectors|schedule|admin)/).pop()+"orders/add_new?type=" + value,"Add new", "status=1,height=600,width=550,scrollbars=yes,toolbar=no,location=no");    
                        }

                    }

                )
                 
                $('#status').change(

                    function(){
                        
                        value = $(this).attr('value');
                        if(value=='_5') {
                            $.ajax({
                                method: "GET",
                                url: String(window.location).match(/(.*)(?:home|orders|quotes|clients|stats|inspectors|schedule|admin)/).pop()+"orders/add_new",
                                data: { type: value }
                            })
                            .done(function( result ) {
                                $("#load_view").html(result);
                                $("#myModal").modal('show');
                               //alert( "Data Saved: " + msg );
                            });
                                                 // window.open(String(window.location).match(/(.*)(?:home|orders|quotes|clients|stats|inspectors|schedule|admin)/).pop()+"orders/add_new?type=" + value,"Add new", "status=1,height=500,width=520,scrollbars=no,toolbar=no,location=no");  
                        }

                    }

                )
        
                $('#stock').change(

                    function(){
                        
                        value = $(this).val();
                        
                        if(value=='_11') {
                            $.ajax({
                                method: "GET",
                                url: String(window.location).match(/(.*)(?:home|orders|quotes|clients|stats|inspectors|schedule|admin)/).pop()+"orders/add_new",
                                data: { type: value }
                            })
                            .done(function( result ) {
                                $("#load_view").html(result);
                                $("#myModal").modal('show');
                               //alert( "Data Saved: " + msg );
                            });
                             // window.open(String(window.location).match(/(.*)(?:home|orders|quotes|clients|stats|inspectors|schedule|admin)/).pop()+"orders/add_new?type=" + value,"Add new", "status=1,height=500,width=520,scrollbars=no,toolbar=no,location=no");  
                        }

                    }

                )
        
                $('#colors').change(
                    function(){
                        value = $(this).val();
                        
                        if(value=='_12') {
                            $.ajax({
                                method: "GET",
                                url: String(window.location).match(/(.*)(?:home|orders|quotes|clients|stats|inspectors|schedule|admin)/).pop()+"orders/add_new",
                                data: { type: value }
                            })
                            .done(function( result ) {
                                $("#load_view").html(result);
                                $("#myModal").modal('show');
                               //alert( "Data Saved: " + msg );
                            });
                            // window.open(String(window.location).match(/(.*)(?:home|orders|quotes|clients|stats|inspectors|schedule|admin)/).pop()+"orders/add_new?type=" + value,"Add new", "status=1,height=500,width=520,scrollbars=no,toolbar=no,location=no");  
                        }
                    }
                )
        
                $('#size').change(

                    function(){
                        
                        value = $(this).val();
                        
                        if(value=='_13') {
                            $.ajax({
                                method: "GET",
                                url: String(window.location).match(/(.*)(?:home|orders|quotes|clients|stats|inspectors|schedule|admin)/).pop()+"orders/add_new",
                                data: { type: value }
                            })
                            .done(function( result ) {
                                $("#load_view").html(result);
                                $("#myModal").modal('show');
                               //alert( "Data Saved: " + msg );
                            });                              
                                                 // window.open(String(window.location).match(/(.*)(?:home|orders|quotes|clients|stats|inspectors|schedule|admin)/).pop()+"orders/add_new?type=" + value,"Add new", "status=1,height=500,width=520,scrollbars=no,toolbar=no,location=no");  
                        }

                    }

                )
                
                $('#quantity').change(

                    function(){
                        
                        value = $(this).attr('value');
                        if(value=='_14') {
                                                 window.open(String(window.location).match(/(.*)(?:home|orders|quotes|clients|stats|inspectors|schedule|admin)/).pop()+"orders/add_new?type=" + value,"Add new", "status=1,height=500,width=520,scrollbars=no,toolbar=no,location=no");  
                        }

                    }

                )
        
                $('#finishing').change(

                    function(){
                        
                        value = $(this).val();
                        
                        if(value=='_15') {
                            $.ajax({
                                method: "GET",
                                url: String(window.location).match(/(.*)(?:home|orders|quotes|clients|stats|inspectors|schedule|admin)/).pop()+"orders/add_new",
                                data: { type: value }
                            })
                            .done(function( result ) {
                                $("#load_view").html(result);
                                $("#myModal").modal('show');
                               //alert( "Data Saved: " + msg );
                            });
                                                 // window.open(String(window.location).match(/(.*)(?:home|orders|quotes|clients|stats|inspectors|schedule|admin)/).pop()+"orders/add_new?type=" + value,"Add new", "status=1,height=500,width=520,scrollbars=no,toolbar=no,location=no");  
                        }

                    }

                )
        
                $('#coating').change(
                    function(){                     
                            value = $(this).val();
                        
                            if(value=='_16') {
                                $.ajax({
                                    method: "GET",
                                    url: String(window.location).match(/(.*)(?:home|orders|quotes|clients|stats|inspectors|schedule|admin)/).pop()+"orders/add_new",
                                    data: { type: value }
                                })
                                .done(function( result ) {
                                    $("#load_view").html(result);
                                    $("#myModal").modal('show');
                                   //alert( "Data Saved: " + msg );
                                });
                             // window.open(String(window.location).match(/(.*)(?:home|orders|quotes|clients|stats|inspectors|schedule|admin)/).pop()+"orders/add_new?type=" + value,"Add new", "status=1,height=500,width=520,scrollbars=no,toolbar=no,location=no");  
                            }
                });
                
                $('#shirt_type').change(
                    function(){                     
                            value = $(this).attr('value');
                            if(value=='_17') {
                             window.open(String(window.location).match(/(.*)(?:home|orders|quotes|clients|stats|inspectors|schedule|admin)/).pop()+"orders/add_new?type=" + value,"Add new", "status=1,height=500,width=520,scrollbars=no,toolbar=no,location=no");  
                            }
                });
                
                $('#shirt_front_color').change(
                    function(){                     
                            value = $(this).attr('value');
                            if(value=='_18') {
                             window.open(String(window.location).match(/(.*)(?:home|orders|quotes|clients|stats|inspectors|schedule|admin)/).pop()+"orders/add_new?type=" + value,"Add new", "status=1,height=500,width=520,scrollbars=no,toolbar=no,location=no");  
                            }
                });
                
                $('#shirt_back_color').change(
                    function(){                     
                            value = $(this).attr('value');
                            if(value=='_19') {
                             window.open(String(window.location).match(/(.*)(?:home|orders|quotes|clients|stats|inspectors|schedule|admin)/).pop()+"orders/add_new?type=" + value,"Add new", "status=1,height=500,width=520,scrollbars=no,toolbar=no,location=no");  
                            }
                });
                
                $('#shirt_size').change(
                    function(){                     
                            value = $(this).attr('value');
                            if(value=='_20') {
                             window.open(String(window.location).match(/(.*)(?:home|orders|quotes|clients|stats|inspectors|schedule|admin)/).pop()+"orders/add_new?type=" + value,"Add new", "status=1,height=500,width=520,scrollbars=no,toolbar=no,location=no");  
                            }
                });
                
                $('#company').change(                                            
                    function(){                        
                        value = $(this).val();
                        
                        if(value=='_1')
                        {   
                            $.ajax({
                                method: "GET",
                                url: String(window.location).match(/(.*)(?:home|orders|quotes|clients|stats|inspectors|schedule|admin)/).pop()+"orders/add_new",
                                data: { type: value }
                            })
                            .done(function( result ) {
                                $("#load_view").html(result);
                                $("#myModal").modal('show');
                               //alert( "Data Saved: " + msg );
                            });
                            // window.open(String(window.location).match(/(.*)(?:home|orders|quotes|clients|stats|inspectors|schedule|admin)/).pop()+"orders/add_new?type=" + value,"Add new", "status=1,height=600,width=550,scrollbars=yes,toolbar=no,location=no");  
                        }
                        else {
                            
                            // Call ajax to fill text box value
                            $.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|quotes|admin)/).pop()+"orders/ajax_control",

                                { type: 'CompanyList', item_type: $(this).attr('value'), id: $(this).attr('value') },
            
                                //Fill in Person form with data
                            
                                function(data) {  
                                
                                     var ret_val  = jQuery.parseJSON( data );

                                     
                                     for( var key in ret_val ){
                                         
                                         $('#client_' + key).attr('value', ret_val[ key ]);
            
                                     }
            
                                });
                            
                        }

                    }
                )
                 
            
                 
                 //$('#client_shipping').change(
                 $('#shipping_method').change(

                    function(){

                        value = $(this).val();
                        
                        if(value=='_6')
                        {
                            $.ajax({
                                method: "GET",
                                url: String(window.location).match(/(.*)(?:home|orders|quotes|clients|stats|inspectors|schedule|admin)/).pop()+"orders/add_new",
                                data: { type: value }
                            })
                            .done(function( result ) {
                                $("#load_view").html(result);
                                $("#myModal").modal('show');
                               //alert( "Data Saved: " + msg );
                            });
                            // window.open(String(window.location).match(/(.*)(?:home|orders|quotes|clients|stats|inspectors|schedule|admin)/).pop()+"orders/add_new?type=" + value,"Add new", "status=1,height=500,width=520,scrollbars=no,toolbar=no,location=no");    
                        }
                    }

                )
                

                $('#paid').change(

                    function(){

                        if( isNaN( $(this).val() )){

                            $(this).attr('value','0.00' );    

                        } else {

                            $('#amount_due').attr('value', Number($('#total').attr('value')) - Number($(this).attr('value')) );

                            if($('#amount_due').attr('value').indexOf('.') == -1 ){

                                $('#amount_due').attr('value', $('#amount_due').attr('value') + '.00' );      
                                

                            }

                        }   

                    }

                )

                $('#amount_due, #subtotal, #total, #tax,#shipping_amount').change( calculate );

                

                //Terms,Types select boxes

//                $("#terms, #type_of_inspection, #type_of_structure, #type_of_foundation, #type_of_utilities, #tax_").change(
                $("#type_of_inspection, #type_of_structure, #type_of_foundation, #type_of_utilities, #tax_").change(

                    function(){

                        value = $(this).attr('value');

                        var regex = /__\d/;

                        if( regex.test( value )){

                            window.open(String(window.location).match(/(.*)(?:home|orders|quotes|clients|stats|inspectors|schedule|admin)/).pop()+"orders/add_new?type=" + value,"Add new","status=1,height=720,width=475");    

                        }

                    }

                )
                
                 
                /**

                * Add decimal zeroes

                */

                $('[name^="item_price_"], #total, #subtotal, #paid, #amount_due,[name="amount"]').each(

                    function(){

                        if(  $(this).attr('value') == false ){

                             $(this).attr('value','0.00');

                        }

                        if($(this).attr('value').indexOf('.') == -1 ){

                            $(this).attr('value', $(this).attr('value') + '.00' );      

                        }                    

                    }

                )           

                //Add new row - On click adds new row to our items table

                $("#add_item_row").click(

                    function(){                    

                        var table = $("#div_order_items table>tbody");

                        var rows  = $("#div_order_items table tr");

                        

                        var row_count = rows.length - 2;

                        var new_row_count = row_count + 1;

                        

                        var last_row_html = rows.last().html().replace(/_\d/g,'_' + new_row_count).replace( '<td>'+row_count+'</td>','<td>'+new_row_count+'</td>');

                        

                        table.append('<tr>' +last_row_html  +'</tr>' );

                        

                        var item_name = 'item_item_id_'+new_row_count;

                        //Add change handler for new item

                        $('[name^="'+item_name+'"]').change( item_change );

                        //Empty input fields

                        $('#item_description_' + new_row_count).attr('value','');

                        $('#item_price_' + new_row_count).attr('value','');

                        //Bind event handler to new #item_price

                        $('#item_price_' + new_row_count).change( item_price_change );

                        //Hover event handler

                        table.find('tr').hover(

                            function(){

                               $(this).children().last().children().show();

                            },

                            function(){

                               $(this).children().last().children().hide();     

                            } 

                        );

                        //Bind event handler for click on new Remove - Recalculate

                        table.find('i').click( recalculate );

                    }

                )

                $("#div_client_title").click(

                    function(){

                        $("#div_client_details").toggle('slow');

                        $(this).find('.right_column').toggle();

                        $("#icon_collapse").toggleClass('icon-minus icon-plus');

                    }

                )

                $("#div_items_title").click(

                    function(){

                        $("#id_div_items").toggle('slow');

                        $(this).find('.right_column').toggle();

                        $("#icon_collapse_items").toggleClass('icon-minus icon-plus');

                    }

                )            

                $("#div_agent_title").click(

                    function(){

                        $("#div_agent").toggle('slow');

                        $(this).find('.right_column').toggle();

                        $(this).find('i').toggleClass('icon-minus icon-plus');

                    }

                )             

                $("#id_div_items tr").hover(

                    function(){

                       $(this).children().last().children().show();

                    },

                    function(){

                       $(this).children().last().children().hide();     

                    } 

                );

                 

                $('#inspector_id, #inspection_time, #inspection_date, #estimated_inspection_time').change( check_inspector );

                

                //$('#div_order_client fieldset'). height($('#div_order_inspection fieldset').height());
                $('#div_order_inspection fieldset'). height($('#div_order_client fieldset').height());

                 

                $('#div_agent_details  fieldset').height( $('#div_ins_details  fieldset').height());

                

                $('#div_id_charge  fieldset').height( $('#div_id_calculation  fieldset').height());

                

                /**

                * Remove Order item row Icon ( when user clicks icon, it removes parent row

                */

                $("#id_div_items tr>td>i").hide();

                /**

                * Recalculate after row was removed

                */

                $("#id_div_items tr>td>i").click( recalculate );

                /**

                * Fixed last column with ( so it doesn change size on hover over the Remove icon )

                */

                $('[class="icon-remove-sign"]').parent().addClass('td_item_remove');

                /**

                * Remove Result div on click

                */

                $("#div_order_added>a").click(

                    function(){

                        $(this).parent().hide();

                    }

                )

         

                /*MODAL*/

                $('#delete').click(

                    function(){

                        $('#p_delete_text').text('Are you sure you want to delete this order?');

                        $('#h_delete_header').text('Delete order');

                        $('#delete_modal').modal('toggle');                                        

                        return false;                     

                    } 

                )

                

                $('#email_order').click( 

                    function(){

                        if( $('#status').attr('value')=='7'){

                            $('#p_modal_text').text('This inspection is pending.  Please change status to email');

                            $('#h_modal_header').text('Email');

                            $('#alert_modal').modal('toggle');

                                                    

                        }

                    }

                )



                $.extend( $.fn.dataTableExt.oStdClasses, {

                    "sWrapper": "dataTables_wrapper form-inline"

                } );            

                

                $('#email').click(

                    function(){

                        var id = $('#id').attr('value') ;

                        return false; 

                        if( isNaN(id) || typeof id == undefined || id == null || id == ''){

                            $('#p_modal_text').text('You must save order before printing or emailing!');

                            $('#h_modal_header').text('Warning');

                            $('#alert_modal').modal('toggle');

                                                     

                        }

                    }

                )

                

                $('#table_charge').removeClass();

                $('#btn_charge').click(

                    function(){

                        $('#div_order_charged').hide(); 

                        $('#charge_modal').modal('toggle');

                    }

                )

                /**

                * Add decimal zeroes

                */

                $('#charge_amount').each(

                    function(){

                        if(  $(this).attr('value') == false ){

                             $(this).attr('value','$0.00');

                        }

                        if($(this).attr('value').indexOf('.') == -1 ){

                            $(this).attr('value', '$'+$(this).attr('value') + '.00' );      

                        }

                        if($(this).attr('value').indexOf('$') == -1 ){

                            $(this).attr('value', '$'+$(this).attr('value') );      

                        }                                            

                    }

                )                

                $('[name="amount"]').change(

                    function(){

                        var value = $(this).attr('value');

                        var amount_due = Number($('#amount_due').attr('value'));

                        var charge_amount = Number($(this).attr('value').replace('$'));

                        $('#amount').attr('value',charge_amount);

                        if( charge_amount > amount_due ){

                            $(this).parent().parent().parent().addClass('warning');

                            $(this).next().removeClass('hidden').text('Amount is larger than amount due!');

                        } else if(isNaN(charge_amount)){

                            $(this).parent().parent().parent().addClass('error');

                            $(this).next().removeClass('hidden').text('Invalid amount!');                            

                        } else {

                            $(this).next().addClass('hidden').text('');

                            $(this).parent().parent().parent().removeClass('warning').removeClass('error');    

                        }

                        $('#charge_amount').attr('value', value );

                    }

                )

                $('#cc_num').change(

                    function(){

                        if(isNaN($(this).attr('value')) || $(this).attr('value').length < 13){

                           $(this).next().text('Invalid Credit card number value');

                           $(this).parent().parent().addClass('error');

                        } else {

                           $(this).next().text('');

                           $(this).parent().parent().removeClass('error');                            

                        }

                    }

                )

                $('#cc_date').change(

                    function(){

                        if(isNaN($(this).attr('value'))){

                           $(this).next().text('Invalid Expire date value');

                           $(this).parent().parent().addClass('error');

                        } else {

                           $(this).next().text('');

                           $(this).parent().parent().removeClass('error');                            

                        }

                    }

                )

                $('#btn_charge_modal').click(

                    function(){                        

                        $('#div_order_charged').hide();

                        var cc_num  = $('#cc_num');

                        var cc_date = $('#cc_date');

                        var amount  = $('#amount');

                        var error   = false;

                        

                        if( cc_num.attr('value')=="" || isNaN(cc_num.attr('value'))){

                           cc_num.next().text('Invalid CC number value');

                           cc_num.parent().parent().addClass('error');

                           error = true;                            

                        }

                        if( cc_date.attr('value')=="" || isNaN(cc_date.attr('value'))){

                           cc_date.next().text('Invalid Expire date value');

                           cc_date.parent().parent().addClass('error');

                           error = true                            

                        }

                        if( amount.attr('value')=="" || isNaN(amount.attr('value'))){

                           amount.next().text('Invalid amount!');

                           amount.parent().parent().parent().addClass('error');

                           error = true                              

                        }

                        if( error ){

                            $('#div_order_charged span').text('Please correct the fields below');

                            $('#div_order_charged').addClass('alert alert-error').show();

                        } else {

                            if(confirm('Are you sure you want to charge this credit card with '+amount.attr('value')+'$?')){

                                $('#form_cc').submit();

                                $('#charge_modal').modal('toggle');

                            }

                        }

                    }

                )

                $('[name="span_help"]').tooltip();

                

                $('.fill_in').hide();

                $('#select_fill_in').change(

                    function(){

                        var value = $(this).attr('value');

                        var prefix  = 'none';

                        var fill_in = false;

                        

                        var array = new Array('name','address','zip','city');

                        switch(value){

                            case '1':

                                for( key in array ){

                                    $('#charge_modal [name="'+array[key]+'"]').attr('value', '');

                                }

                                $('#charge_modal [name="state"]').find('[selected="selected"]').removeAttr('selected');                              

                            break;

                            case '2':

                                fill_in = true;

                                prefix = 'agent_';

                            break;

                            case '3':

                                fill_in = true;

                                prefix = 'client_';                            

                            break;

                            case '4':

                                fill_in = true;

                                prefix = '';

                            break;

                            case '5':

                                fill_in = true;

                                for( key in array ){

                                    $('#charge_modal [name="'+array[key]+'"]').attr('value', '');

                                }

                                $('#charge_modal [name="state"]').find('[selected="selected"]').removeAttr('selected');                                

                            break;                                                                                                                

                        }

                        

                        if( fill_in ){

                            $('.fill_in').show();

                        } else {

                            $('.fill_in').hide();

                        }



                        if( prefix ){

                            for( key in array ){

                                if( key == 'name'){

                                    var names = ret_val['details']['name'].split(' ');    

                                }

                                $('#charge_modal [name="'+array[key]+'"]').attr('value', $('#'+prefix +''+array[key]).attr('value'));

                            }

                            

                            if( prefix == ''){

                                var names = $('#meet_with').attr('value').split(' ');     

                            } else {

                                 

                            }

                            

                            

                            $('#charge_modal [name="last_name"]').attr('value','');

                            for( index in names ){

                                if( index == 0 ){

                                    $('#charge_modal [name="first_name"]').attr('value',names[index]);

                                    //$('#charge_modal [name="first_name"]').parent().parent().addClass('warning');

                                    //$('#charge_modal [name="first_name"]').next().text('Plese verify this field');

                                } else{

                                    $('#charge_modal [name="last_name"]').attr('value', $('#charge_modal [name="last_name"]').attr('value') + ' ' + names[index]);    

                                   // $('#charge_modal [name="last_name"]').parent().parent().addClass('warning');

                                    //$('#charge_modal [name="last_name"]').next().text('Plese verify this field'); 

                                }

                            }

                            

                            $('#charge_modal [name="state"]').find('[selected="selected"]').removeAttr('selected');                            

                            $('#charge_modal [name="state"]').find('[value="'+ $('#'+prefix +'state').attr('value') +'"]').attr('selected','selected');
                            
                            $('#charge_modal [name="country"]').attr('value','US');

                        } else {

                             

                        }

                        

                    }

                )

                

            }   //end quotes

    
});


$(document).ready(function () {
    $(document).on('submit','#div_add_new form',function(e){
        e.preventDefault();
        // var controlid = $('form.well input[name="control_id"]').val();
        // console.log($(this).serialize());
        $.ajax({
            url : $(this).attr('action'),
            type: "POST",
            data: $(this).serialize(),
            success: function (data) {
                
                // refresh_item _select(controlid);
                $("#load_view").html(data);
            },
            error: function (jXHR, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });
    });
});