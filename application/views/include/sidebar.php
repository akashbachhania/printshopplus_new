<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7 lt-ie10"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8 lt-ie10"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9 lt-ie10"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        
        <title>Proton UI - Tables</title>
        <meta name="description" content="Page Description">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="<?php echo base_url()?>assets/styles/bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url()?>assets/styles/custom.css">

        <!-- Page-specific Plugin CSS: -->
        <link rel="stylesheet" href="<?php echo base_url()?>assets/styles/vendor/select2/select2.css">
        <link rel="stylesheet" href="<?php echo base_url()?>assets/styles/vendor/datatables.css" media="screen" />


        <!-- Proton CSS: -->
        <link rel="stylesheet" href="<?php echo base_url()?>assets/styles/proton.css">
        <link rel="stylesheet" href="<?php echo base_url()?>assets/styles/vendor/animate.css">

        <!--DatePicker-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'application/views/assets/style/datepicker.css' ?>"/>
        <link rel="stylesheet"  href="<?php echo base_url() . 'application/views/assets/style/jquery.fancybox.css?v=2.1.5' ?>" />
        <link rel="stylesheet"  href="<?php echo base_url() . 'application/views/assets/style/jquery.timepicker.css' ?>" />
        <!-- adds CSS media query support to IE8   -->
        <!--[if lt IE 9]>
            <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
            <script src="scripts/vendor/respond.min.js"></script>
        <![endif]-->

        <!-- Fonts CSS: -->
        <link rel="stylesheet" href="<?php echo base_url()?>assets/styles/font-awesome.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url()?>assets/styles/font-titillium.css" type="text/css" />

        <!-- Common Scripts: -->
        <script>
        (function () {
          var js;
          if (typeof JSON !== 'undefined' && 'querySelector' in document && 'addEventListener' in window) {
            js = 'http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js';
          } else {
            js = 'http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js';
          }
          document.write('<script src="' + js + '"><\/script>');
        }());
        </script>
        <script src="<?php echo base_url()?>assets/scripts/vendor/modernizr.js"></script>
        <script src="<?php echo base_url()?>assets/scripts/vendor/jquery.cookie.js"></script>
        
    </head>

    <body>
       <script>
            var theme = $.cookie('protonTheme') || 'default';
            $('body').removeClass (function (index, css) {
                return (css.match (/\btheme-\S+/g) || []).join(' ');
            });
            if (theme !== 'default') $('body').addClass(theme);
        </script>
        <!--[if lt IE 8]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <nav class="main-menu" data-step='2' data-intro='This is the extendable Main Navigation Menu.' data-position='right'>

            <ul>
                <li class="has-subnav">
                    <a id="a_home" href="<?php echo site_url('orders/view_orders/all/home')?>">
                        <i class="icon-home nav-icon"></i>
                        <span class="nav-text">
                            Home
                        </span>
                        <i class="icon-angle-right"></i>
                    </a>
                    <?php $this->load->view('include/submenu');?>
                </li>

            <?php if( in_array($user->person_type, array(Person::TYPE_ADMIN, Person::TYPE_USER )) ) {?>
                <li class="has-subnav">
                    <a id="a_orders"  href="<?php echo site_url('orders/new_order')?>">
                        <i class="icon-laptop nav-icon"></i> 
                        <span class="nav-text">
                           Orders
                        </span>
                        <i class="icon-angle-right"></i>
                    </a>
                    <?php
                        if($this->uri->segment(1) == 'orders') 
                            $this->load->view('include/submenu');
                    ?>
                </li>

                <li class="has-subnav">
                    <a id="a_quotes"  href="<?php echo site_url('quotes/new_order')?>"> 
                        <i class="icon-laptop nav-icon"></i> 
                        <span class="nav-text">
                            Quotes
                        </span>
                        <i class="icon-angle-right"></i>
                    </a>
                    <?php
                        if($this->uri->segment(1) == 'quotes') 
                            $this->load->view('include/submenu');
                    ?>
                </li>

                <li class="has-subnav">
                    <a id="a_clients" href="<?php echo site_url('clients')?>">
                        <i class="icon-user nav-icon"></i> 
                        <span class="nav-text">
                           Clients
                        </span> 
                        <i class="icon-angle-right"></i>
                    </a>
                    <?php
                        if($this->uri->segment(1) == 'clients')  
                            $this->load->view('include/submenu');?>
                </li>

                <?php if( $user->company_id == 11 ){ ?>                                                                            
                <li class="has-subnav">
                    <a id="a_reports" href="<?php echo site_url('reports')?>">
                        <i class="icon-bar-chart nav-icon"></i> 
                        <span class="nav-text">
                           Reports
                        </span>
                        <i class="icon-angle-right"></i>
                    </a>
                    <?php
                        if($this->uri->segment(1) == 'reports') 
                            $this->load->view('include/submenu');?>
                </li>
                <?php } ?>

                <li class="has-subnav">
                    <a id="a_stats" href="<?php echo site_url('stats')?>">
                        <i class="icon-bar-chart nav-icon"></i> 
                        <span class="nav-text">
                           Stats
                        </span> 
                        <i class="icon-angle-right"></i>
                    </a>
                    <?php
                        if($this->uri->segment(1) == 'stats')  
                            $this->load->view('include/submenu');?>
                </li>

                <li class="has-subnav">
                    <a id="a_inspectors" href="<?php echo site_url('inspectors')?>">
                        <i class="icon-user nav-icon"></i> 
                        <span class="nav-text">
                           Sales Rep
                        </span>
                        <i class="icon-angle-right"></i>
                    </a>
                    <?php
                        if($this->uri->segment(1) == 'inspectors')  
                            $this->load->view('include/submenu');?>
                </li>                                                         
            <?php } ?>

                <li class="has-subnav">
                    <a class="" id="a_settings" href="<?php echo site_url('home/settings')?>" target="__blank">
                        <i class="icon-gears nav-icon"></i>
                        <span class="nav-text">My Settings</span>
                        <i class="icon-angle-right"></i>
                    </a>
                </li>
            </ul>

            <ul class="logout">
                <li>
                    <a href="<?= base_url()?>login">
                        <i class="icon-off nav-icon"></i>
                        <span class="nav-text">
                            Logout
                        </span>
                    </a>
                </li>  
            </ul>
        </nav>
        <!-- <section class="sidebar extended"> -->
            <script>
            $(document).ready(function() {
                if ($.cookie('protonSidebar') == 'retracted') {
                    $('.sidebar').removeClass('extended').addClass('retracted');
                    $('.wrapper').removeClass('retracted').addClass('extended');
                }
                if ($.cookie('protonSidebar') == 'extended') {
                    $('.wrapper').removeClass('extended').addClass('retracted');
                    $('.sidebar').removeClass('retracted').addClass('extended');
                }
            });
            </script>
            <!-- <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="clearfix">
                        <img src="<?php echo base_url()?>assets/images/proton-logo.png" alt="proton-logo">
                        <h5>
                            <span class="title">
                                Proton
                            </span>
                            <span class="subtitle">
                                CMS ADMIN PANEL
                            </span>
                        </h5>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="title">
                        <i class="icon-shopping-cart"></i>
                        <span>
                            CATEGORIES
                        </span>
                        <a href="javascript:;" class="add">
                            <i class="icon-plus-sign"></i>
                            <span>
                                ADD NEW
                            </span>
                        </a>
                    </div>
                    <div class="input-group">
                        <input type="text" class="form-control">
                        <div class="input-group-btn">
                            <button class="btn btn-default btn-search" type="button"><i class="icon-search"></i></button>
                        </div>
                    </div>
                </div>
                <div class="panel-body tree-body">
                    <div class="scrollable">
                        <div id="proton-tree"></div>
                    </div>
                </div>
            </div>
            <div class="sidebar-handle">
                <i class="icon-ellipsis-horizontal"></i>
                <i class="icon-ellipsis-vertical"></i>
            </div> -->
        <!-- </section> -->