<section class="wrapper extended scrollable">
    <script>
        if (!($('body').is('.dashboard-page') || $('body').is('.login-page'))){
            if ($.cookie('protonSidebar') == 'retracted') {
                $('.wrapper').removeClass('retracted').addClass('extended');
            }
            if ($.cookie('protonSidebar') == 'extended') {
                $('.wrapper').removeClass('extended').addClass('retracted');
            }
        }
    </script>
           
    <nav class="user-menu">
        <a href="javascript:;" class="main-menu-access">
            <i class="icon-proton-logo"></i>
            <i class="icon-reorder"></i>
        </a>
    </nav>
    
    <section class="title-bar">
        <div>
            <span><?php echo $user->name ?></span>
        </div>
    </section>
    
    <nav class="quick-launch-bar">
        <ul>

            <li>
                <a id="a_home" href="<?php echo site_url('orders/view_orders/all/home')?>">
                    <i class="icon-home"></i>
                    <span>Home</span>
                </a>
            </li>

            <?php if( in_array($user->person_type, array(Person::TYPE_ADMIN, Person::TYPE_USER )) ) {?>
            
            <li>
                <a id="a_orders"  href="<?php echo site_url('orders/new_order')?>">
                    <i class="icon-laptop"></i>
                    <span>Orders</span>
                </a>
            </li>
            <li>
                <a id="a_quotes"  href="<?php echo site_url('quotes/new_order')?>">
                    <i class="icon-laptop"></i>
                    <span>Quotes</span>
                </a>
            </li>
            <li>
                <a id="a_clients" href="<?php echo site_url('clients')?>">
                    <i class="icon-user"></i>
                    <span>Clients</span>
                </a>
            </li>
            
            <?php if( $user->company_id == 11 ){ ?>                                                                            
            <li>
                <a id="a_reports"   href="<?php echo site_url('reports')?>">
                    <i class="icon-bar-chart"></i>
                    <span>Reports</span>
                </a>
            </li>
            
            <?php } ?>
            <li>
                <a id="a_stats"   href="<?php echo site_url('stats')?>"> 
                    <i class="icon-bar-chart"></i>
                    <span>Stats</span>
                </a>
            </li>                                    
            
            <li>
                <a id="a_inspectors"  href="<?php echo site_url('inspectors')?>">
                    <i class="icon-user"></i>
                    <span>Sales Rep</span>
                </a>
            </li>                                                         
            <?php } ?>
            
            <?php if( in_array($user->person_type, array(Person::TYPE_ADMIN, Person::TYPE_USER, Person::TYPE_INSPECTOR )) ) {?>
                <li>
                    <a id="a_schedule"  href="<?php echo site_url('schedule')?>"> 
                        <i class="icon-calendar"></i>
                        <span>Schedule</span>
                    </a>
                </li>
            <?php } ?>
            <?php if( in_array($user->person_type, array(Person::TYPE_ADMIN)) ) {?>                                    
                <li>
                    <a id="a_admin" href="<?php echo site_url('admin')?>">
                        <i class="icon-user"></i>
                        <span>Admin</span>
                    </a>
                </li>
                <li>
                    <a id="a_admin" href="<?php echo site_url('admin/transactions')?>">
                        <i class="icon-money"></i>
                        <span>Transactions</span>
                    </a>
                </li>                                                         
            <?php } ?>                


            <li>
                <a href="<?php echo site_url('home/settings')?>" target="__blank">
                    <i class="icon-gears"></i>
                    <span>My Settings</span>
                </a>
            </li>
        </ul>
        
    </nav>