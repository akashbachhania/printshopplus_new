
        <script src="<?php echo base_url()?>assets/scripts/bootstrap.min.js"></script>

        <!-- Proton base scripts: -->
        
        <script src="<?php echo base_url()?>assets/scripts/main.js"></script>
        <script src="<?php echo base_url()?>assets/scripts/proton/common.js"></script>
        <script src="<?php echo base_url()?>assets/scripts/proton/main-nav.js"></script>
        <script src="<?php echo base_url()?>assets/scripts/proton/user-nav.js"></script>

        <!-- Page-specific scripts: -->
        <script src="<?php echo base_url()?>assets/scripts/proton/sidebar.js"></script>
        <script src="<?php echo base_url()?>assets/scripts/proton/tables.js"></script>
        <!-- jsTree -->
        <script src="<?php echo base_url()?>assets/scripts/vendor/jquery.jstree.js"></script>
        <!-- Data Tables -->
        <!-- http://datatables.net/ -->
            <!-- // <script src="<?php echo base_url()?>assets/scripts/vendor/jquery.dataTables.min.js"></script> -->
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.10/js/jquery.dataTables.js"></script>
        <!-- Data Tables for BS3 -->
        <!-- https://github.com/Jowin/Datatables-Bootstrap3/ -->
        <!-- NOTE: Original JS file is modified -->
            <script src="<?php echo base_url()?>assets/scripts/vendor/datatables.js"></script>
        <!-- Select2 Required To Style Datatable Select Box(es) -->
        <!-- https://github.com/fk/select2-bootstrap-css -->
            <script src="<?php echo base_url()?>assets/scripts/vendor/select2.min.js"></script>

            <!--Custom Scripts-->
            <script src="<?php echo base_url() ?>assets/scripts/oldjs/joblist.js" type="text/javascript"></script>
            <script src="<?php echo base_url() . 'application/views/assets/js/custom.js' ?>" type="text/javascript"></script>
            <script src="<?php echo base_url() . 'application/views/assets/js/bootstrap-datepicker.js' ?>" type="text/javascript"></script>
            <script src="<?php echo base_url() . 'application/views/assets/js/jquery.fancybox.js?v=2.1.5' ?>" type="text/javascript"></script>
            <script src="<?php echo base_url() . 'application/views/assets/js/jquery.timepicker.js' ?>" type="text/javascript"></script>


        <!-- script added from old site -->
             <script src="<?php echo base_url() ?>assets/scripts/oldjs/vpb_uploader.js" type="text/javascript"></script>
            <script src="<?php echo base_url() ?>assets/scripts/oldjs/uploader/fileuploader.js" type="text/javascript"></script>
            <script src="<?php echo base_url() ?>assets/scripts/oldjs/galleria/galleria-1.2.8.js" type="text/javascript"></script>
        


            <script src="<?php echo base_url() ?>application/views/assets/js/quotes.js" type="text/javascript"></script>
            <script src="<?php echo base_url() ?>assets/scripts/oldjs/bootstrap-tooltip.js" type="text/javascript"></script>
            <script src="<?php echo base_url() ?>assets/scripts/oldjs/bootstrap-popover.js" type="text/javascript"></script>
            <script src="<?php echo base_url() ?>assets/scripts/oldjs/bootstrap-modal.js" type="text/javascript"></script>
            <script src="<?php echo base_url() ?>assets/scripts/oldjs/bootstrap-collapse.js" type="text/javascript"></script>
            <script src="<?php echo base_url() ?>assets/scripts/oldjs/estilos.js" type="text/javascript"></script>        
            <script src="<?php echo base_url() ?>assets/scripts/oldjs/popup.js" type="text/javascript"></script>
            <script src="<?php echo base_url() ?>assets/scripts/oldjs/jquery.formatCurrency.all.js" type="text/javascript"></script>
            <script src="<?php echo base_url() ?>assets/scripts/oldjs/jquery.validate.js" type="text/javascript"></script>
            <script src="http://feather.aviary.com/js/feather.js" type="text/javascript"></script>

        <!-- script added from old site -->            

        <script type="text/javascript">
            $(document).ready(function(){
                
                $('#reset_charge_amount').on('click',function(){
                    $('#payment__n').val('');
                });
                    
                $('#c_n').on('click',function(){
                    $('#order__notes').val('');
                    // $('.s_n').css('display','none');
                });

            });   

        </script>
      </body>
</html>
