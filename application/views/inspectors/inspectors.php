<?php  
if( $saved !== null){
    if( $saved === true ){
        $saved = 'alert alert-success';
        $msg   = '<strong>Success: </strong>Your inspector has been saved';                    
    } else if ( $saved === false ) {
        $saved = 'alert alert-error';
        $msg   = '<strong>Error: </strong>Your update has not been saved (Please correct fields with red border)';            
    } else if ( $saved === 'update') {
        $saved = 'alert alert-info';
        $msg   = '<strong>Success: </strong>Your inspector has been updated successfully!';             
    } else if ( $saved == 'booked'){
        $saved = 'alert alert-error';
        $msg   = '<strong>Error: </strong>Inspector already has booked inspection for given timoff period!';        
    }
} else {
    $saved = '';                         
    $msg  = '';
}
/**
* We'll override names
*/
$overrides = array(
    'status_string::name' => 'Status',
    'term::name' => 'Terms',
    'agent::name' => 'Agent',
    'inspector::name' => 'Rep',
    'inspection::name' => 'Inspection',
    'client::phone_1' => 'Phone',
    'company_order_id' => 'ID'
);

$fields = array(
   'name','address','city','state','zip','phone_1','phone_2','email', 'password','timeoff_date_start','timeoff_time_start','timeoff_date_end','timeoff_time_end', 'active','id','company_id','person_type'
);
if( $orders && is_object( $orders['0'] ) && isset( $orders['0']->client )){
    $client = $orders['0']->client;    
}
$options = array(
    1 => 'Active Sales Rep',
    0 => 'Disabled Sales Rep'
)  
?>

<?php $this->load->view('include/header'); ?>

    <div class="row">
        <div class="col-md-5">
            <div class="panel panel-default panel-block" id="div_inspectors_table">
                <div id="data-table" class="panel-heading datatable-heading">
                    <h4 class="section-title">Sales Rep</h4>
                </div>
                <?php echo $this->order_helper->generate_table( $inspectors, array(
                                    'id',
                                    'name'
                                    ), 
                                    $this->table);
                ?>
                <a id="btn_new_person" href="<?php echo site_url('inspectors/add_new/Person')?>" target="__blank" class="btn btn-info" style="margin:10px 0 10px 10px">Add new</a>
            </div>    
        </div>

        <div class="col-md-7">
            
                
            <?php if( $saved ){ ?>
                        <div id="div_order_added" class="<?php echo $saved ?>">
                                <a class="close" data-dismiss="alert">�</a>
                                <?php echo $msg ?>
                        </div>
                    <?php } ?>                    
                    <?php 
                        if( $selected_inspector ){ ?>
                        <div class="panel panel-default panel-block">                
                            <div id="data-table" class="panel-heading datatable-heading">
                                <h4 class="section-title">Sales Rep details: <?= $selected_inspector->name ?></h4>
                            </div>
                            <div class="list-group">
                                <div class="list-group-item">
                                    <?php
                                $times = array(''=>'') + $times;  
                                echo $this->order_helper_new->get_object_form( $selected_inspector, $fields, $this->table, 
                                    array(
                                        'active' => array( 
                                            'element'  => Order_helper::ELEMENT_DROPDOWN, 
                                            'options'  => $options,
                                            'selected' => $selected_inspector->active ),
                                        'timeoff_date_start' => array(
                                            'date' => true,
                                            'inline' => 'timeoff_time_start',
                                            'class'  => 'input-small'
                                        ),
                                        'timeoff_time_start' => array( 
                                            'element'  => Order_helper::ELEMENT_DROPDOWN, 
                                            'options'  => $times,
                                            'selected' => $selected_inspector->timeoff_time_start,
                                            'class'    => 'input-small inline'  ),
                                        'state'=> array('element'=> Order_helper::ELEMENT_DROPDOWN,
                                            'options'=>  $states,
                                            'selected'=> $selected_inspector->state ),                                                                            
                                        'timeoff_date_end' => array(
                                            'date' => true,
                                            'inline' => 'timeoff_time_end',
                                            'class'  => 'input-small'                                        
                                        ),
                                        'timeoff_time_end' => array(    
                                            'element'  => Order_helper::ELEMENT_DROPDOWN, 
                                            'options'  => $times,
                                            'selected' => $selected_inspector->timeoff_time_end,
                                            'class'  => 'input-small inline' ),
                                                                    
                                        '__form'   => array(
                                            'action' => site_url('inspectors'), 
                                            'submit_value' => 'Save',
                                            'submit_name'  => 'save_inspector'
                                            )
                                        )
                                ); ?>  
                                  </div>
                            </div>
                        </div>

                          <?php  }
                        unset($order_fields['id']);
                        $order_fields = array('company_order_id') + $order_fields;
                        ?>          
                            
                   <div class="panel panel-default panel-block">
                        <div id="data-table" class="panel-heading datatable-heading">
                            <h4 class="section-title"><?php echo ($selected_inspector) ? 'Orders for '.$selected_inspector->name : 'Sales Rep Orders'; ?></h4>
                        </div>
                        <?php echo $this->order_helper->get_order_table( $orders, $order_fields, $this->html_table, $overrides );?>
                   </div>                          
            
        </div>
    </div>

    <div class="row">
        <div class="col-lg-5"></div>
        <div class="col-lg-7">


        </div>
    </div>

</section>
<script type="text/javascript">
    $(document).ready(function(){
        $('table').attr('id','tableSortable');
        $('.form-group label').removeAttr('style');
        $('.form-group label').before('<div class="col-lg-1"></div>');
        $('.form-group label').removeClass('col-lg-4');
        $('.form-group label').addClass('col-lg-2');
    });
</script>