<?php                       
    if( $saved !== null){
        if( $saved === true ){         
            $saved = 'alert alert-success';
            $msg   = '<strong>Success: </strong>Your order has been saved';                    
        } else if ( $saved === false ) {
            $saved = 'alert alert-error';
            $msg   = '<strong>Error: </strong>Your order has not been saved (Please correct fields with red border)';            
        } else if ( $saved === 'update') {
            $saved = 'alert alert-info';
            $msg   = '<strong>Success: </strong>Your order has been updated successfully!';             
        } else if ( $saved === 'inspector_taken'){
            $saved = 'alert alert-error';
            $msg   = '<strong>Error: </strong>I\'m sorry, '.$order->inspector->name.' is already booked for that time.  Please select another time or another inspector!';             
        } else if( $saved === 'email-sent'){
            $saved = 'alert alert-success';
            $msg   = '<strong>Success: </strong>Emails have been sent';             
        } else if( $saved === Authorize_payment::STATUS_FAILED ){
            $saved = 'alert alert-error';
            if( $order->errors ) {
                $msg   = '<strong>Error: </strong>'.implode($order->errors);
                $order->errors = array();     
            } else {
                $msg   = '<strong>Error: </strong>Transaction could not be performed at this time'; 
            }           
        } else if( $saved === Authorize_payment::STATUS_SUCCESS ){
            $saved = 'alert alert-success';
            $msg   = '<strong>Success: </strong>Credit card has been charged';
            $order->errors = array();             
        }
    } else {
        $saved = '';                         
        $msg  = '';
    }
?>      
            <form id="form_order" action="<?php echo site_url('orders/new_order')?>" method="post">
                <div class="row">
                    <div id="div_order_details" class="span12">
                    <?php if( $saved ){ ?>
                        <div id="div_order_added" class="<?php echo $saved ?>">
                                <a class="close" data-dismiss="alert">�</a>
                                <?php echo $msg ?>
                        </div>
                    <?php } ?>                      
                        <?php  echo  $this->order_helper->get_object_form(
                                        $order, 
                                        array('order_date','DueDate','Sales_Rep','terms','status'), 
                                        Order_helper::FORM_INLINE,
                                        array(
                                        'order_date'      => array(
                                            'date' => true,
                                        ),
                                        'DueDate' => array(
                                            'date' => true,
                                        ),
                                        'Sales_Rep' =>
                                             array('element' => Order_helper::ELEMENT_DROPDOWN,
                                                   'options' => $salesrep,
                                                   'selected'=> $order->salesrep ),                                                                               
                                        'terms' => 
                                            array('element'=> Order_helper::ELEMENT_DROPDOWN,
                                                  'options'=> $terms,
                                                  'selected'=>$order->terms ),
                                        'status'=>
                                            array('element'=> Order_helper::ELEMENT_DROPDOWN,
                                                  'options'=> $status,
                                                  'selected'=>$order->status ),
                                        '__legend' => 'Order details'
                                             
                                    )
                                 );?>
                    </div>
                </div> 
                <div id ="div_client_details" class="row">                                             
                    <div class="span6" id="div_order_client">
                        <?php  echo $this->order_helper->get_object_form($order, array('agent::name','client::name','client::address_1','client::address_2','client::state',	'client::city','client::zip','client::phone_1','client::phone_2','client::email','client::how_did_you_hear_about_us','client::shipping','client::id',) ,null, 
                                array(
                                  'agent::name' => array( 
                                        'element'  => Order_helper::ELEMENT_DROPDOWN, 
                                        'options'  => $agents,
                                        'selected' => (int)$order->agent->id ),
                     
                                  'client::state' => array( 
                                        'element'  => Order_helper::ELEMENT_DROPDOWN, 
                                        'options'  => $us_states,
                                        'selected' => $order->client->state ),                                        
                                  'client::shipping' => array( 
                                        'element'  => Order_helper::ELEMENT_DROPDOWN, 
                                        'options'  => $shipping,
                                        'selected' => $order->shipping ),                                        
                                  '__legend' => 'Client details'),
                                  array('client'=>'client'),
                                  array('client::name' => 'name')
                                );
                        ?>
                    </div>
                    <div class="span6" id="div_order_inspection">
                        <?php echo $this->order_helper->get_object_form($order, array('Job Name','stock','colors','size','quantity','finishing','coating','Others','notes_inspection_details'), null,
                              array(
                                    'notes_inspection_details' => 
                                        array('element'=>Order_helper::ELEMENT_TEXTAREA,
                                              'rows'=>6),
                                    'pool' => 
                                        array('element'=>Order_helper::ELEMENT_CHECKBOX),                                        
                                    'spa' => 
                                        array('element'=>Order_helper::ELEMENT_CHECKBOX),                                                                               
                                    'state' => array( 
                                        'element'  => Order_helper::ELEMENT_DROPDOWN, 
                                        'options'  => $us_states,
                                        'selected' => $order->state ),                                        
                                    '__legend'=>'Production details')
                        ); ?>
                    </div>
                </div>                    
				<?php /*
                <div id="div_agent" class="row">
                    <div id="div_agent_details" class="span6">
                        <?php echo $this->order_helper->get_object_form($order->agent, array('name','company','address','city','state','zip','phone_1','phone_2','email','id'),null, 
                                array(
                                  'state' => array( 
                                        'element'  => Order_helper::ELEMENT_DROPDOWN, 
                                        'options'  => $us_states,
                                        'selected' => $order->agent->state ),                                        
                                  '__legend' => 'Agent details'
                                ),
                                'agent');?>
                    </div>
                    <div id="div_ins_details"class="span6">
                    <?php echo $this->order_helper->get_object_form(
                                        $order, 
                                        array('type_of_inspection',
                                              'inspector_id',
                                              'type_of_structure',
                                              'estimated_age',
                                              'estimated_inspection_time',
                                              'type_of_foundation',
                                              'square_footage',
                                              'type_of_utilities',                                              
                                              'utility_status',
                                              'report_due',
                                              'notes_order',
                                              'id'),
                                         null,
                                         array(
                                            'notes_order' => array(
                                                'element' => Order_helper::ELEMENT_TEXTAREA,
                                            ),
                                            'type_of_inspection' => array(
                                                'element' => Order_helper::ELEMENT_DROPDOWN,
                                                'options' => $inspection_types,
                                                'selected'=> $order->type_of_inspection,
                                            ),
                                            'inspector_id' => array(
                                                'element' => Order_helper::ELEMENT_DROPDOWN,
                                                'options' => $inspectors,
                                                'selected'=> $order->inspector_id,
                                            ),                                            
                                            'type_of_structure' => array(
                                                'element' => Order_helper::ELEMENT_DROPDOWN,
                                                'options' => $structure_types,
                                                'selected'=> $order->type_of_structure,
                                            ),
                                            'estimated_age' => array(
                                                'element' => Order_helper::ELEMENT_DROPDOWN,
                                                'options' => $estimated_ages,
                                                'selected'=> $order->estimated_age,
                                            ),                                            
                                            'type_of_utilities' => array(
                                                'element' => Order_helper::ELEMENT_DROPDOWN,
                                                'options' => $utility_types,
                                                'selected'=> $order->type_of_utilities,
                                            ),
                                            'type_of_foundation' => array(
                                                'element' => Order_helper::ELEMENT_DROPDOWN,
                                                'options' => $foundation_types,
                                                'selected'=> $order->type_of_foundation,
                                            ),                                             
                                            'estimated_inspection_time' => array(
                                                'element' => Order_helper::ELEMENT_DROPDOWN,
                                                'options' => $inspection_times,
                                                'selected'=> $order->estimated_inspection_time,
                                            ),
                                            '__legend' =>'Additional inspection details'                                                                                                                                                                      
                                         )     
                                              
                                     );
                    ?>                                    
                    </div> 
                </div>
				<?php */ ?>
				<div class="row">
                    <div id="div_order_notes" class="span12">
                    <?php echo $this->order_helper->get_object_form($order, array('order_notes'), null,
                              array(
                                    'order_notes' => 
                                        array('element'=>Order_helper::ELEMENT_TEXTAREA,
                                              'rows'=>6),                                        
                                    '__legend'=>'Order Notes')
                        ); ?>
                    </div>
                </div> 
                <div id="id_div_items" class="row">
                    <div class="span12 well">
                        <legend>Charges</legend>
                        <div name="controls" id="div_order_items">
                        
                        <?php 
                             
                            $this->table->set_heading(
                                'Item',
                                'Description',
                                'Price',                
                                ''                
                            );
                            
                            $count = ($item_count = count( $order->items )) ? $item_count : 3;
                            
                            
                            for( $i=0;$i<=$count;$i++){
                                //Use items if available
                                $description = '';
                                $price       = '';
                                $selected    = '';
                                if( isset( $order->items[$i])){
                                    $description = $order->items[$i]->description;
                                    $price       = $order->items[$i]->price;
                                    $selected    = $order->items[$i]->item_id;
                                }
                                $this->table->add_row(
                                    form_dropdown( 'item_item_id_' .$i, $items, $selected ),
                                    $this->order_helper->get_text_field( 'item_description_' . $i, 'item_description_' .$i, $description, '' ),
                                    $this->order_helper->get_text_field( 'item_price_' . $i, 'item_price_' . $i, $price, '' ),
                                    '<i class="icon-remove-sign"></i>' 
                                );
                            }
                            echo $this->table->generate();
                        ?>
                        <a id="add_item_row" class="btn"><i class="icon-plus"></i>Add rows</a>            
                        </div>
                    </div>                
                </div>
                <div class="row">
                    <?php if( $payment_gateway ){?>
                    <div name="controls" id="div_id_charge" class="span7">
                        <div class="well">           
                            <fieldset>
                                 <legend><i name="icon_collapse">&nbsp;&nbsp;</i>Receive payment</legend>
                                 <table id="table_charge">
                                    <tr>
                                        <td>
                                             <div><img class="cc_image"  src="<?php echo base_url('application/views/assets/img/cc_cirius.png')?>"/></div> 
                                             <div><img class="cc_image"  src="<?php echo base_url('application/views/assets/img/cc_master.png')?>"/></div> 
                                             <div><img class="cc_image"  src="<?php echo base_url('application/views/assets/img/cc_amex.png')?>"/></div> 
                                             <div><img class="cc_image"  src="<?php echo base_url('application/views/assets/img/cc_visa.png')?>"/></div> 
                                        </td>
                                        <td>
                                            <img   class="cc_image" src="<?php echo base_url('application/views/assets/img/credit-cards-icon.png')?>"/><br />
                                        </td>
                                        <td>
                                            <div name="controls cc_image">
                                            <div class="control-group">
                                                <div class="controls">
                                                </div>
                                                <div class="control-group">
                                                    <div class="controls">
                                                            <input id="charge_amount" type="text" value="<?php echo $order->amount_due ?>" name="amount">
                                                    </div>
                                                </div>
                                            </div>                                        
                                            <a href="#" class="btn class="input-small"" id="btn_charge">Charge this amount</a> 
                                        </td>
                                    </tr>
                                 </table>
                            </fieldset>
                         </div>                         
                    </div>
                    <?php } ?>
                    <div name="controls" id="div_id_calculation" class="<?php echo !$payment_gateway ?'offset7 ':''?>span5">
                    <?php
                        echo $this->order_helper->get_object_form($order, array('subtotal', 'tax', 'shipping', 'total' ,'paid', 'amount_due' ),null, 
                                    array(
                                      'tax_type' => array(
                                            'prepend' => '%',
                                            'element' => Order_helper::ELEMENT_DROPDOWN, 
                                            'options' => $taxes ,
                                            'selected' => $order->tax_type ),
                                      'subtotal' => array(
                                        'class' => 'input-small',
                                        'prepend' => '$'
                                      ),
                                      'tax' => array(
                                        'class' => 'input-small',
                                        'prepend' => '%', 
                                      ),
									  'shipping' => array(
                                        'class' => 'input-small',
                                        'prepend' => '$', 
                                      ),
                                      'total' => array(
                                        'class' => 'input-small',
                                        'prepend' => '$', 
                                      ),
                                      'paid' => array(
                                        'class' => 'input-small',
                                        'prepend' => '$', 
                                      ),
                                      'amount_due' => array(
                                        'class' => 'input-small',
                                        'prepend' => '$', 
                                      ),
                                      '__legend'=> 'Calculation'                                                                                                      
                                    ));
                    ?>
                    </div>
                </div>               
                <div class="row">
                    <div class="form-inline span5 offset3" id="div_order_submit">
                        <input id="save"   class="btn" type="submit" name="btn_save_order"   value="Save"/>
                        <input id="print"  class="btn" type="submit" name="btn_print_order"  value="Print"/>
                        <input id="email_order"  class="btn" type="submit" name="btn_email_order"  value="Email"/>
                        <input id="cancel" class="btn" type="submit" name="btn_cancel_order" value="Cancel"/>
                        <?php if ( $user->company_id == 11 ) {?>
                        <input id="report" class="btn" type="submit" name="btn_report_order" value="Report"/>
                        <?php } ?>
                        <input id="delete" class="btn" type="submit" name="btn_delete_order" value="Delete"/>                        
                    </div>        
                </div>                                
                <div class="modal hide fade" id="alert_modal">
                    <div class="modal-header">
                        <a class="close" data-dismiss="modal">�</a>
                        <h3 id="h_modal_header">Modal header</h3>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-block alert-error">
                            <p id="p_modal_text">One fine body�</p>
                        </div>
                        
                    </div>
                    <div class="modal-footer">
                        <a data-dismiss="modal" href="#" class="btn">Close</a>
                    </div>
                </div>
                <div class="modal hide fade" id="delete_modal">
                    <div class="modal-header">
                        <a class="close" data-dismiss="modal">�</a>
                        <h3 id="h_delete_header"></h3>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-block alert-error">
                            <p id="p_delete_text">One fine body�</p>
                        </div>
                        
                    </div>
                    <div class="modal-footer">
                        <a id="delete_cancel" data-dismiss="modal" href="#" class="btn">Cancel</a>
                        <a id="delete_yes"   href="<?php echo site_url('orders/delete/' . $order->id)?>" class="btn">Yes</a>
                    </div>
                </div>                                                        
            </form>
            <div class="modal hide fade" id="charge_modal">
                <div class="modal-header">
                    <a class="close" data-dismiss="modal">�</a>
                    <h3 id="h_modal_header">Charge Credit Card</h3>
                </div>
                <div class="modal-body">
                    <div id="div_order_charged">
                            <a class="close" data-dismiss="alert">�</a>
                            <span></span>
                    </div>                
                    <form class="form-horizontal" id="form_cc" action="<?php echo site_url('orders/charge')?>" method="post">            
                        <fieldset>
                             <div name="controls">
                                <div class="control-group">
                                    <label class="control-label" for="subtotal">Charge Amount:</label>
                                    <div class="controls">                                         
                                        <div class="input-prepend">
                                            <span class="add-on">$</span>
                                            <input id="amount" class="input-small" type="text" value="<?php echo $order->amount_due ?>" name="amount">
                                            <span  name="span_help"     class="help-inline hidden"></span>
                                        </div>
                                    </div>                                  
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="subtotal">CC Number:</label>                         
                                    <div class="controls">                           
                                         <input id="cc_num" class="input_charge" type="text" value="" name="cc_num">
                                         <span  name="span_help"     class="help-inline hidden"></span>                                           
                                    </div>
                                </div>                                
                                <div class="control-group">
                                    <label class="control-label" for="subtotal">Expire Date:</label>                          
                                    <div class="controls">                           
                                         <input id="cc_date" class="input_charge" type="text" value="" name="cc_date">
                                         <span  name="span_help"     class="help-inline hidden"></span>                                          
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="subtotal">Security code:</label>                          
                                    <div class="controls">                           
                                         <input id="cc_cvn" class="input_charge" type="text" value="" name="cc_cvn">
                                         <span  data-placement="top" rel="tooltip"  data-original-title="help help help" name="span_help"   class="help-inline hidden">Whats this?</span>                                          
                                    </div>
                                </div>                                
                                <div class="control-group">
                                    <label class="control-label" for="subtotal">Billing address:</label>                          
                                    <div class="controls">                           
                                         <select id="select_fill_in"  class="input_charge">
                                            <option value="1">No billing address</option>
                                            <option value="2">Copy from Agent</option>
                                            <option value="3">Copy from Client</option>
                                            <option value="4">Copy from Meet With</option>
                                            <option value="5">Manual fill in</option>
                                         </select>
                                         <span  data-placement="top" rel="tooltip"  data-original-title="help help help" name="span_help"   class="help-inline hidden">Whats this?</span>                                          
                                    </div>
                                </div>                                 
                                <div class="control-group fill_in">
                                    <label class="control-label" for="subtotal">First Name</label>                          
                                    <div class="controls">                           
                                         <input id="cc_date" class="input_charge" type="text" value="" name="first_name">
                                         <span  data-placement="top" rel="tooltip"  data-original-title="help help help" name="span_help"   class="help-inline hidden">Whats this?</span>                                          
                                    </div>
                                </div>
                                <div class="control-group fill_in">
                                    <label class="control-label" for="subtotal">Last Name</label>                          
                                    <div class="controls">                           
                                         <input id="cc_date" class="input_charge" type="text" value="" name="last_name">
                                         <span  data-placement="top" rel="tooltip"  data-original-title="help help help" name="span_help"   class="help-inline hidden">Whats this?</span>                                          
                                    </div>
                                </div> 
                                <div class="control-group fill_in">
                                    <label class="control-label" for="subtotal">Address</label>                          
                                    <div class="controls">                           
                                         <input id="cc_date" class="input_charge" type="text" value="" name="address">
                                         <span  data-placement="top" rel="tooltip"  data-original-title="help help help" name="span_help"   class="help-inline hidden">Whats this?</span>                                          
                                    </div>
                                </div> 
                                <div class="control-group fill_in">
                                    <label class="control-label" for="subtotal">City</label>                          
                                    <div class="controls">                           
                                         <input id="cc_date" class="input_charge" type="text" value="" name="city">
                                         <span  data-placement="top" rel="tooltip"  data-original-title="help help help" name="span_help"   class="help-inline hidden">Whats this?</span>                                          
                                    </div>
                                </div>
                                <div class="control-group fill_in">
                                    <label class="control-label" for="subtotal">Zip</label>                          
                                    <div class="controls">                           
                                         <input id="cc_date" class="input_charge" type="text" value="" name="zip">
                                         <span  data-placement="top" rel="tooltip"  data-original-title="help help help" name="span_help"   class="help-inline hidden">Whats this?</span>                                          
                                    </div>
                                </div>
                                <div class="control-group fill_in">
                                    <label class="control-label" for="subtotal">State</label>                          
                                    <div class="controls">                           
                                         <?php echo form_dropdown('state', $us_states, '', 'class="input_charge"' )?>
                                         <span  data-placement="top" rel="tooltip"  data-original-title="help help help" name="span_help"   class="help-inline hidden">Whats this?</span>                                          
                                    </div>
                                </div>
                                <div class="control-group fill_in">
                                    <label class="control-label" for="subtotal">Country</label>                          
                                    <div class="controls">                           
                                         <input id="cc_date" class="input_charge" type="text" value="" name="country">
                                         <span  data-placement="top" rel="tooltip"  data-original-title="help help help" name="span_help"   class="help-inline hidden">Whats this?</span>                                          
                                    </div>
                                </div>                                                                                                                                                                                                                                                                                                                             
                             </div>
                        </fieldset>
                        <input type="hidden" name="order_id" value="<?php echo $order->id?>"/>
                     </div>                    
                </form>
                <div class="modal-footer">
                    <a href="#" class="btn" id="btn_charge_modal">Charge</a>
                    <a data-dismiss="modal" href="#" class="btn">Close</a>
                </div>
            </div>