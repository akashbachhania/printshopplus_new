    <?php

    if( isset( $fluid_layout ) && $fluid_layout == true ){

        $class = 'container-fluid';

    } else {

        $class = 'container';

    }

    

if( $orders )  {

    

    $total_orders = count( $orders );

    $total_paid   = 0;

    $total_due    = 0;

    $total_grand  = 0;

    

    $orders_agent = array();

    $orders_inspector = array();

    $orders_city = array();

    foreach( $orders as $order ){

        $total_paid  += $order->paid;

        $total_due   += $order->amount_due;

        $total_grand += $order->total;

        

        @$orders_agent[ $order->agent->name ]++;

        @$orders_inspector[ $order->inspector->name ]++;

        @$orders_city[ $order->city ]++;

        

    }

}    

?>



<!DOCTYPE html>

<html>

    <head>

        <title>Print Shop Plus</title>

        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'application/views/assets/style/bootstrap.css' ?>"/>

        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'application/views/assets/style/docs.css' ?>"/>

        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'application/views/assets/style/prettify.css' ?>"/>

        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'application/views/assets/style/datepicker.css' ?>"/>

        <script src="<?php echo base_url() . 'application/views/assets/js/jquery.js' ?>" type="text/javascript"/></script>

        <script src="<?php echo base_url() . 'application/views/assets/js/bootstrap-datepicker.js' ?>" type="text/javascript"/></script>

        <script src="<?php echo base_url() . 'application/views/assets/js/jquery.dataTables.js' ?>" type="text/javascript"/></script>        

        <script src="<?php echo base_url() . 'application/views/assets/js/custom.js' ?>" type="text/javascript"/></script>

        <script src="<?php echo base_url() . 'application/views/assets/js/bootstrap-tooltip.js' ?>" type="text/javascript"/></script>

        <script src="<?php echo base_url() . 'application/views/assets/js/bootstrap-popover.js' ?>" type="text/javascript"/></script>

        <script type="text/javascript" src="https://www.google.com/jsapi"></script>

        <script type="text/javascript">



          // Load the Visualization API and the piechart package.

          google.load('visualization', '1.0', {'packages':['corechart']});



          // Set a callback to run when the Google Visualization API is loaded.

          google.setOnLoadCallback(drawChart);



          // Callback that creates and populates a data table,

          // instantiates the pie chart, passes in the data and

          // draws it.

          function drawChart() {

            <?php 

                $summaries = array(

                    'orders_city',

                    'orders_agent',

                    'orders_inspector'

                );

                

                $divs = array(

                    'orders_city'      => 'chart_city',

                    'orders_agent'     => 'chart_agent',

                    'orders_inspector' => 'chart_inspector',

                );

                

                foreach( $summaries as $summary ){

                ?>

                // Create the data table.

                var data = new google.visualization.DataTable();

                data.addColumn('string', 'Topping');

                data.addColumn('number', 'Slices');

                data.addRows([

                <?php

                    foreach( $$summary as $title=>$value ){

                        echo "['$title', $value],";

                    }

                ?>  

                  

                ]);



                // Set chart options

                var options = {'title':'',

                               'width':400,

                               'height':300};



                // Instantiate and draw our chart, passing in some options.

                var chart = new google.visualization.PieChart(document.getElementById('<?php echo $divs[ $summary ]?>'));

                chart.draw(data, options);

                <?php } ?>

          }

        </script>

    </head>

    <body>

        <ul class="nav nav-tabs">

            <li><a class="btn btn-primary" href="<?php echo site_url('home')?>"><i class="icon-home icon-white"></i> Home</a></li>

            <li><a class="btn btn-primary" href="<?php echo site_url('orders/new_order')?>"><i class="icon-list-alt icon-white"></i> Orders</a></li>

            <li><a class="btn btn-primary" href="<?php echo site_url('clients')?>"><i class="icon-user icon-white"></i> Clients</a></li>

            <li><a class="btn btn-primary" href="<?php echo site_url('schedule')?>"><i class="icon-time icon-white"></i> Schedule</a></li>

            <li><a class="btn btn-primary" href="<?php echo site_url('stats')?>"><i class="icon-signal icon-white"></i> Stats</a></li>

            <li><a class="btn btn-primary" href=""><i class="icon-eye-open icon-white"></i> Sales Rep</a></li>

            <li><a class="btn btn-primary" href=""><i class="icon-file icon-white"></i> Reports</a></li>

        </ul>

        <div class="<?php echo $class ?>">

            <section id="gridSystem">

