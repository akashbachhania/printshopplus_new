<?php

if( $orders )  {
    
    $total_orders = count( $orders );
    $total_paid   = 0;
    $total_due    = 0;
    $total_grand  = 0;
    $total_tax    = 0;
    $shipping_amount = 0;
    
    $orders_inspector = array();
    $orders_clients = array();
    $orders_items = array();
    foreach( $orders as $order ){
        $total_paid  += $order->paid;
        $total_due   += $order->amount_due;
        $total_grand += $order->total;
        if($order->tax > 0) {
            $total_tax   += ($order->total / 100) * $order->tax;
        }
        $shipping_amount += $order->shipping_amount;
        
        @$orders_inspector[ $order->inspector->name ]++;
        @$orders_clients[ $order->client->name ]++;
        
        if(is_array($order->items)) {
            foreach ($order->items as $item)
                @$orders_items[ $item->name ]++;
        }
       
    }
    $total_tax = ($total_tax > 0) ? round($total_tax,2) : $total_tax;
    $total_grand = ($total_grand > 0) ? round($total_grand,2) : $total_grand;
    $total_paid = ($total_paid > 0) ? round($total_paid,2) : $total_paid;
    $total_due = ($total_due > 0) ? round($total_due,2) : $total_due;
    $shipping_amount = ($shipping_amount > 0) ? round($shipping_amount,2) : $shipping_amount;
}    
$this->load->view('include/sidebar');
?>
<?php if( $orders )  { ?>

        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
        <script type="text/javascript">

          // Load the Visualization API and the piechart package.
          google.load('visualization', '1.0', {'packages':['corechart']});

          // Set a callback to run when the Google Visualization API is loaded.
          google.setOnLoadCallback(drawChart);

          // Callback that creates and populates a data table,
          // instantiates the pie chart, passes in the data and
          // draws it.
          function drawChart() {
            var data;
            var options;
            var chart;
            
            // Create the data table.
            data = new google.visualization.DataTable();
            data.addColumn('string', 'Topping');
            data.addColumn('number', 'Slices');
            data.addRows([
                    ['Paid', <?php echo $total_paid ?>],
                    ['Unpaid', <?php echo $total_due ?>],
            ]);
            options = {'title':'Payment of orders',
                           'width':380,
                           'height':300};

            // Instantiate and draw our chart, passing in some options.
            chart = new google.visualization.PieChart(document.getElementById('chart_payment'));
            chart.draw(data, options);                        
            <?php 
                $summaries = array(
                    'orders_clients',                    
                    'orders_inspector',
                    'orders_items',
                );
                
                $divs = array(
                    'orders_clients'      => 'chart_clients',
                    'orders_inspector' => 'chart_inspector',
                    'orders_items'     => 'chart_items',
                );
                $titles = array(
                    'orders_clients'      => 'Orders by client',
                    'orders_inspector' => 'Orders per Sales Rep',
                    'orders_items'     => 'Orders by item',
                );                
                foreach( $summaries as $summary ){
                    if( !$summary ) continue;
                ?>
                // Create the data table.
                data = new google.visualization.DataTable();
                data.addColumn('string', 'Topping');
                data.addColumn('number', 'Slices');
                data.addRows([
                <?php
                
                    foreach( $$summary as $title=>$value ){
                        echo "['$title', $value],";
                    }
                ?>  
                  
                ]);

                // Set chart options
                options = {'title':'<?php echo $titles[ $summary ] ?>',
                               'width':380,
                               'height':300};

                // Instantiate and draw our chart, passing in some options.
                chart = new google.visualization.PieChart(document.getElementById('<?php echo $divs[ $summary ]?>'));
                chart.draw(data, options);
                <?php } ?>
          }
        </script>
        <?php }?>

<?php $this->load->view('include/header'); ?>

    <div class="row">
        <div class="col-lg-5">
            <div class="panel panel-default panel-block">
                <div class="panel-heading">
                    <h4>Stats filter</h4>
                </div>
                <div class="list-group">
                    <div class="list-group-item">
                        <form class="form-horizontal well" action="<?php site_url("stats/show_stats")?>" method="post" style="background:transparent;border:0">


                                    <div class="form-group row">
                                        <label class="control-label col-lg-4" for="start_date">Date from:</label>
                                        <div class="controls col-lg-5">
                                            <input  data-datepicker="datepicker" type="text" class="form-control" name="start_date" id="start_date" value="<?php echo isset($start_date) ? $start_date : '' ?>">
                                        </div>
                                    </div>
                                    <div class="form-group row"> 
                                        <label class="control-label col-lg-4" for="end_date">Date to:</label>
                                        <div class="controls col-lg-5">
                                            <input  data-datepicker="datepicker" type="text" class="form-control" name="end_date" id="end_date" value="<?php echo isset($end_date) ? $end_date : '' ?>">
                                        </div>
                                    </div>
                                   <div class="form-group row">                         
                                        <label class="control-label col-lg-4" for="person_type">Filter</label>
                                        <div class="controls col-lg-5">
                                            <select  class="form-control" id="person_type" name="person_type">
                                                <option value="ALL" <?php echo (isset($person_type) and $person_type == 'ALL') ? 'selected="selected"' : '' ?> >All Orders</option>                                                
                                                <option value="<?php echo Person::TYPE_AGENT; ?>" <?php echo (isset($person_type) and $person_type == Person::TYPE_AGENT) ? 'selected="selected"' : '' ?>>By Client</option>
                                                <option value="<?php echo Person::TYPE_INSPECTOR; ?>" <?php echo (isset($person_type) and $person_type == Person::TYPE_INSPECTOR) ? 'selected="selected"' : '' ?>>By Sales Rep</option>
                                            </select>
                                        </div>
                                   </div>
                                   <div id="div_stat_persons" class="form-group row">                         
                                        <label class="control-label col-lg-4" for="person_id">Name</label>
                                        <div class="controls col-lg-5">
                                            <?php echo form_dropdown('person_id', $persons, $person_id, ' class="form-control" id="person_id" ')?>
                                        </div>
                                   </div>  
                                
                                
                                   <div class="form-group row">
                                        <label class="control-label col-lg-4" for="submit"></label>
                                        <div class="controls col-lg-5">
                                            <input id="submit" name="submit" type="submit" class="form-control btn btn-info" value="Submit">
                                        </div>                                                                                                  
                                   </div>

                        </form>
                    </div>
                </div>
            </div>            
        </div>

        <div class="col-lg-4">
            
            <?php
             
            if( $orders ){
                ?>
            <div class="panel panel-default panel-block">
                <div class="panel-heading">
                    <h4>Orders summary:</h4>
                </div>                
                    <?php
                        $this->table->clear();
                        $this->table->add_row("<strong>Total #</strong>", $total_orders);
                        $this->table->add_row("<strong>Total amount (\$) </strong>", $total_grand);
                        $this->table->add_row("<strong>Total paid (\$)</strong>", $total_paid);
                        $this->table->add_row("<strong>Total due (\$)</strong>", $total_due);
                        $this->table->add_row("<strong>Total Tax (\$)</strong>", $total_tax);
                        $this->table->add_row("<strong>Total Shipping (\$)</strong>", $shipping_amount);
                        //
                        echo $this->table->generate();
                    ?>
                
            </div>
            <div class="panel panel-default panel-block">
                <div id="chart_payment" >
                </div>                
            </div>

                <div id="div_stats_breakdown">
                    <div class="panel panel-default panel-block">
                        <div class="panel-heading">
                            <h4>Orders by client:</h4>
                        </div>
                            <?php
                                $this->table->clear();
                                foreach( $orders_clients as $client=>$orders ){
                                    $this->table->add_row("<strong>$client</strong>", $orders);                                
                                }
                                echo $this->table->generate();
                            ?>
                        
                    </div>
                    <div class="panel panel-default panel-block">
                        <div id="chart_clients">
                        </div>
                    </div>
                    <div class="panel panel-default panel-block">
                        <div class="panel-heading">                    
                            <h4>Orders by Sales Rep:</h4>
                        </div>
                            <?php
                                $this->table->clear();
                                foreach( $orders_inspector as $client=>$orders ){                            
                                    $this->table->add_row("<strong>$client</strong>", $orders);                                                        
                                }
                                echo $this->table->generate();
                            ?>
                        
                    </div>
                    <div class="panel panel-default panel-block">
                        <div id="chart_inspector" >
                        </div>  
                    </div>
                    <div class="panel panel-default panel-block">            
                        <div class="panel-heading">                    
                            <h4>Orders by item:</h4>
                        </div>
                            <?php
                                $this->table->clear();
                                foreach( $orders_items as $client=>$orders ){                           
                                    $this->table->add_row("<strong>$client</strong>", $orders);                                                              
                                }
                                echo $this->table->generate();
                            ?>
                        
                    </div>
                    <div class="panel panel-default panel-block">
                        <div id="chart_items" >
                        </div>  
                    </div>
                </div>
                <?php
            } else {
                echo 'There are no orders matching your criteria';
            }
        ?>
        
        </div>

    </div>

</section>
<script type="text/javascript">
    
    $(document).ready(function(){
        $('.panel-heading').css('padding','10px');
        // $('#start_date').on('click',function(){
        //     $('div.datepicker').css({'top':'39px!important','left':'14px!important'});    
        // });
        
    });

</script>
<style type="text/css">
    .datepicker{
        top:39px!important;
        left: 14px!important;
    }
</style>