<?php
	//$run["runs"]id=$this->uri->segment(3);
	$posturl= isset($orderid)? site_url('orders/update_runorders/'.$orderid) : site_url('orders/update_runorders');
	
?>
<?php foreach($runs as $run){
?>
	<div class="row">
		<div id="div_order_details" class="well">			
			<form id="frmrunorder" action="<?echo $posturl;?>" method="post" enctype="multipart/form-data">
			<input type="hidden" name="runid" value="<?php echo $run["runs"]["id"];?>" />
			<legend>Run #<?php echo $run["runs"]["id"];?></legend>
			<table width="100%">						  
			  <tr>
				<td>Size</td>
				<td><input type="text" name="Size" id="Size_<?php echo $run["runs"]["id"];?>" size="30" value="<?php echo $run["runs"]["size"];?>"></td>
				<td>Paperstock</td>
				<td><input type="text" name="PaperStock" id="PaperStock_<?php echo $run["runs"]["id"];?>" size="30" value="<?php echo $run["runs"]["paperstock"];?>"></td> 				
				<td>Colors</td>
				<td><input type="text" name="Colors" id="Colors_<?php echo $run["runs"]["id"];?>" size="30" value="<?php echo $run["runs"]["colors"];?>"></td>
				<td>Quantity</td>
				<td><input type="text" name="Quantity" size="30" value="<?php echo $run["runs"]["quantity"];?>" id="Quantity_<?php echo $run["runs"]["id"];?>"></td>   
			  </tr>
			  <tr>
				<td>Coating</td>
				<td>
					<select onchange="ChangeCoatingById(this);" id="Coating_<?php echo $run["runs"]["id"];?>"  name="Coating" width="100">
					<?php
					echo $run["runs"]["coating"];
					foreach($coatlist as $items ){
						echo '<option value="'. $items.'" '.($run["runs"]["coating"]==$items?'selected="selected"':"").'>'. $items.'</option>';
					}  
					?>
					
				</td>
				<td>Due Date</td>
				<td><input id="due_date_<?php echo $run["runs"]["id"];?>" class="input-small" type="text" value="<?php echo !empty($run["runs"]["due_date"])&&$run["runs"]["due_date"]!='1970-01-01'?date('m-d-Y', strtotime($run["runs"]["due_date"])):'' ;  ?>" name="due_date" data-datepicker="datepicker"></input></td>    
				<td>Proofs</td>
				<td><input type="text" name="Proofs" id="Proofs<?php echo $run["runs"]["id"];?>" size="30" value="<?php echo $run["runs"]["proofs"];?>"></td>
				<td></td>
				<td></td>    
			  </tr> 
			  <tr>
				<td colspan="8">
					<table width="100%">
						<tr>
							<td>Printing</td>
							<td>Start Date/Time</td>
							<td><input id="printing_start_date<?php echo $run["runs"]["id"];?>" class="input-small" type="text" value="<?php echo !empty($run["runs"]["printing_start_date"])&&$run["runs"]["printing_start_date"]!='1970-01-01'?date('m-d-Y', strtotime($run["runs"]["printing_start_date"])):'' ;  ?>" name="printing_start_date" data-datepicker="datepicker"></input></td>
							<td>
								<input id="printing_start_time<?php echo $run["runs"]["id"];?>" class="input-small timepicker" type="text" value="<?php echo $run["runs"]["printing_start_time"];?>" name="printing_start_time" onfocus="getTime(this.id);"></input>
							</td>
							<td>Operator</td>
							<td><input type="text" name="printing_operator" id="printing_operator<?php echo $run["runs"]["id"];?>" class="input-small" size="30" value="<?php echo $run["runs"]["printing_operator"];?>"></td>
							<td>Sheet Count</td>
							<td><input type="text" name="printing_sheet_count" id="printing_sheet_count<?php echo $run["runs"]["id"];?>" class="input-small" size="30" value="<?php echo $run["runs"]["printing_sheet_count"];?>"></td>
						</tr>
						<tr>
							<td></td>							
							<td>Finish Date/Time</td>
							<td><input id="printing_end_date<?php echo $run["runs"]["id"];?>" class="input-small" type="text" value="<?php echo !empty($run["runs"]["printing_end_date"])&&$run["runs"]["printing_end_date"]!='1970-01-01'?date('m-d-Y', strtotime($run["runs"]["printing_end_date"])):'' ;  ?>" name="printing_end_date" data-datepicker="datepicker"></input></td>
							<td>
								<input id="printing_end_time<?php echo $run["runs"]["id"];?>" class="input-small timepicker" type="text" value="<?php echo $run["runs"]["printing_end_time"];?>" name="printing_end_time" onfocus="getTime(this.id);"></input>
							</td>
							<td colspan="4"></td>
						</tr>
						<tr>
							<td>Coating</td>
							<td>Start Date/Time</td>
							<td><input id="coating_start_date<?php echo $run["runs"]["id"];?>" class="input-small" type="text" value="<?php echo !empty($run["runs"]["coating_start_date"])&&$run["runs"]["coating_start_date"]!='1970-01-01'?date('m-d-Y', strtotime($run["runs"]["coating_start_date"])):'' ;  ?>" name="coating_start_date" data-datepicker="datepicker"></input></td>
							<td>
								<input id="coating_start_time<?php echo $run["runs"]["id"];?>" class="input-small timepicker" type="text" value="<?php echo $run["runs"]["coating_start_time"];?>" name="coating_start_time" onfocus="getTime(this.id);"></input>
							</td>
							<td>Operator</td>
							<td><input type="text" name="coating_operator" id="coating_operator<?php echo $run["runs"]["id"];?>" class="input-small" size="30" value="<?php echo $run["runs"]["coating_operator"];?>"></td>
							<td>Sheet Count</td>
							<td><input type="text" name="coating_sheet_count" id="coating_sheet_count<?php echo $run["runs"]["id"];?>" class="input-small" size="30" value="<?php echo $run["runs"]["coating_sheet_count"];?>"></td>
						</tr>
						<tr>
							<td></td>							
							<td>Finish Date/Time</td>
							<td><input id="coating_end_date<?php echo $run["runs"]["id"];?>" class="input-small" type="text" value="<?php echo !empty($run["runs"]["coating_end_date"])&&$run["runs"]["coating_end_date"]!='1970-01-01'?date('m-d-Y', strtotime($run["runs"]["coating_end_date"])):'' ;  ?>" name="coating_end_date" data-datepicker="datepicker"></input></td>
							<td>
								<input id="coating_end_time<?php echo $run["runs"]["id"];?>" class="input-small timepicker" type="text" value="<?php echo $run["runs"]["coating_end_time"];?>" name="coating_end_time" onfocus="getTime(this.id);"></input>
							</td>
							<td colspan="4"></td>
						</tr>
						<tr>
							<td>Cutting</td>
							<td>Start Date/Time</td>
							<td><input id="cutting_start_date<?php echo $run["runs"]["id"];?>" class="input-small" type="text" value="<?php echo !empty($run["runs"]["cutting_start_date"]) && $run["runs"]["cutting_start_date"]!='1970-01-01'?date('m-d-Y', strtotime($run["runs"]["cutting_start_date"])):'' ;  ?>" name="cutting_start_date" data-datepicker="datepicker"></input></td>
							<td>
								<input id="cutting_start_time<?php echo $run["runs"]["id"];?>" class="input-small timepicker" type="text" value="<?php echo $run["runs"]["cutting_start_time"];?>" name="cutting_start_time" onfocus="getTime(this.id);"></input>
							</td>
							<td>Operator</td>
							<td><input type="text" name="cutting_operator" id="cutting_operator<?php echo $run["runs"]["id"];?>" class="input-small" size="30" value="<?php echo $run["runs"]["cutting_operator"];?>"></td>
							<td>Sheet Count</td>
							<td><input type="text" name="cutting_sheet_count" id="cutting_sheet_count<?php echo $run["runs"]["id"];?>" class="input-small" size="30" value="<?php echo $run["runs"]["cutting_sheet_count"];?>"></td>
						</tr>
						<tr>
							<td></td>
							<td>Finish Date/Time</td>
							<td><input id="cutting_end_date<?php echo $run["runs"]["id"];?>" class="input-small" type="text" value="<?php echo !empty($run["runs"]["cutting_end_date"])&&$run["runs"]["cutting_end_date"]!='1970-01-01'?date('m-d-Y', strtotime($run["runs"]["cutting_end_date"])):'' ;  ?>" name="cutting_end_date" data-datepicker="datepicker"></input></td>
							<td>
								<input id="cutting_end_time<?php echo $run["runs"]["id"];?>" class="input-small timepicker" type="text" value="<?php echo $run["runs"]["cutting_end_time"];?>" name="cutting_end_time" onfocus="getTime(this.id);"></input>
							</td>
							<td colspan="4"></td>
						</tr>
					</table>
				</td>
			  </tr> 
			  <tr>
				<td>Additional Info/Comments</td>
				<td colspan="7">
					<textarea name="AdditionalInfo" id="AdditionalInfo_<?php echo $run["runs"]["id"];?>" rows="5" cols="60" style="width:100%"><?php echo $run["runs"]["additional_info"];?></textarea>
				</td>
			  </tr>
			  <tr>
				<td valign="top">Upload Image</td>
				<td valign="top" colspan="7">
					<input type="file" id="photo_file_<?php echo $run["runs"]["id"];?>" name="photo_file" size="50"><a href="<?php echo site_url('application/run_orders_files/'.$run["runs"]["upload_image"]);?>" class="fancybox"><img style="height:70px !important;" src="<?php echo site_url('application/run_orders_files/'.$run["runs"]["upload_image"]);?>" /></a>
				</td>
			  </tr>
			  <tr>
				<td colspan="8" align="center">
					<div id="div_orders_table" class="well">
                    <legend>Orders</legend>
                    <?php echo $this->order_helper->get_order_tables( $run["orders"], $statuslist, $operatorlist, array(
								'id',
								'edit',
								'client_id',
                                'order_date',								
								'order_notes',
								'report_due',
								'sales_rep',
								'status',
								'Operator'
                          ), 
                          $this->html_table,
                          $overrides);
                    ?>
                </div>
				</td>
			  </tr> 
			  <tr>
				<td colspan="8" align="center">
					<div id="process<?php echo $run["runs"]["id"];?>"></div>
				</td>
			  </tr>
			  <tr>
				<td colspan="8" align="center">
					<input type="submit" value="Update" onclick="return validate_form(<?php echo $run["runs"]["id"];?>);" />&nbsp;&nbsp;<input type="button" value="Delete" onclick="return DeleteRun(<?php echo $run["runs"]["id"];?>);" />&nbsp;&nbsp;<a href="<?php echo site_url('orders/generate_run_pdf/'.$run["runs"]["id"]);?>" target="_blank"><input type="button" value="Pdf Preview" /></a>
				</td>
			  </tr>			  
			</table>
			</form>
		</div>
	</div> 					
	<?php } ?>
<script type="text/javascript">
function getTime(timeId) {	
	if($('#'+timeId).val() == ''){
		var time_t = "";
		var d = new Date();
		var cur_hour = d.getHours();

		(cur_hour < 12) ? time_t = "am" : time_t = "pm";
		(cur_hour == 0) ? cur_hour = 12 : cur_hour = cur_hour;
		(cur_hour > 12) ? cur_hour = cur_hour - 12 : cur_hour = cur_hour;
		var curr_min = d.getMinutes().toString();
		var curr_sec = d.getSeconds().toString();
		if (curr_min.length == 1) { curr_min = "0" + curr_min; }
		if (curr_sec.length == 1) { curr_sec = "0" + curr_sec; }
		document.getElementById(timeId).value=cur_hour + ":" + curr_min + time_t;
	}
}
</script>