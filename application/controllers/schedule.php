<?php
class Schedule extends MY_Controller{
    
    public function __construct(){
   
               
        parent::__construct();
        $this->authorize_or_redirect();
        $this->load->model('Schedule_model','schedule_model', TRUE );                    
    }
    public function index(){  
        
        /**
        * Start date
        */
        $filter = new Filter();
        $filter->set_company_id( $this->user->company_id );
        
        if( !($start_date = $this->input->post('report_due'))){
              $start_date = gmdate('m-d-Y');
              
        }
        
        preg_match('/(?P<month>\d\d)-(?P<day>\d\d)-(?P<year>\d\d\d\d)/', $start_date, $match );
        
        $filter->set_start_date( $start_date );
        $end_date = gmdate( 'Y-m-d', (@mktime(0,0,0, $match['month'], $match['day'], $match['year'])+ 15*24*60*60) );
        $filter->set_end_date( $end_date );
         
        if( $this->user->person_type == Person::TYPE_INSPECTOR) {
            $filter->set_inspector_id( $this->user->id );
            $inspector_id = $this->user->id;
        } else {
            if( $inspector_id = $this->input->post('inspector_id') ){
                $filter->set_inspector_id( $inspector_id );
            }            
        }

        $input_order = new Order();
        $input_order->set_inspector_id( $inspector_id );
        $input_order->set_report_due( $start_date );
        
         
        $orders = $this->schedule_model->get_orders( $filter );
        
        $person_filter = new Person();
        $person_filter->set_company_id( $this->user->company_id );
        $person_filter->set_person_type( Person::TYPE_INSPECTOR );
        
        
        $inspectors = $this->order_model->get_object( $person_filter );
        
         
        $dates = array();
        for( $i=0;$i<11;$i++){
            $dates[] = gmdate('m-d',  @mktime(0,0,0, $match['month'], $match['day'])+ $i*24*60*60 );
            $dows[] = gmdate('l',  @mktime(0,0,0, $match['month'], $match['day'], $match['year'])+ $i*24*60*60 );
        }
        
        $this->load->view('include/sidebar', array('fluid_layout'=>true,'user'=>$this->user, 'companies'=>$this->companies));
        $this->load->view('schedule/schedule', array( 'orders'       => $orders, 
                                                      'input_order'  => $input_order,
                                                      'inspectors'   => $inspectors,
                                                      'user'         => $this->user,
                                                      'dows'         => $dows,
                                                      'dates'   => $dates   ));
                                                      
        $this->load->view('include/footer', array( $orders ));        
    }
    function get_am_pm( $hour ){
        $return = $hour;
        if( preg_match('/(?P<hour>\d+):(?P<minutes>\d\d)/', $hour, $match )){
            if( intval( $match['hour']) > 12 ){
                $suffix = 'PM';
                $hour   = intval( $match['hour']) - 12;
            } else {
                $suffix = 'AM';
                $hour  = $match['hour'];
            }
            $return = $hour . ':' .$match['minutes'] . $suffix;
        }
        return $hour; 
    }
}
