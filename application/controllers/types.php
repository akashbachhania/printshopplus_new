<?php
class Types extends MY_Controller{
    /**
    * New item constants
    */
    const NEW_CLIENT    = '_3';
    const NEW_AGENT     = '_1';
    const NEW_INSPECTOR = '_2';
    
    const NEW_ITEM    = 'new_item';
    const NEW_TAX     = 'new_tax';
    
    const NEW_TERMS         = '__1';
    const NEW_UTILITY       = '__2'; 
    const NEW_FOUNDATION    = '__3';
    const NEW_INSPECTION    = '__4';     
    const NEW_STRUCTURE     = '__5';

    const ADD_NEW = '-Add new-';
        
    public function __construct(){ 
        parent::__construct();
        //This section is only for Admins and Users
        $this->authorize_or_redirect( array(Person::TYPE_USER));
        
        $this->load->model('type_model','type_model');
        
    }
    
    public function index(){
         
        $saved   = null;
        $objects = null;
        
        $selected_type = '0';
        
        $class = $this->uri->segment(3, 0);
        $id    = $this->uri->segment(4, 0);
        
        if( $type = $this->input->post('type_select')){
            if( in_array( $type, array('Person','Base_item','Tax')) || preg_match( '/^Type_\d$/', $type )){
                
                $class = preg_replace('/_\d/', '', $type );
                
                $filter = new $class();
                $filter->set_company_id( $this->user->company_id );
                
                switch( $class ){
                    case 'Type':
                        preg_match('/_(?P<type>\d)/', $type, $match );
                        $filter->set_type( $match['type'] );
                    break;
                } 
                $selected_type = $type;
                $objects = $this->type_model->get_object( $filter );
                
                if( $class == 'Person'){
                    foreach( $objects as $index=>$object ){
                        if( !in_array( $object->person_type, array( Person::TYPE_CLIENT, Person::TYPE_AGENT, Person::TYPE_INSPECTOR))){
                            unset( $objects[$index]);
                        }
                    }                  
                }
            }
        } 
         
      
        $select_array = array(
            '0'      => '-Select item-',
            'Person' => 'Users',
            'Type_' . Type::TYPE_TERM        => 'Terms',
            'Type_' . Type::TYPE_FOUNDATION  => 'Types of foundation',
            'Type_' . Type::TYPE_STRUCTURE   => 'Types of structure',
            'Type_' . Type::TYPE_INSPECTION  => 'Types of inspection',
            'Type_' . Type::TYPE_UTILITY     => 'Types of utilities',
            'Base_item' => 'Charges',
            'Tax' => 'Taxes'        
        );
        /**
        * Load views
        */
        $this->load->view('common/header', array('fluid_layout'=>true,'user'=>$this->user,   'companies'=>$this->companies ));
        $this->load->view('types/types', array( 'objects'       => $objects,
                                                'select_array'  => $select_array,
                                                'selected_type' => $selected_type,
                                                'saved' => $saved,
                                              ));
        $this->load->view('common/footer');         
    }
    public function add_new(){
    
        $object = null;
     
        //Saving new stuff
        if( $this->input->post('save') ){
             
            if( $class = $this->input->post('object_type', true) && in_array( $class, array('Person','Tax','Type','Base_item')) ){
                
                $object = $this->order_mapper->create_object_from_input( $class );
                $object->set_company_id( $this->user->company_id);
                
                if( $object->validate()){
                    $old_id = $object->id;
                    $object->set_id($this->order_model->save_or_update_object( $object ));
                    if( $old_id != $object->id ){
                        $saved = true;    
                    } else {
                        $saved = 'update';
                    }
                    
                } else {
                    $saved = false;
                } 
                $data  = array('object' => $object, 'saved' => $saved );     
            }
            
        } else if( ($this->uri->segment(3,0) == 'show_object') && 
                   ($class  = $this->uri->segment(4, 0)) && 
                   ($id  = $this->uri->segment(5, 0)) ){
            
            if( in_array( $class, array('Person','Tax','Type','Base_item'))){
                $filter = new $class();
                $filter->set_id( $id );
                $filter->set_company_id( $this->user->company_id );
                
                $objects = $this->type_model->get_object( $filter );
                $object  = null;
                if( $objects ){
                    $object = $objects['0'];
                }
                $data  = array('object' => $object, 'saved' => null );
            }
                        
        } else {
            /**
            * User clicked ADD NEW 
            */
            switch( ($type = $this->input->get('type',true) )){
                case self::NEW_FOUNDATION:
                case self::NEW_INSPECTION:
                case self::NEW_STRUCTURE:
                case self::NEW_TERMS:
                case self::NEW_UTILITY:
                     $object = new Type();
                     $object->set_type( str_replace('_','', $type) );
                     $object->set_company_id( $this->user->company_id);
                     $data = array( 'object' => $object );
                     break;
                case self::NEW_ITEM:
                     $item = new Base_item();
                     $item->set_company_id( $this->user->company_id);
                     $data = array( 'object' => $item );
                     break;
                case self::NEW_TAX:
                     $tax = new Tax();
                     $tax->set_company_id( $this->user->company_id);
                     $data = array( 'object' => $tax );
                     break;                     
                case self::NEW_AGENT:                             
                case self::NEW_CLIENT:
                case self::NEW_INSPECTOR:
                     $person = new Person();
                     $person->set_company_id( $this->user->company_id);
                     $person->set_person_type( str_replace('_','', $type) );
                     $data = array( 'object' => $person );
                     break;   
            }
        }
        
        if( $data ){
            $data['action'] = site_url('types/add_new');
        }
        
        $this->load->view('common/header', array('fluid_layout'=>true, 'user'=>$this->user, 'companies'=>$this->companies));             
        $this->load->view('common/add_new', $data );             
        $this->load->view('common/footer');           
    }
    public function show_object(){
        

        
        $this->load->view('common/header', array('fluid_layout'=>true, 'user'=>$this->user, 'companies'=>$this->companies));             
        $this->load->view('types/show_type', $data );             
        $this->load->view('common/footer');                
    }
}
