<?php
class Login extends MY_Controller{
    public function __construct(){ 
        parent::__construct();
    }
    public function index(){
          
        $view = 'login/login';
        $data =  array();
        $data['failed'] = false;
        
        if( ($user = $this->input->post('username')) && ($pass = $this->input->post('password')) ){
            
            $filter = new Person();
            $filter->set_email(strtolower($user));
            $filter->set_password(($pass));

            if( $filter->email && $filter->password ){
                            
                $persons = $this->order_model->get_persons( $filter );

                if( $persons  ){
                    $person = reset( $persons );
                    switch( $person->person_type ){
                        case Person::TYPE_AGENT:                    
                        case Person::TYPE_INSPECTOR:                  
                        case Person::TYPE_USER:
                            $company_filter = new Company();  
                            $company_filter->set_id( $person->company_id );
                            $companies = $this->order_model->get_object( $company_filter );
                            if( $companies ){
                                $company = $companies['0'];
                                
                                if($company->active and $company->expire_company < date('Y-m-d H:i:s')) {
                                    $this->db->where('id',$person->company_id);
                                    $this->db->update('dr_companies',array('active' => 0));
                                    break;
                                }
                                
                                if( $company->active ){
                                    $this->session->set_userdata(array(
                                        'id' => $person->id,
                                        'person_type' => $person->person_type,
                                        'company' => serialize( $company )
                                    ));
                                    header('Location: ' . site_url('orders/view_orders/all'));                                
                                }
                            }
                            break;
                        case Person::TYPE_ADMIN:
                            $this->session->set_userdata(array(
                                'id' => $person->id,
                                'person_type' => $person->person_type,
                            ));
                            header('Location: ' . site_url('orders/view_orders/all'));                                                   
                        break;
                    }  
                }  else {
                    $data['failed'] = true;
                }                
            } else {
                $data['failed'] = true;
            }
        } else {
            $this->session->sess_destroy();
        }        
        $this->load->view( $view, $data );
    }
    function register() {
        
        $this->load->library('form_validation');  
        $email = '';
        if(isset($_POST['stripeToken']) and isset($_POST['stripeEmail'])) {
            $token = @$_POST['stripeToken'];
            $email = @$_POST['stripeEmail'];
            $package = @$_POST['subscription_type'];
            unset($_POST);
            
            require_once(APPPATH.'third_party/strip_lib/Stripe.php');
            $stripe = array(
              "secret_key"      => "sk_live_i3OqfZUCCjaggf07I1MRZNM2",
              "publishable_key" => "pk_live_ztd3vG1VLqd9zOkeNulfAGbD"
            );
            
            Stripe::setApiKey($stripe['secret_key']);
           
            $customer = Stripe_Customer::create(array(
                'email' => $email,
                'card'  => $token
            ));
            
             
            if(isset($customer->id)) {
                $customInfo = Stripe_Customer::retrieve($customer->id); 
                
                $packages = $this->packages();                
                $pack = (isset($packages[$package])) ?  $packages[$package] : $packages['PS1'];                
                $subscription = $customInfo->subscriptions->create(array("plan" => $pack['plan_id']));
                $this->session->set_userdata('user_id_pack',$pack['plan_id']);
                
                if(isset($subscription->id)) {
                    $this->session->set_userdata('user_id_add',$customer->id);
                    $this->session->set_userdata('user_id_sub',$subscription->id);
                }
            }
        }
        
       $cust_id = $this->session->userdata('user_id_add');
       $sub_id  = $this->session->userdata('user_id_sub');
       $pack_id  = $this->session->userdata('user_id_pack');
        
        $this->form_validation->set_rules('company_name', 'Company Name', 'trim|required');
        $this->form_validation->set_rules('your_full_name', 'Your Full Name', 'trim|required');
        $this->form_validation->set_rules('email_text', 'Email', 'trim|required|valid_email|is_unique[dr_persons.email]|is_unique[dr_companies.email]');
        $this->form_validation->set_rules('address_text', 'Address', 'trim|required');
        $this->form_validation->set_rules('city_text', 'City', 'trim|required');
        $this->form_validation->set_rules('zip_text', 'Zip', 'trim|required');
        $this->form_validation->set_rules('state_text', 'State', 'trim|required');
        $this->form_validation->set_rules('phone_text', 'Phone', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|matches[re_password]');
        $this->form_validation->set_rules('re_password', 'Retype Password', 'trim|required');
        
        if($this->form_validation->run()== FALSE || $cust_id == '' || $sub_id == '') { 
            
            $error = '';
            if($cust_id == '' || $sub_id == '') {
                $error = 'Payment Error.';
            }
            
            $data = array();
            $data['email'] = $email;
            $data['error'] = $error;
            $data['states'] = $this->order_model->get_us_states();
            $this->load->view( 'login/register', $data );
        } else {
            
            $company = new Company();
            $company->set_name($this->input->post('company_name'));
            $company->set_email($this->input->post('email_text'));
            $company->set_address($this->input->post('address_text'));
            $company->set_city($this->input->post('city_text'));
            $company->set_zip($this->input->post('zip_text'));
            $company->set_state($this->input->post('state_text'));
            $company->set_phone_1($this->input->post('phone_text'));
            $company->set_datetime(date('Y-m-d H:i:s'));
            $company->set_expire_company(date('Y-m-d H:i:s',  strtotime('+ 3 months')));
            $company->set_active( 1);
            $company->set_stripe_id($cust_id);
            $company->set_subscription_id($sub_id);
            if( $company->validate()) {
                $company_id = $this->order_model->save_or_update_object( $company );
                $this->session->set_userdata('user_id_add','');
                $this->session->set_userdata('user_id_sub','');
                $this->session->set_userdata('user_id_pack','');
                
                $primary = new Person();
                $primary->set_name($this->input->post('your_full_name'));
                $primary->set_email($this->input->post('email_text'));
                $primary->set_address($this->input->post('address_text'));
                $primary->set_city($this->input->post('city_text'));
                $primary->set_zip($this->input->post('zip_text'));
                $primary->set_state($this->input->post('state_text'));
                $primary->set_phone_1($this->input->post('phone_text'));
                $primary->set_person_type( Person::TYPE_USER );
                $primary->set_company_id( $company_id );
                $primary->set_password( $this->input->post('password') );
                $primary->set_active( 1);
                $primary->set_datetime(date('Y-m-d H:i:s'));
                if( $primary->validate()) {
                    $person_id = $this->order_model->save_or_update_object( $primary );                    
                    $this->db->where('id',$company_id);
                    $this->db->update('dr_companies',array('primary_contact' => $person_id,'current_subscription' => $pack_id));
                    
                    $company_filter = new Company();  
                    $company_filter->set_id( $company_id );
                    $companies = $this->order_model->get_object( $company_filter );
                    if( $companies ) {
                        $company = $companies['0'];
                        if( $company->active ){
                            $this->session->set_userdata(array(
                                'id' => $person_id,
                                'name' => $primary->name,
                                'email' => $primary->email,
                                'person_type' => $primary->person_type,
                                'company' => serialize( $company )
                            ));
                            header('Location: ' . site_url('home'));                                
                        }
                    }
                    
                    
                }
            }
        }
    }
    function strip_webhook() {
       
        require_once(APPPATH.'third_party/strip_lib/Stripe.php');
        
        Stripe::setApiKey("sk_live_i3OqfZUCCjaggf07I1MRZNM2");

        // Retrieve the request's body and parse it as JSON
        $input = @file_get_contents("php://input");
        $event_json = json_decode($input);
        
        if(isset($event_json) and isset($event_json->type) and $event_json->type == 'charge.succeeded') {
           
            $cust_id = $event_json->data->object->customer;
            // get company 
            
            $this->db->select('id,current_subscription');
            $this->db->from('dr_companies');
            $this->db->where('stripe_id',$cust_id);
            $result = $this->db->get();
            
            if($result->num_rows() == 0) {
                return false;
            }
            
            $row = $result->row();
            $packages =  $this->packages();                
            $time_update = (isset($packages[$row->current_subscription])) ? $packages[$row->current_subscription] : $packages['PS1'];

            $this->db->where('stripe_id',$cust_id);
            $this->db->update('dr_companies',array('active' => 1,'expire_company' => date('Y-m-d H:i:s',  strtotime('+ '.$time_update['time']))));
            
           
            $input = array();
            $input['name']                  = $event_json->data->object->card->name;
            $input['balance_transaction']   = $event_json->data->object->balance_transaction;
            $input['customer']              = $cust_id;
            $input['paymentamount']         = $event_json->data->object->amount;
            $input['transaction_time']      = date('Y-m-d H:i:s');
            
            $this->db->insert('dr_stripwebhook_response',$input);
            
        }elseif(isset($event_json) and isset($event_json->type) and $event_json->type == 'customer.subscription.deleted') {
            
                $cust_id = $event_json->data->object->customer;
                
                $this->db->select('id,current_subscription');
                $this->db->from('dr_companies');
                $this->db->where('stripe_id',$cust_id);
                $result = $this->db->get();

                if($result->num_rows() == 0) {
                    return false;
                }
                
                $update_company = array();                
                $update_company['active'] = 0;
                $update_company['expire_company'] = date('Y-m-d H:i:s');
                $this->db->where('stripe_id',$cust_id);
                $this->db->update('dr_companies',$update_company);
        }

        // Do something with $event_json
       
        
        http_response_code(200); // PHP 5.4 or greater
        exit;
        
    }
    private function packages() {
        
        $package = array();
        $package['PS1'] = array('plan_id' => 'PS1', 'time' => '30 days');
        $package['PSY'] = array('plan_id' => 'PSY', 'time' => '365 days');
        $package['PSL'] = array('plan_id' => 'PSL', 'time' => '2000 days');
        
        
        return $package;
    }
    
    function register_own() {
        
        $this->load->library('form_validation');  
        $email = '';
        
        $this->form_validation->set_rules('company_name', 'Company Name', 'trim|required');
        $this->form_validation->set_rules('your_full_name', 'Your Full Name', 'trim|required');
        $this->form_validation->set_rules('email_text', 'Email', 'trim|required|valid_email|is_unique[dr_persons.email]|is_unique[dr_companies.email]');
        $this->form_validation->set_rules('address_text', 'Address', 'trim|required');
        $this->form_validation->set_rules('city_text', 'City', 'trim|required');
        $this->form_validation->set_rules('zip_text', 'Zip', 'trim|required');
        $this->form_validation->set_rules('state_text', 'State', 'trim|required');
        $this->form_validation->set_rules('phone_text', 'Phone', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|matches[re_password]');
        $this->form_validation->set_rules('re_password', 'Retype Password', 'trim|required');
        
        if($this->form_validation->run()== FALSE) { 
            
            $data = array();
            $data['email'] = $email;
            $data['error'] = '';
            $data['states'] = $this->order_model->get_us_states();
            $this->load->view( 'login/register', $data );
        } else {
            
            $company = new Company();
            $company->set_name($this->input->post('company_name'));
            $company->set_email($this->input->post('email_text'));
            $company->set_address($this->input->post('address_text'));
            $company->set_city($this->input->post('city_text'));
            $company->set_zip($this->input->post('zip_text'));
            $company->set_state($this->input->post('state_text'));
            $company->set_phone_1($this->input->post('phone_text'));
            $company->set_datetime(date('Y-m-d H:i:s'));
            $company->set_expire_company(date('Y-m-d H:i:s',  strtotime('+ 12 months')));
            $company->set_active( 1);
            $company->set_stripe_id('Free');
            $company->set_subscription_id('Free');
            if( $company->validate()) {
                $company_id = $this->order_model->save_or_update_object( $company );
                
                $primary = new Person();
                $primary->set_name($this->input->post('your_full_name'));
                $primary->set_email($this->input->post('email_text'));
                $primary->set_address($this->input->post('address_text'));
                $primary->set_city($this->input->post('city_text'));
                $primary->set_zip($this->input->post('zip_text'));
                $primary->set_state($this->input->post('state_text'));
                $primary->set_phone_1($this->input->post('phone_text'));
                $primary->set_person_type( Person::TYPE_USER );
                $primary->set_company_id( $company_id );
                $primary->set_password( $this->input->post('password') );
                $primary->set_active( 1);
                $primary->set_datetime(date('Y-m-d H:i:s'));
                if( $primary->validate()) {
                    $person_id = $this->order_model->save_or_update_object( $primary );                    
                    $this->db->where('id',$company_id);
                    $this->db->update('dr_companies',array('primary_contact' => $person_id,'current_subscription' => $pack_id));
                    
                    $company_filter = new Company();  
                    $company_filter->set_id( $company_id );
                    $companies = $this->order_model->get_object( $company_filter );
                    if( $companies ) {
                        $company = $companies['0'];
                        if( $company->active ){
                            $this->session->set_userdata(array(
                                'id' => $person_id,
                                'name' => $primary->name,
                                'email' => $primary->email,
                                'person_type' => $primary->person_type,
                                'company' => serialize( $company )
                            ));
                            header('Location: ' . site_url('home'));                                
                        }
                    }
                    
                    
                }
            }
        }
    }

    
    function forget_pwd(){
        $this->load->view('login/forget_pwd');
    }

    function update_pwd(){


        if( ($user = $this->input->post('username') ) ){
            
            $filter = new Person();
            $filter->set_email(strtolower($user));

            if( $filter->email ){
                            
                $persons = $this->order_model->get_persons( $filter );

                if($persons){
                   // $data = array(
                   //      'id'=>$id = $persons[0]->id
                   //  );
                    
                    $this->load->library('email');

                    $this->email->from( 'no-reply@printshopplus.net', 'No-Reply');
                    $this->email->to('akashbachhania@gmail.com');

                    $this->email->subject('Reset Password');
                    $msg = base_url().'login/update_pwd?email='. $user;
                    $this->email->message($msg);

                    if($this->email->send()){
                        // echo $this->email->print_debugger();die;
                        $data = array(
                        'msg' => 'Email Sent!'
                        );
                        $this->load->view('login/forget_pwd',$data);
                    }



                    // $this->load->view('login/update_pwd');   
                }
                else{
                    header('location:'.base_url().'login/forget_pwd');
                }
            }

        }
        elseif(($email = $this->input->get('email')) && empty($_POST) ){
            $filter = new Person();
            $filter->set_email(strtolower($email));
            
            if( $filter->email ){
                            
                $persons = $this->order_model->get_persons( $filter );
                // echo "<pre>"; print_r($persons);die;
                if($persons){
                    $data = array(
                        'id' => $persons[0]->id
                        );    
                    $this->load->view('login/update_pwd',$data);
                }
            }


        }
        elseif( ($password = $this->input->post('password')) && ($id = $this->input->post('id')) ){
            
            if($this->order_model->update_person_pwd( $password,$id )){
                echo 'Successfully Changed Passowrd!';die;
            }

        }
    }

}