<?php
class Reports extends MY_Controller{
    
    const REPORT_SINGLE = 'single';
    const REPORT_DUPLEX = 'duplex';
    const REPORT_TRIPLEX = 'triplex';
    const REPORT_APARTMENTS = 'apartments';
    const REPORT_COMMERCIAL = 'commercial';
    const REPORT_OTHER = 'other';
                                                      
    const STATUS_SAVED = 'rsaved';
    const STATUS_UPDATED = 'rupdated';
    const STATUS_ERROR = 'rerror';
    
    const SUBMIT_REPORT = 'report_submit';
    const ACTION_SAVE = 'Save';
    const ACTION_DELETE = 'Delete';
    const ACTION_NEXT = 'Next';
    const ACTION_PDF  = 'Pdf Preview';
    const ACTION_HTML = 'Html preview';
    const ACTION_CLOSE = 'Close report';
    const ACTION_BACK = 'Back';
    const ACTION_CANCEL = 'Cancel';
    const ACTION_SHOW = 'Show';
    
    const DUMMY_IMAGE = 'img_default.png';
    const HTML_PREFACE = 'Preface';
    const HTML_DETAILS = 'Details';
    const HTML_DISCLOSURES = 'Disclosures';
    
    const IMAGES_REPORT_FOLDER = 'application/report_images/';
    const PDF_REPORT_FOLDER = 'application/report_pdfs/';
    
    const MAX_IMAGE_SIZE = 4194304;
    
    const DUMMY_VALUE_ID = '_';
    const VIEW_ALL = 'all';
    
    /**
    * 
    * @var Report_model
    */
    public $report_model;
    /*
    * @var Report
    */
    protected $report;
    /**
    * put your comment there...
    * 
    * @var accordion
    */
    public $accordion;
    protected $status;
    protected $subcategories = array();
    protected $categories = array();
    protected $property_types = array();
    protected $status_message = null;
    
    public function __construct(){           
        parent::__construct();
        $this->authorize_or_redirect( array(Person::TYPE_ADMIN,Person::TYPE_USER));        
        $this->load->model('Report_model','report_model', TRUE );
        $this->report_model->set_user($this->user);
        $this->load->library('Bootstrap_helper');
        $this->load->library('Accordion');
        $this->load->library('Report_accordion');
        $this->load->library('Report_marshaller');
        $this->report_marshaller->set_report_model( $this->report_model );
    }  
    public function index(){
        
        $properties = $this->report_model->get_properties();
        
        $templates  = array_merge (
            $this->report_model->get_object(new Template(null,null,null,null,0,Record::ACTIVE_RECORD)),
            $this->report_model->get_object(new Template(null,null,null,null,$this->user->company_id, Record::ACTIVE_RECORD ))
        );
        
        $temp = array();
        foreach( $templates as $template ){
            $temp[ $template->property_id ][] = $template;
        }
        $templates = $temp;
        
         
        $this->load->view('common/header', array(   'fluid_layout'=>false,'user'=>$this->user, 'reports'=>true ));
        $this->load->view('reports/new_report', array('properties'=>$properties, 'templates'=>$templates, 'path'=>site_url('reports/fetch_picture').'/'));
        $this->load->view('common/footer');
    }
    public function new_report(){
         
        $temp_order_id = $this->input->get_post('order_id');  
        $order_id    = null;
        $property_id = null;
        $template_id = null;
          
        $templates = array(''=>'Select Template'); 
        $properties = $this->report_model->get_properties_for_dropdown() + array(''=>'-Select property-');
        
        if( ($temp_property_id = $this->input->get_post('property_id')) && isset( $properties[$temp_property_id])){
            $property_id = $temp_property_id;
            if( $temp_template_id = $this->input->get_post('template_id')){
                if( $this->report_model->get_object( new Template(null,null,$property_id,null,$temp_template_id))){
                    $template_id = $temp_template_id; 
                }
            }
            $templates = $templates + $this->get_templates($property_id);
        }
        /**
        * Type, client, agent, inspector dropdown data
        */
        $clients =  $this->report_model->get_person_names( Person::TYPE_CLIENT, $this->user->company_id ) + array('' => '-Select Client-');
        $agents  =   $this->report_model->get_person_names( Person::TYPE_AGENT, $this->user->company_id ) + array('' => '-Select Agent-');; 
        $inspectors  =   $this->report_model->get_person_names( Person::TYPE_INSPECTOR, $this->user->company_id )+ array('' => '-Select Inspector-');
        $estimated_ages = array_merge( $this->report_model->get_estimated_ages(), array('' => '-Select Age-'));
        $statuses = array(''=>'Select status', Report::STATUS_GOOD=>'Good', Report::STATUS_FAIR=>'Fair', Report::STATUS_POOR=>'Poor' );
         
        
        $types = array_merge($this->report_model->get_types(),$this->report_model->get_general_disclosures());
         

        $filter = new Filter();
        $filter->set_company_id( $this->user->company_id );
        if( $temp_order_id ) {
            $filter->set_order_id($temp_order_id);
        }
        
        $all_orders = $this->report_model->get_orders( $filter );
        $orders = array();
        $report_order = null; 
        foreach( $all_orders as $order ){
            
            $array = array();
            $array[] = $order->address;
            $array[] = $order->city;
            $array[] = $order->zip;
            $array[] = $order->state;
            $address = implode(',', $array);
            
            $orders[ $order->id ] = "[".$order->inspector->name."] $address on [$order->inspection_date]";
            
            if( $temp_order_id ){
                if($temp_order_id == $order->id){
                    $report_order = $order;
                }
            }
        }
        
        /**
        * Standard data passed to new_report 
        * @var mixed
        */
        $default_data_pack = array();
        /**
        * Event handler
        * SUBMIT REPORT   
        */      
        if( $action = $this->input->post(self::SUBMIT_REPORT)){
              
            if( in_array( $action, array( self::ACTION_BACK, self::ACTION_NEXT, self::ACTION_SAVE, self::ACTION_PDF, self::ACTION_HTML, self::ACTION_CLOSE ))){                
                if( $report = $this->marshall_report()){                     
                    $view_data = array();
                    if( $report->validate() ){
                        if( $this->is_old_report_with_new_template_or_no_categories($report)){
                            $report = $this->init_report($report);
                            $this->report_model->save_or_update_object( $report );
                        } else {
                            $this->report_model->save_or_update_object( $report ); //save changes and merge with categories
                            $report = $this->report_model->get_object( new Report($report->id), true); //we need this to get categories from saved version
                        }
                        $this->status = self::STATUS_UPDATED;
                        
                        if( $report_order ) {
                            $report->add_order($report_order);
                        }
                   
                        //Add report to session
                        $this->session->set_userdata( array('report_id' => $report->id) );

                        switch( $action ){
                            case self::ACTION_NEXT:
                               $view_data = null;
                               $call = 'categories';
                            break;
                            case self::ACTION_PDF:
                               $this->report_pdf($report);
                            break;
                            case self::ACTION_HTML:
                               $this->report_html($report);
                               return;
                            break;    
                            case self::ACTION_CLOSE:
                                $this->close_report();
                            break;                        
                        }                        

                    }  else {
                        $this->status = self::STATUS_ERROR;
                        switch( $action ){                                                 
                            case self::ACTION_NEXT:
                                $view_data['view'] = 'reports/new_report_form';
                                $view_data['data'] = &$default_data_pack; 
                            break;
                        }                                            
                    }
                    /**
                    * Add aditional data
                    */
                    $report->add_utility_types( $this->report_model->get_types() );
                    $report->add_general_disclusures( $this->report_model->get_general_disclosures() );                    
                    $this->report = $report;                      
                    /**
                    * Save and Back always go to same place
                    */
                    switch( $action ){
                        case self::ACTION_CLOSE:
                        case self::ACTION_SAVE: 
                            $view_data['view'] = 'reports/new_report_form';
                            $view_data['data'] = &$default_data_pack;                  
                        break;
                        case self::ACTION_BACK:
                            $view_data['view'] = 'reports/new_report';
                            $view_data['data'] = array();
                        break;                                          
                    }                
                    /**
                    * Report couldnt be marshalled    
                    */
                } else {
                     
                    $report = new Report();
                    $view_data = array();
                    $view_data['view'] = 'reports/new_report_form';
                    $view_data['data'] = &$default_data_pack;
                    
                    $report->add_utility_types( $this->report_model->get_types());
                    $report->add_general_disclusures( $this->report_model->get_general_disclosures() );
                }                
            } else if( $action = $this->input->post(self::ACTION_CANCEL)){
                $view_data = array();
                $view_data['view'] = 'home/home';
                $view_data['data'] = array();
            } 
                                   
        } else {
            /**
            * NEW REPORT
            * See if there is report already in progress for this order?
            * If there is, load it.
            */
            if( $report_order ){
                if( !$report = $this->report_model->get_object(new Report(null, $this->user->company_id, null, $report_order->id ), true)){
                    $report = new Report();        
                }                
                $report->add_order($report_order);
                /**
                * Set order status to REPORT STARTED
                */
                $report_order->set_status(Order::ORDER_STATUS_REPORT_STARTED);                
                $this->report_model->save_or_update_object( $report_order );
                
            } else {
                $report = new Report();
            }
            /**
            * If property and template are available, use them
            */
            if( $property_id ){
                $report->_property->set_id( $property_id );
                if( $template_id && isset( $templates[$template_id])){
                     $report->_template->set_id( $template_id );
                }                                     
            }
             
            if( $report->template_id && $report->property_id && count($templates) == 1){
                $templates = $templates + $this->get_templates($report->property_id);
            }
            $report->add_utility_types( $this->report_model->get_types());
            $report->add_general_disclusures( $this->report_model->get_general_disclosures() );           
            $report->set_company_id($this->user->company_id);
            
            
            
            $report_id = $this->report_model->save_or_update_object($report);
            $report->set_id($report_id);
            
            $view_data = array();
            $view_data['view'] = 'reports/new_report_form';
            $view_data['data'] = &$default_data_pack;
            

        }
        if( $report ){
            $this->session->set_userdata( array('report_id' => $report->id) );
        }
        /**
        * No view data, and call is set, 
        * execute call
        */
        if( $view_data == null && $call ){
            $this->$call();
        } else {
            $default_data_pack = array(
                'accordion'=>$this->accordion,
                'report'=>$report,
                'clients'=>$clients,
                'agents'=>$agents,
                'inspectors'=>$inspectors,
                'estimated_ages'=>$estimated_ages, 
                'orders'=>$orders,
                'statuses'=>$statuses,
                'types'=>$types,
                'status' => $this->status,
                'status_message' => $this->status_message,
                'properties'=>$properties,
                'templates'=>$templates,
             ); 
            
            $this->load->view('common/header', array( 'fluid_layout'=>false,'user'=>$this->user, 'reports'=>true ));
            $this->load->view( $view_data['view'], $view_data['data'] );
            $this->load->view('common/footer');            
        } 
    }
    
    protected function process_valid_report( $action, Report $report ){
        if( $this->is_old_report_with_new_template_or_no_categories($report)){
            $report = $this->init_report($report);
            $this->report_model->save_or_update_object( $report );
        } else {
            $this->report_model->save_or_update_object( $report ); //save changes and merge with categories
            $report = $this->report_model->get_object( new Report($report->id), true); //we need this to get categories from saved version
        }
        $this->status = self::STATUS_UPDATED;
        
        if( $report_order ) {
            $report->add_order($report_order);
        }
   
        //Add report to session
        $this->session->set_userdata( array('report_id' => $report->id) );

        switch( $action ){
            case self::ACTION_NEXT:
               $this->categories();
            break;
            case self::ACTION_PDF:
               $this->report_pdf($report);
            break;
            case self::ACTION_HTML:
               $this->report_html($report);
               return;
            break;    
            case self::ACTION_CLOSE:
               $this->close_report();
            break;                        
        }    
    }
    
    protected function process_invalid_report(){
    
    }
    
    public function categories(){ 
              
        //No report in session - go bact to step 1
        if( !$this->report = $this->get_report()){
             $this->new_report();
             return;
        }
        $values = null;
        
        $category_id = null;
        $category_index = null;
        if( $category_id = $this->input->post('category')){
            
            if(preg_match('/(?P<category_id>\d+)_(?P<index>\d+)/', $category_id, $match )){
                $category_id     = $match['category_id'];
                $category_index  = $match['index'];
                /**
                * Check if report has these categories/index, initialize if not
                */
                if( ($report_categories = $this->report->get_categories()) && !isset($report_categories->categories[$category_id][$category_index])){
                    $categories = $this->report_model->get_categories_hierarchy($category_id,$category_index);
                    $category = $categories->get_category($category_id, $category_index);
                    $this->report->add_category( $category, $category_index);
                }                 
            }
        } 
        if( $submited_category = $this->input->post('categories')){
            //if theres update, save report
            if($this->report_marshaller->marshall_category($this->report, $submited_category)){
                $this->set_report($this->report);    
            }
        }
        
         
        
        $this->load->view('common/header', array( 'fluid_layout'=>false,'user'=>$this->user, 'reports'=>true ));
        $this->load->view('reports/categories', array(
                                    'report'=>$this->report,
                                    'accordion'=>$this->report_accordion,
                                    'category_id'=>$category_id,
                                    'category_index'=>$category_index,
                                    'values'=>$values,
                                    'submit_category' => $category_id,
                                    'status' => $this->status
                           ));
        $this->load->view('common/footer');          
    }
    
    public function templates(){  
          
        $action  = ($action = $this->input->post('action')) ? $action :
                   ($action = $this->input->get('action'))  ? $action : '';
        
        $property_id =  ($property_id = $this->input->get('property_id'))  ? $property_id : 
                        ($property_id = $this->input->post('property_id')) ? $property_id : '' ;
                       
        $template_id = ($template_id = $this->input->post('template_id')) ? $template_id : 
                       ($template_id = $this->input->post('select_template')) ? $template_id : 
                       ($template_id = $this->input->get('template_id'))? $template_id : '';
                                
        $template = null;
        if( $action == self::ACTION_SAVE && 
            $template_id && 
            $property_id &&
            $template_categories = $this->input->post('categories')){
            //Template id exists because it is created in a previous step.
            if ($template = new Template(null,null,$property_id,null, $template_id, $this->user->company_id )){
                
                $template = $this->marshall_template( $template_categories, $template ); 
                if( $template->validate() ){
                    $old_id = $template->id;
                    $id = $this->report_model->save_or_update_object($template);
                    if( $old_id == $id ){
                        $this->status = self::STATUS_UPDATED;
                    } else {
                        $this->status = self::STATUS_SAVED;
                    }
                } else {
                   $this->status = self::STATUS_ERROR;  
                }             
            }    
        } else if( $action == self::ACTION_SHOW && $template_id ) {
                       
            $template = $this->report_model->get_object( new Template(null,null,null,null, $template_id, $this->user->company_id ),true);
                                  
        } else if( $action == self::ACTION_DELETE && $template_id ){
            if( $template = $this->report_model->get_object( new Template(null,null,null,null, $template_id, $this->user->company_id ),true)){
                $template->set_active(Record::NONACTIVE_RECORD);
                $this->report_model->save_or_update_object($template);
                $template = null;
            }
        }
         
        $properties = $this->report_model->get_properties();
        $categories = $this->report_model->get_categories_for_dropdown();
        $templates  = $property_id ? $this->get_templates( $property_id ) : array();
   
        
        $this->load->view('common/header', array( 'fluid_layout'=>false,'user'=>$this->user, 'reports'=>true ));
        $this->load->view('reports/templates', array(
                                'properties'=>$properties,
                                'templates' =>$templates,
                                'accordion' =>$this->accordion,
                                'categories'=>$categories,
                                'selected_template'=> $template,
                                'selected_property_id' =>$property_id,
                                'status'    =>$this->status
                           ));
        $this->load->view('common/footer');
                
    }
    
    public function close_report(){ 
        try{            
            $report = $this->get_report(true);
            $this->check_if_not_closed($report);                            
            $report = $this->create_credentials($report);
            $this->notify_involved_persons($report);            
            $this->set_report_status_closed( $report );            
        } catch(Exception $e){
            $this->status = self::STATUS_ERROR;
            $this->status_message = $e->getMessage();
        }
    }
    
    protected function check_if_not_closed($report){
        $filter = new Filter();
        $filter->set_order_id($report->order_id);
        if($order = $this->report_model->get_orders($filter, true) ){
           if( $order->status != Order::ORDER_STATUS_REPORT_STARTED ){
               throw new Exception('Order already closed');    
           }
        } else {
            throw new Exception('Order doesnt exist');
        }
    }
    protected function set_report_status_closed(Report $report){
        $filter = new Filter();
        $filter->set_order_id($report->order_id);        
        $order = $this->report_model->get_orders($filter, true);
        $order->set_status(Order::ORDER_STATUS_REPORT_COMPLETED);
        $this->report_model->save_or_update_object($order);
    }
    
    protected function create_credentials(Report $report ){
        if( ($client = $report->_client) && $client->email ){              
             $report->_client->set_password($client->password ? $client->password : md5(time().$client->email.$client->id));
             $this->report_model->save_or_update_object($report->_client);             
        } else {
            //throw new Exception('Plese check that order has valid client,agent, inspector and that their respective email addresses are present');
        } 
        return $report;
    }
    
    protected function notify_involved_persons($report){
        $messages = $this->compose_emails( $report );
        $attachment_path = $this->create_pdf_report_file($report);
        $this->send_email($messages, $attachment_path, $report);
    }
    
    protected function compose_emails( $credentials, $report ){
        $mail['_client'] = $this->compose_client_mail($report);
        $mail['_agent']  = $this->compose_staff_mail($report);
        $mail['_inspector'] = $this->compose_staff_mail($report);
        return $mail;
    }
    
    protected function compose_client_mail( $report ){
        $body = "Please find attached results of the inspection.";
        return $body;            
    }
    
    protected function compose_staff_mail($report){
        $body = "Please find attached results of the inspection.";
        return $body;         
    }
    
    protected function create_pdf_report_file($report){
        $this->load->library('report_pdf');
        $report = $this->prepare_report_for_display($report);
        $filename = $this->get_local_path().self::PDF_REPORT_FOLDER.'report_'.$report->id.'.pdf';
        $this->report_pdf->create_report( $report, $filename );        
        return $filename;
    }
    
    protected function send_email( $messages, $attachment_path, $report ){
        $company = $this->get_company();
        foreach( $messages as $recipient=>$message ){
            $this->initialize_email();
            $this->email->from( $company->email);
            $this->email->to( $report->$recipient->email);
            $this->email->subject('Inspection results');
            $this->email->attach($attachment_path);
            if(!$this->email->send()){
                throw new Exception('Inspection report emails could not be sent at this time!');
            }
        }
    }
    
    protected function email(){

            

            $company = new Company();
            $company->set_id( $this->user->company_id );
            $company = $this->order_model->get_object( $company );
            if( $company ){
                $company = reset( $company );
            }
            

            
            $order->set_company( $company );
            $filename = 'order_'.$order->id.'.pdf';
            $this->load->library('order_pdf');
            
            $ids = $this->order_model->get_company_order_ids($order);
            $invoice_number = array_search($order->id, $ids)+1;  
            $this->order_pdf->create_pdf( $order, $this->order_helper, $filename, $invoice_number );
             
            $this->email->initialize( $config );
            $this->email->from( $company->email, 'Homeinspectors'); 
            
            
            $inspector_address = array();
            $inspector_address[] = $order->address;
            $inspector_address[] = $order->city;
            $inspector_address[] = $order->zip;
            $inspector_address[] = $order->state;
            $address_str = implode(', ', $inspector_address );
                         
            $this->email->to(  $order->client->email ); 
            $this->email->subject('Thank you for your order for: '. $address_str);
            $this->email->message( $this->get_message_body("Thanks for your order, attached you'll find all your inspection details in PDF format.<br /><br /><br />", $company));
            $this->email->attach( $filename );       
            $this->email->send();

            $this->email->initialize( $config );
            $this->email->from($company->email, 'Homeinspectors'); 
            
            $this->email->to( $order->inspector->email );
            $this->email->subject('Please note this inspection you are scheduled for:' . $address_str);
            $this->email->message( $this->get_message_body('Please note inspection details in PDF format.<br /><br /><br />', $company) );
            $this->email->send(); 
                        
            
            $this->email->initialize( $config );
            $this->email->from($company->email, 'Homeinspectors'); 
            
   
                                    
            $this->email->to(  $order->agent->email ); 
            $this->email->subject('Thank you for your order from '. $address_str);
            $this->email->message( $this->get_message_body("Thanks for your order, attached you'll find all your inspection details in PDF format.<br /><br /><br />", $company));       
            $this->email->send();        
    }
    
    
    protected function marshall_template( array $categories, $template, $db_categories = array(), $db_subcategories = array() ){
    
        $db_categories    = $db_categories ? $db_categories : $this->report_model->get_categories();
        $db_subcategories = $db_subcategories ? $db_subcategories : $this->report_model->get_subcategories();
         
        foreach( $categories as $category_id=>$data  ){
            foreach( $data as $category_index=>$category  ){
                $template_category = $template->init_category( $db_categories[$category_id], $category_index );
                $sub_cats = array();
                foreach( $category as $subcategory_id=>$subcategory_values ){
                    if( $subcategory_id ){
                        $subcategory = $db_subcategories[$category_id][$subcategory_id];
                        $report_subcategory = new Report_subcategory();
                        
                        $report_subcategory->add_name( $subcategory->name );
                        $report_subcategory->set_category_id( $subcategory->category_id );
                        $report_subcategory->set_subcategory_id( $subcategory->id );
                        
                        $values = array();
                        foreach( array_keys($subcategory_values['value_ids']) as $value_id ){
                            if( $value_id && $value_id != self::DUMMY_VALUE_ID ){
                                $values[$value_id] = $subcategory->_values[$value_id];    
                            }
                        }
                        $report_subcategory->add_values( $values );
                        $sub_cats[$subcategory->id] = $report_subcategory;                    
                    }
     
                }
                $template_category->add_subcategories($sub_cats);
            }
        }
        
        return $template;
    }
    protected function marshall_category(Report &$report, $submited_categories){DebugBreak();
        $update = false;   
         
        if( is_array($submited_categories)){
            $category_id = reset(array_keys($submited_categories));
            $index =  reset(array_keys($submited_categories[$category_id]));
            
            if( $update = $this->category_update($submited_categories) ){
                if( !$report->is_category_set($category_id, $index )){
                    $report->add_category(
                        $this->report_model->get_category_for_init($category_id, $index ),
                        $index
                    );
                }
                foreach( $categories[$category_id][$index] as $subcategory_id=>$subcategories ){
                    foreach( $subcategories as $field=>$values ){
                        switch( $field ){
                            case 'rating':
                                $report->categories[$category_id][$index]->_subcategories[$subcategory_id]->set_rating( reset(array_keys($values)));
                            break;
                            case 'value_ids':
                                /**
                                * Check if this is custom added value.
                                * Maybe we should save it in template too?
                                */
                                foreach( $values as $id=>$value){
                                    if( !isset($report->categories[$category_id][$index]->_subcategories[$subcategory_id]->_values[$id])){
                                        $report->categories[$category_id][$index]->_subcategories[$subcategory_id]->_values[$id] = $this->report_model->get_object(new Value($id), true);
                                    }
                                }
                                $report->categories[$category_id][$index]->_subcategories[$subcategory_id]->set_value_ids($values);
                            break;
                            case 'notes':
                                $report->categories[$category_id][$index]->_subcategories[$subcategory_id]->set_notes($values);
                            break;
                        }
                    }
                }                     
            }                
       }
       return $update;
    } 
    
    protected function category_update($categories){   
        $changed = false;
        foreach($categories as $category_id=>$data){
            foreach( $data as $index=> $subcategories ){
                foreach($subcategories as  $values){
                    foreach( $values as $field=>$value){
                        if(in_array($field, array('rating','value_ids'))){
                            $changed = true;
                            break;
                        }
                        if( $field == 'notes' && $value != ''){
                            $changed = true;
                            break;
                        }                    
                    }
                }                
            }
        }
        return $changed;
    }
    
    public function marshall_report(){
        
        $order = new Filter();
        $order->set_company_id($this->user->company_id);
        $order->set_order_id($this->input->post('order_id'));
        
        $report = null;
        
        if( $order = $this->report_model->get_orders($order, true) ){             
            if( $agent = $this->report_model->get_object( new Person( Person::TYPE_AGENT, $this->input->post('agent_id'), $this->user->company_id), true ) ){
                if( $inspector = $this->report_model->get_object( new Person( Person::TYPE_INSPECTOR, $this->input->post('inspector_id'), $this->user->company_id), true )){                    
                      
                    $types  = $this->order_mapper->create_object_array_from_input_values('Type', Order_mapper::SOURCE_POST, '!type_<name>_(?P<value>\d+)!');
                    $client = $this->report_model->get_object( new Person( Person::TYPE_CLIENT, $this->input->post('client_id'), $this->user->company_id), true );
                    
                    foreach( $types as &$type ){
                        $type->set_company_id( $this->user->company_id );
                        if( $type = $this->report_model->get_object( $type, true )){
                            if( $type->type == Type::TYPE_GENERAL_REMARKS ){
                                $general[] = $type;
                            } else if( $type->type == Type::TYPE_UTILITY_STATUS ){
                                $utilities[] = $type;
                            }
                        }
                    }
                    
                    $report = $this->order_mapper->create_object_from_input('Report')
                        ->add_agent( $agent )
                        ->add_client( $client )
                        ->add_inspector( $inspector )
                        ->add_order( $order )
                        ->add_types_selected( $types )
                        ->set_company_id( $this->user->company_id );
     
                } else {
                    $this->errors[] = 'Invalid inspector';
                }
            } else {
                $this->errors[] = 'Invalid agent';     
            }
        } else {
            $this->errors[] = 'Invalid order';     
        }
        
        return $report;
    }
    
    public function report_html( Report $report = null ){   
        if( !$report ){
            $report = $this->get_report( true );
        }
        
        if(preg_match('/(?P<category_id>\d+)_(?P<index>\d+)/', $this->input->get_post('category'), $match )){
           $category_id = $match['category_id'];
           $category_index = $match['index'];
        } else {
           if( in_array( $this->input->get_post('category'), array(self::HTML_DETAILS, self::HTML_DISCLOSURES, self::HTML_PREFACE))){
               $category_id = $this->input->get_post('category');
               $category_index = null;
           } else {
               $category_id = reset(array_keys($report->get_categories()->categories));
               $category_index = 0;                
           }      
        }       

        $report = $this->prepare_report_for_display($report, true);
        
        $this->load->view('common/header', array( 'fluid_layout'=>false,'user'=>$this->user, 'reports'=>true ));
        $this->load->view('reports/report_html', array('report'=>$report, 'category_id'=>$category_id, 'category_index'=>$category_index));
        $this->load->view('common/footer');    
    }
    
    public function report_pdf( Report $report = null){
        if( !$report ){
            $report = $this->get_report(true);
        }
        $this->load->library('report_pdf');
        $report = $this->prepare_report_for_display($report);
        echo $this->report_pdf->create_report( $report );
        die(); 
    }
    
    public function prepare_report_for_display( Report $report, $public_image_path = false ){
        
        $pictures = $this->report_model->get_object( new Picture(null,null,null,null,$report->id));
        foreach( $pictures as &$picture ){
            if( $public_image_path ){ //Public path for html report
                $picture->set_path( site_url('reports/fetch_picture/'.$picture->path));
            } else { //Local path for pdf report
                $picture->set_path( $this->get_local_path() . self::IMAGES_REPORT_FOLDER . $picture->path );    
            }
        } 
        
        $report
            ->add_utility_types( $this->report_model->get_types())
            ->add_general_disclusures( $this->report_model->get_general_disclosures() ) 
            ->add_pictures($pictures)
            ->add_report_number( $this->get_report_number( $report ))
            ->add_property_type( $this->get_property_type( $report ));

        $filename = $this->get_local_path() . 'application/views/assets/img/'.$this->user->company_id.'_logo';
        if( file_exists( $filename )){
            $report->add_company_logo( $filename );
        }
        
        $report = $this->substitute_subcategory_ratings_to_readable($report);
        return $report;
    }
    
    public function substitute_subcategory_ratings_to_readable( Report $report){
        foreach( $report->get_categories()->categories as $category_group ){
            foreach( $category_group as $category ){
                foreach( $category->_subcategories as $subcategory ){
                    switch( $subcategory->_rating ){
                        case 1:
                            $subcategory->set_rating('Good');
                        break;
                        case 2:
                            $subcategory->set_rating('Fair');
                        break;
                        case 3:
                            $subcategory->set_rating('Poor');
                        break;
                    }
                }
            }
        }
        return $report;
    }
    
    public function view_reports(){
        
        $status = $this->uri->segment(3);
        
        if( $status == Order::ORDER_STATUS_REPORT_STARTED ){
            $orders = $this->order_model->get_orders( new Filter(null, $this->user->company_id, null, null, $status) );    
        } else {
            $orders = array_merge(
                 $this->order_model->get_orders(new Filter(null, $this->user->company_id, null, null, Order::ORDER_STATUS_REPORT_STARTED)),
                 $this->order_model->get_orders(new Filter(null, $this->user->company_id, null, null, Order::ORDER_STATUS_REPORT_COMPLETED))    
            );
        }
        
        $this->load->view('common/header',array('fluid_layout'=>false, 'user'=>$this->user, 'companies'=>$this->companies,'reports'=>true));
        $this->load->view('reports/view_reports', array('orders'=>$orders, 'status'=>$status ));
        $this->load->view('common/footer');
    }
        
    public function add_category_copy( $category ){
        
    }
    
    public function get_custom_categories(){
        $categories = $this->report_model->get_object(new Category());        
        $session_categories = $this->session->userdata('extra_categories');
    }
    
    protected function get_report_number( Report $report ){
        return 1;
    }
    
    protected function get_property_type( Report $report ){
        $this->report_model->get_properties_for_dropdown();
        return $this->property_types[$report->property_id]->name;
    }
    
    /**
    * Initialize report
    * - If template is set, only categories from template are used
    * - Otherwise, report is initialized with ALL categories set
    * 
    * @param Report $report
    * @return Report
    */
    public function init_report( Report $report = null){
 
        $report = $report ? $report: new Report();
         
        if( $report->template_id ){
            if ( ($template = $this->report_model->get_object(new Template(null,null,null,null, $report->template_id, $this->user->company_id, Record::ACTIVE_RECORD ),true)) ||
                 ($template = $this->report_model->get_object(new Template(null,null,null,null, $report->template_id, Record::SYSTEM_COMPANY_ID, Record::ACTIVE_RECORD ),true))){
                 
                 $report->set_categories( $template->categories );
            }
        } else {
            $report->init_categories($this->report_model->get_categories(), $this->report_model->get_subcategories());    
        }
        
       
        return $report;
    }
    
    public function is_old_report_with_new_template_or_no_categories(Report $report ){
        $new_template = false;
        if($old_report = $this->report_model->get_object(new Report($report->id), true)){
            if( $old_report->template_id !== $report->template_id || $old_report->categories == null ){
                $new_template = true;
            }
        } else {
            $new_template = true;
        }
        return $new_template;
    }
    
    public function set_report( Report $report ){
        if( $report->validate()){            
            $this->session->set_userdata( array('report_id'=> $this->report_model->save_or_update_object($report) ));
            $this->status = self::STATUS_UPDATED;
        } else {
            $this->status = self::STATUS_ERROR;
        }
        $this->report = $report;
    }
    
    public function get_report($get_related_objects = false){ 
        if(!$this->report ){
            if($id = $this->session->userdata('report_id')){
                $this->report = $this->report_model->get_object( new Report($id, $this->user->company_id), true);
                if( $get_related_objects ){
                    
                    $order = new Filter();
                    $order->set_company_id($this->user->company_id);
                    $order->set_order_id($this->report->order_id);
                                                                 
                    $order = $this->report_model->get_orders( $order, true );
                    $this->report->add_order($order);
                }
            } else {
                $this->report = null;
            }            
        }
        return $this->report;
    }    
    
    protected function get_company(){
        $company = new Company();
        $company->set_id($this->user->company_id);
        return $this->report_model->get_object($company, true);        
    }

    public function create_menus(){
        die('not supported');
        
        $xml = new SimpleXMLElement(file_get_contents('new_xml.xml'));
         
        $categories = array();
        if( $xml ){
            foreach($xml->Worksheet as $worksheet){
                $name = reset(reset($worksheet->attributes()));
                foreach( $worksheet->Table->Row as $row ){
                    $row = (array)$row;
                    $count = count($row['Cell']);
                    if( $count == 3 ){
                        $category = &$categories[$name][ (string)$row['Cell'][0]->Data];
                        if( $string = (string)$row['Cell'][1]->Data){
                            $category[] = $string;    
                        }
                    } else {
                        if( $string = (string)$row['Cell'][0]->Data){
                            $category[] = $string;    
                        }
                    }
                }
            }
        }
        
        foreach( $categories as $category_name=>$subcategories ){
            $category = new Category(null,$category_name);
            $id = $this->report_model->save_or_update_object( $category );
            
            foreach( $subcategories as $subcategory_name=>$check_box_types ){
                $subcategory = new Subcategory(null,$subcategory_name,$id);
                $sub_id = $this->report_model->save_or_update_object( $subcategory );
                
                foreach( $check_box_types as $cbtype ){
                    $value = new Value(null,$cbtype,$sub_id);
                    $this->report_model->save_or_update_object( $value);
                }
            }
        }        
    }

    public function add_value(){
        $return = array();
        if( ($subcategory_id = $this->input->post('subcategory_id')) && ($name = $this->input->post('value'))){ 
            $value = new Value(null,$name, $subcategory_id, $this->user->company_id, Record::ACTIVE_RECORD); 
            if($id = $this->report_model->save_or_update_object($value)){
                $return['status'] = 'success';
                $return['id'] = $id;
            }        
        }
        echo json_encode($return);
    }
    
    public function add_element(){
         
        if( ($name = $this->input->post('value'))&&
            ($category_id = $this->input->post('category_id'))&&
            ($subcategory_id = $this->input->post('subcategory_id'))&&
            ($value_id = $this->input->post('value_id'))&&
            (is_numeric($index = $this->input->post('index'))) &&
            ($element_type = $this->input->post('element_type')) &&
            ($object_type = $this->input->post('object_type')) &&
            ($object_id = $this->input->post('object_id')) 
        ) {  
            if($object = $this->get_object($object_type, $object_id)){                
                $data = $this->set_element(
                    $object,
                    $object_type,
                    $element_type,
                    null,
                    $category_id,
                    $index,
                    $subcategory_id,
                    $value_id,
                    $name
                );
                echo json_encode( 
                  $data
                ); 
            }
        }
    }
    
    public function undo_last_action(){
        if( ($object_type = $this->input->post('object_type')) &&
            ($object_id = $this->input->post('object_id')) 
        ) { 
             if($object = $this->get_object($object_type, $object_id)){
                /**
                * @var Undo
                */
                $deleted_element = $this->report_model->get_last_undo_object($object_id);
                $data =  $this->set_element(
                    $object,
                    $object_type,
                    $deleted_element->type,
                    $deleted_element->object,
                    $deleted_element->category_id,
                    $deleted_element->category_index,
                    $deleted_element->subcategory_id,
                    $deleted_element->value_id,
                    null
                );
                $parent_type = $parent_id = null;
                switch( $deleted_element->type ){
                    case Undo::TYPE_CONCERN:
                        $parent_type = Accordion::TYPE_VALUE;
                    break;
                    case Undo::TYPE_VALUE:
                        $parent_type = Accordion::TYPE_SUBCATEGORY;
                    break;
                    case Undo::TYPE_SUBCATEGORY:
                        $parent_type = Accordion::TYPE_CATEGORY;
                    break;
                    case Undo::TYPE_CATEGORY:
                        $parent_id = 'accordition';
                    break;
                    
                }
                if( $parent_type ){
                    $parent_id = $this->accordion->get_DOM_id(
                        $parent_type, 
                        $deleted_element->category_id,
                        $deleted_element->category_index,
                        $deleted_element->subcategory_id,
                        $deleted_element->value_id                            
                    );
                }
                
                if( $parent_id ){
                    $data['parent_id'] = $parent_id;
                    echo json_encode(
                            $data
                    );
                    $this->report_model->delete_object($deleted_element);                    
                }
             }
        }        
    }
    
    private function set_element($object, $object_type, $element_type, $element, $category_id, $index, $subcategory_id, $value_id, $name){
        $id_array = array(
            'category_id' => $category_id,
            'category_index' => $index,
            'subcategory_id' => $subcategory_id,
            'value_id' => $value_id
        );                
        switch($element_type){
            case Undo::TYPE_CONCERN:
                $concern = $element ? $element: new Concern(null, $name, $value_id, $this->user->company_id, Record::ACTIVE_RECORD);
                $id = $object->add_concern( $concern, $subcategory_id, $index );
                $ancestor_id = '';
                $html = $this->accordion->get_concern_element( $concern, $id_array );
            break;
            case Undo::TYPE_VALUE:
                $value = $element ? $element: new Value(null, $name, $subcategory_id, $this->user->company_id, Record::ACTIVE_RECORD);
                $id = $object->add_value( $value, $index);
                $html = $this->accordion->get_value_accordion( $value, $id_array );
                $ancestor_id = $this->accordion->get_DOM_id(Accordion::TYPE_VALUE, $category_id, $index, $subcategory_id, $id);
            break;
            case Undo::TYPE_SUBCATEGORY:
                $subcategory = $element ? $element: new Subcategory(null, $name, $category_id, $this->user->company_id, Record::ACTIVE_RECORD); 
                $id = $object->add_subcategory( $subcategory, $index);
                $ancestor_id = $this->accordion->get_DOM_id(Accordion::TYPE_SUBCATEGORY, $category_id, $index, $id);
                switch( $object_type ){
                    case 'report':
                        $html = $this->report_accordion->get_subcategory_container( $subcategory, $category_id, $index);
                    break;
                    case 'template':
                        $html = $this->accordion->get_subcategory_accordion( $subcategory, $id_array);
                    break;
                }
            break;
            case Undo::TYPE_CATEGORY:
                $category = $element; //category is added on other place also, TODO refactor
                $id = $object->add_category( $category, $index );
                $ancestor_id = $this->accordion->get_DOM_id(Accordion::TYPE_CATEGORY, $category_id, $index);
                $html = $this->accordion->get_category_accordion( $category );
            break;                    
        }
        if( $id ){
            $this->report_model->save_or_update_object($object);
        }
        return array(
          'id'   => $ancestor_id,
          'html' => $html
        );    
    }
    /**
    * put your comment there...
    * 
    * @param mixed $object_type
    * @param mixed $object_id
    * @return Template
    */
    public function get_object( $object_type, $object_id ){
        $object = null;
        switch( $object_type ){
            case 'template':
                if( $object_id ){
                    $object = $this->report_model->get_object(new Template(null,null,null,null, $object_id,null,null), true);
                }
            break;
            case 'report':
                $object = $this->get_report();
            break;
        }
        return $object;
    }        
        
    public function add_category(){
        $return = array();
        if( $name = $this->input->post('name') ){            
            $category = new Category(null,$name, $this->user->company_id, Record::ACTIVE_RECORD );
            if( !$this->report_model->get_object($category)){
                $id = $this->report_model->save_or_update_object( $category );
                $return['status'] = 'success';
                $return['id'] = $id;
            } else {
                $return['error'] = 'Category with this name already exists';
            }
        } else {
            $return['error'] = 'Invalid name';
        }
        echo json_encode($return);
    }
    
    public function add_property(){
        $return = array();
        if( ($name = $this->input->post('name')) && ($description = $this->input->post('description'))){
            $property = new Property($name,null,null,$this->user->company_id);
            if( !$this->report_model->get_object($property)){
                $property->set_description($description);
                $property->set_id(null);
                $property->set_active(Record::ACTIVE_RECORD);
                if($id = $this->report_model->save_or_update_object($property)){
                    $return['status'] = 'success';
                    $return['id'] = $id;
                }                 
            } else {
                $return['error'] = 'Propety with same name already exists!';
            }
        } else {
            $return['error'] = 'Name and description are empty!';    
        }       
        echo json_encode($return);
    }
    
    public function add_template(){ 
        $return = array();
        if( ($property_id = $this->input->post('property_id') ) && ($name = $this->input->post('name')) && ($description = $this->input->post('description'))){
            $property = new Property(null,null,$property_id, $this->user->company_id);            
            if( $this->report_model->get_object(new Property(null,null,$property_id, Record::SYSTEM_COMPANY_ID )) || 
                $this->report_model->get_object(new Property(null,null,$property_id, $this->user->company_id))
            ){
                if( !$template = $this->report_model->get_object( new Template($name,null,$property_id,null,null,$this->user->company_id)) ){
                     $template = new Template($name,$description,$property_id,null,null,$this->user->company_id, Record::ACTIVE_RECORD);
                     if( ($id = $this->report_model->save_or_update_object($template)) !== null){
                         $return['status'] = 'success';
                         $return['id'] = $id;
                     }                         
                } else {
                    $return['error'] = 'Template with same name already exists';
                }
            } else {
                $return['error'] = 'Invalid property';
            }
        } else {
            $return['error'] = 'Invalid property';
        }       
        echo json_encode($return);
    }
    
    public function add_pictures(){  
        $return = array();        
        if( ($filename = $this->input->get('qqfile')) &&
            ($subcategory_id = $this->uri->segment(3))!== null && 
            ($report_id = $this->session->userdata('report_id')) ){
           
            if( preg_match('/\.(?P<type>\w+)/', $filename, $match )){
               /** */
               $data = file_get_contents('php://input');
               /**
               * TODO too much files check
               */
               if( strlen($data) > 0 && strlen($data) < self::MAX_IMAGE_SIZE ){ 
                    //property level image, we'll set subcategory to 0
                    $subcategory_id = $subcategory_id == 'property'? 0: $subcategory_id;  
                    
                    $picture = new Picture(null,null,$subcategory_id, null, $report_id );
                    $id = $this->report_model->save_or_update_object( $picture );
                    $picture->set_id($id);
                    
                    $filename = 'img_'.$id.'.'.$match['type'];
                    $this->save_image($filename, $data);     
                    $picture->set_path( $filename );
                    $this->report_model->save_or_update_object( $picture );
                    
                    $this->resize_image($filename, 800);
                    $this->resize_image($filename, 150, 'thumb_'.$filename);
                    
                    $return = array(
                        'success'=>'true',
                        'image' => site_url('reports/fetch_picture/'.$filename ),
                        'thumb' => site_url('reports/fetch_picture/thumb_'.$filename )
                    );
               }               
            }                                            
        }
        echo json_encode($return);
    }
    
    public function add_picture_property_button(){
        $return = array();        
        if( ($filename = $this->input->get('qqfile')) &&
            ($property_id = $this->uri->segment(3)) ) {
           
           if( preg_match('/\.(?P<type>\w+)/', $filename, $match )){
               $data = file_get_contents('php://input');
          
               if( strlen($data) > 0 && strlen($data) < self::MAX_IMAGE_SIZE ){
  
                    if($property = $this->report_model->get_object(new Property(null,null,$property_id, $this->user->company_id, null, null ), true)){

                        $filename = 'img_property_'.$property_id.'.'.$match['type'];             
                        $this->save_image($filename, $data);
                        
                        $property->set_image_filename( $filename );                    
                        $this->report_model->save_or_update_object( $property );
                        $this->resize_image($filename, 150);
                        
                        $return = array(
                            'success'=>'true',
                            'filename' => site_url('reports/fetch_picture/'.$filename )
                        );                        
                    }
               }               
           }                                            
        }
        echo json_encode($return);    
    }
    
    protected function save_image( $filename, $data ){
        file_put_contents($this->get_local_path() . self::IMAGES_REPORT_FOLDER . $filename, $data);    
    }
    
    protected function resize_image( $filename, $size, $filename_out = null ){
        if(preg_match('/\.(?P<type>\w+)$/', $filename, $match )){
            switch(strtolower($match['type'])){
                case 'jpg':
                case 'jpeg':
                    $type = 'image/jpeg';
                break;
                case 'png':
                    $type = 'image/png';
                break;
                case 'gif':
                    $type = 'image/gif';
                break;                
            }
        }
        $full_path_in = $this->get_local_path().self::IMAGES_REPORT_FOLDER . $filename;
        $full_path_out = $filename_out ? $this->get_local_path().self::IMAGES_REPORT_FOLDER . $filename_out : $full_path_in;
        $this->image_resize($full_path_in, $full_path_out, $type, $size);
    }
    public function add_new_type(){  
        if( ($type = $this->input->post('type')) &&
            ($name = $this->input->post('name')) &&
            ($description = $this->input->post('description'))                    
        ) {   
             $type = new Type(null, $this->user->company_id, $name, $description, $type, Record::ACTIVE_RECORD );
             if( $type->validate()){
                 $id = $this->report_model->save_or_update_object($type);
                 $type->set_id($id);
                 $this->load->view('reports/component_form_type', array('type' =>$type));
             }
        }
    }
    
    public function save_image_edit(){
        $return = array();
        if( ($image_url = $this->input->post('url')) && ($filename = $this->input->post('postData'))){
            
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_VERBOSE, TRUE);       
            curl_setopt($curl, CURLOPT_URL, $image_url );
            curl_setopt($curl, CURLOPT_HEADER, 0);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $img_data = curl_exec( $curl );     
                   
            $this->log('IMG_data: ' .strlen($img_data));
            
            file_put_contents(
                $this->get_local_path().'application/report_images/'.$filename,
                $img_data
            );
            $return['url'] = site_url('reports/fetch_picture/'.$filename) ;
        }
        echo json_encode($return);
    }

    public function fetch_category_template(){ 
        $return = array();
        if( ($category_id = $this->input->get_post('category_id')) && 
            ($template_id = $this->input->get_post('template_id'))){
                try{
                    /**                 
                    * @var Template
                    */
                    $template = $this->report_model->get_template($template_id);
     
                    $category_index = $template->get_category_index($category_id);

                    $categories = $this->report_model->get_categories_hierarchy($category_id,$category_index);
                    $category = $categories->get_category($category_id, $category_index); 
                    $template->add_category( $category, $category_index );
                    $this->report_model->save_or_update_object($template);

                    $selected_template = new Template();
                    $selected_template->set_categories($categories);                    
                    $this->load->view('reports/component_template',array('selected_template'=>$selected_template, 'accordion'=>$this->accordion));  
                } catch(Exception $e){
                    echo '';
                }                             
        } 
    }
                                                     
    public function fetch_value_template(){  
        $return = array();
        if( $subcategory_id = $this->input->post('subcategory_id')){
            if( $this->report_model->get_object(new Subcategory($subcategory_id,null,null, Record::SYSTEM_COMPANY_ID, Record::ACTIVE_RECORD)) || 
                $this->report_model->get_object(new Subcategory($subcategory_id,null,null, $this->user->company_id, Record::ACTIVE_RECORD))
            ) {
                $values = array_merge(
                    $this->report_model->get_object(new Value(null,null,$subcategory_id, Record::SYSTEM_COMPANY_ID, Record::ACTIVE_RECORD)),
                    $this->report_model->get_object(new Value(null,null,$subcategory_id, $this->user->company_id, Record::ACTIVE_RECORD))
                );
                
                if( $values ){
                    $return['status'] = 'success';
                    foreach( $values as $value ){
                        $return['values'][] = array(
                            'id'   => $value->id,
                            'name' => $value->name
                        );
                    }
                }
            }
        }
        echo json_encode($return); 
    }
    
    public function fetch_picture(){
        if( $filename = $this->uri->segment(3)){    
            $path = parse_url( site_url()); 
            $path = str_replace('index.php','',$path['path']) ;
             
            $path = $_SERVER['DOCUMENT_ROOT'] .  $path  . 'application/report_images/'.$filename;
            
            if( preg_match('/\.(?P<type>\w+)/', $filename, $match )){
                $type = 'image/'. $match['type'];
                header('Content-Type: '.$type);
                header('Content-Length: ' . filesize($path));
                readfile($path);                    
            }   
        }
    }
    
    public function fetch_pictures(){
        $return = array();
        if( ($subcategory_id = $this->input->post('subcategory_id'))!== null && 
            (($report_id = $this->input->post('report_id')) || ($report_id = $this->session->userdata('report_id')))
        ){  
            $pictures = array();  
            if( $report = $this->report_model->get_object( new Report($report_id, $this->user->company_id), true)){
                $pictures = $this->report_model->get_object( new Picture(null,null, $subcategory_id, null, $report_id ) );
                $url = site_url('reports/fetch_picture/') .'/';                
            }
             
            $name = ($subcategory = $report->find_subcategory($subcategory_id, true)) ? 
                     'Images for:' .$subcategory->name : 'Property images';
            $img_srcs = array();
            foreach( $pictures as $picture ){
                $img_srcs[] = $picture->path;
            }  
            $img_srcs = $img_srcs ? $img_srcs: array( self::DUMMY_IMAGE);
            $this->load->view(
                'reports/component_gallery_popup', 
                 array(
                    'pictures'=>$img_srcs, 
                    'url'=>$url, 
                    'subcategory_id'=>$subcategory_id, 
                    'name' => $name
                 )
            );
        } 
    }    
  
    public function fetch_property_templates(){  
        $return = array();
        if( ($property_id = $this->input->post('property_id')) && 
            ($this->report_model->get_object( new Property(null,null,$property_id, $this->user->company_id, Record::ACTIVE_RECORD)) || 
             $this->report_model->get_object( new Property(null,null,$property_id, Record::SYSTEM_COMPANY_ID, Record::ACTIVE_RECORD)))
        ) {
            
            $templates =  array_merge(
                    $this->report_model->get_object(new Template(null,null,$property_id, null,null,Record::SYSTEM_COMPANY_ID, Record::ACTIVE_RECORD)),
                    $this->report_model->get_object(new Template(null,null,$property_id, null,null,$this->user->company_id, Record::ACTIVE_RECORD))
            );
            
            $array = array();
            foreach( $templates as $template ){
                $array[$template->id] = $template->name;
            }
            
            $return['status']    = 'success';
            $return['templates'] = $array;
        }
        echo json_encode($return);
    }
    
    public function fetch_address_popup(){
        if( $report = $this->get_report()){
            $states = $this->report_model->get_us_states();
            $this->load->view('reports/component_address_popup', array('report'=>$report, 'states'=>$states));
        }
    }
    
    public function make_image_public(){
        $return = array();
        if( ($image = $this->input->post('image')) && ($report_id = $this->session->userdata('report_id'))){
            if( preg_match('!/(?P<filename>[^/]+\.\w+)$!', $image, $match )){
                
                $path = parse_url( site_url()); 
                $path = str_replace('index.php','',$path['path']) ;
                $output_file = $_SERVER['DOCUMENT_ROOT'].$path.'application/views/assets/img/'.$match['filename'];
                
                if(file_put_contents( $output_file, file_get_contents($_SERVER['DOCUMENT_ROOT'].$path.'application/report_images/'.$match['filename']) )){
                    $return = array(
                        'status' => 'success',
                        'src' => $match['filename']
                    );                    
                }  
            }
        }
        echo json_encode($return);
    }
    public function delete_image(){
        if( $filename = $this->input->post('image')){                        
            if( preg_match('!/(?P<filename>[^/]+\.\w+)$!', $filename, $match )){
                if( $picture = $this->report_model->get_object(new Picture(null,null,null,$match['filename'], $this->session->userdata('report_id')), true)){                    
                    $this->report_model->delete_object( $picture );
                }
            } 
        }
    }
    
    public function delete_types(){
        $return = array();
        if( $ids = $this->input->post('ids')){
            $ids = explode('&', $ids );
            foreach( $ids as $id ){
                if(!$id) continue;
                if($type = $this->report_model->get_object(new Type($id,$this->user->company_id),true)){
                    $type->set_active(Record::NONACTIVE_RECORD);
                    $this->report_model->save_or_update_object($type);
                }
            }
            $return['status'] = 'success';
        }
        echo json_encode($return);
    }
    
    public function delete_property(){ 
        $return = array();
        if( $id = $this->input->post('property_id')){
            if( $property = $this->report_model->get_object(new Property(null,null,$id,$this->user->company_id,Record::ACTIVE_RECORD),true)){
                $templates = $this->report_model->get_object(new Template(null,null,$id,null,null,$this->user->company_id, Record::ACTIVE_RECORD));
                foreach($templates as $template){
                    $template->set_active(Record::NONACTIVE_RECORD);
                    $this->report_model->save_or_update_object($template);
                }
                
                $property->set_active(Record::NONACTIVE_RECORD);
                $this->report_model->save_or_update_object($property);
                $return['status'] = 'success';
            } 
        }
        echo json_encode($return);
    }
    
    public function delete_element(){
        $return = array();
        if( ($category_id = $this->input->post('category_id'))&&
            ($subcategory_id = $this->input->post('subcategory_id'))&&
            ($value_id = $this->input->post('value_id'))&&
            ($concern_id = $this->input->post('concern_id'))&&
            (is_numeric($index = $this->input->post('index')))&&
            ($element_type = $this->input->post('element_type'))&&
            ($object_type = $this->input->post('object_type'))&&
            ($object_id = $this->input->post('object_id'))
        ) {
            if($object = $this->get_object($object_type, $object_id)){                             
                switch($element_type){
                    case Undo::TYPE_CONCERN:
                        $deleted_object = $object->remove_concern( $category_id, $index, $subcategory_id, $value_id, $concern_id);
                    break;                
                    case Undo::TYPE_VALUE:
                        $deleted_object = $object->remove_value( $category_id, $index, $subcategory_id, $value_id);
                    break;
                    case Undo::TYPE_SUBCATEGORY:
                        $deleted_object = $object->remove_subcategory( $category_id, $index, $subcategory_id );
                    break;
                    case Undo::TYPE_CATEGORY:
                        $deleted_object = $object->remove_category( $category_id, $index );
                    break;
                }
                if( $deleted_object ){
                    $this->report_model->save_or_update_object($object);
                    $this->report_model->save_or_update_object(
                        new Undo(
                            $object_id,
                            $element_type,
                            $category_id,
                            $index,
                            $subcategory_id,
                            $value_id,
                            $concern_id,
                            $deleted_object
                        )
                    );
                }                
            }
        }        
    }
    
    public function unmake_image_public(){
        if( $image = $this->input->post('image')){
            
 
            $filename = $this->get_local_path().'application/views/assets/img/'.$image;              
            @unlink($filename);
                           
        }
    }
    
    protected function get_templates( $property_id ){
                
        $valid = false;
        foreach( $this->report_model->get_properties() as $property ){
            if( $property_id == $property->id ){
                $valid = true;
            }
        }
        
        $templates = array();
        if( $valid ){
            $templates = array_merge( 
                $this->report_model->get_object( new Template(null,null,$property_id,null,null,Record::SYSTEM_COMPANY_ID, Record::ACTIVE_RECORD)), 
                $this->report_model->get_object( new Template(null,null,$property_id,null,null,$this->user->company_id, Record::ACTIVE_RECORD))
            );
            $array = array();
            foreach( $templates as $template ){
                $array[$template->id] = $template->name;
            }
            $array[''] = 'Select template';
            $templates = $array;
        }
        return $templates;
    }
}
