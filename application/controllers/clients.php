<?php
class Clients extends MY_Controller{
    
    const AJAX_CLIENT_ORDERS = 'client_orders';

    private $field_list = array(
            'id',
            'job_name',
            'inspector::name',
            'address',
            'city',
            'paid',
            'amount_due'
    );
    
    public function __construct(){
       
        parent::__construct();
        //This section is only for Admins and Users
        $this->authorize_or_redirect( array(Person::TYPE_ADMIN,Person::TYPE_USER));
        
        $this->load->model('Client_model','client_model', TRUE );                
    }
    public function index(){    
         
        $person = new Person();
        $person->set_order_by( array('name'));
        $person->set_company_id( $this->user->company_id );
        $orders = array();
        $selected_client = null;
        $clients = $this->client_model->get_client_data( $person );
        $saved   = null;
        $client_id = null;
        $buyer = null;
        
        
        if ( $this->input->post('save_client')){  
            $client = $this->order_mapper->create_object_from_input('Person');
            
            if( $client->validate() && ($client->person_type == Person::TYPE_AGENT || $client->person_type == Person::TYPE_CLIENT)){
                $id = $this->order_model->save_or_update_object( $client );
                if( $id == $client->id ){
                    $saved = 'update';
                } else {
                    $saved = true;
                }
            } else {
                $saved = false;
            }
            $client_id = $client->id;                   
        }
        
         
        if( $client_id || ($this->uri->segment(3,0) == 'id' && $client_id = $this->uri->segment(4,0)) || ( $this->uri->segment(3,0) == 'buyer' && $buyer = $this->uri->segment(4,0))){

            
            //Get orders for this AGENT ( AGENT == CLIENT )
            $order_filter = new Filter();
            if( $client_id ){
                $person->set_person_type(null);
                $person->set_id( $client_id );
                $selected_client = reset($this->client_model->get_object( $person ));                
                $order_filter->set_client_id( $client_id );    
            }
            if( $buyer ){
                $order_filter->set_buyer( urldecode($buyer) );
            }
            $order_filter->set_is_quote(0);
            $orders = $this->client_model->get_orders( $order_filter );
            //        echo "<pre>";print_r($orders);die;

        }        
        
		
		$headermenuitems = $this->order_model->get_menuitems();
        
        $states = $this->client_model->get_us_states() + array( ''=>'-Select state-'); 
        $this->load->view('include/sidebar', array('fluid_layout'=>false,'user'=>$this->user, 'companies'=>$this->companies,'headermenuitems'=>$headermenuitems));
        $this->load->view('clients/client', array( 'clients'=>$clients ,
                                                    'orders' => $orders,
                                                    'selected_client' => $selected_client,
                                                    'saved' => $saved,
                                                    'states' => $states,
                                                    'order_fields' => $this->field_list ));
        $this->load->view('include/footer');        
    }
    public function add_new(){
        /**
        * Type of new object
        */
        $class_name = $this->uri->segment(3,0);        
        $errors     = array();
        
        $data = array(); 
        if( $this->input->post('save') && $class_name && in_array( $class_name, array('Person') )){
            
            $object = $this->order_mapper->create_object_from_input($class_name);            
             
            if( $object->validate() && ($object->company_id == $this->user->company_id) && $object->person_type == Person::TYPE_AGENT ){
                $old_id = $object->id;
                $object->set_id($this->order_model->save_or_update_object( $object ));
                if( $old_id != $object->id ){
                    $saved = true;    
                } else {
                    $saved = 'update';
                }
            } else {
                $saved = false;
                $errors = $object->errors;
            } 
            $data  = array('object' => $object, 'saved' => $saved, 'action' => site_url('clients/add_new/'.$class_name));                 
            
        }  else {
            /**
            * User clicked ADD NEW 
            */
            if( $class_name && in_array( $class_name, array('Person'))){
                $object = new $class_name();  
                if( $class_name == 'Person'){
                    $object->set_company_id( $this->user->company_id );
                    $object->set_person_type( Person::TYPE_AGENT );
                }
                $data = array( 'object' => $object, 'action' => site_url('clients/add_new/'. $class_name) );                
            } 
        }
        
        $data['errors'] = $errors;
        
        $data['states'] = $this->client_model->get_us_states() + array( ''=>'-Select state-');
        $this->load->view('common/header', array('fluid_layout'=>false, 'user'=>$this->user, 'companies'=>$this->companies));             
        $this->load->view('common/add_new', $data );             
        $this->load->view('common/footer');        
    }
    
    public function ajax_control(){ DebugBreak();
        switch( $this->input->get('type', TRUE)){
            case self::AJAX_CLIENT_ORDERS:
                echo $this->get_client_orders( $this->input->get('id', TRUE ));
            break;    
        }
    }
    public function get_client_orders( $client_id = null ){ 
         
        if( $client_id ){
            
            $filter = new Filter();
            $filter->set_client_id( $client_id );
            $orders = $this->order_model->get_orders( $filter );
            
            $rows['aaData'] = array();
            foreach( $orders as $order ){
                $row = array();
                foreach( $this->field_list as $field ){
                    if( preg_match('/(?P<object>.+)::(?P<field>.+)/', $field, $matches )){
                        $object = $matches['object'];
                        $field  = $matches['field'];
                        $row[]  = $order->$object->$field;
                    } else {
                        $row[]  = $order->$field;
                    }                       
                }
                $rows['aaData'][] = $row;
            }
             
            echo json_encode( $rows );
        }
    }
    public function export(){
      
        $nl = "\n";
        $person = new Person();
        $person->set_order_by( array('name'));
        $person->set_company_id( $this->user->company_id );
        $clients = $this->client_model->get_client_data( $person );
         
        $csv= 'Name;Phone;Email;Company;Address;City;State;Zip'.$nl;
        foreach( $clients as $client ){
            $csv .= "$client->name;$client->phone_1;$client->email;$client->company;$client->address;$client->city;$client->state;$client->zip$nl";
        }
        
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: public", false); 
        header("Content-Description: File Transfer");
        header("Content-Type: application/force-download");
        header("Accept-Ranges: bytes");
        header("Content-Disposition: attachment; filename=export.csv;");
        header("Content-Transfer-Encoding: binary");
        
        echo $csv;        
        
    }    
}
