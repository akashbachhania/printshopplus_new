<?php
class Stats extends MY_Controller{
    
    public function __construct(){
       
        parent::__construct();
        //This section is only for Admins and Users
        $this->authorize_or_redirect( array(Person::TYPE_ADMIN,Person::TYPE_USER));       
     
        $this->load->model('Stat_model','stat_model', TRUE );          
                                  
    }
    public function index(){
         
        $orders = array();
        $start_date = '';
        $end_date = '';
        $person_id = 0;
        $person_type  = 'ALL';
        $persons = array();
        
        
        if( $this->input->post('submit') ){
            $filter = new Filter();
            $filter->set_company_id( $this->user->company_id );
            if($this->input->post('start_date') != '')
                $filter->set_order_start_date( $this->input->post('start_date'));
            
            if($this->input->post('end_date') != '')
                $filter->set_order_end_date($this->input->post('end_date'));
            
            $person_type = $this->input->post('person_type');
            $person_id   = $this->input->post('person_id');
            
            $start_date = $filter->order_start_date;
            $end_date   = $filter->order_end_date;
            
            
            if( intval( $person_type )){
                $call = '';
                switch( $person_type ){
                    case Person::TYPE_AGENT:
                        $call = 'set_client_id';
                        $persons =  $this->order_model->get_person_names( Person::TYPE_AGENT, $this->user->company_id );
                    break;
                    case Person::TYPE_CLIENT:
                        $call = 'set_client_id'; 
                    break;
                    case Person::TYPE_INSPECTOR;
                        $call = 'set_sales_rep'; 
                        $persons =  $this->order_model->get_person_names( Person::TYPE_INSPECTOR, $this->user->company_id );
                    break;

                }
                $filter->$call( $this->input->post('person_id'));
            }
            
            $orders = $this->stat_model->get_orders( $filter );
        } 
        
        $this->load->view('stats/stats', array( 'orders' => $orders,
                                                'start_date'=> $start_date,
                                                'end_date'=> $end_date,
                                                'person_id' => 0,
                                                'user'=>$this->user,   
                                                'companies'=>$this->companies,
                                                'person_id' => $person_id,
                                                'persons' => $persons,
                                                'person_type' => $person_type
        ));
        $this->load->view('include/footer');                                                                                               
    }
    public function ajax_control(){ 
        if( $this->input->post('person_type') ){
            echo $this->get_persons();
        }
    }
    private function get_persons(){
        $person_type = intval( $this->input->post('person_type'));
                
        $persons = $this->stat_model->get_person_names( $person_type, $this->user->company_id );
        return json_encode( $persons );
    }
} 
?>
