<?php
class Home extends MY_Controller{
    private $field_list = array(
            'id',
            'agent::name',
            'address',
            'inspection_date',
            'inspection_time',
            'inspector::name',  
    );    
    public function __construct(){
            
        parent::__construct();
        $this->authorize_or_redirect();
        $this->load->model('Home_model','home_model', TRUE );               
    }
	
	 public function index(){    
		
		
		$jobid=urldecode($this->uri->segment(5));
		if($jobid == "d")
		{
			$jobid = urldecode($this->uri->segment(6));
			$this->home_model->deletejob($jobid);
		} 
		else if($jobid == "op")
		{
			$jobid = urldecode($this->uri->segment(6));
			$this->home_model->updateoperator($jobid,urldecode($this->uri->segment(7)));
		}
		else if($jobid == "t")
		{
			$jobid = urldecode($this->uri->segment(6));
			$this->home_model->updatetag($jobid,urldecode($this->uri->segment(7)));
		}
		else if($jobid == "s")
		{
			$jobid = urldecode($this->uri->segment(6));
			$this->home_model->updatestate($jobid,urldecode($this->uri->segment(7)));
		}
		
		$tab=urldecode($this->uri->segment(3));
		$data['Title']=$tab;
		
		$comingfrom=urldecode($this->uri->segment(4));
		$data['Sourcepage']=$comingfrom;
		
		//Get job list by order
		$data['jobs'] = $this->home_model->get_jobs_bystatus($tab);
		$data['rowcount'] = count($data['jobs']);
		
		// Get Operator list
		$data['operators'] = $this->home_model->get_operators();
		
		// Get tag list
		//$data['tags'] = $this->home_model->get_tags();
		
		// Get job status list
		$data['jobstatuscollection'] = $this->home_model->get_jobstatus();
		
		$this->load->view('common/header',array('fluid_layout'=>false, 'user'=>$this->user, 'companies'=>$this->companies,'new_order'=>false));
        $this->load->view('home/inproduction',$data);
        $this->load->view('common/footer');
    }
	
	public function editjob(){    
		
					
		$jobid=urldecode($this->uri->segment(5));
		$data['Title']=$jobid;
		
		//Get job list by order
		$data['jobs'] = $this->home_model->get_jobs_byid($jobid);
		$data['rowcount'] = count($data['jobs']);
		
		// Get Operator list
		//$data['operators'] = $this->home_model->get_operators();
		
		// Get tag list
		//$data['tags'] = $this->home_model->get_tags();
		
		// Get job status list
		//$data['jobstatuscollection'] = $this->home_model->get_jobstatus();
		
		$this->load->view('common/header',array('fluid_layout'=>false, 'user'=>$this->user, 'companies'=>$this->companies,'new_order'=>false));
        $this->load->view('home/inproductionedit',$data);
        $this->load->view('common/footer');
    }
	
    public function index1(){
        $times   =  $this->order_model->get_times();   
        /**
        * Fetch all message recipients  connected to current user
        */      
        $person = new Person();  
        $person->set_company_id( $this->user->company_id);
        $recipients = $this->home_model->get_recipients( $person, $this->user );
        /**
        * Fetch all orders
        */     
        $filter = new Filter();
        $filter->set_company_id( $this->user->company_id);
        if( $this->user->person_type == Person::TYPE_INSPECTOR ){
            $filter->set_inspector_id( $this->user->id );    
        }
        
        $orders = $this->order_model->get_orders( $filter );
        foreach( $orders as $index=>$order ){
            if( !in_array( $order->status, array(Order::ORDER_STATUS_DISPATCHED, Order::ORDER_STATUS_PENDING_REPORT, Order::ORDER_STATUS_UNPAID ))){
                unset( $orders[$index]);
            }
        }
        /**
        * Fetch all messages
        */  
        $msg_filter = new Message();
        $msg_filter->set_to_user_id( $this->user->id );
         
        $messages = $this->home_model->get_messages( $msg_filter, $this->user );
        
        $this->load->view('common/header', array('fluid_layout'=>false,'user'=>$this->user, 'companies'=>$this->companies, 'responsive_css'=>true));
        $this->load->view('home/home', array( 'orders'       => $orders, 
                                              'order_fields' => $this->field_list, 
                                              'messages'     => $messages,
                                              'user'         => $this->user,
                                              'recipients'   => $recipients,
                                              'times'        => $times
                                               ));
        $this->load->view('common/footer', array( $orders ));
    }
    public function show_message(){ 
        $filter = new Message();
        $filter->set_id( intval($this->uri->segment(3)));
        
        $messages = $this->home_model->get_messages( $filter );
        if( $messages ){
            $message = $messages['0'];
        }
        
        $this->load->view('common/header', array('fluid_layout'=>false, 'companies'=>$this->companies, 'responsive_css'=>true));
        $this->load->view('home/message', array( 'message'=> $message ));
        $this->load->view('common/footer');
    }
    public function show_user(){
        
    }
    public function get_message(){
        $return = array();
        if( $message_id = intval($this->input->post('message_id'))){
            $filter = new Message();
            $filter->set_id( $message_id );
             
            if($messages = $this->home_model->get_messages( $filter, $this->user )){
                $message = reset( $messages);
 
                $return['from_user_id'] = $message->_user_from->name;
                $return['message'] = $message->message;
                $return['message_id'] = $message->id;
                
            } else {
                $return  = array(
                    'message' => 'Cant find message'
                );               
            }
            
        }
        echo json_encode($return);
    }
    public function settings(){
         
        $saved = null;
        
        $path = parse_url( site_url());
         
        $path =  str_replace('index.php','',$path['path']) ; 
        $path = $_SERVER['DOCUMENT_ROOT'] .  $path  . 'application/views/assets/img/';
        
        $filename = $this->user->company_id.'_logo.jpg';
        
        $company = new Company();
        $company->set_id( $this->user->company_id );           
        $company = $this->order_model->get_object( $company );
        
        $primary_contact = (isset($company[0]->primary_contact) and $company[0]->primary_contact !='') ? $company[0]->primary_contact : 0;
        
        if( $this->input->post('save') ){
            
            if( $this->input->post('password') == $this->input->post('password_2')){
                $this->user->email = $this->input->post('username');
                $this->user->password = $this->input->post('password');                
                $this->home_model->save_or_update_object( $this->user );                
            }
            /**
            * TODO file checkup
            */
            if( isset( $_FILES['file'])){
                
                $this->image_resize( $_FILES['file']['tmp_name'], $path . $filename, $_FILES['file']['type'], 150 );
            }
            $saved = true; 
        }
         
        $file = null;
        if( file_exists( $path. $filename)){
             
            $file = str_replace('index.php','',site_url() .'application/views/assets/img/' . $filename);
        }
        
        $users = array();
        if($this->user->id == $primary_contact){  
            
            $person_filter = new Person();
            $person_filter->set_company_id( $this->user->company_id );    
            $person_filter->set_person_type( Person::TYPE_USER );
            $users = $this->order_model->get_object( $person_filter );
        }
        $departments = $this->order_model->getJobStatusWithDefault($this->user->company_id);
        
        $this->load->view('include/sidebar', array('fluid_layout'=>false,'dont_display_header' =>true, 'companies'=>$this->companies));
        $this->load->view('home/settings', array('user' => $this->user, 'file'=>$file,'saved'=>$saved,'primary_contact' => $primary_contact,'users'=>$users, 'departments' => $departments));
        $this->load->view('include/footer');
       
    }
    function update_order() {
        $items = $this->input->post('item');
        if(is_array($items)) {
            $order = 0;
            foreach ($items as $job) {
                $checkOrder = $this->order_model->getJobStatusOrderById($job,$this->user->company_id);
                if($checkOrder) {
                    $this->db->where('id',$checkOrder->id);
                    $this->db->update('jobstatus_order',array('orderBy' => $order));
                } else {
                    $this->db->insert('jobstatus_order',array('orderBy' => $order,'company_id' => $this->user->company_id,'job_id' => $job));
                }
                $order++;
            }
        }
    }
    public function add_new(){
         
        /**
        * Type of new object
        */
        $class_name = $this->uri->segment(3,0);
        /**
        * Company id (Only for new USER)
        */
        $company_id = $this->user->company_id;
        
        $show_user_id = $this->uri->segment(4,0);
        /**
        * Show user -> click on Coordinators table entry
        * 
        * 
        */
        $show_user = $this->uri->segment(5,0);
        
        $date = array();
        if( $this->input->post('save') && in_array( $class_name, array('Person') ) ){
            
            $object = $this->order_mapper->create_object_from_input($class_name);            
            
            if( $object->validate()){
                $old_id = $object->id;
                $object->set_id($this->order_model->save_or_update_object( $object ));
                if( $old_id != $object->id ){
                    $saved = true;    
                } else {
                    $saved = 'update';
                }
            } else {
                $saved = false;
            } 
            $data  = array('object' => $object, 'saved' => $saved, 'action' => site_url('admin/add_new/'.$class_name));                 
            
        }  else {
            /**
            * User clicked ADD NEW 
            */
            if( $show_user ){
              $filter = new Person();
              //company_id is user_id in this instance
              $filter->set_id( $show_user_id );
              $users = $this->order_model->get_object( $filter );
              $user = new Person();
              if( $users ){
                  $user = reset($users);
              }
              $data = array( 'object' => $user, 'action' => site_url('admin/add_new/'. $class_name) );
            } else if( in_array( $class_name, array('Person'))){
                $object = new $class_name();
                if( $class_name == 'Person'){
                    $object->set_company_id( $show_user_id );
                    $object->set_person_type( Person::TYPE_USER );
                }
                $data = array( 'object' => $object, 'action' => site_url('admin/add_new/'. $class_name) );                
            } 
        }
        
        $data['states'] = $this->order_model->get_us_states();
        $this->load->view('include/sidebar', array('fluid_layout'=>true, 'user'=>$this->user,'dont_display_header'=>true));             
        $this->load->view('common/add_new', $data );             
        $this->load->view('include/footer');           
    }
    function image_resize( $f, $new_file, $type, $size = null){
        
        $imgFunc = '';
        switch($type) {
            case 'image/gif':
                $img = ImageCreateFromGIF($f);
                $imgFunc = 'ImageGIF';
                break;
            case 'image/jpeg':
                $img = ImageCreateFromJPEG($f);
                $imgFunc = 'ImageJPEG';
                break;
            case 'image/png':
                $img = ImageCreateFromPNG($f);
                $imgFunc = 'ImagePNG';
                break;
        }
        
        list($w,$h) = GetImageSize($f);
         
        $percent = $size / (($w>$h)?$w:$h);
        if($percent>0 and $percent<1)
        {
            $nw = intval($w*$percent);
            $nh = intval($h*$percent);
            $img_resized = ImageCreateTrueColor($nw,$nh);
            imagecopyresampled($img_resized, $img, 0, 0, 0, 0, $nw, $nh, $w, $h);
        }
         if($imgFunc == 'ImagePNG')
             $imgFunc($img_resized, $new_file,9,PNG_ALL_FILTERS);
         else             
            $imgFunc($img_resized, $new_file,100);
         
        ImageDestroy($img_resized);         
    }    
    public function ajax_control(){
        
   
    }
    public function send_message(){
           
        $message = $this->order_mapper->create_object_from_input('Message');
        $message->set_from_user_id( $this->user->id );
        $message->set_datetime( gmdate('Y-m-d h:i:s'));
        
        if( $message->validate()){
            $this->home_model->save_or_update_object( $message );
            $return['success'] = 'true';
        } else {
            $return['success'] = 'false';
        }
        echo json_encode( $return );
    }
    public function delete_message(){ 
        $message = new Message();
        $message->set_id( $this->input->post('message_id'));
        $message->set_to_user_id( $this->user->id );
        
         
        $messages = $this->order_model->get_object( $message );
        if( $messages ){
            $this->order_model->delete_object( $message );
            $return['success'] = 'true';
        } else {
            $return['success'] = 'false';
        }
        echo json_encode( $return );
    }
    public function get_order(){
         
        $fields = array(
            'id',
            'amount_due',
            'paid',
            'total',
            'tax',
            'subtotal',
            'meet_with',
            'city',
            'zip',
            'notes_inspection_details',
            'order_date',
            'address',
            'estimated_inspection_time',
            'status'
        );
        
        $order_filter = new Filter();
        $order_filter->set_company_id($this->user->company_id);
        $order_filter->set_order_id( $this->input->post('id', TRUE ));
        
        $orders = $this->home_model->get_orders( $order_filter );
        $return = array();
        if( $orders ){
            $order = $orders['0'];
            foreach( $fields as $field ) {
                $return[ $field ] = $order->$field;
            }
            foreach( $order->items as $item ){
                $return['items'][]=array( 'name'=>$item->description ,'price' => $item->price );
            }
            if( $order->status = Order::ORDER_STATUS_REPORT_STARTED ||$order->status == Order::ORDER_STATUS_REPORT_COMPLETED ){
                $return['url'] = site_url('reports/new_report?order_id='.$order->id);
            }
        }
        
        echo json_encode( $return );
    }
}
?>
