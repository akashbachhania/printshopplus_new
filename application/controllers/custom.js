


/* Default class modification */

$.extend( $.fn.dataTableExt.oStdClasses, {

    "sWrapper": "dataTables_wrapper form-inline"

} );



/* API method to get paging information */

$.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )

{

    return {

        "iStart":         oSettings._iDisplayStart,                                       

        "iEnd":           oSettings.fnDisplayEnd(),

        "iLength":        oSettings._iDisplayLength,

        "iTotal":         oSettings.fnRecordsTotal(),

        "iFilteredTotal": oSettings.fnRecordsDisplay(),

        "iPage":          Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),

        "iTotalPages":    Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )

    };

}



/* Bootstrap style pagination control */

$.extend( $.fn.dataTableExt.oPagination, {

    "bootstrap": {

        "fnInit": function( oSettings, nPaging, fnDraw ) {

            var oLang = oSettings.oLanguage.oPaginate;

            var fnClickHandler = function ( e ) {

                e.preventDefault();

                if ( oSettings.oApi._fnPageChange(oSettings, e.data.action) ) {

                    fnDraw( oSettings );

                }

            };



            $(nPaging).addClass('pagination').append(

                '<ul>'+

                    '<li class="prev disabled"><a href="#">&larr; '+oLang.sPrevious+'</a></li>'+

                    '<li class="next disabled"><a href="#">'+oLang.sNext+' &rarr; </a></li>'+

                '</ul>'

            );

            var els = $('a', nPaging);

            $(els[0]).bind( 'click.DT', { action: "previous" }, fnClickHandler );

            $(els[1]).bind( 'click.DT', { action: "next" }, fnClickHandler );

        },



        "fnUpdate": function ( oSettings, fnDraw ) {

            var iListLength = 5;

            var oPaging = oSettings.oInstance.fnPagingInfo();

            var an = oSettings.aanFeatures.p;

            var i, j, sClass, iStart, iEnd, iHalf=Math.floor(iListLength/2);



            if ( oPaging.iTotalPages < iListLength) {

                iStart = 1;

                iEnd = oPaging.iTotalPages;

            }

            else if ( oPaging.iPage <= iHalf ) {

                iStart = 1;

                iEnd = iListLength;

            } else if ( oPaging.iPage >= (oPaging.iTotalPages-iHalf) ) {

                iStart = oPaging.iTotalPages - iListLength + 1;

                iEnd = oPaging.iTotalPages;

            } else {

                iStart = oPaging.iPage - iHalf + 1;

                iEnd = iStart + iListLength - 1;

            }



            for ( i=0, iLen=an.length ; i<iLen ; i++ ) {

                // Remove the middle elements

                $('li:gt(0)', an[i]).filter(':not(:last)').remove();



                // Add the new list items and their event handlers

                for ( j=iStart ; j<=iEnd ; j++ ) {

                    sClass = (j==oPaging.iPage+1) ? 'class="active"' : '';

                    $('<li '+sClass+'><a href="#">'+j+'</a></li>')

                        .insertBefore( $('li:last', an[i])[0] )

                        .bind('click', function (e) {

                            e.preventDefault();

                            oSettings._iDisplayStart = (parseInt($('a', this).text(),10)-1) * oPaging.iLength;

                            fnDraw( oSettings );

                        } );

                }



                // Add / remove disabled classes from the static elements

                if ( oPaging.iPage === 0 ) {

                    $('li:first', an[i]).addClass('disabled');

                } else {

                    $('li:first', an[i]).removeClass('disabled');

                }



                if ( oPaging.iPage === oPaging.iTotalPages-1 || oPaging.iTotalPages === 0 ) {

                    $('li:last', an[i]).addClass('disabled');

                } else {

                    $('li:last', an[i]).removeClass('disabled');

                }

            }

        }

    }

} );

/**

* Here we'll assign event handlers for AJAX events

* - New item: for all Selects

* - Client/Agent: on Name change, details are filled in

* - Items table: on Item change, details are filled in, price is recalculated

* - Items table: Add new row, new row is inserted to table

*/

$(document).ready( 

        function(){

            

            if( $('#div_home').length > 0 ) {

                $('#a_home').addClass('active').parent().addClass('active');

            }

            if( $('#div_order_details').length > 0 || $('#div_view_orders').length > 0 ) {

                $('#a_orders').addClass('active').parent().addClass('active');

            }            

            if( $('#div_clients_table').length > 0 ) {

                $('#a_clients').addClass('active').parent().addClass('active');

            } 

            if( $('#div_inspectors_table').length > 0 ) {

                $('#a_inspectors').addClass('active').parent().addClass('active');

            } 

            if( $('#div_schedule').length > 0 ) {

                $('#a_schedule').addClass('active').parent().addClass('active');

            }  

            if( $('#div_stats_filter').length > 0 ) {

                $('#a_stats').addClass('active').parent().addClass('active');

            }

            if( $('#div_companies_table').length > 0 ) {

                $('#a_admin').addClass('active').parent().addClass('active');

            }

            if( String(window.location).match(/(.*)(?:reports)/)){

                $('#a_reports').addClass('active').parent().addClass('active');    

            }

            

            $('.alert').click(

                function(){

                    $(this).hide();

                }

            )

            /**

            * Datepicker                                   

            */

            $('[data-datepicker="datepicker"]').change(

                function(){

                    var value = $(this).attr('value');

                    $(this).attr('value', value.substring(5,7)+'-'+value.substring(8,10)+'-'+value.substring(0,4));

                     

                }

            )



            $('#btn_window_close').click(

                function(){

                    window.close();

                }

            )

            /**

            * Set default look for all tables                        

            */

            $('table').addClass('table table-striped ');

            

            /**

            * Common: Orders table - All orders table are under same id div  #div_orders_table

            */

            /**

            * This is Orders table drill down           

            */

            $('#div_orders_table table tr').each(

                function(){
                    var editString = $(this).children().first().next().text();
                       if($(this).children().first().text() != 'Id'){
                                              
                            $(this).prepend('<td><input type="checkbox" name="ord_' + $(this).children().first().text() + '" value="' + $(this).children().first().text() + '"></td>');                                

                        } 
                        else{
                            var imgpath = $(location).attr('hostname') +'/live/application/views/assets/img/trash.png';
                            $(this).prepend("<td><img id='deleteall' name='deleteall' src='http://"+ imgpath +"' title='Delete' /></td>");
                            //$(this).prepend("<td></td>");

                        }
                        
                        

                        $(this).children().each(

                            function(){

                                var text = $(this).text();

                                if( text == 'Total' ){

                                    $(this).addClass('right ctotal'); 

                                }     

                                if( text == 'Paid' ){

                                    $(this).addClass('cpaid');    

                                }

                                if( text == 'Amount due'){

                                    $(this).addClass('cdue');    

                                }

                                if( text == 'Id'){

                                    $(this).addClass('chidden');

                                }                                                                     

                            }                

                        );



                }

            )

            

             

            var order_table_config = new Array();

            order_table_config =  {

                 "aoColumnDefs": [

                                            {

                                                "fnRender": function ( oObj, sVal ) {

                                                    var td = $('td:contains("'+sVal+'")');

                                                    var number = sVal.match(/>(.+)?</).pop();                                                    

                                                    var vclass = 'right ';

                                                    if( Number(number) > 0 ){

                                                        vclass+='paid';

                                                    }

                                                    td.formatCurrency();

                                                    var html = td.html();

                                                    var ret  = sVal.replace( number, html ).replace('<a','<a class="'+vclass+'" ');

                                                     

                                                    return ret;

                                                },

                                                "aTargets": [ 'cpaid' ], 

                                            },

                                            {

                                                "fnRender": function ( oObj, sVal ) {                                                    

                                                    var td = $('td:contains("'+sVal+'")');

                                                    var number = sVal.match(/>(.+)?</).pop();                                                    

                                                    var vclass = 'right ';

                                                    if( Number(number) > 0 ){

                                                        vclass+='unpaid';

                                                    }

                                                    td.formatCurrency();

                                                    var html = td.html();

                                                    var ret  = sVal.replace( number, html ).replace('<a','<a class="'+vclass+'" ');

                                                     

                                                    return ret;

                                                },

                                                "aTargets": [ 'cdue' ],                                                

                                            },

                                            {

                                                "fnRender": function ( oObj, sVal ) {                                                    

                                                    var td = $('td:contains("'+sVal+'")');

                                                    var number = sVal.match(/>(.+)?</).pop();

                                                    td.formatCurrency();

                                                    var html = td.html();

                                                    var ret  = sVal.replace( number, html );

                                                    return ret;

                                                },

                                                "aTargets": [ 'ctotal' ],

                                                "sClass":"right"                                                

                                            },

                                            /*{

                                                "fnRender": function ( oObj, sVal ) {                                                    

                                                    return sVal;

                                                },

                                                "aTargets": [ 'chidden' ],

                                                "sClass":"xxhidden"                                                

                                            }*/                                                                                         

                                        ],                

                    "sDom": "<<l><f>r>t<'row'<'span2'i><p>>", 

                    "sPaginationType": "bootstrap",

                   

                     "oLanguage": {

                        "sInfo": "_TOTAL_ Orders (_START_ to _END_)",

                        "sInfoEmpty": "No matching Orders",

                        "sEmptyTable": "-",

                        "sSearch": "Search:",

                        "sLengthMenu": "_MENU_",

                        "oPaginate": {

                             "sNext": "Next",

                             "sPrevious": "Prev",

                        }                         

                    }

            };                 

            /**

            * Config table

            */
            var oDataTable = $('#div_orders_table table').dataTable(order_table_config);



            

            /**

            * Drill down for orders table                       

            */

            $('#div_orders_table table tr i').click(

                function(){

  

                    if( $(this).parent().parent().next().attr('class') == 'tr_orders_drill' ){

                        if($(this).parent().parent().next().is(':visible')){

                            $(this).parent().parent().next().hide('slow');

                            $(this).removeClass('icon-minus');

                            $(this).addClass('icon-plus');

                        } else {

                            //$(this).parent().parent().next().show('slow');

                            $(this).parent().parent().next().slideDown('slow');;

                            $(this).removeClass('icon-plus');

                            $(this).addClass('icon-minus');                            

                        }

                            

                    } else {

                        

                        var length = $(this).parent().parent().children().length;

                        

                        var id  = $(this).parent().parent().children().first().next().text();

                        var row = $(this).parent().parent();

                        

                        var location = String(window.location);

                          

                        $.post(String(window.location).match(/(.*)(?:home|\/orders\/|clients|stats|inspectors|schedule|admin|\/reports\/)/).pop()+"/home/get_order", 

                            { id: id },

                            function(data) {

                                var response = jQuery.parseJSON( data );

                                row.after( '<tr class="tr_orders_drill"><td colspan="'+length+'">'+get_drill_down_order( response )+'</td></tr>' );

                                $('.tr_orders_drill').hide();  

                                row.next().show('slow');                                                                

                            });                          

                        $(this).removeClass('icon-plus');

                        $(this).addClass('icon-minus'); 

                    } 

                    return false;

                }

            )

             

            /**

            * Legend click                          

            */

            /*

            $('legend').each(

                function(){

                    $(this).html( '<i name="icon_collapse" >&nbsp;&nbsp;</i>' + $(this).text() );

                }

            ) */

            /**

            * Admin company select box

            */

            $('#select_admin_view_companies').change(

                function(){

                    $.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+"/admin/ajax_control", 

                        { company_id: $(this).attr('value') },

                        //Fill in Person form with data

                        function(data) {                            

                            location.reload();

                        });                    

                }

            )

            /**

            * 

            */
			
			// Fire delete all order image button click event
			$('[id^="deleteall"]').click(

                    
			    function(){
			        
                     var chkArray = [];
                     var rowIndexList = [];
     
     
                    /* look for all checkboes that have a parent id called 'div_orders_table' attached to it and check if it was checked */
                    $("#div_orders_table input:checked").each(function() {                       
                        rowIndexList.push($(this).closest('tr')[0].sectionRowIndex); 
                        chkArray.push($(this).val());
                    });
                     
                    /* we join the array separated by the comma */
                    var selected;
                    selected = chkArray.join(',');
                     
                    /* check if there is selected checkboxes, by default the length is 1 as it contains one single comma */
                    if(selected.length > 1){
                        
                        var status = confirm("Are you sure you want to delete this job? This can not be undone.");
                        if(status==true) {
                            $.post("http://"+$(location).attr('hostname')+"/live/orders/deleteorder", 
    
                                { orderids: encode_array(chkArray) },
                            
                                                function(data) {  
                                                
                                                    var ret_val  = jQuery.parseJSON( data );
                                                     
                                                     if( ret_val['status'] == 'success' ){
                                                     	window.location=location.href;
                                                          /*for (rowIndex in rowIndexList) {                                                            
                                                            
                                                            oDataTable.fnDeleteRow(rowIndexList[rowIndex]);  
                                                                             
                                                          }  
                                                          oDataTable.fnDraw();  */
                                                                      
                                                    } else {
                
                                                        alert('Error in deleting orders.');
                
                                                    }
                            
                                                }
                            );
    
                        }
                         
                    }else{
                        alert("Please select order to delete.");   
                    }
                    
                }
            )
			
			$('#terms').change(

                    function(){
						
						value = $(this).attr('value');
						if(value=='_7')
						{
	                       window.open(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+"orders/add_new?type=" + value,"Add new", "status=1,height=500,width=520,scrollbars=no,toolbar=no,location=no");    
						}

                    }

                )
		

            $('[name="select_edit"]').change(

                function(){
					
					var item_type = $('[name="object_type"]').attr('value');
                    var item_id   = $(this).attr('value');
					var selected_item_type = 'SalesRep';
					
					switch(item_type)
					{
					case 'Terms':
					  selected_item_type="terms";
					  break;
					case 'JobStatus':
					  selected_item_type="jobstatus";
					  break;
					case 'ShippingMethod':
					  selected_item_type="shipping";
					  break;
					case 'OperatorList':
                      selected_item_type="OperatorList";
                      break;
                    case 'Person':
                      selected_item_type = 'CompanyList';
                      break;
                    case 'Base_item':
                      selected_item_type = 'item';
                      break;
                    case 'Coating':
                      selected_item_type = 'Coating';
                      break;
					default:
					  selected_item_type = 'SalesRep';
					  break;
					}
					
					$.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+"orders/ajax_control", 

                    { type: selected_item_type, item_type: item_type, id: item_id },

                    //Fill in Person form with data

                    function(data) {  
					
						 var ret_val  = jQuery.parseJSON( data );
						 
                         for( var key in ret_val ){
							 
                             $('#' + key).attr('value', ret_val[ key ]);

                         }

                    });
					
                }

            )

            /**

            * ORDERS - Select box refresh after creation of new item

            */

            if( $('#select_refresh').attr('value') != undefined ){
				
                window.opener.refresh_item_select(  $('#select_refresh').attr('value'));

            } else {

                var href = String(window.location); 
                

                if( href.match(/orders\/add_new/) ){

                    window.opener.refresh_all();

                }                

            }            

            /**

            * ORDER

            */

            if( String(window.location).match(/orders/) ){

                //Total

                /**

                * Tax value holder field, used in calculation

                */

                $('#div_id_calculation').append('<input type="hidden" value="0" id="tax_type_percentage"/>');



                //Client/Agent/inspector - Drop down change

                $('#client_name, #name, #inspector_id').change(
		
                    function(){

                        
                        //Open pop up for creation of new person

                        if( $(this).attr('value') == '_1' ||

                            $(this).attr('value') == '_2'|| 

                            $(this).attr('value') == '_3' ){ //ddd//

                            window.open(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+"orders/add_new?type=" + $(this).attr('value') ,"Add new", "status=1,height=720,width=475");

                        

                        //Update person form    

                        } else {

                             //Send AJAX request

                             $.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+"/orders/ajax_control", 

                                    { value: $(this).attr('value'), name: $(this).attr('name'), type: 'person'},

                                    //Fill in Person form with data

                                    function(data) {                            

                                         var ret_val  = jQuery.parseJSON( data );

                                         for( var key in ret_val ){

                                             $('#' + key).attr('value', ret_val[ key ]);

                                         }

                                    });                         

                        }

                    }

                )



 		       

                $('#sales_rep,#terms,#status,#shipping_method,#Coating,#company').click(

                    function(){

                        

                        var parent = $(this);

                        //$.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+"orders/ajax_control",
                        	$.post("http://"+$(location).attr('hostname')+"/live/orders/ajax_control",  

                            { id: $(this).attr('id'), type: 'refresh_dropdown' },

                            function(data) {

                                //alert(data);
								
                                var ret_val  = jQuery.parseJSON( data );

                                var ids      = new Array();                        

                                parent.children().each(

                                    function(){

                                        ids.push( $(this).attr('value'));

                                    }

                                );

                                

                                for( var key in ret_val ){

                                    if( jQuery.inArray(key ,ids) == -1){

                                        parent.children().removeAttr('selected');

                                        parent.append('<option selected="selected" value="'+key+'">'+ret_val[key]+'</option>');

                                        parent.trigger('change');

                                    }

                                }

                            });                         

                    }

                );

				// Print button clicked
				$('#btn_print_order').click(

                    function(){

$("#form_order").submit(function(){
  alert("Submitted");
});

						/*alert('test');

                        var query = $('#form_order').serialize();
                        alert(query);
				        var url = String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+"orders/new_order";
				        alert(url);
				        $.post(url, query, function (response) {
				         	alert (response);
				        });*/
                        

                        
                    }

                );
                

                //Item - Drop down change

                $('[name^="item_item_id_"]').change( item_change );

                

                                           

                //Item price change, recalculation - TODO JS optimization

                $('[id^="item_price_"]').change( item_price_change );

                

                $('#tax_type').change(

                    function(){

                        

                        value = $(this).attr('value');

                        var regex = /new_tax/;

                        if( regex.test( value )){

                            window.open(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+"/orders/add_new?type=" + value,"Add new", "status=1,height=720,width=475");    

                        } else {

                            var tax_type = $(this).attr('value');

                            

                            if( Number(tax_type )>0){

                                $.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+"/orders/ajax_control", 

                                    { type: 'tax', id: tax_type},                        

                                    function(data) {                            

                                        var hidden = $('#tax_type_percentage');

                                        var response = jQuery.parseJSON( data );

       

                                        hidden.attr('value', response['value']);

                                        $('#tax').trigger('change');

                                    });                             

                            }

        

                        }

                    }

                )
				
				// 
				
			
				
				 $('#sales_rep').change(

                    function(){
						
						value = $(this).attr('value');
						if(value=='_4')
						{
	                       window.open(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+"orders/add_new?type=" + value,"Add new", "status=1,height=500,width=520,scrollbars=no,toolbar=no,location=no");    
						}

                    }

                )
				 
				 $('#status').change(

                    function(){
						
						value = $(this).attr('value');
						if(value=='_5')
						{
                       		window.open(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+"orders/add_new?type=" + value,"Add new", "status=1,height=500,width=520,scrollbars=no,toolbar=no,location=no");  
						}

                    }

                )
                
                $('#company').change(

                    function(){
                        
                        value = $(this).attr('value');
                        if(value=='_9')
                        {
                            window.open(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+"orders/add_new?type=" + value,"Add new", "status=1,height=600,width=550,scrollbars=yes,toolbar=no,location=no");  
                        }
                        else {
                            
                            // Call ajax to fill text box value
                            $.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+"orders/ajax_control",

                                { type: 'CompanyList', item_type: $(this).attr('value'), id: $(this).attr('value') },
            
                                //Fill in Person form with data
                            
                                function(data) {  
                                
                                     var ret_val  = jQuery.parseJSON( data );

                                     
                                     for( var key in ret_val ){
                                         
                                         $('#client_' + key).attr('value', ret_val[ key ]);
            
                                     }
            
                                });
                            
                        }

                    }

                )
				 
			
				 
				 //$('#client_shipping').change(
				 $('#shipping_method').change(

                    function(){

						value = $(this).attr('value');
						if(value=='_6')
						{
                       		window.open(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+"orders/add_new?type=" + value,"Add new", "status=1,height=500,width=520,scrollbars=no,toolbar=no,location=no");    
						}
                    }

                )
				

                $('#paid').change(

                    function(){

                        if( isNaN( $(this).attr('value'))){

                            $(this).attr('value','0.00' );    

                        } else {

                            $('#amount_due').attr('value', Number($('#total').attr('value')) - Number($(this).attr('value')) );

                            if($('#amount_due').attr('value').indexOf('.') == -1 ){

                                $('#amount_due').attr('value', $('#amount_due').attr('value') + '.00' );      
								

                            }

                        }   

                    }

                )

                $('#amount_due, #subtotal, #total, #tax,#shipping_amount').change( calculate );

                

                //Terms,Types select boxes

//                $("#terms, #type_of_inspection, #type_of_structure, #type_of_foundation, #type_of_utilities, #tax_").change(
                $("#type_of_inspection, #type_of_structure, #type_of_foundation, #type_of_utilities, #tax_").change(

                    function(){

                        value = $(this).attr('value');

                        var regex = /__\d/;

                        if( regex.test( value )){

                            window.open(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+"orders/add_new?type=" + value,"Add new","status=1,height=720,width=475");    

                        }

                    }

                )

				
				 
                /**

                * Add decimal zeroes

                */

                $('[name^="item_price_"], #total, #subtotal, #paid, #amount_due,[name="amount"]').each(

                    function(){

                        if(  $(this).attr('value') == false ){

                             $(this).attr('value','0.00');

                        }

                        if($(this).attr('value').indexOf('.') == -1 ){

                            $(this).attr('value', $(this).attr('value') + '.00' );      

                        }                    

                    }

                )           

                //Add new row - On click adds new row to our items table

                $("#add_item_row").click(

                    function(){                    

                        var table = $("table>tbody");

                        var rows  = $("table tr");

                        

                        var row_count = rows.length - 2;

                        var new_row_count = row_count + 1;

                        

                        var last_row_html = rows.last().html().replace(/_\d/g,'_' + new_row_count).replace( '<td>'+row_count+'</td>','<td>'+new_row_count+'</td>');

                        

                        table.append('<tr>' +last_row_html  +'</tr>' );

                        

                        var item_name = 'item_item_id_'+new_row_count;

                        //Add change handler for new item

                        $('[name^="'+item_name+'"]').change( item_change );

                        //Empty input fields

                        $('#item_description_' + new_row_count).attr('value','');

                        $('#item_price_' + new_row_count).attr('value','');

                        //Bind event handler to new #item_price

                        $('#item_price_' + new_row_count).change( item_price_change );

                        //Hover event handler

                        table.find('tr').hover(

                            function(){

                               $(this).children().last().children().show();

                            },

                            function(){

                               $(this).children().last().children().hide();     

                            } 

                        );

                        //Bind event handler for click on new Remove - Recalculate

                        table.find('i').click( recalculate );

                    }

                )

                $("#div_client_title").click(

                    function(){

                        $("#div_client_details").toggle('slow');

                        $(this).find('.right_column').toggle();

                        $("#icon_collapse").toggleClass('icon-minus icon-plus');

                    }

                )

                $("#div_items_title").click(

                    function(){

                        $("#id_div_items").toggle('slow');

                        $(this).find('.right_column').toggle();

                        $("#icon_collapse_items").toggleClass('icon-minus icon-plus');

                    }

                )            

                $("#div_agent_title").click(

                    function(){

                        $("#div_agent").toggle('slow');

                        $(this).find('.right_column').toggle();

                        $(this).find('i').toggleClass('icon-minus icon-plus');

                    }

                )             

                $("#id_div_items tr").hover(

                    function(){

                       $(this).children().last().children().show();

                    },

                    function(){

                       $(this).children().last().children().hide();     

                    } 

                );

                 

                $('#inspector_id, #inspection_time, #inspection_date, #estimated_inspection_time').change( check_inspector );

                

                //$('#div_order_client fieldset'). height($('#div_order_inspection fieldset').height());
                $('#div_order_inspection fieldset'). height($('#div_order_client fieldset').height());

                 

                $('#div_agent_details  fieldset').height( $('#div_ins_details  fieldset').height());

                

                $('#div_id_charge  fieldset').height( $('#div_id_calculation  fieldset').height());

                

                

                /**

                * Remove Order item row Icon ( when user clicks icon, it removes parent row

                */

                $("#id_div_items tr>td>i").hide();

                /**

                * Recalculate after row was removed

                */

                $("#id_div_items tr>td>i").click( recalculate );

                /**

                * Fixed last column with ( so it doesn change size on hover over the Remove icon )

                */

                $('[class="icon-remove-sign"]').parent().addClass('td_item_remove');

                /**

                * Remove Result div on click

                */

                $("#div_order_added>a").click(

                    function(){

                        $(this).parent().hide();

                    }

                )

         

                /*MODAL*/

                $('#delete').click(

                    function(){

                        $('#p_delete_text').text('Are you sure you want to delete this order?');

                        $('#h_delete_header').text('Delete order');

                        $('#delete_modal').modal('toggle');                                        

                        return false;                     

                    } 

                )

                

                $('#email_order').click( 

                    function(){

                        if( $('#status').attr('value')=='7'){

                            $('#p_modal_text').text('This inspection is pending.  Please change status to email');

                            $('#h_modal_header').text('Email');

                            $('#alert_modal').modal('toggle');

                                                    

                        }

                    }

                )



                $.extend( $.fn.dataTableExt.oStdClasses, {

                    "sWrapper": "dataTables_wrapper form-inline"

                } );            

                

                $('#email').click(

                    function(){

                        var id = $('#id').attr('value') ;

                        return false; 

                        if( isNaN(id) || typeof id == undefined || id == null || id == ''){

                            $('#p_modal_text').text('You must save order before printing or emailing!');

                            $('#h_modal_header').text('Warning');

                            $('#alert_modal').modal('toggle');

                                                     

                        }

                    }

                )

                

                $('#table_charge').removeClass();

                $('#btn_charge').click(

                    function(){

                        $('#div_order_charged').hide(); 

                        $('#charge_modal').modal('toggle');

                    }

                )

                /**

                * Add decimal zeroes

                */

                $('#charge_amount').each(

                    function(){

                        if(  $(this).attr('value') == false ){

                             $(this).attr('value','$0.00');

                        }

                        if($(this).attr('value').indexOf('.') == -1 ){

                            $(this).attr('value', '$'+$(this).attr('value') + '.00' );      

                        }

                        if($(this).attr('value').indexOf('$') == -1 ){

                            $(this).attr('value', '$'+$(this).attr('value') );      

                        }                                            

                    }

                )                

                $('[name="amount"]').change(

                    function(){

                        var value = $(this).attr('value');

                        var amount_due = Number($('#amount_due').attr('value'));

                        var charge_amount = Number($(this).attr('value').replace('$'));

                        $('#amount').attr('value',charge_amount);

                        if( charge_amount > amount_due ){

                            $(this).parent().parent().parent().addClass('warning');

                            $(this).next().removeClass('hidden').text('Amount is larger than amount due!');

                        } else if(isNaN(charge_amount)){

                            $(this).parent().parent().parent().addClass('error');

                            $(this).next().removeClass('hidden').text('Invalid amount!');                            

                        } else {

                            $(this).next().addClass('hidden').text('');

                            $(this).parent().parent().parent().removeClass('warning').removeClass('error');    

                        }

                        $('#charge_amount').attr('value', value );

                    }

                )

                $('#cc_num').change(

                    function(){

                        if(isNaN($(this).attr('value')) || $(this).attr('value').length < 13){

                           $(this).next().text('Invalid Credit card number value');

                           $(this).parent().parent().addClass('error');

                        } else {

                           $(this).next().text('');

                           $(this).parent().parent().removeClass('error');                            

                        }

                    }

                )

                $('#cc_date').change(

                    function(){

                        if(isNaN($(this).attr('value'))){

                           $(this).next().text('Invalid Expire date value');

                           $(this).parent().parent().addClass('error');

                        } else {

                           $(this).next().text('');

                           $(this).parent().parent().removeClass('error');                            

                        }

                    }

                )

                $('#btn_charge_modal').click(

                    function(){                        

                        $('#div_order_charged').hide();

                        var cc_num  = $('#cc_num');

                        var cc_date = $('#cc_date');

                        var amount  = $('#amount');

                        var error   = false;

                        

                        if( cc_num.attr('value')=="" || isNaN(cc_num.attr('value'))){

                           cc_num.next().text('Invalid CC number value');

                           cc_num.parent().parent().addClass('error');

                           error = true;                            

                        }

                        if( cc_date.attr('value')=="" || isNaN(cc_date.attr('value'))){

                           cc_date.next().text('Invalid Expire date value');

                           cc_date.parent().parent().addClass('error');

                           error = true                            

                        }

                        if( amount.attr('value')=="" || isNaN(amount.attr('value'))){

                           amount.next().text('Invalid amount!');

                           amount.parent().parent().parent().addClass('error');

                           error = true                              

                        }

                        if( error ){

                            $('#div_order_charged span').text('Please correct the fields below');

                            $('#div_order_charged').addClass('alert alert-error').show();

                        } else {

                            if(confirm('Are you sure you want to charge this credit card with '+amount.attr('value')+'$?')){

                                $('#form_cc').submit();

                                $('#charge_modal').modal('toggle');

                            }

                        }

                    }

                )

                $('[name="span_help"]').tooltip();

                

                $('.fill_in').hide();

                $('#select_fill_in').change(

                    function(){

                        var value = $(this).attr('value');

                        var prefix  = 'none';

                        var fill_in = false;

                        

                        var array = new Array('name','address','zip','city');

                        switch(value){

                            case '1':

                                for( key in array ){

                                    $('#charge_modal [name="'+array[key]+'"]').attr('value', '');

                                }

                                $('#charge_modal [name="state"]').find('[selected="selected"]').removeAttr('selected');                              

                            break;

                            case '2':

                                fill_in = true;

                                prefix = 'agent_';

                            break;

                            case '3':

                                fill_in = true;

                                prefix = 'client_';                            

                            break;

                            case '4':

                                fill_in = true;

                                prefix = '';

                            break;

                            case '5':

                                fill_in = true;

                                for( key in array ){

                                    $('#charge_modal [name="'+array[key]+'"]').attr('value', '');

                                }

                                $('#charge_modal [name="state"]').find('[selected="selected"]').removeAttr('selected');                                

                            break;                                                                                                                

                        }

                        

                        if( fill_in ){

                            $('.fill_in').show();

                        } else {

                            $('.fill_in').hide();

                        }



                        if( prefix ){

                            for( key in array ){

                                if( key == 'name'){

                                    var names = ret_val['details']['name'].split(' ');    

                                }

                                $('#charge_modal [name="'+array[key]+'"]').attr('value', $('#'+prefix +''+array[key]).attr('value'));

                            }

                            

                            if( prefix == ''){

                                var names = $('#meet_with').attr('value').split(' ');     

                            } else {

                                 

                            }

                            

                            

                            $('#charge_modal [name="last_name"]').attr('value','');

                            for( index in names ){

                                if( index == 0 ){

                                    $('#charge_modal [name="first_name"]').attr('value',names[index]);

                                    //$('#charge_modal [name="first_name"]').parent().parent().addClass('warning');

                                    //$('#charge_modal [name="first_name"]').next().text('Plese verify this field');

                                } else{

                                    $('#charge_modal [name="last_name"]').attr('value', $('#charge_modal [name="last_name"]').attr('value') + ' ' + names[index]);    

                                   // $('#charge_modal [name="last_name"]').parent().parent().addClass('warning');

                                    //$('#charge_modal [name="last_name"]').next().text('Plese verify this field'); 

                                }

                            }

                            

                            $('#charge_modal [name="state"]').find('[selected="selected"]').removeAttr('selected');                            

                            $('#charge_modal [name="state"]').find('[value="'+ $('#'+prefix +'state').attr('value') +'"]').attr('selected','selected');

                            

                            $('#charge_modal [name="country"]').attr('value','US');

                        } else {

                             

                        }

                        

                    }

                )

                

            }    

            /**

            * ************

            * ************

            * Clients view

            * ************

            * ************

            */

            if( String(window.location).match(/clients/) ){

                $('#a_excel').tooltip();

                $('#div_clients_table table tr').click(

                    function(){

                        var id = $(this).children().first().text();

                        

                        if( id == ''){

                            var form = $( document.createElement('form') );

                            id = escape($(this).children().first().next().text());                    

                            form.attr('action',String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+'/clients/index/buyer/'+id);

                            form.attr('method','get');

                            $('#div_clients_table').append(form);

                            form.submit();                        

                        } else {

                            var form = $( document.createElement('form') );                    

                            form.attr('action',String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+'/clients/index/id/'+id);

                            form.attr('method','get');

                            $('#div_clients_table').append(form);

                            form.submit();                         

                        }

                                                                 

                    }

                )

                

                var clients_table = $('#div_clients_table table').dataTable( {

                        "aoColumnDefs": [

                            {

                                "fnRender": function ( oObj, sVal ) {

                                    var span = $('span[value="'+sVal+'"]');

                                    if( Number(sVal) > 0 ){

                                        span.addClass('unpaid');

                                    }

                                    span.formatCurrency();

                                    

                                    return span.html();

                                },

                                "aTargets": [ 2 ],

                                "sClass": "right"

                            },

                        ],                         

                        "sPaginationType": "bootstrap",

                        "oLanguage": {

                            "sInfo": "_TOTAL_ clients (_START_ to _END_)",

                            "sSearch": "Search:",

                            "sLengthMenu": "_MENU_",

                            "oPaginate": {

                                 "sNext": "Next",

                                 "sPrevious": "Prev",

                            }                         

                        },                     

                } );

                 

                clients_table.fnSort( [ [1,'asc'] ] );

                $('#div_clients_table input, #div_clients_orders input').addClass('span2');

                                 

            }

            if( String(window.location).match(/reports/) ){

                

                Galleria.loadTheme( String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin|\breports\b)/).pop().replace('index.php/','')+'application/views/assets/js/galleria/galleria.classic.min.js');



                //New report area

                

                //Delete property - menu item

                $('[id^="menu_delete_property_"]').click(

                    function(){

                        if(confirm('Are you sure that you want to delete this property type?')){

                            var property_id = $(this).attr('id').replace('menu_delete_property_','');

                            var menu_item = $(this);

                            $.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin|reports)/).pop()+"reports/delete_property", 

                                { property_id:property_id },

                                

                                function(data) {

                                    var ret_val  = jQuery.parseJSON( data );                                

                                    if( ret_val['status'] == 'success' ){

                                        menu_item.parent().parent().parent().parent().remove();    

                                    } else {

                                        $('#div_new_report_first').prepend( get_status_div('<strong>Warning: </strong>Property either system default or doesnt exist!','alert-error'));

                                    }

                                }

                            );                            

                        }

                    }

                )

                /*Undo remove element from report/template*/

                $('#btn_undo').click(

                    function(){

                        var object_type = get_location_object_type();

                        var object_id   = get_object_id_based_on_type(object_type);

                        $.post(get_server_url('reports/undo_last_action'), 

                            {object_id: object_id,

                             object_type: object_type

                            },

                            function(data) {

                                var ret_val  = jQuery.parseJSON( data );                                

                                if( ret_val['parent_id'] != '' ){

                                    $('#'+ret_val['parent_id']+' > .accordion-group > .accordion-body > .accordion-inner > .div_concern').before(ret_val['html']);

                                    add_new_value_handlers('#'+ret_val['id']);

                                    

                                    var location_type = get_location_object_type(); 

                                    add_checkbox_handlers('#'+ret_val['id'], location_type);

                                    if( location_type == 'template'){

                                        $('input[type="checkbox"]').hide();

                                    }

                                }

                            }

                        );                    

                    }

                )

                 

                add_subcategory_container_handlers();

                add_new_value_handlers();

                

                //Append popup on change of address

                $('#_address').click(

                    function(){

                        if( $('#address_modal').length == 0 ){

                            $.post( 

                                get_server_url('reports/fetch_address_popup/'),

                                { report_id: $('#id').val()},

                                function(data){

                                    if( data != ''){

                                        $('.container').append(data);

                                        $('#address_modal').modal('toggle');

                                        $('#btn_modal_save_address').click(

                                            function(){  

                                                var address = (address = $('#modal_address').attr('value')) ? address : '';

                                                var zip = (zip = $('#modal_zip').attr('value')) ? zip : '';

                                                var city = (city = $('#modal_city').attr('value')) ? city : '' ;

                                                var state = (state = $('#modal_state').find('[selected="selected"]').text()) ? state : '' ;

                                                var full_address = 

                                                    address+','+    

                                                    city+','+

                                                    zip+','+

                                                    state;

                                                    

                                                $('#address').attr('value', address);

                                                $('#zip').attr('value', zip);

                                                $('#city').attr('value', city);

                                                $('#state').attr('value', state);

                                                

                                                

                                                $('#_address').attr('value', full_address).parent().parent().parent().parent().children().first().text(full_address);

                                            }

                                        )

                                    }

                                }

                            )                            

                        }  else {

                            $('#address_modal').modal('toggle');

                        }



                    }

                )

                

                $('#div_report_html table').removeClass('table-striped');

                //Append images button - HTML helper doesnt support this for now

                if( $('#div_new_report [name="controls"]').length == 1){

                    var div = '<div  class="control-group stop">'+

                                    '<label for="attendees" class="control-label"></label>'+                            

                                    '<div id="div_picture_upload" class="controls">'+                           

                                        '<a id="btn_property_images" class="btn">Add Images</a>'+                                         

                                    '</div>'+

                              '</div>';

                    $('#div_new_report [name="controls"]').append(div);

                    

                    $('#btn_property_images').click(

                        function(){

                            get_gallery('0');

                        }

                    )

                }

                

                $('#property_id').change(

                    function(){

                        var property_id = $(this).attr('value');

                        if(!isNaN( property_id )){

                            

                            $.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin|reports)/).pop()+"reports/fetch_property_templates", 

                                { property_id:property_id },

                                

                                function(data) {

                                    var ret_val  = jQuery.parseJSON( data );                                

                                    if( ret_val['status'] == 'success' ){

                                        

                                        var options = '<option selected="selected" value="">Select Template</option>';

                                        for(key in ret_val['templates']){

                                            options += '<option value="'+key+'">'+ret_val['templates'][key]+'</option>';

                                        }

                                          

                                        var template_select = $('#template_id');

                                        template_select.children().remove();

                                        template_select.append(options);

                                    }

                                }

                            );                            

                        }

                    }

                )

                

                $('#attendees').parent().parent().addClass('stop');

                $('[for^="type_id_add"]').parent().addClass('stop');

                

                $('.delete_all').click(

                    function(){

                        var ids = new Array();

                        var elements;

                        if($(this).attr('id').match(/disclosure/)){

                           elements = $('[id^=type_id_general_]');

                           elements.each(

                                function(){

                                    if($(this).attr('checked')=='checked'){

                                        ids.push( $(this).attr('id').replace('type_id_general_',''));

                                    }

                                }

                           )

                        } else {

                           elements = $('[id^=type_id_utility_]');

                           elements.each(

                                function(){

                                    if($(this).attr('checked')=='checked'){

                                        ids.push( $(this).attr('id').replace('type_id_utility_',''));

                                    }

                                }

                           ) 

                        }

                        

                       $.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin|reports)/).pop()+"reports/delete_types", 

                            { ids:encode_array(ids) },

                            

                            function(data) {                                                                     

                                var ret_val = jQuery.parseJSON( data );

                                if( ret_val['status']=='success'){

                                    for( key in ids ){

                                        elements.each(

                                            function(){

                                                if($(this).attr('id').match(ids[key])){

                                                   $(this).parent().parent().parent().toggle('slow');

                                                   $(this).parent().parent().parent().remove();

                                                }

                                            }

                                        )            

                                    }

                                }

                            }

                        );

                    }

                )                            

                /*Hiding types in new_report_form*/

                $('.hide_all').click(

                    function(){

                        var elements;

                        if($(this).attr('id').match(/disclosure/)){

                           elements = $('[id^=type_id_general_]');

                        } else {

                           elements = $('[id^=type_id_utility_]');

                        }                        

                        elements.parent().parent().parent().toggle('slow');

                        

                        if( $(this).text() == 'Hide'){

                            $(this).text('Show options');

                        } else {

                            $(this).text('Hide'); 

                        }

                    }

                )

                //$('.hide_all').toggle('click');

                

                /*Adding types in new_report_form*/

                $('.add_new_type').click(

                    function(){

                        

                        var prev = $(this).prev();

                        var description = prev.find('.new_description').attr('value');

                        var name = prev.find('.new_name').attr('value');

                        var type = prev.find('.new_name').attr('id');

                        

                        if( name != '' && description != '' ){

                            $.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin|reports)/).pop()+"reports/add_new_type", 

                                { name:name, description:description, type:type },

                                

                                function(data) {                                                                     

                                    if( data != '' ){

                                        prev.parent().parent().after(data);

                                    } else{

                                        

                                    }                                    

                                }

                            );                            

                        } else {

                            

                        }

                        prev.find('.new_description').attr('value','');

                        prev.find('.new_name').attr('value','');

                        prev.find('.new_name').attr('id');                        

                        $(this).prev().toggle('slow');

                        

                    }

                    

                )



                

                

                $('.subcategory_label').click(

                    function(){

                        $(this).parent().find('.subcategory_content').toggle('slow');

                    }

                )               

                $('.notes').hide();

                $('.category_button').click(

                    function(){

     

                        var form = $('#form_categories');

                        form.append('<input type="hidden" name="category" value="'+$(this).attr('id')+'">');

                        form.append('<input type="hidden" name="pass_submit_category" value="'+$(this).attr('id')+'">');

                        form.submit();

                    }

                )

                $('.category_add').click(

                    function(){

                        var text    = $(this).prev().text().replace(/\[.+?\]/);

                        var count   = $('[name^="'+$(this).prev().attr('name')+'"]').length+1;

                        var last_id = $(this).parent().html().match(/id="(.+?)"/).pop();

                        var id = last_id.replace(/_\d+/,'')+'_'+count;

                        

                        text = text +'['+count+']';

                        

                        $(this).parent().after( '<div>'+$(this).parent().html().replace(/id=".+?"/,'id="'+id+'"').replace(/>/+text+/\[.+?\]</)+'</div>');

                        //Atach handler

                        $('.category_button').click(

                            function(){

                                var form = $('#form_categories');

                                form.append('<input type="hidden" name="category" value="'+$(this).attr('id')+'">');

                                form.append('<input type="hidden" name="pass_submit_category" value="'+$(this).attr('id')+'">');

                                form.submit();

                            }

                        )                        

                    }

                );

                $('.category_remove').click(

                    function(){

                        

                    }

                );

                $('.btnnotes').click(

                    function(){

                        $(this).parent().next().toggle('slow');

                    }

                )

                

                $('.icon_toggle').click(

                    function(){

                        $(this).parent().parent().find('.subdiv').toggle('slow');

                        $(this).toggleClass('icon-minus').toggleClass('icon-plus');

 

                    }

                )

                $('#icon_add_sub').click(

                    function(){

                        $(this).parent().next().toggle('slow');

                        $(this).toggleClass('icon-minus').toggleClass('icon-plus');

                    }

                )

                $('.toggle_all').click(

                    function(){

                        $('.icon_toggle').trigger('click');

                    }

                )

                

                $('#div_new_property').hide();

                $('#btn_new_property').click(

                    function(){

                        $('#div_new_property').toggle('slow');

                        if( $(this).text().match(/New/)){

                            $(this).text('Close');

                        } else {

                            $(this).text('New property');

                        }                        

                    }

                )

                $('[name="a_property_id"]').click(

                    function(){

                        $('#submit_property_id').attr('value',$(this).attr('value'));

                        $('#form_submit_property').submit();

                    }

                )

                /**

                * Templates

                */

                var property_button_image_uploader;

                $('#btn_add_property').click(

                    function(){

                        

                        var name = $('#property_name').attr('value');

                        var description = $('#property_description').attr('value');

                        var button = $(this);

                        $.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin|reports)/).pop()+"reports/add_property", 

                            { name: name, description: description },

                            

                            function(data) {                                

                                var ret_val  = jQuery.parseJSON( data );

                                if( ret_val['status'] == 'success'){                                    

                                    var div = '<li class="property"><a href="#"  name="'+name+'" id="property_'+ret_val['id']+'">'+name+'<i class="icon-chevron-right pull-right"></i></a></li>'

                                    $('#properties ul').append(div);

                                    $('#property_'+ret_val['id']).click(

                                        function(){

                                            $('#submit_property_id').attr('value',$(this).attr('value'));

                                            $('#form_submit_property').submit();

                                        }

                                    )

                                    $('#btn_add_property_button_image').show();

                                    property_button_image_uploader = init_uploader(

                                        'div_add_new_property_image',

                                        'btn_add_property_button_image',

                                        function(id, fileName, responseJSON) {

                                            if (responseJSON.success) {

                                                $('#property_image').attr('src', responseJSON.filename);

                                                $('.qq-upload-list').remove();

                                                $('#div_add_image_'+subcategory_id).removeClass('qq-upload-button-hover')

                                            }

                                        },

                                        String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin|reports)/).pop()+"reports/add_picture_property_button/"+ret_val['id']

                                     );                                    

                                } else {

                                    add_error_div(ret_val['error']);

                                }

                           }

                        );                         

                    }

                )

                //Add button image to property

                                



                /**

                * Reports/index/ property template

                * 

                */

                $('.dropdown-toggle').dropdown();

                /**

                New template - hidden by default

                */

                $('#div_new_template').hide();

                $('#div_new_cat_subcat').hide();

                /**

                * Save template

                * 

                */

                if($('input[name="template_id"]').attr('value') ){

                   $('#btn_save_template').show();  

                } else {

                   $('#btn_save_template').hide();

                }

                

                /**

                Show new template div

                */

                $('#btn_new_template').click(

                    function(){

                        $('#div_new_template').toggle('slow');

                        if( $(this).text().match(/New/)){

                            $(this).text('Cancel');

                        } else {

                            $(this).text('New template');

                        }

                    }

                );

                $('#select_template').change(

                    function(){

                        if( $(this).attr('value')){

                            $('[name="template_id"]').attr('value', $(this).attr('value'));

                            $('#form_show_template').submit();                            

                        }



                    }

                )

                /**

                Create new template

                */

                 

                $('#btn_add_template').click(

                    function(){

                        

                        var name = $('#template_name').attr('value');

                        var description = $('#template_description').attr('value');

                        var property_id = $('#selected_property_id').attr('value');

                        var button = $(this);

                        var error = '';

                        

                        if( name == ''){

                            error += 'Name must contain letters<br />';

                        }

                        if( description == ''){

                            error += 'Description cant be empty<br />';

                        }

                        if( property_id == ''){

                            error += 'You must select property first<br />'

                        }

                        if( error == ''){

                            $.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin|reports)/).pop()+"reports/add_template", 

                                { name: name, description: description, property_id:property_id },

                                

                                function(data) {                                

                                    var ret_val  = jQuery.parseJSON( data );

                                    if( ret_val['status'] == 'success'){

                                        $('#select_template').children().removeAttr('selected');                                    

                                        var option = '<option value="'+ret_val['id']+'" selected="selected">'+name+'</option>';

                                        $('#select_template').append(option);

                                        $('#div_new_template').toggle('slow');

                                        

                                        $('#accordition').children().remove();

                                        $('#form_submit_template .status_legend').text(name);

                                        $('[name="template_id"]').attr('value',ret_val['id']);

                                        

                                        $('#select_template').trigger('change');

                                        

                                    } else {

                                        var status_div = '<div id="template_error" class="alert alert-error">'+

                                                            '<a type="button" class="close" data-dismiss="alert">�</a> '+

                                                            '<strong>Warning!</strong>'+ret_val['error']+

                                                         '</div>';

                                        button.parent().prepend(status_div);

                                        

                                        $('#div_status .close').click(

                                            function(){

                                                $(this).parent().remove();

                                            }

                                        )

                                    }

                               }

                            );                        

                        } else {

                           add_error_div( error );

                        }                            

                     }

                )

                //Add new subcategory to a selected template

                $('#btn_add_to_template').click(

                    function(){

                        var selected =  $('#select_category');

                        var category_id = selected.attr('value');

                        var category_name = selected.find('[value="'+category_id+'"]').text();

                        var template_id = $('[name="template_id"]').val();

                        

                        if( category_id ){

                            $.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin|reports)/).pop()+"reports/fetch_category_template", 

                                { category_id:category_id, template_id: template_id },

                                

                                function(data) {                                

                                    if( data != '' ){

                                        $('#form_submit_template fieldset').removeClass('hidden');

                                        

                                        var matches = data.match(/id=\"cat_acc_(\d+)_(\d+)\"/);

                                        var category_id = matches[1];

                                        var category_index = matches[2];

                                        

                                        if( category_index>1 ){      

                                        }

                                        $('#accordition').append(data);

                                        $('#cat_acc_'+category_id+'_'+category_index).collapse('hide');

                                        /**

                                        * Unselect/select child input fields on parent change

                                        */

                                        add_checkbox_handlers();

                                        add_new_value_handlers('#cat_acc_'+category_id+'_'+category_index);

                                        

                                        $('#btn_save_template').show();                                        

                                    }

                                }

                            );                            

                        } else {

                            add_error_div('You must select category first');                            

                        }

                        

                    }

                )

                add_checkbox_handlers();

                $('[name="property_type"]').click(

                    function(){

                        $('#div_select_'+$(this).attr('value')).toggle('slow');

                    }

                )

                

                $('#btn_add_category').click(

                    function(){

                            

                            var name = $('#new_category_name').attr('value');

                            

                            $.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin|reports)/).pop()+"reports/add_category", 

                                { name: name },

                                

                                function(data) {                                

                                    var ret_val  = jQuery.parseJSON( data );

                                    if( ret_val['status'] == 'success'){

                                        $('#select_category').children().removeAttr('selected');

                                        $('#select_category').append('<option value="'+ret_val['id']+'" selected="selected">'+name+'</option>');

                                    } else {

                                        add_error_div(ret_val['error']);

                                    }

                               }

                            );                            

                    }

                )

                $('#btn_add_new_cat_subcat').click(

                    function(){

                        $('#div_new_cat_subcat').toggle('slow');

                    }                

                )

 

                $('.category_button_html').click(

                    function(){

                        var form = $('#form_categories');

                        form.append('<input type="hidden" name="category" value="'+$(this).attr('id')+'">');

                        form.submit();

                    }

                )                

            }

            /**

            * Inspectors            

            */             

            if( String(window.location).match(/inspectors/) ){



                $('#div_inspectors_table table tr').click(

                    function(){

                        var id = $(this).children().first().text();

                        var form = $( document.createElement('form') );                    

                        form.attr('action',String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+'/inspectors/index/'+id);

                        form.attr('method','get');

                        $('#div_inspectors_table').append(form);

                        form.submit();                                                             

                    }

                )

                var inspectors_table = $('#div_inspectors_table table').dataTable( {

                         

                        "sPaginationType": "bootstrap",

                        "oLanguage": {

                            "sInfo": "_TOTAL_ clients (_START_ to _END_)",

                            "sSearch": "Search:",

                            "sLengthMenu": "_MENU_",

                            "oPaginate": {

                                 "sNext": "Next",

                                 "sPrevious": "Prev",

                            }                         

                        },                     

                } );                 

                inspectors_table.fnSort( [ [1,'asc'] ] );

                $('#div_inspectors_table input, #div_inspectors_orders input').addClass('span2');                                 

            }

            



                

                

                

                            

            /**

            * Home 

            */            

            $('#div_home').each(

                function(){

                    var row = $(this);

                    $(this).find('legend').click(

                        function(){

                            var action = 'maximize';

                            if( !$(this).parent().parent().hasClass('minimize')){

                                action = 'minimize';

                            }

                            if( action == 'minimize'){

                                $(this).parent().find('div').hide('slow');

                                                                  

                            } else {

                                $(this).parent().find('div').show('slow');                            

                            }

                           

                            $(this).parent().parent().toggleClass('minimize');                             

                            $(this).parent().parent().siblings().toggleClass('maximize');                             

                        }

                    )

                }

            )

            /**

            * Btn message reply

            */

            $('#btn_home_new_msg').click(

                function(){

                    $(this).hide();

                    $('#home_message_reply').slideDown('slow');

                    $('[name="btn_close"]').show();

                    return false;

                }

            )

            /**

            * Btn message close

            */

            $('[name="btn_close"]').click(

                function(){

                    $(this).hide();

                    $('#home_message_reply').slideUp('slow');

                    $('#btn_home_new_msg').show();                    

                    return false;

                }

            )

            /**

            * Messages table    

            */

            $('#div_home_all_msg table').dataTable( {

                    "oLanguage": {

                        "sInfo": "_TOTAL_ messages (_START_ to _END_)",

                        "sSearch": "Search:",

                        "sLengthMenu": "_MENU_",

                        "oPaginate": {

                             "sNext": "",

                             "sPrevious": "",

                        }                         

                    }

                                    

                }

            )

            $('#div_home_all_msg input[type="text"]').addClass('span2');

            $('#home_message_reply select, #home_message_reply textarea').addClass('span3');

            

            var open_rows = [];

            $('#div_home_all_msg table tr').click(

                function(){

                    

                   var table = $(this).parent().parent().dataTable();

                   

                   if( table.fnIsOpen( this )){

                       //alert( $(this).next().html());

                       //$( this ).next().hide('slow'); 

                       //$( this ).next().slideUp('slow'); 

                       //$('div.drill_down',$(this)).slideUp('slow'); 

                       var index =  table.fnClose( this );

                       

                       

                   } else {

                        

                       var href;

                       var row = this;

                       if( href = $(this).find('a').attr('id').match(/\d+/g).pop()){

                             $.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+"/home/get_message", 

                                    { message_id: href},

                                    

                                    function(data) {                            

                                         var ret_val  = jQuery.parseJSON( data );

                                         var index    = table.fnOpen( row ,get_message_div( ret_val ), 'details' );

                                         $('#div_message_drill' ).slideDown('slow');

                                         

                                         $('[name="btn_send"]').click(

                                            function(){

                                                

                                                var message    = $(this).parent().find('[name="message"]').attr('value');

                                                var to_user_id = $(this).parent().find('[name="to_user_id"]').attr('value');

                                                var type       = $(this).parent().find('[name="type"]').attr('value');

                                                

                                                if( to_user_id == null ){

                                                    to_user_id = $(this).parent().find('[name="to_user_id"]').attr('value');

                                                }

                                                

                                                var parent = $(this).parent();

                                                var button = $(this);

                                                $.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+"/home/send_message", 

                                                    { message: escape(message), to_user_id: to_user_id, type: type },

                                                    //Fill in Item description and price, recalculate new totals, etc.

                                                    function(data) {

                                                        var response = jQuery.parseJSON( data );

                                                        

                                                        if( response['success'] == 'true'){

                                                            append_status_div( $(parent), 'success');

                                                            button.attr('disabled','disabled');

                                                        } else {

                                                            append_status_div( $(parent), 'fail');

                                                        } 

                                                        

                                                    });                        

                                                return false;

                                                   

                                            } );

                                            

                                            /**

                                            * Btn message delete

                                            */

                                            $('[name="btn_delete"]').click( function(){

                                                  

                                                var message_id    = $(this).parent().find('[name="message_id"]').attr('value');

                                                

                                                var parent = $(this).parent();

                                                

                                                $.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+"/home/delete_message", 

                                                    { message_id: message_id },

                                                    //Fill in Item description and price, recalculate new totals, etc.

                                                    function(data) {

                                                        var response = jQuery.parseJSON( data );                                                          

                                                        if( response['success'] == 'true'){

                                                           $(parent).parent().parent().remove();

                                                           $(row).remove();

                                                        } 

                                                        

                                                    });                        

                                                return false;

                                                   

                                            } );                                             

                                            

                                    });                            

                       }



                   }

                                     

                }

            )

            /**

            * Btn message delete

            */

            $('[name="btn_deletes"]').click( function(){

                  

                var message_id    = $(this).parent().find('[name="message_id"]').attr('value');

                

                var current = $(this);

                

                $.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+"/home/delete_message", 

                    { message_id: message_id },

                    //Fill in Item description and price, recalculate new totals, etc.

                    function(data) {

                        var response = jQuery.parseJSON( data );                                                          

                        if( response['success'] == 'true'){

                            alert(current.attr('name'));   

                            current.parent().parent().parent().prev().remove();

                            current.parent().parent().parent().remove();  

                        } else {

                             

                        } 

                        

                    });                        

                return false;

                   

            } );

            /**

            * Btn message delete

            */

            $('[name="btn_cancel"]').click( function(){

                window.close();

            } );            

                        

            /**

            * Btn message reply

            */

            $('[name="btn_send"]').click( function(){

               

                var message    = $(this).parent().find('[name="message"]').attr('value');

                var to_user_id = $(this).parent().find('[name="to_user_id"]').attr('value');

                var type       = $(this).parent().find('[name="type"]').attr('value');

                

                if( to_user_id == null ){

                    to_user_id = $(this).parent().find('[name="to_user_id"]').attr('value');

                }

                

                var parent = $(this).parent();

                 

                $.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+"/home/send_message", 

                    { message: escape(message), to_user_id: to_user_id, type: type },

                    //Fill in Item description and price, recalculate new totals, etc.

                    function(data) {

                        var response = jQuery.parseJSON( data );

                                                                                    

                        if( response['success'] == 'true'){

                            append_status_div( $(parent), 'success');

                            button.attr('disabled','disabled');

                        } else {

                            append_status_div( $(parent), 'fail');

                        } 

                        

                    });                        

                return false;

                   

            } );



                        

            $('#div_error').hide();

            $('#password_repeat, #password').change(

                function(){

                    

                    var id = $(this).attr('id');

                    var other = 'password_repeat';

                    

                    if( id=='password_repeat'){

                        other = 'password';

                    }

                    

                    if($(this).attr('value') != $('#'+other).attr('value')){

                        $('#div_error').show();

                        $('#p_error').text('Both passwords must be same');

                        $(this).addClass('error');

                        $('#'+other).addClass('error');

                    } else {

                        $('#div_error').hide();

                        $(this).removeClass('error');

                        $('#'+other).removeClass('error');                        

                    }

                }

            )

            

            

            

        /**

        * Schedule                  

        */

            /**

            * Scheduled colors

            */

            var colors = new Array( "CC6600",

                                    "CCFF66",

                                    "CCCC33",                                    

                                    "CCFF00",

                                    "#DE82C8",

                                    "#DED882",

                                    "#B1E813",

                                    "FFFF66",

                                    "FFFF00",

                                    "FFCC00",

                                    "CC6633",

                                    "FF9966",

                                    "FFFF99",

                                    "CCFFFF",

                                    "CCFFCC");

                                    

            var used = new Array();

            $('#div_schedule table').addClass('table-bordered');

            $('#div_schedule table a').each(

                function(){

                    var value = $(this).text();

                    if( value != '' && jQuery.inArray(value,used) == -1){

                        color ='#'+ colors.pop();                                                                                               

                        $('a:contains("'+value+'")').parent().css('background-color', color );

                        used.push( value );

                    }

                }

            )

            

            $('#form_schedule input, #form_schedule select').change(

                function(){

                    $('#form_schedule').submit();

                    

                }

            )

            $('#div_schedule table a').popover();

            

            $('#form_schedule>div').removeClass('well');

            

            



            /**

            * Schedule

            */

            $('#div_schedule table tr').first().find('th').addClass('center_text');

            

            $('#div_schedule table td a').parent().addClass('scheduled').addClass('center_text');

            /**

            * Stats

            */

            $("#div_stats_filter  #person_type").change(

                function(){

                    if( $(this).attr('value').match(/\d/)){

                         $.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+"/stats/ajax_control", 

                                { person_type: $(this).attr('value')},

                                function(data) {                            

                                    var persons = jQuery.parseJSON( data );

                                    var person_id = $('#person_id');

                                    var html;

                                    for( var person in persons ){

                                       html += '<option value="'+person+'">' + persons[person] + '</option>';

                                    }

                                    person_id.html( html );

                                    $('#div_stat_persons').show();

                                });                            

                    }

                }

            )

            /**

            * Admin

            */

            $('#company_select').change(

                function(){

                    $('#form_companies').submit();

                }

            )

            /**

            * Types

            */

            $(".collapse").collapse();

            $('#type_select').change(

                function(){

                    $('#form_types').submit();

                }

            )            

            

        }

        

);

/**

* Select box - Item change callback - Fills in description and price. Recalculates order value

*/

function item_change(){

    

    if( $(this).attr('value').match(/new_item/)){

      var name = $(this).attr('name');

      var type = $(this).attr('value');

      window.open(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+"orders/add_new?type="+type+"&select_name="+name,"Add new", "status=1,height=720,width=475");                    

    } else {

     //Send AJAX request

     $.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+"orders/ajax_control", 

            { value: $(this).attr('value'), name: $(this).attr('name'), type: 'itemdropdown'},

            //Fill in Item description and price, recalculate new totals, etc.

            function(data) {                            

                 var ret_val  = jQuery.parseJSON( data );

                 $('#item_description_' + ret_val["row"]).attr('value', ret_val["description"]);

                 $('#item_price_' + ret_val["row"]).attr('value', ret_val["price"]);

                 

                 

                 if($('#item_price_' + ret_val["row"]).attr('value').indexOf('.') == -1 ){

                    $('#item_price_' + ret_val["row"]).attr('value', $('#item_price_' + ret_val["row"]).attr('value') + '.00' );      

                 }               

                 /**

                 * Perform calculation

                 */

                 calculate();

            });                        

    }

                    

}



 

function get_message_div( data )
{

  var div =

    '<div id="div_message_drill">'+

      '<table>'+

        '<tr><td class="drill_down_message"><b>Message</b></td><td><textarea class="ta_home_message" rows ="3" name="message_in">'+data['message']+'</textarea></td></tr>'+

        '<tr><td class="drill_down_message"><b>Response</b></td><td><textarea class="ta_home_message" name="message" rows="3"></textarea></td></tr>'+

      '</table>'+

      '<input name="btn_send"   type="submit" class="btn" value="Reply"/>'+

      '<input name="btn_delete" type="submit" class="btn" value="Delete"/>'+

      '<input name="to_user_id" type="hidden" class="btn" value="'+data['from_user_id']+'"/>'+

      '<input name="type" type="hidden" class="btn" value="'+data['type']+'"/>'+

      '<input name="message_id" type="hidden" class="btn" value="'+data['message_id']+'"/>'+

    '</div>';

    

  return div;

}

function message_send(){

               

    var message    = $(this).parent().find('[name="message"]').text();

    var to_user_id = $(this).parent().find('[name="to_user_id"]').attr('value');

    var type       = $(this).parent().find('[name="type"]').attr('value');

    

    if( to_user_id == null ){

        to_user_id = $(this).parent().find('[name="to_user_id"]').attr('value');

    }

    

    var parent = $(this).parent();

     

    $.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+"/home/send_message", 

        { message: escape(message), to_user_id: to_user_id, type: type },

        //Fill in Item description and price, recalculate new totals, etc.

        function(data) {

            var response = jQuery.parseJSON( data );

            /**

            if( response['response'] == 'success'){

                //success div

                //disable reply    

            } else {

                

            } */

            

        });                         

    return false;

                   

}

function append_status_div( object , status ){

    var warning;

    var message;

    if( status == 'success'){

        warning = 'alert alert-success';

        message = '<strong>Success: </strong>Your message has been sent';

    } else {

        warning = 'alert alert-error';

        message = '<strong>Error: </strong>Your message has not been sent'; 

    }

    div = '<div id="div_status" class="'+warning+'">'+

        '<a class="close" data-dismiss="alert">�</a>'+

        message +

    '</div>';

     

    $( object ).parent().prepend( div );

    $('#div_status').click(

        function(){

            $(this).hide();

        }

    )

}

function div_show( object ){

    var divs = $(object).parent().parent().find('[name="controls"]');  

    divs.hide('slow');

    $(object).removeClass('icon-minus');

    $(object).addClass('icon-plus');

    

    $(object).click(

        function(){

            $(this).parent().parent().find('[name="controls"]').show('slow');

            $(this).removeClass('icon-plus');

            $(this).addClass('icon-minus');    

        }

    )    

        

}

function get_drill_down_order( response ){

    

    var order =

    '<div>'+

        '<div class="drill_down_row">'+

            

            '<table class="table table-striped table-bordered">'+

                '<tr><td colspan="3" ><legend>Order details</legend></td><tr>'+

                '<tr><td><strong>Meet with:</strong></td><td colspan="2">'+response['meet_with']+'</td></tr>'+

                '<tr><td><strong>Address:</strong></td><td colspan="2">'+response['address']+'</td></tr>'+

                '<tr><td><strong>Zip:</strong></td><td colspan="2">'+response['zip']+'</td></tr>'+

                '<tr><td><strong>City:</strong></td><td colspan="2">'+response['city']+'</td></tr>'+

                '<tr><td><strong>Order created:</strong></td><td colspan="2">'+response['order_date']+'</td></tr>'+

            '</table>'+

        '</div>'+

        '<div class="drill_down_row">'+

            

            '<table class="table table-striped table-bordered">'+

                '<tr><td colspan="3" ><legend>Charges</legend></td><tr>';            

                for( var key in response['items']){

                     

                    order += '<tr><td>'+response['items'][key]['name']+'</td><td>'+response['items'][key]['price']+'</td></tr>';

                }

                order +=             

                '<tr><td><strong>Subtotal:</strong></td><td>'+response['subtotal']+'</td></tr>'+ 

                '<tr><td><strong>Tax:</strong></td><td>'+response['tax']+'</td></tr>'+ 

                '<tr><td><strong>Paid:</strong></td><td>'+response['paid']+'</td></tr>'+ 

                '<tr><td><strong>Due:</strong></td><td>'+response['amount_due']+'</td></tr>'+ 

                '<tr><td><strong>Total:</strong></td><td>'+response['total']+'</td></tr>'+

            '</table>'+

        '</div>';

        

        if( response['notes_inspection_details'] != '') {

            order += 

            '<div class="drill_down_row">'+

                '<legend>Notes</legend>'+

                 response['notes_inspection_details']+

            '</div>';  

        }

        

        if( response['status'] == '12' || response['status']== 13){

            order += 

            '<div class="drill_down_row">'+

                '<a class="btn" target="_blank" href="'+response['url']+'">Report</a>'+

            '</div>';            

        }

    

    order += '</div>';

     

    return order;    

}

function recalculate(){         

    //Atleast one row must be present

    if( $(this).parent().parent().parent().children().length > 1 ){

        $(this).parent().parent().find('[id^="item_price_"]').attr('value',0);

        

        calculate();

        $(this).parent().parent().remove();                          

    }             

}

function calculate(){

     $('#subtotal').attr('value', 0);

      

     var total    = $('#total');

     //Calculate prices

     var subtotal = 0;

     $('[id^="item_price_"]').each( function( index, subtotal_value ){

            subtotal += Number($(this).attr('value'));

            //Subtotal value

            $('#subtotal').attr('value', subtotal );

            if ( $('#subtotal').attr('value').indexOf('.') == -1){

               $('#subtotal').attr('value', $('#subtotal').attr('value') +'.00');    

            }            

             

            //Total value

            $('#total').attr('value', subtotal  + subtotal * Number( $('#tax').attr('value') )/100 + Number($('#shipping_amount').attr('value')) );

            if ( $('#total').attr('value').indexOf('.') == -1){

               $('#total').attr('value', $('#total').attr('value') +'.00');    

            }             

            //Amount due value

            $('#amount_due').attr('value', Number($('#total').attr('value')) - Number($('#paid').attr('value')) );

            

            if ($('#amount_due').attr('value').indexOf('.') == -1){

               $('#amount_due').attr('value', $('#amount_due').attr('value') +'.00');    

            }                    

         }

     )    

}

function item_price_change(){



    if( isNaN($(this).attr('value')) ){

        $(this).attr('value', '0.00');

        $(this).parent().parent().find('[name^="item_item_id_"]').trigger('change');

        calculate();

    } else {

        if ( $(this).attr('value').indexOf('.') == -1){

            $(this).attr('value', $(this).attr('value') +'.00');    

        }

        calculate();

    }

                    

}

function check_inspector(){



    var inspector_id = $('#inspector_id').attr('value');

    var inspection_time = $('#inspection_time').attr('value');

    var inspection_date = $('#inspection_date').attr('value');

    var inspection_duration = $('#estimated_inspection_time').attr('value');

    var order_id = $('#id').attr('value');

    

    $.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+"/orders/ajax_control", 

        { type: 'inspector_check', 

          inspector_id: inspector_id, 

          inspection_time: inspection_time, 

          inspection_date: inspection_date, 

          inspection_duration: inspection_duration,

          order_id: order_id

        },                        

        function(data) {

            var response = jQuery.parseJSON( data );

            

            if( response['not_available'] ){

                $('#p_modal_text').text('I\'m sorry, '+response['inspector_name']+' is already booked for that time.  Please select another time or another inspector!');

                $('#h_modal_header').text('Inspector error');

                $('#alert_modal').modal('toggle');        

            }

            

        });      

   

}



function refresh_all(){

    //$('#name, #client_name, #inspector_id, #terms, #type_of_inspection, #type_of_structure, #type_of_foundation, #type_of_utilities').trigger('click');
   
     
    //$('[id^="operator_"]').trigger('click');
    
    $('#sales_rep,#terms,#status,#shipping_method,#Coating,#company').trigger('click');

}

function refresh_item_select( name ){



    var parent = $('[name="'+name+'"]');

    

    $.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+"orders/ajax_control", 

        { id: 'item', type: 'refresh_dropdown' },

        function(data) {

            

            var ret_val  = jQuery.parseJSON( data );

            var ids      = new Array();                        

            parent.children().each(

                function(){

                    ids.push( $(this).attr('value'));

                }

            )

            for( var key in ret_val ){

                if( jQuery.inArray(key ,ids) == -1){

                    parent.children().removeAttr('selected');

                    parent.append('<option selected="selected" value="'+key+'">'+ret_val[key]+'</option>');

                    parent.trigger('change');

                }

            }

        });

}

function get_url(){

    var location = String(window.location);

}

function format_currency( object){

    /* 

    if(object.text().indexOf('.') == -1 ){

       object.text( object.text() + '.00');

    }

    object.text( object.text() + '$');*/

    object.formatCurrency();

    object.parent().addClass('right');   

}

function add_error_div( error ){

    $('.row').first().append(

        '<div id="div_status" class="alert alert-error">'+

            '<a type="button" class="close" data-dismiss="alert">�</a> '+

            '<strong>Warning!</strong>'+error+

         '</div>'                            

    );

    $('#div_status .close').click(

        function(){

            $(this).parent().remove();

        }

    )     

}

function add_checkbox_handlers( ancestor, report_section ){

    

    ancestor = ancestor != null ? ancestor : ' ';

    

    $(ancestor+' .remove_element').click(

        function(){

            var element = $(this);

            var object_type = get_location_object_type();

            var object_id   = get_object_id_based_on_type(object_type);

            $.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin|reports)/).pop()+"reports/delete_element",

                { category_id: element.attr('data-category-id'),

                  index: element.attr('data-category-index'),                  

                  subcategory_id: element.attr('data-subcategory-id'),                   

                  value_id: element.attr('data-value-id'), 

                  concern_id: element.attr('data-concern-id'), 

                  element_type: element.attr('data-type'), 

                  object_type: object_type,

                  object_id: object_id

                },

                function(data){

                    element.parent().parent().remove();           

                }

            );

            

        }

    )

    

    if( get_location_object_type() == 'template' ){

        $(ancestor+' [id^="cat_"]').change(

            function(){

                if( $(this).attr('checked') == 'checked'){

                    $('#cat_acc_' + $(this).attr('id').replace('cat_','') ).find('input[id^="subcat_"]').attr('checked','checked').trigger('change');

                } else {

                    $('#cat_acc_' + $(this).attr('id').replace('cat_','') ).find('input[id^="subcat_"]').removeAttr('checked').trigger('change'); 

                }                                                

            }

        )

        $(ancestor+' [id^="subcat_"]').change(

            function(){

                if( $(this).attr('checked') == 'checked'){

                    $('#sub_acc_' + $(this).attr('id').replace('subcat_','') ).find('input[id^="value_"]').attr('checked','checked').trigger('change');

                } else {

                    $('#sub_acc_' + $(this).attr('id').replace('subcat_','') ).find('input[id^="value_"]').removeAttr('checked').trigger('change'); 

                }

            }

        )

        $(ancestor+' [id^="value_"]').change(

            function(){

                if( $(this).attr('checked') == 'checked'){

                    $('#val_acc_' + $(this).attr('id').replace('value_','') ).find('input[id^="concern_"]').attr('checked','checked');

                } else {

                    $('#val_acc_' + $(this).attr('id').replace('value_','') ).find('input[id^="concern_"]').removeAttr('checked'); 

                }

            }

        )    

    } else {

        $(ancestor+' [id^="value_"]').change(

            function(){

                if( $(this).attr('checked') == 'checked'){

                    $(this).parent().parent().find('.collapse').collapse('show');

                } else {

                    $('#val_acc_' + $(this).attr('id').replace('value_','') ).find('input[id^="concern_"]').removeAttr('checked');

                }

                

            }

        )     

    }            

}

function add_new_value_handlers(ancestor){

    

    ancestor = ancestor != null ? ancestor : ' ';

        

    $(ancestor+' .add_description').hide();

    //Toggles new item under section ares

    $(ancestor+' .btn_add_desc').click(

        function(){

            

            if( $(this).text().match(/New/)){

                $(this).attr('data-old-text', $(this).text());

                $(this).text('Cancel');

                $(this).prev().show('slow'); 

            } else {

                $(this).text($(this).attr('data-old-text'));

                $(this).prev().hide('slow');

            }

        }

    )

    //Button - Adds new item, based on type               

    $(ancestor+' .add_description .btn').click(

        function(){

            var add_desc = $(this).parent();             

            var element = $(this);

            var object_type = get_location_object_type();

            var object_id   = get_object_id_based_on_type(object_type);

            

            $.post(get_server_url("/reports/add_element"), 

                { value: element.prev().attr('value'), 

                  subcategory_id: element.attr('data-subcategory-id'), 

                  category_id: element.attr('data-category-id'), 

                  value_id: element.attr('data-value-id'), 

                  index: element.attr('data-category-index'),

                  element_type: element.attr('data-type'), 

                  object_type: object_type,

                  object_id: object_id

                },

                

                function(data) {                                

                    if( data != ''){

                        var ret_val  = jQuery.parseJSON( data );                        

                        add_desc.parent().before(ret_val['html']);

                        add_new_value_handlers('#'+ret_val['id']);

                        

                        if(object_type == 'report'){

                            add_subcategory_container_handlers('#'+ret_val['id'])                            

                        }

                    }

                }

            );                        

        }

    )    

}



function add_subcategory_container_handlers( ancestor ){

        ancestor = ancestor != null ? ancestor : ' ';

        $(ancestor+' .category_add,'+ancestor+' .category_remove,'+ancestor+' .excluded,'+ancestor+' .add_description').hide();

        

        $(ancestor+' .subcategory_rating').change(

            function(){

                if($(this).is(':checked')){

                    var id = $(this).attr('id');

                    $(this).parent().parent().find('.subcategory_rating').each(

                        function(){

                            if( $(this).attr('id') != id ){

                                $(this).attr('checked', false );

                            }

                        }

                    );

                }

            }

        )

        

        $(ancestor+' .btnnotes').click(

            function(){

                $(this).parent().next().toggle('slow');

            }

        );

                                    

        $(ancestor+' .excluded_toggle').change(                                                      

            function(){

                var excluded = $(this).parent().parent().find('.excluded'); 

                if( $(this).attr('checked') == 'checked'){

                    excluded.show('slow');

                } else {

                    excluded.hide('slow');

                    excluded.attr('value','');

                }

            }

        );

        

        $(ancestor+' .picture_button').click(

            function(){                         

                var subcategory_id = $(this).attr('id').replace('picture_','');

                $('#image_subcategory').attr('value', subcategory_id );                        

                get_gallery(subcategory_id);                                  

            }

        )

}

function add_event_handlers_on_gallery_modal( parent_selector, subcategory_id ){

    

    var subcat_galleria = Galleria.get('div_galleria_'+subcategory_id)[0];

    

    var uploader = new qq.FileUploader({

        element: document.getElementById('image_upload_modal_body_'+subcategory_id),

        action: String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin|reports)/).pop()+"reports/add_pictures/"+subcategory_id+'/',

        button: $('#image_modal_'+subcategory_id+' .add_image')[0],

        debug: true,

        onComplete: function(id, fileName, responseJSON) {

            if (responseJSON.success) {

                subcat_galleria = null;

                var gallerias = Galleria.get();

                for( key in gallerias ){

                    if($(gallerias[key]._dom.target).attr('id') == 'div_galleria_'+subcategory_id){

                       subcat_galleria = gallerias[key];

                       break;

                    }

                }

                if(subcat_galleria.getDataLength() > 0){

                    subcat_galleria.push(

                    {image: responseJSON.image,

                     thumb: responseJSON.thumb

                    });

                    

                    if( subcat_galleria.getData(0).image.match(/img_default/)){

                        //subcat_galleria.show(1);

                        subcat_galleria.splice(0,1);

                    }

                } else {

                    data = [{image: responseJSON.image, thumb: responseJSON.thumb}];

                    subcat_galleria.load(data) ;

                }

            }

        }                        

    });

     

    var featherEditor = 

        featherEditor = new Aviary.Feather({

        apiKey: '4727e238f',

        apiVersion: 2,

        tools: ['draw', 'stickers','text'],

        onError: function(error){

            alert(error.message);   

        },

        onReady: function(){

            setInterval(function(){

                    var icons = $('.avpw_overlay_icon');

                    icons.hide();

                    icons.find('[src*="red_arrow"]').parent().parent().show();

                    icons.find('[src*="blue_arrow"]').parent().parent().show();

                    icons.find('[src*="green_circle"]').parent().parent().show();



            },500);

        },

        postUrl: String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin|reports)/).pop()+"reports/save_image_edit", 

        });

    

    $(parent_selector+' .add_image').click(

        function(){

            $('#div_galleria_'+subcategory_id).hide('slow');

            $('#image_upload_container_'+subcategory_id).show('slow');

            //$('#image_upload_modal_body_' +subcategory_id).find('ul').remove();

        }

    )

    

    $(parent_selector+' .clear_upload').click(

        function(){

            $('#image_upload_modal_body_' +subcategory_id).find('ul').remove();

            $('#div_galleria_'+subcategory_id).show('slow');

            $('#image_upload_container_'+subcategory_id).hide('slow');

        }

    )

    

    $('#dismiss_image_modal_'+subcategory_id).click(

        function(){

            $('#image_modal_'+subcategory_id).modal('show');

        }

    )

    $(parent_selector+' .edit_image').click(

        function(){                    

            var src = Galleria.get(0).getData().image;

            $('#img_edit').attr('src', src);



                         

            $.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin|reports)/).pop()+"reports/make_image_public", 

                { image: src },

                

                function(data) {                                                                                              

                    var ret_val  = jQuery.parseJSON( data );

                    if( ret_val['status'] == 'success'){

                         featherEditor.launch({

                            image: 'img_edit',

                            url: String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin|reports)/).pop().replace('index.php/','')+'application/views/assets/img/'+ret_val['src'],

                            postData: ret_val['src'],

                            onSave: function(imageID, newURL) {

                                    $.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin|reports)/).pop()+"reports/save_image_edit", 

                                        { url: newURL, postData: ret_val['src'] },

                                        

                                        function(data) { 

                                            ret = jQuery.parseJSON( data ); 

                                            if( ret['url']){

                                                

                                                var index = subcat_galleria.getIndex();                                                

                                                subcat_galleria.splice( index, 1);                                                

                                                subcat_galleria.push({ image: src+'/fool'});

                                                subcat_galleria.show(-1)

                                                subcat_galleria.show(index);

                                                    

                                            } else {

                                                alert('Image cannot be displayed at this time');

                                            }                            

                                               

                                        }

                                    )                                                    

                            },                                                

                        });                                                  

                    }                        

               }

            );                

            return false;                    

        }

    )

    

    $(parent_selector+' .delete_image').click(

        function(){

            if(confirm('Are you sure you want to delete this image?')){

                $.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin|reports)/).pop()+"reports/delete_image", 

                    { image: subcat_galleria.getData().image},

                    

                    function(data) { 

                       subcat_galleria.splice( subcat_galleria.getIndex(),1).show( subcat_galleria.getIndex()-1);                                   

                    }

                )                        

            }

        }

    )    

}

function get_status_div( message, alert_class){

    return '<div id="template_error" class="alert '+alert_class+'">'+

                '<a type="button" class="close" data-dismiss="alert">�</a> '+

                message+

             '</div>';

}

function encode_array(array){

    var str = '';

    for(key in array){

        str+='&'+array[key];

    }

    return str;

}

function init_galleria(){

     

    if( Galleria.get(0) == undefined ){

        

         

    }  

    return Galleria.get(0);

}



function init_uploader(element_id, button, callback, url){

    var uploader = new qq.FileUploader({

        element: document.getElementById(element_id),

        action: url,

        debug: 'true',

        button: document.getElementById(button),

        onComplete: callback,                     

    });

    return uploader;

}



function get_gallery( subcategory_id ){

    var gallery_popup = $('#image_modal_'+subcategory_id);

    if( gallery_popup.length != 0){

        gallery_popup.modal('toggle');

    } else {

        $.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin|reports)/).pop()+"reports/fetch_pictures", 

            { subcategory_id: subcategory_id },

            function(data) { 

                if( data != ''){

                    append_gallery_and_attach_event_handlers(data, subcategory_id);

                }

            }

        );    

    }

}



function append_gallery_and_attach_event_handlers( data, subcategory_id ){

    $('.container').append(data);

    Galleria.run('#div_galleria_'+subcategory_id, {

        transition: 'fade',

        imageCrop: false,

        responsive: true,

        height: 0.40,

    });                                        

    $('#image_modal_'+subcategory_id).modal('toggle');

    //set event handlers for modal control buttons

    add_event_handlers_on_gallery_modal('#image_modal_'+subcategory_id, subcategory_id);

}



function get_server_url(additional){

    return String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin|reports)/).pop()+additional;

}



function get_location_object_type(){

    var location = String(window.location).match(/templates|categories/);

    var object_type = 'report';

    if( location && location.length > 0 ){        

        if( location.pop() == 'templates'){

            object_type = 'template';

        } else{

            object_type = 'report';

        }    

    }

    return object_type;    

}



function get_object_id_based_on_type(type){

    var object_id = '';

    if(type == 'template'){

        object_id = $('#current_template_id').val();

    } else if(type == 'report'){

        object_id = $('#report_id').val();

    }

    return object_id;

}

// Start


function ChangeJobStatus(orderid)
{
    var rowIndex;
    var control= document.getElementById('status_'+orderid);
    var status = confirm("Are you sure you'd like to move to '"+control.options[control.selectedIndex].text+"' ?");
    if(status==true) {
            var element = $(this);
            if(control.options[control.selectedIndex].text == "On Press") {
				var checkedItems="";
				$("#DataTables_Table_0 input:checkbox:checked").map(function(){
					checkedItems += $(this).val() + ",";
				  });
				 if(checkedItems == "")
					checkedItems=orderid + ",";
				
				var checkedItems = checkedItems.replace(/,\s*$/, '');
                window.location="http://"+$(location).attr('hostname')+"/live/orders/run_orders?oid="+checkedItems+"&jid="+control.options[control.selectedIndex].value;
            }
            else {
                
                $.post("http://"+$(location).attr('hostname')+"/live/orders/updateorder_status",     
                    { orderid: orderid, jobstatusid: control.options[control.selectedIndex].value},
                    
                                        function(data) {  
                                        
                                            var ret_val  = jQuery.parseJSON( data );
                                             
                                             if( ret_val['status'] == 'success' ){
                                                   //element.parent().parent().remove();  
                                                   /* 
                                                    rowIndex=$(control).closest('tr')[0].sectionRowIndex;
                                                    var oDataTable = $('#div_orders_table table').dataTable();
                                                    oDataTable.fnDeleteRow(rowIndex); 
                                                    oDataTable.fnDraw(); */
                                                    
                                            } else {
                
                                                alert('Error in updating orders.');
                
                                            }
                    
                                        }
                    );
                
            }
                   
    }
    else
    {
    	//window.location=location.href;
    	alert('canceled');
    	$.post("http://"+$(location).attr('hostname')+"/live/orders/getcurrent_jobstatus",     
                    { orderid: orderid},
                    
                    function(data) {  
                    
                        var ret_val  = jQuery.parseJSON( data );
                         
                         if( ret_val != null ){
                               
                               //alert(ret_val);
                               
                               for( var key in ret_val ){
                               	
                               		//$("#control id").val('the value of the option');
                               		alert(ret_val[ key ]);
							 
                             		//$('#' + key).attr('value', ret_val[ key ]);

                         		}
                               
                                
                         } 

                    }
        );
    	
    }
    
}

function ChangeCoating()
{
		var value = $('#Coating').attr('value');
		if(value=='-Add/Edit-')
		{
       		//window.open(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin|run_orders)/).pop()+"orders/add_new?type=_10","Add new", "status=1,height=500,width=520,scrollbars=no,toolbar=no,location=no");
       		window.open("http://"+$(location).attr('hostname')+"/live/orders/add_new?type=_10","Add new", "status=1,height=500,width=520,scrollbars=no,toolbar=no,location=no");  
		}
}
function ChangeCoatingById(coatingid)
{
		
		var value = $(coatingid).attr('value');
		//var value = $('#Coating_'+coatingid).attr('value');
		
		if(value=='-Add/Edit-')
		{
       		//window.open(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin|run_orders)/).pop()+"orders/add_new?type=_10","Add new", "status=1,height=500,width=520,scrollbars=no,toolbar=no,location=no");
       		window.open("http://"+$(location).attr('hostname')+"/live/orders/add_new?type=_10","Add new", "status=1,height=500,width=520,scrollbars=no,toolbar=no,location=no");  
		}
}



function ChangeOperator(orderid)
{
    
   
    
    var value = $('#operator_'+orderid).attr('value');
    if(value=='_8')
    {
        window.open("http://"+$(location).attr('hostname')+"/live/orders/add_new?type=" + value,"Add new", "status=1,height=500,width=520,scrollbars=no,toolbar=no,location=no");
          
    } else {
        
        var control= document.getElementById('operator_'+orderid);
        var status = confirm("Are you sure you'd like to change operator to '"+control.options[control.selectedIndex].text+"' ?");
        if(status==true) {
            
            $.post("http://"+$(location).attr('hostname')+"/live/orders/updateorder_operator",     
                { orderid: orderid, operatorid: control.options[control.selectedIndex].value},
                
                                    function(data) {  
                                    
                                        var ret_val  = jQuery.parseJSON( data );
                                         
                                         if( ret_val['status'] == 'success' ){
                                                
                                                alert('Order updated successfully.')
                                                
                                        } else {
            
                                            alert('Error in updating orders.');
            
                                        }
                
                                    }
                );
    
            
           
        }
        else
        {
	        
        	window.location=location.href;
        }
    }
}

function validate_form(runid){
	if(document.getElementById("Size_"+runid).value == "")
	{
		alert("Please enter size");
		document.getElementById("Size_"+runid).focus();
		return false;
	}
	if(document.getElementById("PaperStock_"+runid).value == "")
	{
		alert("Please enter paperstock");
		document.getElementById("PaperStock_"+runid).focus();
		return false;
	}
	if(document.getElementById("Quantity_"+runid).value == "")
	{
		alert("Please enter quantity");
		document.getElementById("Quantity_"+runid).focus();
		return false;
	}
	if(document.getElementById("Coating_"+runid).value == "")
	{
		alert("Please enter coating");
		document.getElementById("Coating_"+runid).focus();
		return false;
	}
	if(document.getElementById("Proofs_"+runid).value == "")
	{
		alert("Please enter proofs");
		document.getElementById("Proofs_"+runid).focus();
		return false;
	}	
	if(document.getElementById("photo_file_"+runid).value == "")
	{
		alert("Please upload image");
		document.getElementById("photo_file_"+runid).focus();
		return false;
	}
	//Update_Run_Order(runid);
}

function DeleteRun(runid){
	if(window.confirm("Are you sure you want to delete this run")){
		window.location.href="http://"+$(location).attr('hostname')+"/live/orders/delete_runorder?rid=" + runid;
	}else{
		return false;
	}
}


$(document).ready(function() {
	/*
	 *  Simple image gallery. Uses default settings
	 */
	$('.fancybox').fancybox();
});
 $(function() {
	$('.timepicker').timepicker();
  });
  


