<?php
class Inspectors extends MY_Controller{
    const AJAX_CLIENT_ORDERS = 'client_orders';

    private $field_list = array(
            'id',
            'agent::name',
            'inspector::name',
            'address',
            'city',
            'paid',
            'amount_due'
    );
    
    public function __construct(){
  
        
               
        parent::__construct();
        //This section is only for Admins and Users
        $this->authorize_or_redirect( array(Person::TYPE_ADMIN,Person::TYPE_USER));
        //Inspector model for this project. Contains Extended base classes from Order model)
        $this->load->model('Inspector_model','inspector_model', TRUE );                  
    }
    public function index(){     
         
        $person = new Person();
        $person->set_company_id( $this->user->company_id );
        $person->set_person_type( Person::TYPE_INSPECTOR );
        $inspectors = $this->inspector_model->get_object( $person );
        
        $orders = array();
        $selected_inspector = null;
        $saved   = null;
        $inspector_id = null;
          
        if ( $this->input->post('save_inspector')){ 
            $inspector = $this->order_mapper->create_object_from_input('Person');
             
            if( $inspector->validate() && $inspector->person_type == Person::TYPE_INSPECTOR ){
                if( $this->inspector_not_booked( $inspector )){
                    $id = $this->order_model->save_or_update_object( $inspector );
                    if( $id == $inspector->id ){
                        $saved = 'update';
                    } else {
                        $saved = true;
                    }                    
                } else {
                    $saved = 'booked'; 
                }

            } else {
                $saved = false;
            }
            $inspector_id = $inspector->id;                   
        }
         
        if( $inspector_id || $inspector_id = $this->uri->segment(3,0)){
            $person->set_id( $inspector_id );
            $selected_inspector = reset($this->inspector_model->get_object( $person ));
            
            //Get orders for this client
            $order_filter = new Filter();
            $order_filter->set_sales_rep( $inspector_id );
            
            $orders = $this->inspector_model->get_orders( $order_filter );
        }        
        
        $headermenuitems = $this->order_model->get_menuitems();
		
        $times = $this->order_model->get_times();
        $states = $this->inspector_model->get_us_states() + array( ''=>'-Select state-');  
        $this->load->view('include/sidebar', array('fluid_layout'=>false,'user'=>$this->user, 'companies'=>$this->companies,'times'=>$times,'headermenuitems'=>$headermenuitems));
        $this->load->view('inspectors/inspectors', array( 'inspectors'=>$inspectors ,
                                                    'orders' => $orders,
                                                    'selected_inspector' => $selected_inspector,
                                                    'saved' => $saved,
                                                    'states' => $states,
                                                    'order_fields' => $this->field_list ));
        $this->load->view('include/footer');        
    }
    public function add_new(){ 
        /**
        * Type of new object
        */
        $class_name = $this->uri->segment(3,0);
        $errors     = array();
                
        $data = array(); 
        if( $this->input->post('save') && $class_name && in_array( $class_name, array('Person') )){
             
            $object = $this->order_mapper->create_object_from_input($class_name);            
             
            if( $object->validate() && $object->person_type == Person::TYPE_INSPECTOR ){
                $old_id = $object->id;
                $object->set_id($this->order_model->save_or_update_object( $object ));
                if( $old_id != $object->id ){
                    $saved = true;    
                } else {
                    $saved = 'update';
                }
            } else {
                $saved = false;
                $errors = $object->errors;
            } 
            $data  = array('object' => $object, 'saved' => $saved, 'action' => site_url('inspectors/add_new/'.$class_name));                 
            
        }  else {
            /**
            * User clicked ADD NEW 
            */
            if( $class_name && in_array( $class_name, array('Person'))){
                $object = new $class_name();  
                if( $class_name == 'Person'){
                    $object->set_company_id( $this->user->company_id );
                    $object->set_person_type( Person::TYPE_INSPECTOR );
                }
                $data = array( 'object' => $object, 'action' => site_url('inspectors/add_new/'. $class_name) );                
            } 
        }
        $data['errors'] = $errors;
        $times = $this->order_model->get_times();
        $data['states'] = $this->order_model->get_us_states() + array( ''=>'-Select state-'); 
        $this->load->view('common/header', array('fluid_layout'=>false, 'user'=>$this->user, 'companies'=>$this->companies, 'times'=>$times));             
        $this->load->view('common/add_new', $data );             
        $this->load->view('common/footer');        
    }
    private function inspector_not_booked( Person $inspector ){
        
        $filter = new Filter();
        $filter->set_inspector_id( $inspector->id );

        
        $orders = $this->order_model->get_orders( $filter );
        
        
        $start = date_parse($inspector->timeoff_date_start.''.$inspector->timeoff_time_start);
        $end   = date_parse($inspector->timeoff_date_end.''.$inspector->timeoff_time_end);
        $start_time = mktime($start['hour'],$start['minute'],0,$start['month'],$start['day'],$start['year']);
        $end_time   = mktime($end['hour'],$end['minute'],0,$end['month'],$end['day'],$end['year']);
        
        $available = true; 
        foreach( $orders as $order ){
  
            $test  = date_parse($order->inspection_date .''.$order->inspection_time );
            $test_time  = mktime($test['hour'],$test['minute'],0,$test['month'],$test['day'],$test['year']);
            
            $hour   = intval(substr( $order->inspection_time, 0, 2));
            $minute = intval(substr( $order->inspection_time, 3, 2));
            $test_end = date_parse($order->inspection_date .''. date( 'H:i', (mktime($hour, $minute)+ $order->estimated_inspection_time * 60*60) ));    
            
            $test_time_end = mktime($test_end['hour'],$test_end['minute'],0,$test_end['month'],$test_end['day'],$test_end['year']);
                 
            if( ( $start_time<=$test_time && $test_time<=$end_time ) ||
                ( $start_time<=$test_time_end && $test_time_end <= $end_time ) ||
                ( $start_time<=$test_time_end && $test_time_end<=$end_time ) ||
                ( $test_time<= $start_time && $end_time <= $test_time_end  ) ){
                $available = false;
                break;
            }
                         
        }
        return $available;
        
    }
    
    
}
