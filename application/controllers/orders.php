<?php

class Orders extends MY_Controller{
    const VIEW_ALL = 'all';
    /**
    * Order processing constants
    */
    const ORDER_CHARGE_AMOUNT = 'charge_amount_submit';
    const ORDER_S       = 'save_n';
    const ORDER_SAVE    = 'btn_save_order';
    const SHIPPING_SAVE = 'shipping_submit';
    const ORDER_PRINT   = 'btn_print_order';
    const WAYBILL_PRINT = 'btn_print_waybill';
    const ORDER_EMAIL   = 'btn_email_order';
    const ORDER_CANCEL  = 'btn_cancel_order';
    const ORDER_REPORT  = 'btn_report_order';
    /**
    * Client field prefixes
    */
    const PREFIX_CLIENT    = 'client_';
    const PREFIX_AGENT     = 'agent_';
    const PREFIX_INSPECTOR = 'inspector_';
    /**
    * New item constants
    */
    const NEW_CLIENT    = '_3';
    const NEW_AGENT     = '_1';
    const NEW_INSPECTOR = '_2';
    const NEW_SALESREP  = '_4';
    const NEW_JOBSTATUS = '_5';
    const NEW_SHIPPING = '_6';
    const NEW_TERMS = '_7';
    const NEW_OPERATOR = '_8';
    const NEW_COMPANY = '_9';
    const NEW_COATINGLIST = '_10';
    
    const NEW_STOCK = '_11';
    const NEW_COLORS = '_12';
    const NEW_SIZE = '_13';
    const NEW_QUANTITY = '_14';
    const NEW_FINISHING = '_15';
    const NEW_COATING = '_16';
    
    const NEW_SHIRTTYPE = '_17';
    const NEW_SHIRTFRONTCOLOR = '_18';
    const NEW_SHIRTBACKCOLOR = '_19';
    const NEW_SHIRTSIZE = '_20';
	
    
    const NEW_ITEM    = 'new_item';
    const NEW_TAX     = 'new_tax';
    
    
    const NEW_UTILITY       = '__2'; 
    const NEW_FOUNDATION    = '__3';
    const NEW_INSPECTION    = '__4';     
    const NEW_STRUCTURE     = '__5';
	


    const ADD_NEW = '-Add/Edit-';
    const EDIT = '-Edit-';
    const EDIT_MENU = '__edit';
    /**
    * Ajax control constants
    */
    const AJAX_ITEM   = 'item';	
    const AJAX_ITEMDROPDOWN = 'itemdropdown';
    const AJAX_PERSON = 'person';
    const AJAX_TAX    = 'tax';
    const AJAX_INSPECTOR_CHECK = 'inspector_check';
    const AJAX_REFRESH = 'refresh_dropdown';
    const AJAX_TYPE_EDIT = 'get_record';
    const AJAX_GET_SALESREP_ITEM = 'SalesRep';
    const AJAX_GET_JOBSTATUS_ITEM = 'jobstatus';    
    const AJAX_GET_STOCK_ITEM = 'stock';
    const AJAX_GET_COLORS_ITEM = 'colors';
    const AJAX_GET_SIZE_ITEM = 'size';
    const AJAX_GET_QUANTITY_ITEM = 'quantity';
    const AJAX_GET_FINISHING_ITEM = 'finishing';
    const AJAX_GET_COATINGS_ITEM = 'coatings';  
    const AJAX_GET_SHIRTTYPE_ITEM = 'shirt_type';  
    const AJAX_GET_SHIRTFRONTCOLOR_ITEM = 'shirt_front_color';  
    const AJAX_GET_SHIRTBACKCOLOR_ITEM = 'shirt_back_color';  
    const AJAX_GET_SHIRTSIZE_ITEM = 'shirt_size';  
    const AJAX_GET_SHIPPING_ITEM = 'shipping';
    const AJAX_TERMS_ITEM   = 'terms';
    const AJAX_OPERATOR_ITEM   = 'OperatorList';
    const AJAX_COMPANYLIST_ITEM   = 'CompanyList';
    const AJAX_GET_COATING_ITEM ='coating';
    
    private $saved = null;
    private $errors = array();
    
    public function __construct(){            
        parent::__construct();
        //This section is only for Admins and Users
        $this->authorize_or_redirect( array(Person::TYPE_ADMIN,Person::TYPE_USER));
        $this->load->library('authorize_payment');
    }
    /**
    * Process creation of new order
    * 
    */
    public function save_order_pdf(){
		$order = $this->marshall_order();
		$update = $this->save_print_order($order);
		if($update == "update"){
			$return["save_order"]='updated';
		}else{
			$return["save_order"]=$update;
		}
		echo json_encode($return);
	}


    public function new_order(){ 
        $saved = null;
        /**
        * Save new order
        */
        if( $this->input->post( self::ORDER_CHARGE_AMOUNT , true )){
            
            $order = $this->marshall_order();
            $this->save_current_order($order);  
                                        
        }
        elseif( $this->input->post( self::ORDER_S , true )){
            
            $order = $this->marshall_order();
            $this->save_current_order($order);   
                                        
        }
        elseif( $this->input->post( self::ORDER_SAVE , true )){
            
            $order = $this->marshall_order();
            $this->save_current_order($order);   
			                            
        }
        elseif( $this->input->post( self::SHIPPING_SAVE , true )){
            
            $order = $this->marshall_order();
            $this->save_current_order($order);   
                                        
        } 
        else if( $this->input->post( self::ORDER_EMAIL, true)) {
            $order = $this->marshall_order();
            if( in_array($this->save_current_order($order), array( true, 'update')) && $order->status != Order::ORDER_STATUS_PENDING){
                $this->email_order( $order );
                $this->saved = 'email-sent';
				                      
            }             
        } 

        else if( $this->input->post( self::WAYBILL_PRINT, true)) {    
            $order = $this->marshall_order();
            if( in_array($this->save_current_order($order), array(true, 'update'))  ){
                $this->print_order( $order );  
            }                     
        }

        else if( $this->input->post( self::ORDER_PRINT, true)) {             
            $order = $this->marshall_order();
            if( in_array($this->save_current_order($order), array(true, 'update'))  ){
                $this->print_order( $order );  
				                       
            }                     
        } else if( $this->input->post( self::ORDER_CANCEL, true)) {
            $order = new Order();                       
        } else if( $this->input->post( self::ORDER_REPORT, true)) {
            $order = $this->marshall_order();   
            if($this->save_current_order($order) !== false ){
                header('Location: '.site_url('reports/new_report?order_id='.$order->id));
            }                       
        } else {
            $order = new Order();
            $order->set_order_date( gmdate('m-d-Y'));
	    $order->set_report_due( gmdate('m-d-Y'));
			   
        }
        
        if( isset( $order )){
            $this->display_order( $order, $this->saved );   
			   
        }
        
    }
    private function save_current_order( Order $order ){
        
        
        
         /**
        * We'll validate this order. If valid it will be saved.
        */   
        //if( $order->validate()  ){
        	//if( $this->inspector_available( $order )){
        	//log_message('debug','Order ID :'.$$old_order->id);
        		// Getting the old order
	        	$order_id = $this->uri->segment(3);
                        
                        $order_id = (isset($order->id) and $order->id > 0) ? $order->id : $order_id;
                        
		        $filter = new Filter();
		        $filter->set_order_id( $order_id );
		        $filter->set_company_id( $this->user->company_id);
		        $old_order = reset($this->order_model->get_orders( $filter ));      
                        
                        $company_order_id = $old_order->company_order_id;
                        
                        if($company_order_id < 1) {                            
                            $order->set_company_order_id($this->getCompanyNextOrderId()); 
                        } else {
                            $order->set_company_order_id($company_order_id); 
                        }
                        
                        
                if( $order_id = $this->order_model->save_order( $order, $old_order )){
                    if( isset( $order->id ) && $order->id == $order_id ){
                        $this->saved = 'update';
                    } else {
                        $order->set_id( $order_id );
                        $this->saved = true; 
                        
                        $session_id = $this->session->userdata('user_order');
                                                
                        // update related jobs                        
                        $this->db->where('order_id',$session_id);
                        $this->db->update('dr_orders_jobs',array('order_id' => $order_id));
                        $this->session->set_userdata('user_order','');
                        
                        $this->db->where('order_id',$session_id);
                        $this->db->update('dr_orders_shirts',array('order_id' => $order_id));
                    }
                    
                } else {
                    $this->saved = false;
                }                                    
           // } else {
            //    $this->saved = 'inspector_taken';    
           // }      		       
        //} else {
        //    $this->saved = false;
        //}
        return $this->saved;        
		    
    }
    private function getCompanyNextOrderId() {
        
        $company_id = $this->user->company_id;
        //$query = 'select company_order_id from dr_orderss where company_id = '.$company_id.' order by company_order_id DESC limit 1';
        $this->db->select('company_order_id');
        $this->db->from('dr_orderss');
        $this->db->where('company_id',$company_id);
        $this->db->order_by('company_order_id','DESC');
        $this->db->limit(1);
        $result = $this->db->get();
        
        if($result->num_rows() > 0) {
            
            $result = $result->row();
            $company_order_id = $result->company_order_id;
            return $company_order_id + 1;
        }
        return 1000;
    }

    private function save_print_order( Order $order ){
		$order_id = $this->uri->segment(3);
		$filter = new Filter();
		$filter->set_order_id( $order_id );
		$filter->set_company_id( $this->user->company_id);
		$old_order = reset($this->order_model->get_orders( $filter ));
		if( $order_id = $this->order_model->save_order( $order, $old_order )){
			if( isset( $order->id ) && $order->id == $order_id ){
				$this->saved = 'update';
			} else {
				$order->set_id( $order_id );
				$this->saved = $order_id;    
                                $session_id = $this->session->userdata('user_order');
                                                
                                // update related jobs                        
                                $this->db->where('order_id',$session_id);
                                $this->db->update('dr_orders_jobs',array('order_id' => $order_id));
                                $this->session->set_userdata('user_order','');

                                $this->db->where('order_id',$session_id);
                                $this->db->update('dr_orders_shirts',array('order_id' => $order_id));
			}
			
		}
        return $this->saved;		    
    }
	
    public function email_order( Order $order ){
         
        if( $order->id ) {
        	
            $config= array();
            $config['mailtype']  = 'html';
            $config['protocol']  = 'smtp';
            $config['charset']='utf-8';
            $config['smtp_host'] = 'ssl://email-smtp.us-west-2.amazonaws.com';
            $config['smtp_user'] = 'AKIAIYFUHJXH3MKWF4DA';
            $config['smtp_pass'] = 'Ag23WyoeIjPa/6qfOoPUsmme8RLPoz03OoDZhRyXNPVD';
            $config['smtp_port'] = "465";
            $config['smtp_timeout'] = 60;
            $config['smtp_crypto'] = 'ssl'; 
            $config['newline'] = "\r\n"; 

            // $this->load->library('email'); 
            //$config['mailtype']  = 'html';
            //$config['protocol']  = 'smtp';
            //$config['smtp_host'] = 'https://64.207.190.29';
            //$config['smtp_user'] = 'orders@printshopplus.net';
            //$config['smtp_pass'] = '8yBd3?y9';

            $this->load->library('email', $config);

            $company = new Company();
            $company->set_id( $this->user->company_id );
            $company = $this->order_model->get_object( $company );
            if( $company ){
                $company = reset( $company );
            }
            
            $order->set_company( $company );
            $filename = 'orders_pdfs/'.date('Y').'/'.date('m').'/order_'.$order->id.'.pdf';
            
            if (!file_exists('orders_pdfs/'.date('Y'))) {
                mkdir('orders_pdfs/'.date('Y'), 0755, true);
                mkdir('orders_pdfs/'.date('Y').'/'.date('m'), 0755, true);
            } else {
                if (!file_exists('orders_pdfs/'.date('Y').'/'.date('m'))) {
                     mkdir('orders_pdfs/'.date('Y').'/'.date('m'), 0755, true);
                }
            }
            
            $this->load->library('order_pdf');
            
            $ids = $this->order_model->get_company_order_ids($order);
            //$invoice_number = array_search($order->id, $ids)+1;
            $invoice_number = $order->company_order_id;  
			
            $this->order_pdf->create_pdf( $order, $this->order_helper, $filename, $invoice_number );
             
            // $this->email->from( $company->email, $company->name); 
            // $this->email->reply_to( $company->email);
            $this->email->from( "orders@printshopplus.net", 'Print Shop Plus');
            $this->email->reply_to($company->email);
            
            // $filename =  $filename;
            $this->email->to($order->client->email);
            // $this->email->to('truongthanhtungitvn@gmail.com');//
            $this->email->subject('Thank you for your order');
            $this->email->message( "Thank you for your order, attached you'll find all your order details in PDF format.");
            $this->email->attach( $filename );       
            $this->email->send();
        
        }             
    }
 
    function mail_attachment($filename, $path, $mailto, $from_mail, $from_name, $replyto, $subject, $message) {
        $file = $path.$filename;
        $file_size = filesize($file);
        $handle = fopen($file, "r");
        $content = fread($handle, $file_size);
        fclose($handle);
        $content = chunk_split(base64_encode($content));
        $uid = md5(uniqid(time()));
        $name = basename($file);
        $header = "From: ".$from_name." <".$from_mail.">\r\n";
        $header .= "Reply-To: ".$replyto."\r\n";
        $header .= "MIME-Version: 1.0\r\n";
        $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
        $header .= "This is a multi-part message in MIME format.\r\n";
        $header .= "--".$uid."\r\n";
        $header .= "Content-type:text/plain; charset=iso-8859-1\r\n";
        $header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
        $header .= $message."\r\n\r\n";
        $header .= "--".$uid."\r\n";
        $header .= "Content-Type: application/octet-stream; name=\"".$filename."\"\r\n"; // use different content types here
        $header .= "Content-Transfer-Encoding: base64\r\n";
        $header .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n\r\n";
        $header .= $content."\r\n\r\n";
        $header .= "--".$uid."--";
        if (mail($mailto, $subject, "", $header)) {
            //echo "mail send ... OK"; // or use booleans here
        } else {
            //echo "mail send ... ERROR!";
        }
    }
 
 
    private function get_message_body( $message, $company ){
        
        $path = parse_url( site_url());
        $path = str_replace('index.php','',$path['path']); 
        $path = $_SERVER['DOCUMENT_ROOT'] . $path . 'application/views/assets/img/';
         
        $filename = $order->company_id.'_logo'; 
         
        $img = '';
        if( file_exists( $path.$filename )){
            $img =  str_replace('index.php','',site_url() .'application/views/assets/img/' . $filename);
             
        }      
        
        $html = '
        <html>
            <body>
            '.$message.'
             <table width="50%"><tr>
            '. ($img ?'<td><img src="'.$img.'" width="150" height="100" /></td>' : '') .'
            <td width="50%" style="color:#000000;"><span style="font-weight: bold; font-size: 14pt;">'.$company->name.'</span><br />'.$company->address.'<br />'.$company->city.','.$company->state.','. $company->zip. '<br />'.$order->company->phone_1.'<br />'. $order->company->email .'<br /></td>
            </tr></table>
            </body>
        </html>';
        
        return $html;
    }
	
	public function print_order_pdf(){
		$this->load->library('order_pdf');
		$order_id = $this->uri->segment(3);
                $filter = new Filter();
                $filter->set_order_id( $order_id );
                $order = reset($this->order_model->get_orders( $filter ));
                $filter_company = new Company();
                $filter_company->set_id( $order->company_id );        
                $companies = $this->order_model->get_object( $filter_company );
                $ids = $this->order_model->get_company_order_ids($order);
                 if( $companies ){
                    $order->set_company( reset( $companies ));
                }
        
                //$invoice_number = array_search($order->id, $ids)+1;
        
		$invoice_number = $order->company_order_id;
		$this->order_pdf->create_pdf( $order, $this->order_helper, false, $invoice_number );        
        
	}
	
    public function print_order( Order $order ){
        $this->load->library('order_pdf');
        $filter_company = new Company();
        $filter_company->set_id( $order->company_id );        
        $companies = $this->order_model->get_object( $filter_company );
          
        //$ids = $this->order_model->get_company_order_ids($order);
        if( $companies ){
            $order->set_company( reset( $companies ));
        }
        
        //$invoice_number = array_search($order->id, $ids)+1;
        
	$invoice_number = $order->company_order_id;
        
        if($this->input->post('btn_print_waybill')){
            $this->order_pdf->create_waybill_pdf( $order, $this->order_helper, false, $invoice_number );
        }else{
            $this->order_pdf->create_pdf( $order, $this->order_helper, false, $invoice_number );
        }
        
    }

    function generate_bulk_waybill(){
        $this->load->library('order_pdf');

        $defaultPath = 'pdfs/';
        
        if(!is_dir($defaultPath)){
            mkdir($defaultPath,0777);
        }

        foreach (explode(',',$this->input->get('id')) as $value) {
            
            $filter = new Filter();
            $filter->set_order_id( $value );
            $filter->set_company_id( $this->user->company_id);

            $order = $this->order_model->get_orders( $filter ,true);

            $filter_company = new Company();
            $filter_company->set_id( $order->company_id );        
            $companies = $this->order_model->get_object( $filter_company );
              
            //$ids = $this->order_model->get_company_order_ids($order);
            if( $companies ){
                $order->set_company( reset( $companies ));
            }
                
            $invoice_number = $order->company_order_id;

            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        
            $charactersLength = strlen($characters);

            $randomString = '';
    
            for ($i = 0; $i < 20; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }

            $pdfname = $defaultPath.$randomString.'.pdf';

            $this->order_pdf->create_waybill_pdf( $order, $this->order_helper, $pdfname, $invoice_number );
            
           $er[] = $pdfname;
        }

        echo json_encode( $er );
    }

    function generate_bulk_print(){
        $this->load->library('order_pdf');

        $defaultPath = 'pdfs/';
        
        if(!is_dir($defaultPath)){
            mkdir($defaultPath,0777);
        }

        foreach (explode(',',$this->input->get('id')) as $value) {
            
            $filter = new Filter();
            $filter->set_order_id( $value );
            $filter->set_company_id( $this->user->company_id);

            $order = $this->order_model->get_orders( $filter ,true);


            $filter_company = new Company();
            $filter_company->set_id( $order->company_id );        
            $companies = $this->order_model->get_object( $filter_company );
              
            //$ids = $this->order_model->get_company_order_ids($order);
            if( $companies ){
                $order->set_company( reset( $companies ));
            }




            $invoice_number = $order->company_order_id;

            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        
            $charactersLength = strlen($characters);

            $randomString = '';
    
            for ($i = 0; $i < 20; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }

            $pdfname = $defaultPath.$randomString.'.pdf';

            $this->order_pdf->create_pdf( $order, $this->order_helper, $pdfname, $invoice_number );
            
           $er[] = $pdfname;
        }

        echo json_encode( $er );
    }

    private function marshall_order(){  
	    
        $client = $this->order_mapper->create_object_from_input('Person','post', Order_mapper::PREFIX_CLIENT )
          ->set_company_id( $this->user->company_id)
          ->set_person_type( Person::TYPE_AGENT );
        

        if( $client->id == null and $client->name !=''){
            if( $client->validate() ){
                 $client->set_id($this->order_model->save_or_update_object( $client ));
            } else {
                $this->errors['client'] = $client->errors;
            }
        } elseif($client->name !='') {
            if( $client->validate() ){
                
            } else {
                $this->errors['client'] = $client->errors;
            }
        }
        
	/**
        * Get order from POST
        */ 
         $order  = $this->order_mapper->create_object_from_input('Order')
                  ->set_company_id( $this->user->company_id)
                 ->set_notes_by($this->user->id)
		 ->set_client( $client );
        
         
        /**
        * Get items from post
        */
        $items   = $this->order_mapper->create_object_array_from_input('Item', Order_mapper::SOURCE_POST, 'item_<name>_<index>');
        $filter_item = new Base_item();        
        /**
        * Only items with ID are used
        */ 
        foreach( $items as $item ){
            
            if( $item->item_id > 0 ){
                $filter_item->set_id( $item->item_id);
                $base_items = $this->order_model->get_object( $filter_item );
                $base_item = reset( $base_items );
                $item->set_name( $base_item->name );
                $order->add_item( $item );                    
            }
        }
        // echo "<pre>"; print_r($order);die;
        return $order;        
    }
	/*
	private function marshall_order(){  
        $client = $this->order_mapper->create_object_from_input('Person','post', Order_mapper::PREFIX_CLIENT )
                  ->set_company_id( $this->user->company_id)
                  ->set_person_type( Person::TYPE_CLIENT );
        
        
        if( $client->id == null ){
            $client->set_id($this->order_model->save_or_update_object( $client ));
        }
          
        
        $agent  = $this->order_mapper->create_object_from_input('Person','post', Order_mapper::PREFIX_AGENT)
                  ->set_company_id( $this->user->company_id)
                  ->set_person_type( Person::TYPE_AGENT );                                               
        
		// Get order from POST
         
        $order  = $this->order_mapper->create_object_from_input('Order')
                  ->set_company_id( $this->user->company_id)
                  ->set_agent( $agent )
                  ->set_client( $client );
        
         
        $filter = new Person();
        $filter->set_id( $this->input->post('inspector_id'));
        $filter->set_company_id( $this->user->company_id );
        
        $inspector = reset($this->order_model->get_object( $filter ));
        
        if( $inspector ){
            $order->set_inspector( $inspector );
        }
        $order->set_spa(0);
        if( $this->input->post('spa')){
            $order->set_spa( 1 );
        }
        $order->set_pool(0);
        if( $this->input->post('pool')){
            $order->set_pool( 1 );
        }
        
        $items   = $this->order_mapper->create_object_array_from_input('Item', Order_mapper::SOURCE_POST, 'item_<name>_<index>');
        $filter_item = new Base_item();        
       
        //* Only items with ID are used
         
        foreach( $items as $item ){
            
            if( $item->item_id > 0 ){
                $filter_item->set_id( $item->item_id);
                $base_items = $this->order_model->get_object( $filter_item );
                $base_item = reset( $base_items );
                $item->set_name( $base_item->name );
                $order->add_item( $item );                    
            }
        }
        return $order;        
    }*/
	
	
    /**
    * Controller handler for adding new items to various Drop down menus (select boxes )
    * - User click  ADD NEW
    * - User clicks SAVE_NEW ( after inputing new data ) 
    */
    public function add_new(){   

        // print_r($_POST);
        //Saving new stuff
        // if( $_POST['save'] ){
            // print_r($_POST);
            //object can be one of 3 types
            if( $class = $_POST['object_type'] ){
                
                $object_class = $class;
                $object = $this->order_mapper->create_object_from_input( $class );
                $object->set_company_id( $this->user->company_id);
                
                if( $object->validate()){
                    $old_id = $object->id;
                    $object->set_id($this->order_model->save_or_update_object( $object ));
                    if( $old_id != $object->id ){
                        $saved = true;  
                        $object = new $class();
                        $object->set_id( null);
                        $object->set_company_id( $this->user->company_id );
                    } else {
                        $saved = 'update';
                    }
                } else {
                    $this->errors = $object->errors;
                    $saved = false;
                }
                
                $data  = array('object' => $object, 'saved' => $saved    );
                
                if( $saved &&  $class  == 'Base_item'){
                    $data['select_name'] = $this->uri->segment(3);
                }
                
                if( $saved == 'update' ){
                    $data['selected_item'] = $object->id;
                }                             
            }
            
        // } 
         else { 

            $type = $_GET['type'];
            $object_class = $type;
            
            /**
            * User clicked ADD NEW 
            */
            switch( $type ){
                case self::NEW_FOUNDATION:
                case self::NEW_INSPECTION:
                case self::NEW_STRUCTURE:
				case self::NEW_COMPANY:
					 $person = new Person();                     
                     $param = Person::TYPE_COMPANY;
					 $person->set_company_id( $this->user->company_id);
                     $call  = 'set_person_type';
                     $person->set_person_type( $param );
                     $data = array( 'object' => $person  );
                     break;
				case self::NEW_OPERATOR:
					 $object = new OperatorList();
                     $data = array( 'object' => $object  );
					 $object->set_company_id( $this->user->company_id);
                     break;
				case self::NEW_COATINGLIST:
					 $object = new Coating();
                     $data = array( 'object' => $object  );
					 $object->set_company_id( $this->user->company_id);
					 $data = array( 'object' => $object  );
                     break;
                case self::NEW_TERMS:
					 $object = new Terms();
                     $data = array( 'object' => $object  );
					 $object->set_company_id( $this->user->company_id);
                     break;
				case self::NEW_SHIPPING:
				 	 $object = new ShippingMethod();
                     $data = array( 'object' => $object  );
					 $object->set_company_id( $this->user->company_id);
                     break;
		     case self::NEW_JOBSTATUS:
                        $object = new JobStatus();
                     
                        $data = array( 'object' => $object  );
			$object->set_company_id( $this->user->company_id);
                     break;
                     case self::NEW_STOCK:
                        $object = new Stock();
                        $data = array( 'object' => $object  );
			$object->set_company_id( $this->user->company_id);
                     break;
                    case self::NEW_COLORS:
                        $object = new Colors();
                        $data = array( 'object' => $object  );
			$object->set_company_id( $this->user->company_id);
                     break;
                     case self::NEW_SIZE:
                        $object = new Size();
                        $data = array( 'object' => $object  );
			$object->set_company_id( $this->user->company_id);
                     break;
                     case self::NEW_QUANTITY:
                        $object = new Quantity();
                        $data = array( 'object' => $object  );
			$object->set_company_id( $this->user->company_id);
                     break;
                     case self::NEW_FINISHING:
                        $object = new Finishing();
                        $data = array( 'object' => $object  );
			$object->set_company_id( $this->user->company_id);
                     break;
                     case self::NEW_COATING:
                        $object = new Coatings();
                        $data = array( 'object' => $object  );
			$object->set_company_id( $this->user->company_id);
                     break;
                    case self::NEW_SHIRTTYPE:
                        $object = new ShirtType();
                        $data = array( 'object' => $object  );
			$object->set_company_id( $this->user->company_id);
                     break;
                     case self::NEW_SHIRTFRONTCOLOR:
                        $object = new ShirtFrontColor();
                        $data = array( 'object' => $object  );
			$object->set_company_id( $this->user->company_id);
                     break;
                     case self::NEW_SHIRTBACKCOLOR:
                        $object = new ShirtBackColor();
                        $data = array( 'object' => $object  );
			$object->set_company_id( $this->user->company_id);
                     break;
                     case self::NEW_SHIRTSIZE:
                        $object = new ShirtSize();
                        $data = array( 'object' => $object  );
			$object->set_company_id( $this->user->company_id);
                     break;
                    case self::NEW_SALESREP:
                             $object = new SalesRep();
                     $call  = 'set_company_id';
                     $data = array( 'object' => $object  );
					 $object->set_company_id( $this->user->company_id);
                     break;
                case self::NEW_UTILITY:
                     $object = new Type();
                     $param = str_replace('_','', $type);
                     $call  = 'set_type';
                     
                     $object->set_type( $param );
                     $object->set_company_id( $this->user->company_id);
                     $data = array( 'object' => $object  );
                     break;
                case self::NEW_ITEM:
                     $item = new Base_item();
                     $item->set_company_id( $this->user->company_id);
                     $data = array( 'object' => $item, 'select_name' => $this->input->get('select_name')  );                         
                     break;
				
                case self::NEW_TAX:
                     $tax = new Tax();
                     $tax->set_company_id( $this->user->company_id);
                     $data = array( 'object' => $tax );
                     break;                     
                case self::NEW_AGENT:                             
                case self::NEW_CLIENT:
                case self::NEW_INSPECTOR:
                     $person = new Person();
                     
                     $param = str_replace('_','', $type);
                     $call  = 'set_person_type';                     
                     $person->set_company_id( $this->user->company_id);
                     $person->set_person_type( $param );
                     $data = array( 'object' => $person  );
                     break;   
            }                
              
        }
        
        $select_name = '';
        if( $_GET['select_name']){  
            $select_name = '/'. $_GET['select_name'];    
        }
        if( $data ){
            $data['action'] = site_url('orders/add_new' . $select_name);
            
            $object_class = get_class($data['object']); 
            $filter = new $object_class;
			//if($object_class=='Base_item')
			//{
				$filter->set_company_id( $this->user->company_id);	
			//}
            
            
            if( isset($call)){
                $filter->$call( $param );
            }
            $objects = $this->order_model->get_object( $filter );  
            $data['objects'] = $objects;
        }
        $headermenuitems = $this->order_model->get_menuitems($this->user->company_id);
        $data['states'] = $this->order_model->get_us_states() + array( ''=>'-Select state-');
        $data['errors'] = $this->errors;
        // $this->load->view('include/sidebar', array('fluid_layout'=>true, 'user'=>$this->user, 'companies'=>$this->companies, 'headermenuitems'=>$headermenuitems,'dont_display_header' => true));             
        $dat = $this->load->view('common/add_new', $data );             
        // $this->load->view('include/footer', array('dont_display_header' => true));           
        echo $dat;
    }
	
	public function run_orders(){

            $order_id = $_GET["oid"];
            $jobStatusId=$_GET["jid"];

            $filter = new Filter();
            $filter->set_order_id( $order_id );
            $filter->set_company_id( $this->user->company_id);

            $get_orders = $this->order_model->get_orders( $filter );
            $old_order = reset($get_orders);        

            $orders = $this->order_model->get_run_orders($this->user->company_id,$order_id);
            $headermenuitems = $this->order_model->getJobStatusWithDefault($this->user->company_id);;

            $statuslist  =  $this->order_model->get_jobstatus_order($this->user->company_id);
            $operatorlist  =  $this->order_model->get_operatorlist($this->user->company_id);

            $coatinglist  =  array('' => '-Select-') + $this->order_model->get_coatinglist($this->user->company_id) + array(self::NEW_COATINGLIST => self::ADD_NEW );

            $max_run_id = $this->order_model->get_max_runorderid();

            $this->load->view('common/header',array('fluid_layout'=>false, 'user'=>$this->user, 'companies'=>$this->companies,'new_order'=>true,'headermenuitems'=>$headermenuitems));        
            $this->load->view('run/run_order', array('orders'=>$orders,'statuslist'=>$statuslist, 'operatorlist'=>$operatorlist,'coatlist'=>$coatinglist,'max_run_id'=>$max_run_id));
            $this->load->view('common/footer');  		
        }
	
	public function generate_run_pdf(){		
		$this->load->library('run_pdf');
        $filter = new Filter();
		
        $filter->set_company_id( $this->user->company_id);
        $old_order = reset($this->order_model->get_orders( $filter ));	
		
        $headermenuitems = $this->order_model->getJobStatusWithDefault($this->user->company_id);
		
		$statuslist  =  $this->order_model->get_jobstatus_order($this->user->company_id);
		$operatorlist  =  $this->order_model->get_operatorlist($this->user->company_id); 
		
		$runid=$this->uri->segment(3);
		$runs = array();

		$run = $this->order_model->get_rundetails_byid($runid);
		$orders = $this->order_model->get_run_orders($this->user->company_id,$run["order_ids"]);
		
		//$runs[] =array('run'=>$run,'orders'=>$orders,'statuslist'=>$statuslist,'operatorlist'=>$operatorlist);
		
        $this->run_pdf->create_pdf(array('run'=>$run,'orders'=>$orders,'statuslist'=>$statuslist,'operatorlist'=>$operatorlist));
		//redirect('/orders/view_runorder');
	}
	
	public function generate_run_pdf_temp(){		
		$this->load->library('run_pdf');
        $filter = new Filter();
		
        $filter->set_company_id( $this->user->company_id);
        $old_order = reset($this->order_model->get_orders( $filter ));	
		
        $headermenuitems = $this->order_model->getJobStatusWithDefault($this->user->company_id);
		
		$statuslist  =  $this->order_model->get_jobstatus_order($this->user->company_id);
		$operatorlist  =  $this->order_model->get_operatorlist($this->user->company_id); 
		
		$auto_id=$this->uri->segment(3);
		$runs = array();

		$run = $this->order_model->get_run_temp_details_byid($auto_id);
		$orders = $this->order_model->get_run_orders($this->user->company_id,$run["order_ids"]);
		
		//$runs[] =array('run'=>$run,'orders'=>$orders,'statuslist'=>$statuslist,'operatorlist'=>$operatorlist);
		
        $this->run_pdf->create_pdf(array('run'=>$run,'orders'=>$orders,'statuslist'=>$statuslist,'operatorlist'=>$operatorlist));
		//redirect('/orders/view_runorder');
	}
	
	public function view_runorder(){
         
        $filter = new Filter();
		
        $filter->set_company_id( $this->user->company_id);
        $old_order = reset($this->order_model->get_orders( $filter ));	
		
        $headermenuitems = $this->order_model->getJobStatusWithDefault($this->user->company_id);
		
		$statuslist  =  $this->order_model->get_jobstatus_order($this->user->company_id);
		$operatorlist  =  $this->order_model->get_operatorlist($this->user->company_id);
		$coatinglist  =  array('' => '-Select-') + $this->order_model->get_coatinglist($this->user->company_id) + array(self::NEW_COATINGLIST => self::ADD_NEW );

		$runlist= $this->order_model->get_runlist($this->user->company_id);
        $this->load->view('common/header',array('fluid_layout'=>false, 'user'=>$this->user, 'companies'=>$this->companies,'new_order'=>true,'headermenuitems'=>$headermenuitems));
		$runs = array();
		foreach($runlist as $run){
			$orders = $this->order_model->get_run_orders($this->user->company_id,$run["order_ids"]);
			$runs[] =array('runs'=>$run,'orders'=>$orders);
		}
		$this->load->view('run/view_run_order', array('runs'=>$runs,'statuslist'=>$statuslist,'coatlist'=>$coatinglist, 'operatorlist'=>$operatorlist));		
        $this->load->view('common/footer'); 
    }
	
	function getMicrotime()
	{
		if (version_compare(PHP_VERSION, '5.0.0', '<'))
		{
			return array_sum(explode(' ', microtime()));
		}
		
		return microtime(true);
	}
	
	function replacespecialcharacters($url) {
	  $url = str_replace(array("�", "�"), "a", $url); // Additional Swedish filter
	  $url = str_replace(array("�", "�"), "a", $url); // Additional Swedish filter
	  $url = str_replace(array("�", "�"), "o", $url); // Additional Swedish filter

	  $url = preg_replace("/[^a-z0-9\s\-]/i", "", $url); // Remove special characters
	  $url = preg_replace("/\s\s+/", " ", $url); // Replace multiple spaces with one space
	  $url = trim($url); // Remove trailing spaces
	  $url = preg_replace("/\s/", "-", $url); // Replace all spaces with hyphens
	  $url = preg_replace("/\-\-+/", "-", $url); // Replace multiple hyphens with one hyphen
	  $url = preg_replace("/^\-|\-$/", "", $url); // Remove leading and trailing hyphens
	  $url = strtolower($url);

	  return $url;
	}
	
	public function save_runorders_temp(){
		$return = array();
		$file_name="";
		if(isset($_FILES['photo_file']['size']) && $_FILES['photo_file']['size'] != 0){
			$file_extension=strtolower(end(explode('.',$_FILES['photo_file']['name'])));
			$file_name=$this->replacespecialcharacters($this->getMicrotime()).".".$file_extension;
			$file_path=ROOTPATH.'run_orders_files/'.$file_name;
			move_uploaded_file(strip_tags($_FILES['photo_file']['tmp_name']), $file_path);			
		}
		$return["auto_id"]=$this->order_model->save_run_order_temp($file_name);
		echo json_encode($return);
	}

	public function save_runorders(){
		$return = array();
		$file_name="";
		if(isset($_FILES['photo_file']['size']) && $_FILES['photo_file']['size'] != 0){
			$file_extension=strtolower(end(explode('.',$_FILES['photo_file']['name'])));
			$file_name=$this->replacespecialcharacters($this->getMicrotime()).".".$file_extension;
			$file_path=ROOTPATH.'run_orders_files/'.$file_name;
			move_uploaded_file(strip_tags($_FILES['photo_file']['tmp_name']), $file_path);			
		}
		$return["run_id"]=$this->order_model->save_run_order($file_name,$this->user->company_id);				
		//$return["status"]="success";
		//if($this->uri->segment(2) !='next') {
			redirect('/orders/view_runorder');
		//}
		//else {
			//redirect('orders/generate_run_pdf/'.$return["run_id"]);
		//}
		//redirect('orders/generate_run_pdf/'.$return["run_id"]);
		//.$max_run_id
		//echo json_encode($return);
	}
	
	public function update_runorders(){
		$return = array();
		$file_name="";
		$runid=$this->input->post('runid');
		if(isset($_FILES['photo_file']['size']) && $_FILES['photo_file']['size'] != 0){
			$file_extension=strtolower(end(explode('.',$_FILES['photo_file']['name'])));
			$file_name=$this->replacespecialcharacters($this->getMicrotime()).".".$file_extension;
			$file_path=ROOTPATH.'run_orders_files/'.$file_name;
			move_uploaded_file(strip_tags($_FILES['photo_file']['tmp_name']), $file_path);
		}else{			
			$run_details=$this->order_model->get_rundetails_byid($runid);
			$file_name=$run_details["upload_image"];
		}
		$return["status"]="success";
		$return["run_id"]=$this->order_model->save_run_order($file_name);		
		//echo json_encode($return);		
		redirect('/orders/view_runorder');
	}	
	
	public function delete_runorder(){
		$this->order_model->delete_runorder($_GET["rid"]);	
		redirect('/orders/view_runorder');
	}

    public function view_orders() {         
                
        $status = $this->uri->segment(3);
		
		if($jobstatus && $status == 3){
			redirect('/orders/view_runorder');
		}
		$headermenuitems = $this->order_model->getJobStatusWithDefault($this->user->company_id);
                
                // statistics
                
                $start_date = ($start_date = $this->input->post('start_date')) ? $start_date : gmdate('m-d-Y', time()-7*24*60*60);
                $end_date   = ($end_date = $this->input->post('end_date'))     ? $end_date   : gmdate('m-d-Y');
                
                $allJobs    = $this->order_model->countJobsStatistics($this->user->company_id,0,$start_date , $end_date);
                $completedJobs = $this->order_model->countJobsStatistics($this->user->company_id,3,$start_date , $end_date);
                $activeJobs = $allJobs - $completedJobs;
                
                // end statistics
		
		$this->load->view('include/sidebar',array('fluid_layout'=>false,
                    'user'=>$this->user, 
                    'companies'=>$this->companies,
                    'new_order'=>true
                    ,'headermenuitems'=>$headermenuitems,
                    'start_date' => $start_date,
                    'end_date'   => $end_date,
                    'allJobs' => $allJobs,
                    'completedJobs' => $completedJobs,
                    'activeJobs' => $activeJobs,
                        ));        
		$this->load->view('orders/view_orders', array('status'=>$status));
        $this->load->view('include/footer');
    }

    public function test(){
        $headermenuitems = $this->order_model->getJobStatusWithDefault($this->user->company_id);
                
                // statistics
                
                $start_date = ($start_date = $this->input->post('start_date')) ? $start_date : gmdate('m-d-Y', time()-7*24*60*60);
                $end_date   = ($end_date = $this->input->post('end_date'))     ? $end_date   : gmdate('m-d-Y');
                
                $allJobs    = $this->order_model->countJobsStatistics($this->user->company_id,0,$start_date , $end_date);
                $completedJobs = $this->order_model->countJobsStatistics($this->user->company_id,3,$start_date , $end_date);
                $activeJobs = $allJobs - $completedJobs;
        
        $this->load->view('include/sidebar',array('fluid_layout'=>false,
                    'user'=>$this->user, 
                    'companies'=>$this->companies,
                    'new_order'=>true
                    ,'headermenuitems'=>$headermenuitems,
                    'start_date' => $start_date,
                    'end_date'   => $end_date,
                    'allJobs' => $allJobs,
                    'completedJobs' => $completedJobs,
                    'activeJobs' => $activeJobs,
                        ));
        $this->load->view('include/content');
        $this->load->view('include/footer');
    }
    
    public function view_ordersajax() {
        $status = $this->uri->segment(3,0);
               
        $search = $this->input->get('sSearch');
        
        $sLimit = 0;
        $offset = 0;
        $columnOrder = '';
        $orderBy     = '';
        $orders = array();
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ) {
            $offset = $this->input->get('iDisplayStart');
            $sLimit = $this->input->get('iDisplayLength');
        }
        
        $columnOrder = (isset($_GET['iSortCol_0']) and $_GET['iSortCol_0'] !='') ? $this->input->get('iSortCol_0') : 1;
        $orderBy     = (isset($_GET['sSortDir_0']) and $_GET['sSortDir_0'] !='') ? $this->input->get('sSortDir_0') : 'DESC';
        $sLimit      = ($sLimit == 0 ) ? 5 : $sLimit;
        
        if($status == 'all') {
            $status = 0;
        }
        $where = '';
                
        
        if($search == '') {
            $query = 'SELECT ordr.id,ordr.company_order_id,ordr.order_date,ordr.client_id,ordr.sales_rep,ordr.operator_id,ordr.report_due'
                    . ' FROM dr_orderss ordr WHERE ordr.company_id ='.$this->user->company_id.' and ordr.is_quote=0';
            $queryCount = 'SELECT count(ordr.id) as total FROM dr_orderss ordr WHERE ordr.company_id ='.$this->user->company_id.'  and ordr.is_quote=0';
        } else {
            $search = trim($search);
            $query = 'SELECT ordr.id,ordr.company_order_id,ordr.order_date,ordr.client_id,ordr.sales_rep,ordr.operator_id,ordr.report_due
                    from dr_orderss ordr
                    inner join dr_persons person
                    on ordr.client_id = person.id
                    where ordr.company_id = '.$this->user->company_id.' and ordr.is_quote=0 and (person.name like "%'.$search.'%"  or ordr.order_date like "%'.$search.'%" or ordr.report_due like "%'.$search.'%")';
            
            $queryCount = 'SELECT count(ordr.id) as total FROM dr_orderss ordr inner join dr_persons person
                    on ordr.agent_id = person.id
                    where ordr.company_id = '.$this->user->company_id.' and ordr.is_quote=0 and (person.name like "%'.$search.'%"  or ordr.order_date like "%'.$search.'%" or ordr.report_due like "%'.$search.'%")';
            
        }
        
        
        if($status > 0) {
            $union = 'SELECT ordr.id '.
                    'FROM dr_orderss ordr inner join dr_orders_shirts jobs ON ordr.id = jobs.order_id  WHERE ordr.company_id ='.$this->user->company_id.' and ordr.is_quote=0'.
                    ' and jobs.department= '.$status.'';
            $where .= ' AND ordr.id IN (SELECT ordr.id '.
                    'FROM dr_orderss ordr inner join dr_orders_jobs jobs ON ordr.id = jobs.order_id  WHERE ordr.company_id ='.$this->user->company_id.' and ordr.is_quote=0'.
                    ' and jobs.department='.$status.'  UNION '.$union.') ';
        } else {
            $union = 'SELECT ordr.id '.
                    'FROM dr_orderss ordr inner join dr_orders_shirts jobs ON ordr.id = jobs.order_id  WHERE ordr.company_id ='.$this->user->company_id.' and ordr.is_quote=0'.
                    ' and jobs.department!= 3';
            
            $where .= ' AND ordr.id IN (SELECT ordr.id '.
                    'FROM dr_orderss ordr inner join dr_orders_jobs jobs ON ordr.id = jobs.order_id  WHERE ordr.company_id ='.$this->user->company_id.' and ordr.is_quote=0'.
                    ' and jobs.department!= 3 UNION '.$union.') ';
        }
        
        $queryCount .= $where;
        
        if($columnOrder == 0) {
            $where .='order by ordr.id DESC';
        }elseif($columnOrder == 3) {
            $where .='order by ordr.order_date '.$orderBy;
        }elseif($columnOrder == 6) {
            $where .='order by ordr.report_due '.$orderBy;
        }
        
        
        if( $sLimit > 0 ){
            $where .= " LIMIT $sLimit ";        
        }
        if( $offset ){
            //$offset = $sLimit * $offset;            
            $where .= " OFFSET $offset ";    
        }
        
        $query .= $where;
        
        $result         = $this->db->query( $query );
        $resultTotal    = $this->db->query( $queryCount );
        $rowTotal       = $resultTotal->row();
        $rowTotal       = $rowTotal->total;
        
        $output = array(
		"sEcho" => intval($_GET['sEcho']),
		"iTotalRecords" => $rowTotal,
		"iTotalDisplayRecords" =>$rowTotal,
		"aaData" => array()
	);
        
        
                
        if($result->num_rows() > 0) {
            foreach ($result->result() as $order) {
                
                
                $order_id = $order->id;                
                $clientName = '';                
                if($order->client_id > 0) {
                    
                        $client = new Person();
                        $client->set_id( $order->client_id );
                        $client_temp = $this->order_model->get_object( $client );
                        $clientName = $client_temp[0]->name;
                }
                $agentName = '';
                if($order->sales_rep > 0) {
            
                    $agent = new Person();
                    $agent->set_id( $order->sales_rep );
                    $agent_temp = $this->order_model->get_object( $agent );
                    $agentName = $agent_temp[0]->name;

                }
                
                
                $getJobDetail = $this->order_model->orderJobDetails($order->id);
                $getShirtDetail = $this->order_model->orderShirtJobDetails($order->id);
                
                if($getJobDetail) {
                    foreach($getJobDetail as $job) {                        
                        if($status > 0 and $job->department_id != $status) {
                            continue;
                        } else if ($job->department_id == 3 and $status != 3) {
                            continue;
                        }
                        $job_detail = '<a href="'.base_url().'orders/show_order/'.$order_id.'">- Job Name: '.$job->job_name.'<br />- Stock: '.$job->stock_name.'<br />- Colors: '.$job->color_name.'<br />- Qty: '.$job->quantity_name.'<br />- Finishing: '.$job->finishing_name.'<br />- Coating: '.$job->coat_name.'<br />- Others: '.$job->others.'</a>' ;
                        $output['aaData'][] = $this->setOrderRow($order_id,$job->job_id,$clientName,$agentName,$job->department_id,$order,$job_detail,'print');
                    } 
                }
                
                if($getShirtDetail) {
                    foreach($getShirtDetail as $job) {      
                        if($status > 0 and $job->department_id != $status) {
                            continue;
                        } else if ($job->department_id == 3 and $status != 3) {
                            continue;
                        }
                        $job_detail = '<a href="'.base_url().'orders/show_order/'.$order_id.'">- Job Name: '.$job->job_name.'<br />- Type: '.$job->type_name.'<br />- Front Color: '.$job->fcolor_name.'<br />- Back Color: '.$job->bcolor_name.'<br />- Others: '.$job->others.'</a>' ;
                        $output['aaData'][] = $this->setOrderRow($order_id,$job->job_id,$clientName,$agentName,$job->department_id,$order,$job_detail,'shirt');
                    } 
                }
            }
        }
        //echo "<pre>";print_r($output);die;
        echo json_encode( $output );
    }
    private function setOrderRow($order_id,$job_id,$clientName,$agentName,$department_id,$order,$job_detail,$type = 'print') {
        $statuslist  =  $this->order_model->getJobStatusWithDefault($this->user->company_id);
	$operatorlist  =  $this->order_model->get_operatorlist($this->user->company_id);
        $row = array();
        $link = base_url().'orders/show_order/'.$order_id;
        $row['DT_RowClass'] = '';
        $row['DT_RowId'] = 'order_'.$order_id.$job_id;
        $row[] = '<input type="checkbox" value="'.$order_id.'" name="ord_'.$order_id.'" data-department="'. $job_id .'" data-type="'.$type.'">';
        // $row[] = '<a href="'.base_url().'orders/show_order/'.$order_id.'"><img title="edit" src="'.base_url().'application/views/assets/img/edit.png" name="edit" id="edit"></a>';
        $row[] = '<a href="'.$link.'">'.$order->company_order_id.'</a>';
        $row[] = date('m-d-y', strtotime($order->order_date));
        //$row[] = $order->order_date;
        $row[] = '<a href="'.base_url().'clients/index/id/'.$order->client_id.'" target="_blank">'.$clientName.'</a>';
        $row[] = $job_detail;
        $row[] = $order->report_due;
        $row[] = '<a href="'.base_url().'inspectors/index/'.$order->sales_rep.'" target="_blank">'.$agentName.'</a>';
        
        $fieldcontrol='<select onchange="ChangeJobStatus('.$job_id .',\''.$type.'\')" id="status_'. $job_id .'">';
        $fieldcontrol.='<option value="">Select</option>';
        foreach( $statuslist as $key => $value){
                if($key == $department_id) {
                        $fieldcontrol.='<option value="'. $key.'" selected="selected">'. $value .'</option>';
                } else {
                        $fieldcontrol.='<option value="'. $key.'">'. $value .'</option>';
                }			        	
        }					   
        $fieldcontrol.='</select>';
        
        
        $row[] = $fieldcontrol;

        $fieldOperator='<select onchange="javascript:ChangeOperator('.$order->id .')" id="operator_'. $order_id .'">';
        $fieldOperator.='<option value="">-Select-</option>';
        $fieldOperator.='<option value="_8">-Add/Edit-</option>';		


        foreach( $operatorlist as $rows) {
                if($rows->id == $order->operator_id) {
                        $fieldOperator.='<option value="'. $rows->id.'" selected="selected">'. $rows->name .'</option>';
                } else {
                        $fieldOperator.='<option value="'. $rows->id.'">'. $rows->name .'</option>';
                }			        	
        }					   
        $fieldOperator.='</select>';

        $row[] = $fieldOperator;
        return $row;
    }

    public function show_order(){
        $order_id = $this->uri->segment(3);
        $filter = new Filter();
        $filter->set_order_id( $order_id );
        $filter->set_company_id( $this->user->company_id);

        $temp = $this->order_model->get_orders( $filter );
        //echo "<pre>";print_r($temp);die;
        $old_order = reset($temp);
        
        $this->display_order( $old_order );
                
    }
    function duplicate_order() {
        
        $order_id = $this->uri->segment(3);

        if($order_id > 0) {

            $filter = new Filter();
            
            $filter->set_order_id( $order_id );

            $filter->set_company_id( $this->user->company_id);

            $old_order = reset($this->order_model->get_orders( $filter ));
            
            if($old_order->id == $order_id) {
                $jobOrder_id = $order_id;
                $order = new Order();
                $order = $old_order;
                $order->set_id(null);
                $order->set_company_order_id($this->getCompanyNextOrderId());
                $order_id = $this->order_model->save_order( $order );
                 $jobs = new Ordersjob(null,$jobOrder_id);                 
                 $jobsResult = $this->order_model->get_object( $jobs );
                 if($jobsResult) {
                     foreach ($jobsResult as $job) {
                            // dulicating order jobs detail to new order
                            $orderJob = new Ordersjob(null,$order_id);                            
                            $orderJob->set_job_name($job->job_name);
                            $orderJob->set_coating($job->coating);
                            $orderJob->set_stock($job->stock);
                            $orderJob->set_colors($job->colors);
                            $orderJob->set_size($job->size);
                            $orderJob->set_quantity($job->quantity);
                            $orderJob->set_others($job->others);
                            $orderJob->set_finishing($job->finishing);
                            $orderJob->set_additional_notes($job->additional_notes);
                            $orderJob->set_created_at(date('Y-m-d'));
                            $orderJob->set_department(1);
                            $this->order_model->save_or_update_object( $orderJob );                            
                     }
                 }
                 redirect('orders/show_order/'.$order_id);
            }
         }
         
    }
    public function delete(){
         
        $order_id = $this->uri->segment(3);
        $order = new Order();
        $order->set_id( $order_id );
        
        $this->order_model->delete_order( $order );
        
        $this->new_order();
		
	        
    }
	
	public function deleteorder(){
		
		$return = array();
        if( $ids = $this->input->post('orderids')){
            $ids = explode('&', $ids );
            foreach( $ids as $id ){
                if(!$id) continue;
				$this->order_model->delete_order_all($id);
            }
            $return['status'] = 'success';
        }
        echo json_encode($return);	
		
		
    }
	
	public function getcurrent_jobstatus(){
		
		$return = array();   
                if($item = $this->input->post('orderid')){
        	$return= $this->order_model->get_jobstatusbyid($item);
			
			
		}
        echo json_encode($return);
		
    }
	
	public function getcurrent_operator(){
		
		$return = array();   
        if($item = $this->input->post('orderid')){
        	$return= $this->order_model->get_currentoperatorbyid($item);
			
			
		}
        echo json_encode($return);
		
    }
	
	
	public function updateorder_status(){
		$return = array();
                $order_id     = $this->input->post('orderid');
		$department_id = $this->input->post('jobstatusid');
                $type = $this->input->post('type');
                
                $items = $this->input->post('orderids');  
                $shirtitems = $this->input->post('shirtids'); 
                
                if($order_id !='' and $type == 'print'){
                    $this->order_model->update_order_jobstatus($order_id,$department_id);	
		}else if($order_id !='' and $type == 'shirt'){
                    $this->order_model->update_order_shirtstatus($order_id,$department_id);	
		} elseif($items !='') {
                    $items = explode('&', $items );
                    foreach( $items as $id ){
                        if(!$id) continue;
                        
                        $this->order_model->update_order_jobstatus($id,$department_id);
                    }
                } 
                
                if($shirtitems !='') {
                    $shirtitems = explode('&', $shirtitems );
                    foreach( $shirtitems as $id ){
                        if(!$id) continue;                        
                        $this->order_model->update_order_shirtstatus($id,$department_id);
                    }
                }
                
                $return['status'] = 'success';
                $return['department_id'] = $department_id;

                echo json_encode($return);		
    }
	
	public function updateorder_operator(){
		
		$return = array();
                $order_id = $this->input->post('orderid');
		$operator_id = $this->input->post('operatorid');
                
                
                $items = $this->input->post('orderids');  
                if($order_id !=''){
                    $this->order_model->update_order_operator($order_id,$operator_id);	
		}elseif($items !='') {
                    $items = explode('&', $items );
                    foreach( $items as $id ){
                        if(!$id) continue;
                        
                        $this->order_model->update_order_operator($id,$operator_id);
                    }
                }
                
        $return['status'] = 'success';
        echo json_encode($return);	
		
		
    }
	
    private function display_order( Order $order, $saved = null ){
        
        /**
        * Lets fetch additional view data from DB, this is mainly Person names with ids for 
        * drop down menus and various other types for drop downs
        */
        $clients =  array( self::NEW_CLIENT => self::ADD_NEW  ) + $this->order_model->get_person_names( Person::TYPE_CLIENT, $this->user->company_id ) + array('' => '-Select Agent-');
        $agents  =  array( self::NEW_AGENT => self::ADD_NEW ) + $this->order_model->get_person_names( Person::TYPE_AGENT, $this->user->company_id ) + array('' => '-Select Company-');; 
        $inspectors  =  array(self::NEW_INSPECTOR => self::ADD_NEW ) + $this->order_model->get_person_names( Person::TYPE_INSPECTOR, $this->user->company_id )+ array('' => '-Select Inspector-');
        
        $terms   =  array(self::NEW_TERMS => self::ADD_NEW ) + $this->order_model->get_terms($this->user->company_id)+ array('' => '-Select-');
        $items   =  array(self::NEW_ITEM  => self::ADD_NEW ) + $this->order_model->get_items( $this->user->company_id ) + array('' => '-Select Charge-');;
        $taxes   =  array();
        $estimated_ages = array( $this->order_model->get_estimated_ages()) + array('' => '-Select Age-');;
        
        $status  =  array('' => ' -Select- ') + $this->order_model->getJobStatusWithDefault($this->user->company_id);

        $salesrep = array(self::NEW_INSPECTOR => self::ADD_NEW ) + $this->order_model->get_person_names( Person::TYPE_INSPECTOR, $this->user->company_id ) + array('' => '-Select-');
        

        $shipping_method  =  array(self::NEW_SHIPPING => self::ADD_NEW ) + $this->order_model->get_shippingmethod($this->user->company_id) + array('' => '-Select-');
        $stocks  =  array('' => ' -Select- ') + array(self::NEW_STOCK => self::ADD_NEW ) + $this->order_model->get_stock($this->user->company_id);
        $colors  =  array('' => ' -Select- ') + array(self::NEW_COLORS => self::ADD_NEW ) + $this->order_model->get_colors($this->user->company_id);
        $sizes  =  array('' => ' -Select- ') + array(self::NEW_SIZE => self::ADD_NEW ) + $this->order_model->get_size($this->user->company_id);
        
        $quantities  =  array('' => ' -Select- ') + array(self::NEW_QUANTITY => self::ADD_NEW ) + $this->order_model->get_quantity($this->user->company_id);
        $finishings  =  array('' => ' -Select- ') + array(self::NEW_FINISHING => self::ADD_NEW ) + $this->order_model->get_finishing($this->user->company_id);
        $coating     =  array('' => ' -Select- ') + array(self::NEW_COATING => self::ADD_NEW ) + $this->order_model->get_coating($this->user->company_id);
        
        
        $shirt_type     =  array('' => ' -Select- ') + array(self::NEW_SHIRTTYPE => self::ADD_NEW ) + $this->order_model->get_shirt_type($this->user->company_id);
        $shirt_front_color     =  array('' => ' -Select- ') + array(self::NEW_SHIRTFRONTCOLOR => self::ADD_NEW ) + $this->order_model->get_shirt_front_color($this->user->company_id);
        $shirt_back_color     =  array('' => ' -Select- ') + array(self::NEW_SHIRTBACKCOLOR => self::ADD_NEW ) + $this->order_model->get_shirt_back_color($this->user->company_id);
        $shirt_size     =  array('' => ' -Select- ') + array(self::NEW_SHIRTSIZE => self::ADD_NEW ) + $this->order_model->get_shirt_size($this->user->company_id);
        
        $companylist =  array( self::NEW_AGENT => self::ADD_NEW ) + $this->order_model->get_person_names( Person::TYPE_AGENT, $this->user->company_id ) + array('' => '-Select-');
        $times   =  $this->order_model->get_times();
         
        $utility_types    =  array(self::NEW_UTILITY     => self::ADD_NEW )+ $this->order_model->get_types( Type::TYPE_UTILITY,     $this->user->company_id ) + array('' => '-Select Utility-');;
        $structure_types  =  array(self::NEW_STRUCTURE   => self::ADD_NEW ) + $this->order_model->get_types( Type::TYPE_STRUCTURE,   $this->user->company_id)+ array('' => '-Select Structure-');;
        $foundation_types =  array(self::NEW_FOUNDATION  => self::ADD_NEW ) + $this->order_model->get_types( Type::TYPE_FOUNDATION,  $this->user->company_id)+ array('' => '-Select Fooundation-');;
        $inspection_types =  array(self::NEW_INSPECTION  => self::ADD_NEW ) + $this->order_model->get_types( Type::TYPE_INSPECTION,  $this->user->company_id)+ array('' => '-Select Inspection-');;
        
        $us_states =  $this->order_model->get_us_states() + array( ''=>'-Select state-');
        $inspection_times =  $this->order_model->get_duration_times();
        
        $company = new Company();
        $company->set_id($this->user->company_id);
        $temp = $this->order_model->get_object($company);
        $company = reset($temp);
	    $headermenuitems = $this->order_model->getJobStatusWithDefault($this->user->company_id);


        //Load Defaualt page header        
        $this->load->view('include/sidebar', array('fluid_layout'=>false, 'user'=>$this->user, 'companies'=>$this->companies, 'new_order'=>true,'headermenuitems'=>$headermenuitems));
 
        //Load New order view and pass required data
        $this->load->view('orders/new_order', 
                            array( 'order'   => $order,
                                   'times'   => $times,
                                   'terms'   => $terms,
                                   'status'  => $status,
				   'salesrep' => $salesrep,
                                   'clients' => $clients,
                                   'agents'  => $agents,
                                   'inspectors' => $inspectors,
                                   'items'   => $items,
                                   'taxes'   => $taxes,
				    'companylist' => $companylist,
                                   'us_states'   => $us_states,
                                   'inspection_times'   => $inspection_times,
                                   'utility_types'      => $utility_types,
                                   'structure_types'    => $structure_types,
                                   'estimated_ages'     => $estimated_ages,
                                   'inspection_types'   => $inspection_types,
                                   'foundation_types'   => $foundation_types,
                                   'payment_gateway'    => $company->payment_gateway,
                                    'shipping_method'	=> $shipping_method,	
                                    'stocks'	=> $stocks,
                                    'colors' => $colors,
                                    'sizes' =>$sizes,
                                    'quantities'	=> $quantities,
                                    'finishings' => $finishings,
                                    'coating' => $coating,
                                    'shirt_type' => $shirt_type,
                                    'shirt_front_color' => $shirt_front_color,
                                    'shirt_back_color' => $shirt_back_color,
                                    'shirt_size' => $shirt_size,
                                   'saved'   => isset( $saved ) ? $saved : null,
                                    'errors'   => $this->errors,
                                 ));
        //Load Footer
        $this->load->view('include/footer');        
    }
    /**
    * Ajax entry point. All ajax call are handled through this
    * Type parameter determines call 
    */
    public function ajax_control(){
        
        switch( $this->input->post('type')){
            case self::AJAX_ITEM:
                echo $this->ajax_get_item();
            break;
		case self::AJAX_ITEMDROPDOWN:
                echo $this->ajax_get_itemdropdown();
            break;
            case self::AJAX_PERSON:
                echo $this->ajax_get_person();
            break;
            case self::AJAX_TAX:
                echo $this->ajax_get_tax();                               
            break;
            case self::AJAX_INSPECTOR_CHECK:
                echo $this->ajax_check_inspector();
            break;
            case self::AJAX_REFRESH:
                echo $this->ajax_refresh();
            break;
            case self::AJAX_TYPE_EDIT:
                echo $this->ajax_get_type();
            break;     
            case self::AJAX_GET_SALESREP_ITEM:
                echo $this->ajax_get_salesrep_byid();
            break;
            case self::AJAX_GET_COATING_ITEM:
                echo $this->ajax_get_coating_byid();
            break;
            case self::AJAX_GET_JOBSTATUS_ITEM:
                echo $this->ajax_get_jobstatus_byid();
            break;
            case self::AJAX_GET_STOCK_ITEM:
                echo $this->ajax_get_productiondetail_byid();
            break;
            case self::AJAX_GET_COLORS_ITEM:
                echo $this->ajax_get_productiondetail_byid();
            break;
            case self::AJAX_GET_SIZE_ITEM:
                echo $this->ajax_get_productiondetail_byid();
            break;
            case self::AJAX_GET_QUANTITY_ITEM:
                echo $this->ajax_get_productiondetail_byid();
            break;
            case self::AJAX_GET_FINISHING_ITEM:
                echo $this->ajax_get_productiondetail_byid();
            break;
            case self::AJAX_GET_COATINGS_ITEM:
                echo $this->ajax_get_productiondetail_byid();
            break;
            case self::AJAX_GET_SHIRTTYPE_ITEM:
                echo $this->ajax_get_productiondetail_byid();
            break;
            case self::AJAX_GET_SHIRTFRONTCOLOR_ITEM:
                echo $this->ajax_get_productiondetail_byid();
            break;
            case self::AJAX_GET_SHIRTBACKCOLOR_ITEM:
                echo $this->ajax_get_productiondetail_byid();
            break;
            case self::AJAX_GET_SHIRTSIZE_ITEM:
                echo $this->ajax_get_productiondetail_byid();
            break;
            case self::AJAX_GET_SHIPPING_ITEM:
                echo $this->ajax_get_shippingmethod_byid();
            break;  
			case self::AJAX_OPERATOR_ITEM:
                echo $this->ajax_get_operator_byid();
            break;              
			case self::AJAX_TERMS_ITEM:
                echo $this->ajax_get_term_byid();
            break;
			case self::AJAX_COMPANYLIST_ITEM:
                echo $this->ajax_get_company_byid();
            break;                      
        }
    }
	
	
	 public function ajax_get_itemdropdown(){  

        for( $i=0; $i<15; $i++){

            if( $this->input->post('name') == 'item_item_id_'.$i ){

                $this->load->model('Order_model','order_model', TRUE );                     

                if( $item = $this->order_model->get_filtereditems( $this->input->post('value'), $this->user->company_id )){

                

                    $return = json_encode(

                        array(

                            'description' => $item->description, 

                            'price' => $item->price,

                            'row'   => $i,

                            )

                        );

                    break;

                }

            }            

        }

return $return;

} 
	
	
	/**
    * Ajax - Get Coating item 
    */
    public function ajax_get_coating_byid(){  
        //for( $i=0; $i<15; $i++){            
			$this->load->model('Order_model','order_model', TRUE );                     
			if( $item = $this->order_model->get_coating_byid( $this->input->post('id') )){
						
					$return = json_encode(
					array(
						'name' => $item->name, 
						'active' => $item->active,
						'id'   => $item->id,
						'company_id' => $this->user->company_id
						)
					);
					
					
			
			}
      
        return $return;
    }
	
	
	public function ajax_get_company_byid(){
        $return = array();
        if( ($person_id = $this->input->post('id'))){ 
            $person = new Person();
            $person->set_id( $person_id );  
			                              
            if( $person = reset($this->order_model->get_persons( $person  ))){
                
                //$prefix = $person->person_type == Person::TYPE_AGENT ? 'agent_' : 'client_';
                foreach( $person as $field=>$value){
                    $array[ $field ] = $value;
                }
                $return =  $array ;
            }
        }            
        return json_encode($return);
    } 
	
	
	/**
    * Ajax - Get term item 
    */
    public function ajax_get_term_byid(){  

			$this->load->model('Order_model','order_model', TRUE );                     
			if( $item = $this->order_model->get_term_byid( $this->input->post('id') )){
						
					$return = json_encode(
					array(
						'name' => $item->name,						 
						'active' => $item->active,
						'id'   => $item->id
						)
					);
					
			}			
      
        return $return;
    }
	
	
	/**
    * Ajax - Get Shipping item 
    */
    public function ajax_get_shippingmethod_byid(){  
        //for( $i=0; $i<15; $i++){            
			$this->load->model('Order_model','order_model', TRUE );                     
			if( $item = $this->order_model->get_shippingmethod_byid( $this->input->post('id') )){
						
					$return = json_encode(
					array(
						'name' => $item->name,
						'charges' => $item->charges,						 
						'active' => $item->active,
						'id'   => $item->id
						)
					);
					
			}
			else {
				$return = json_encode(
					array(
						'name' => $this->input->post('id'),
						//'charges' => $item->charges,						 
						'active' => '0',
						'id'   => '3'
						)
					);
			}
      
        return $return;
    }
	
	
	public function ajax_get_operator_byid(){  
        //for( $i=0; $i<15; $i++){            
			$this->load->model('Order_model','order_model', TRUE );                     
			if( $item = $this->order_model->get_operator_byid( $this->input->post('id') )){
						
					$return = json_encode(
					array(
						'name' => $item->name, 
						'active' => $item->active,
						'id'   => $item->id,
						)
					);
					
					
			
			}
      
        return $return;
    }
	
	/**
    * Ajax - Get job Status item 
    */
    public function ajax_get_jobstatus_byid() {           
			                     
			if( $item = $this->order_model->get_jobstatus_byid( $this->input->post('id') )){
						
					$return = json_encode(
					array(
						'name' => $item->name, 
						'active' => $item->active,
						'id'   => $item->id,
						)
					);
					
					
			
			}
      
        return $return;
    }
    
    public function ajax_get_productiondetail_byid(){  
               
			     
			if( $item = $this->order_model->get_productiondetail_byid( $this->input->post('id'),$this->input->post('item_type') )){
						
					$return = json_encode(
					array(
						'name' => $item->name, 
						'active' => $item->active,
						'id'   => $item->id,
						)
					);
					
					
			
			}
      
        return $return;
    }
	
	/**
    * Ajax - Get Sales Rep item 
    */
    public function ajax_get_salesrep_byid(){  
        //for( $i=0; $i<15; $i++){            
			$this->load->model('Order_model','order_model', TRUE );                     
			if( $item = $this->order_model->get_salesrep_byid( $this->input->post('id') )){
						
					$return = json_encode(
					array(
						'name' => $item->name, 
						'active' => $item->active,
						'id'   => $item->id,
						)
					);
					
					
			
			}
      
        return $return;
    }
	
    /**
    * Ajax - Get item 
    */
    
     public function ajax_get_itembyid(){  
        for( $i=0; $i<15; $i++){
            if( $this->input->post('name') == 'item_item_id_'.$i ){
            	
                $this->load->model('Order_model','order_model', TRUE );                     
                if( $item = $this->order_model->get_item( $this->input->post('value'), $this->user->company_id )){
                    $return = json_encode(
                        array(
                            'description' => $item->description, 
                            'price' => $item->price,
                            'row'   => $i,
                            )
                        );
                    break;
                }
            }            
        }
	 }
    public function ajax_get_item(){  
        /*for( $i=0; $i<15; $i++){
        	log_message('debug','Item ID :'. $i . ' : '.$this->input->post('value'));
            if( $this->input->post('name') == 'item_item_id_'.$i ){
            	log_message('debug','Item control :'. $this->input->post('name'));
                $this->load->model('Order_model','order_model', TRUE );                     
                if( $item = $this->order_model->get_item( $this->input->post('value'), $this->user->company_id )){
                    $return = json_encode(
                        array(
                            'description' => $item->description, 
                            'price' => $item->price,
                            'row'   => $i,
                            )
                        );
                    break;
                }
            }            
        }*/
        
        $this->load->model('Order_model','order_model', TRUE );                     
		if( $item = $this->order_model->get_item( $this->input->post('id') )){
					
				$return = json_encode(
				array(
					'name' => $item->name, 
					'description' => $item->description, 
                    'price' => $item->price,
                    'active' => $item->active,
                    'id'   => $item->id,
                    'company_id'   => $item->company_id,
					)
				);
				
		}        
        return $return;
    }

    /**
    * Ajax - Get person 
    */
    public function ajax_get_person(){
        $return = array();
        if( ($person_id = $this->input->post('value')) && $this->input->post('name') != 'inspector_id' ){ 
            $person = new Person();
            $person->set_id( $person_id )
                    ->set_company_id( $this->user->company_id);                                
            if( $person = reset($this->order_model->get_persons( $person  ))){
                
                $prefix = $person->person_type == Person::TYPE_AGENT ? 'agent_' : 'client_';
                foreach( $person as $field=>$value){
                    $array[ $prefix . $field ] = $value;
                }
                $return =  $array ;
            }
        }            
        return json_encode($return);
    } 
    public function ajax_get_tax(){
        $return = array();   
        if( ($tax_id = $this->input->post('id'))){
            
            $tax_filter = new Tax();
            $tax_filter->set_id( $tax_id );
            $tax_filter->set_company_id( $this->user->company_id );
            
            $taxes = $this->order_model->get_object( $tax_filter );
            if( $taxes ){
                $return['value'] = $taxes['0']->value;
            } 
        }
        return json_encode( $return );
    }
    public function ajax_check_inspector(){  
        $return = array();
        $overlap = false;  
        if( ($inspector_id = $this->input->post('inspector_id')) &&
            ($inspection_time = $this->input->post('inspection_time')) &&
            ($inspection_date = $this->input->post('inspection_date')) &&
            ($inspection_duration = $this->input->post('inspection_duration'))&&
            ($id = $this->input->post('order_id')) 
            )
          {
            $new_order = new Order();
            $new_order->set_inspection_time( $inspection_time );
            $new_order->set_estimated_inspection_time( $inspection_duration );
            $new_order->set_id( $id );
            $new_end_time = $this->get_end_time( $new_order );
            


            $available = true;
            $person = new Person();
            $person->set_id( $inspector_id );
            
            $persons = $this->order_model->get_object($person);
            
            $available = true;
            
            if( $persons ){
                
                $inspector = reset( $persons );
                if( $inspector->timeoff_date_start && 
                    $inspector->timeoff_date_end && 
                    $inspector->timeoff_time_end && 
                    $inspector->timeoff_time_start ) {
                        
                    $start = date_parse($inspector->timeoff_date_start.''.$inspector->timeoff_time_start);
                    $end   = date_parse($inspector->timeoff_date_end.''.$inspector->timeoff_time_end);
                    $start_time = mktime($start['hour'],$start['minute'],0,$start['month'],$start['day'],$start['year']);
                    $end_time   = mktime($end['hour'],$end['minute'],0,$end['month'],$end['day'],$end['year']);
           
                    $test       = date_parse($inspection_date .''.$inspection_time );
                    $test_time  = mktime($test['hour'],$test['minute'],0,$test['month'],$test['day'],$test['year']);
                    
                    $test_end       = date_parse($inspection_date .''.$new_end_time );
                    /**
                    * This date is in US format, we'll switch day && month
                    */
                    $test_time_end  = mktime($test_end['hour'],$test_end['minute'],0, $test_end['month'],$test_end['day'],$test_end['year']); 
                    
                    if( ($start_time<=$test_time && $test_time<=$end_time) || 
                        ( $start_time<=$test_time_end && $test_time_end<=$end_time ) ||
                        ( $test_time<= $start_time && $end_time <= $test_time_end  )  || 
                        ( $start_time <= $test_time && $test_time_end <= $end_time )
                        ){
                            $available = false;
                            $return['not_available'] = true;
                            $return['inspector_name'] = $inspector->name;                         
                         
                    } 
                                   
                }   
            }            

            if( $available ){
                $filter = new Filter();
                $filter->set_inspector_id( $inspector_id );
                $filter->set_start_date( $inspection_date );
                $filter->set_end_date( $inspection_date );
                $filter->set_company_id( $this->user->company_id );
                
                $orders = $this->order_model->get_orders( $filter );
                
                $overlap_order = null;
                foreach( $orders as $old_order ){
                    $old_end_time = $this->get_end_time( $old_order );
                    
                    if( $new_order->id != $old_order->id && (($new_order->inspection_time <= $old_order->inspection_time && $new_end_time >= $old_end_time ) ||
                        ($new_order->inspection_time >= $old_order->inspection_time && $new_order->inspection_time <= $old_end_time ) ||
                        ($new_end_time >= $old_order->inspection_time && $new_end_time <= $old_end_time ))){
                            
                        
                            $overlap_order = $old_order;
                            break;                        
                                                    
  
                    }
                       
                }
                if( $overlap_order !== null ){
                    $return['not_available'] = true;
                    $return['overlap_order']['inspection_time'] = $old_order->inspection_time;
                    $return['overlap_order']['estimated_inspection_time'] = $old_order->estimated_inspection_time;
                    $return['inspector_name'] = $old_order->inspector->name;
                }                
            }

        }
        return json_encode( $return );
    }
    function inspector_available( Order $order ){
         
        $new_order = new Order();
        $new_order->set_inspection_time( $order->inspection_time );
        $new_order->set_estimated_inspection_time( $order->estimated_inspection_time );
        $new_order->id = $order->id;
        $new_end_time = $this->get_end_time( $new_order );
        
        $person = new Person();
        $person->set_id( $order->inspector_id );
        
        $persons = $this->order_model->get_object($person);
        
        $available = true;
        if( $persons ){
            
            $inspector = reset( $persons );
            if( $inspector->timeoff_date_start && 
                $inspector->timeoff_date_end && 
                $inspector->timeoff_date_end && 
                $inspector->timeoff_time_start ) {
                    
                $start = date_parse($inspector->timeoff_date_start.''.$inspector->timeoff_time_start);
                $end   = date_parse($inspector->timeoff_date_end.''.$inspector->timeoff_time_end);
                $start_time = @mktime($start['hour'],$start['minute'],0,$start['month'],$start['day'],$start['year']);
                $end_time   = @mktime($end['hour'],$end['minute'],0,$end['month'],$end['day'],$end['year']);
       
                $test       = date_parse($order->inspection_date .''.$order->inspection_time );
                $test_time  = @mktime($test['hour'],$test['minute'],0,$test['month'],$test['day'],$test['year']);
                
                $test_end       = date_parse($order->inspection_date .''.$new_end_time );
                $test_time_end  = @mktime($test_end['hour'],$test_end['minute'],0,$test_end['month'],$test_end['day'],$test_end['year']); 
                
                if( ( $start_time<=$test_time && $test_time<=$end_time ) ||
                    ( $start_time<=$test_time_end && $test_time_end <= $end_time ) ||
                    ( $start_time<=$test_time_end && $test_time_end<=$end_time ) ||
                    ( $test_time<= $start_time && $end_time <= $test_time_end  ) ){
                    $available = false;
                    
                } 
                               
            }   
        }
        
        if( $available ){
            $filter = new Filter();
            $filter->set_inspector_id( $order->inspector_id );
            $filter->set_start_date( $order->inspection_date);
            $filter->set_end_date( $order->inspection_date );
            $filter->set_company_id( $this->user->company_id );
            
            $orders = $this->order_model->get_orders( $filter );
            
            $overlap_order = null;
            foreach( $orders as $old_order ){
                $old_end_time = $this->get_end_time( $old_order );
                
                if( ($new_order->inspection_time <= $old_order->inspection_time && $new_end_time >= $old_end_time ) ||
                    ($new_order->inspection_time >= $old_order->inspection_time && $new_order->inspection_time <= $old_end_time ) ||
                    ($new_end_time >= $old_order->inspection_time && $new_end_time <= $old_end_time )){
                        
                        if( $new_order->id != $old_order->id ){
                            $overlap_order = $old_order;
                            break;                        
                        }

                }   
            }
            if( $overlap_order !== null ){
                $available = false;
            }            
        }
        
        return $available;
    }
    function ajax_refresh(){   
        $return = array();
        $filter = null;
		//log_message('debug','Test id '.$this->input->post('id'));
		//$type = $this->input->post('id');
		
		// log_message('debug','Test id 1 '.$type);
		//if (preg_match('/^operator/i', $type)) {
		//if($type=='operator_2050') {

          //     $type = "operator";
			
    //    }
		
        switch(($type = $this->input->post('id'))){
            case 'sales_rep':
                //$filter = new SalesRep();
                //$filter->set_active(1);
                $filter = new Person();
                $filter->set_active(1);
                $filter->set_person_type( Person::TYPE_INSPECTOR );       
            break;
			case 'operetor':
                $filter = new OperatorList();
                $filter->set_active(1);
            break;
            case 'status':
                $filter = new JobStatus();
                $filter->set_active(1);
            break;
            case 'stock':
                $filter = new Stock();
                $filter->set_active(1);
            break;
            case 'colors':
                $filter = new Colors();
                $filter->set_active(1);
            break;
            case 'size':
                $filter = new Size();
                $filter->set_active(1);
            break;
            case 'quantity':
                $filter = new Quantity();
                $filter->set_active(1);
            break;
            case 'finishing':
                $filter = new Finishing();
                $filter->set_active(1);
            break;
            case 'coating':
                $filter = new Coatings();
                $filter->set_active(1);
            break;
            case 'shirt_type':
                $filter = new ShirtType();
                $filter->set_active(1);
            break;
            case 'shirt_front_color':
                $filter = new ShirtFrontColor();
                $filter->set_active(1);
            break;
            case 'shirt_back_color':
                $filter = new ShirtBackColor();
                $filter->set_active(1);
            break;
            case 'shirt_size':
                $filter = new ShirtSize();
                $filter->set_active(1);
            break;
            case 'shipping_method':
                $filter = new ShippingMethod();
                $filter->set_active(1);
            break;  
            case 'Coating':
                $filter = new Coating();
                $filter->set_active(1);
            break;   
            case 'inspector_id': 
                $filter = new Person();
                $filter->set_active(1);
                $filter->set_person_type( Person::TYPE_INSPECTOR );            
            break;
            case 'type_of_structure':
                $filter = new Type();
                $filter->set_active(1); 
                $filter->set_type( Type::TYPE_STRUCTURE);
                break; 
            case 'type_of_foundation':
                $filter = new Type();
                $filter->set_active(1);
                $filter->set_type( Type::TYPE_FOUNDATION);
                break; 
            case 'type_of_inspection':
                $filter = new Type();
                $filter->set_active(1); 
                $filter->set_type( Type::TYPE_INSPECTION);
                break; 
		case 'company':
                $filter = new Person();
                $filter->set_active(1); 
                $filter->set_person_type( Person::TYPE_AGENT );     
                break; 
            case 'type_of_utilities':
                $filter = new Type();
                $filter->set_active(1); 
                $filter->set_type( Type::TYPE_UTILITY);
                break;  
            case 'terms':
                $filter = new Terms();
                $filter->set_active(1);
                break; 
            break;
            case 'item':
                $filter = new Base_item();
		$filter->set_active(1);
            break;
			default :
				//$filter = new SalesRep();
                //$filter->set_active(1);
                
        }
		
		
        if( $filter ){	
            $filter->set_company_id( $this->user->company_id );
            $objects =  $this->order_model->get_object( $filter );
            foreach( $objects as $object ){
                $return[ $object->id ] = $object->name;
            }
        }        
        return json_encode( $return );
    }
    
    function ajax_get_type(){     
        $return = array();
        $filter = null;
        if( ($type = $this->input->post('item_type'))){
            $filter = new $type();
            $filter->set_id( $this->input->post('id'));
            $filter->set_company_id( $this->user->company_id );
        }
        
        if( $filter ){
            $filter->set_company_id( $this->user->company_id );
            $objects =  $this->order_model->get_object( $filter );
            if( $objects ){
                $object = reset($objects );
                foreach( $object as $field=>$value ){
                    $return[ $field ] = $value;
                }                
            }
        }        
        return json_encode( $return );
    }
    public function charge(){  
        $result = array();
        $order = new Order();  
        $saved = Authorize_payment::STATUS_FAILED;
        if( ($cc_num = $this->input->post('cc_num')) &&
            ($cc_date = $this->input->post('cc_date')) &&
            ($amount = $this->input->post('amount')) &&
            ($order_id = $this->input->post('order_id'))
        ){
            $order = new Filter();
            $order->set_order_id( $order_id );
            $order->set_company_id($this->user->company_id);
             
            $company = new Company();
            $company->set_id($this->user->company_id);
            $company = reset($this->order_model->get_object($company));
            
            if( ($order = reset($this->order_model->get_orders( $order ))) && $company->payment_gateway ){
                
                $result = $this->authorize_payment->send_payment_request( $amount, $cc_num, $cc_date, $order_id, $company->merchant, $company->transaction, 'https://api.authorize.net/xml/v1/request.api' );
                if( $result['status'] == Authorize_payment::STATUS_SUCCESS ){
                    $order->set_paid( $order->paid + $amount );
                    $order->set_amount_due( $order->amount_due - $amount );
                    $this->order_model->save_order($order);
                    $saved = Authorize_payment::STATUS_SUCCESS;
                }
                if( $result['message']) {
                    $order->errors[] = $result['message'];
                }
                  
                $transaction = new Transaction();
                $transaction->set_order_id( $order_id )
                ->set_request( $this->authorize_payment->request )
                ->set_response( $this->authorize_payment->response )
                ->set_datetime( gmdate('Y-m-d h:i:s'))
                ->set_status( $result['status'])
                ->set_amount( $amount )
                ->set_message( $result['message'] );
                
                $this->order_model->save_or_update_object( $transaction );
                 
            } 
        }
        
        $this->display_order( $order, $saved );
    }
    function get_end_time( Order $order ){
        $hour   = intval(substr( $order->inspection_time, 0, 2));
        $minute = intval(substr( $order->inspection_time, 3, 2));
        return ( @date( 'H:i', (mktime($hour, $minute)+ $order->estimated_inspection_time * 60*60) ));    
    }
    function submit_job($order_id) {
        
        $order_type = $this->input->get('order_type');
        
        if($order_type == '1' and $this->input->post('job_name') != '') {
            $order = new Filter();            
            $order->set_order_id( $order_id );
            $order->set_company_id($this->user->company_id);
            $order = $this->order_model->get_orders($order,true);        
            $session_id = $this->session->userdata('user_order');
            if($order or $session_id == $order_id) {
                
                $job_id = $this->input->post('job_id');
                $job_id = ($job_id > 0) ? $job_id :  null;
                $job_name = $this->input->post('job_name');
                
                $orderJob = new Ordersjob($job_id,$order_id);
                $orderJob->set_job_name($job_name);
                $orderJob->set_coating($this->input->post('coating'));
                $orderJob->set_stock($this->input->post('stock'));
                $orderJob->set_colors($this->input->post('colors'));
                $orderJob->set_size($this->input->post('size'));
                $orderJob->set_quantity($this->input->post('quantity'));
                $orderJob->set_department($this->input->post('department'));
                $orderJob->set_others($this->input->post('others'));
                $orderJob->set_finishing($this->input->post('finishing'));
                $orderJob->set_additional_notes($this->input->post('additional_notes'));
                $job_id = $this->order_model->save_or_update_object( $orderJob );
                $jobDetail = $this->order_model->jobDetailById($job_id);
                
                ?>
                        <div class="panel panel-default" id="job_detail_<?php echo $job_id;?>">
                            <div class="panel-heading" style="padding:10px;">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#accordion_<?php echo $job_id?>">
                                        <?php echo $jobDetail->job_name?>
                                    </a>
                                </h4>
                            </div>
                            <div id="accordion_<?php echo $jobDetail->job_id?>" class="panel-collapse collapse in">
                                <div class="panel-body">
                                <b>Stock:</b> <?php echo $job_name;?><br />
                                <b>Color:</b> <?php echo $jobDetail->color_name?><br />
                                <b>Size:</b> <?php echo $jobDetail->size_name?><br />
                                <b>Quantity:</b> <?php echo $jobDetail->quantity?><br />
                                <b>Finishing:</b> <?php echo $jobDetail->finishing_name?><br />
                                <b>Coating:</b> <?php echo $jobDetail->coat_name?><br />
                                <b>Department:</b> <?php echo $jobDetail->department_name?><br />
                                <b>Others:</b> <?php echo $jobDetail->others?><br />
                                <b>Additional notes:</b> <?php echo $jobDetail->additional_notes?><br />
                                <a href="javascript:void(0)" onclick="editProductionDetail(<?php echo $jobDetail->job_id?>)">Edit</a>
                                | <a href="javascript:void(0)" onclick="deleteProductionDetail(<?php echo $jobDetail->job_id?>)">Delete</a>
                                </div>
                            </div>
                        </div>
                <?php
            }
            
        }elseif($order_type == '2') {
            $shirt = $this->input->post('shirt');
            if ($shirt['job_name'] !='') {
                
                $job_id = $shirt['job_id'];
                $job_id = ($job_id > 0) ? $job_id :  null;
                $job_name = $shirt['job_name'];
                
                $orderJob = new Ordersshirt($job_id,$order_id);
                $orderJob->set_job_name($job_name);
                $orderJob->set_type($shirt['type']);
                $orderJob->set_front_color($shirt['front_color']);
                $orderJob->set_back_color($shirt['back_color']);
                $orderJob->set_additional_notes($this->input->post('additional_notes'));
                $orderJob->set_department($this->input->post('department'));
                $orderJob->set_others($this->input->post('others'));
                
                $size       = $shirt['size'];
                $quantity   = $shirt['quantity'];
                $quantity_size = array();
                foreach($size as $key => $value) {
                    if($value !='') {
                        $quantity_size[] = array('size'=> $value,'quantity' => $quantity[$key]);
                    }
                }
                $orderJob->set_quantity_size(serialize($quantity_size));                 
                $job_id = $this->order_model->save_or_update_object( $orderJob );
                $jobDetail = $this->order_model->jobShirtDetailById($job_id);
                ?>
                        <div class="panel panel-default" id="job_shirt_<?php echo $job_id;?>">
                            <div class="panel-heading" style="padding:10px;">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#accordion_shirt_<?php echo $job_id?>">
                                        <?php echo $jobDetail->job_name?>
                                    </a>
                                </h4>
                            </div>
                            <div id="accordion_shirt_<?php echo $jobDetail->job_id?>" class="panel-collapse collapse in">
                                <div class="panel-body">
                                <b>Stock:</b> <?php echo $job_name;?><br />
                                <b>Type:</b> <?php echo $jobDetail->type_name;?><br />
                                <b>Front Color:</b> <?php echo $jobDetail->fcolor_name;?><br />
                                <b>Back Color:</b> <?php echo $jobDetail->bcolor_name;?><br />
                                <?php 
                                $sizeQTy = unserialize($jobDetail->quantity_size);
                                $sizeQuantity = array();
                                if(is_array($sizeQTy)) {
                                    $i = 1;                    
                                    foreach ($sizeQTy as $size) {
                                          $sizeObject = new ShirtSize($size['size']);
                                          $jobsResult = $this->order_model->get_object( $sizeObject,true );
                                              ?>
                                               <b>Size #<?php echo $i;?>:</b> <?php echo $size['size'];?><br />
                                               <b>Quantity #<?php echo $i;?>:</b> <?php echo $size['quantity'];?><br />
                                               <?php
                                               $i++;
                                    }
                                }
                                ?>
                                <b>Department:</b> <?php echo $jobDetail->department_name;?><br />
                                <b>Others:</b> <?php echo $jobDetail->others;?><br />  
                                <b>Additional notes:</b> <?php echo $jobDetail->additional_notes;?><br />
                                <a href="javascript:void(0)" onclick="editShirtDetail(<?php echo $jobDetail->job_id?>)">Edit</a>
                                | <a href="javascript:void(0)" onclick="deleteShirtDetail(<?php echo $jobDetail->job_id?>)">Delete</a>
                                </div>
                            </div>
                        </div>
                <?php
                
            }
        }
    }
    public function getJobInfo() {
        $order_type = $this->input->post('job_type');
        $jobId = $this->input->post('job_id');
        $data = array('succcess' => false);
        if($jobId > 0 and $order_type == 1) {
            $job = new Ordersjob($jobId);
            $job = $this->order_model->get_object($job,true);   
            if($job) {
                $data['succcess']= true;
                $data['info']['id'] = $job->id;
                $data['info']['job_name'] = $job->job_name;
                $data['info']['stock'] = $job->stock;
                $data['info']['colors'] = $job->colors;
                $data['info']['size'] = $job->size;
                $data['info']['quantity'] = $job->quantity;
                $data['info']['finishing'] = $job->finishing;
                $data['info']['coating'] = $job->coating;
                $data['info']['department'] = $job->department;
                $data['info']['others'] = $job->others;
                $data['info']['additional_notes'] = $job->additional_notes;
            }
        } else if($jobId > 0 and $order_type == 2) {
            
                $job = new Ordersshirt($jobId);
                $job = $this->order_model->get_object($job,true);   
                $data['succcess']= true;
                $data['info']['id'] = $job->id;
                $data['info']['job_name'] = $job->job_name;
                $data['info']['type'] = $job->type;
                $data['info']['front_color'] = $job->front_color;
                $data['info']['back_color'] = $job->back_color;
                $data['info']['department'] = $job->department;
                $data['info']['others'] = $job->others;
                $data['info']['additional_notes'] = $job->additional_notes;
                $data['info']['size'] = '';
                $data['info']['quantity'] = '';
                
                $sizeQTy = unserialize($job->quantity_size);
                $sizeQuantity = array();
                if(is_array($sizeQTy)) {
                    $i = 1;                    
                    foreach ($sizeQTy as $size) {
                        if($i == 1) {
                            $data['info']['size'] = $size['size'];
                            $data['info']['quantity'] = $size['quantity'];
                        } else {
                            $sizeQuantity[] = $size;
                        }
                        $i++;
                    }                       
                } 
                $data['info']['size_quantity'] = $sizeQuantity;
        }
        echo json_encode($data);
        exit;
    }
    function delJobInfo() {
        $jobId = $this->input->post('job_id');
        $order_type = $this->input->post('job_type');
        $data = array('succcess' => false);
        if($order_type == 1 and $jobId > 0) {
            $job = new Ordersjob($jobId);
            $job = $this->order_model->get_object($job,true);   
            if($job) {
                $data['succcess']= true;
                $this->order_model->delete_object($job);   
            }
        } else if($order_type == 2 and $jobId > 0) {
            $job = new Ordersshirt($jobId);
            $job = $this->order_model->get_object($job,true);   
            if($job) {
                $data['succcess']= true;
                $this->order_model->delete_object($job);   
            }
        }
        echo json_encode($data);
    }

    /*
    function order_add_jobs() {
        
        $order = new Filter();
        $orders = $this->order_model->get_orders($order);   
        foreach($orders as $order) {
                $orderJob = new Ordersjob(null,$order->id);
                $orderJob->set_job_name($order->job_name);
                $orderJob->set_coating($order->coating);
                $orderJob->set_stock($order->stock);
                $orderJob->set_colors($order->colors);
                $orderJob->set_size($order->size);
                $orderJob->set_quantity($order->quantity);
                $orderJob->set_others($order->others);
                $orderJob->set_finishing($order->finishing);
                $orderJob->set_additional_notes($order->additional_notes);
                $orderJob->set_created_at(date('Y-m-d'));
                $this->order_model->save_or_update_object( $orderJob );
        }
    }
     * 
     */
}  
?>