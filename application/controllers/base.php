<?php
class Base extends CI_Controller{
    
    public function __construct(){
        
        parent::__construct();
        
        if( ($id=$this->session->userdata('id') == FALSE) && 
            ($person_type=$this->session->userdata('person_type') == FALSE )){
            header('Location: ' . site_url('login/'));
        } else {
            $this->user = new Person();
            $this->user->set_id( $id );
            $this->user->set_person_type( $person_type );
            
            //Main Model for this project. Contains DB communication and Record classes (Order,Person,Item, etc )
            $this->load->model('Order_model','order_model', TRUE ); 
            //Some helper libraries our view uses
            $this->load->library('table');
            //Used for Record form creation
            $this->load->library('order_helper');
            //Used for Mapping of Input to Record class
            $this->load->library('order_mapper', array('input'=>$this->input));
            $this->load->helper('form');
            $this->load->helper('url');              
        }
        
    }
    protected function authorize_or_redirect( $type = Person::TYPE_USER){
        $authenticated = false;
        if( $this->user->id && $this->user->person_type == $type ){
                $authenticated = true;
        } else {
            header('Location: ' . site_url('login'));
        }
        return $authenticated;
    }
}
?>
