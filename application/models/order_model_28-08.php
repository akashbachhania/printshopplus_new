<?php

class Order_model extends CI_Model {

    const QUERY_UPDATE = '1';
    const QUERY_INSERT = '2';

    private $object_cache = array();

    public function __construct() {
        parent::__construct();
    }

    public function get_company_order_ids(Order $order) {
        $query = "SELECT id FROM `dr_orderss` WHERE company_id = '$order->company_id' ORDER BY id ASC";
        $result = $this->db->query($query);

        $ids = array();
        foreach ($result->result() as $id) {
            $ids[] = $id->id;
        }
        return $ids;
    }
    
    public function orderJobDetails($order_id) {
        $query = "SELECT jb.id as job_id,jb.job_name,jb.additional_notes,jb.others,
                            coat.`name` as coat_name,
                            col.`name` as color_name,
                            stock.`name` as stock_name,
                            size.`name` as size_name,
                            quan.`name` as quantity_name,
                            finish.`name` as finishing_name,
                            dept.`name` as department_name,
                            dept.`id` as department_id
                            FROM dr_orders_jobs as jb
                            LEFT JOIN dr_coating as coat
                            ON jb.coating = coat.`id`
                            LEFT JOIN dr_colors as col
                            ON jb.colors = col.`id`
                            LEFT JOIN dr_stock as stock
                            ON jb.stock = stock.`id`
                            LEFT JOIN dr_size as size
                            ON jb.size = size.`id`
                            LEFT JOIN dr_quantity as quan
                            ON jb.quantity = quan.`id`
                            LEFT JOIN dr_finishing as finish
                            ON jb.finishing = finish.`id`
                            LEFT JOIN jobstatus as dept
                            ON jb.department = dept.`id`
                            where jb.order_id  = ".$order_id.' order by jb.id desc';
        $result = $this->db->query($query);
        if($result->num_rows() > 0) {
            return $result->result();
        }
        return false;
    }
    
    public function jobDetailById($job_id) {
        $query = "SELECT jb.id as job_id,jb.job_name,jb.additional_notes,jb.others,
                            coat.`name` as coat_name,
                            col.`name` as color_name,
                            stock.`name` as stock_name,
                            size.`name` as size_name,
                            quan.`name` as quantity_name,
                            finish.`name` as finishing_name,
                            dept.`name` as department_name,
                            dept.`id` as department_id
                            FROM dr_orders_jobs as jb
                            LEFT JOIN dr_coating as coat
                            ON jb.coating = coat.`id`
                            LEFT JOIN dr_colors as col
                            ON jb.colors = col.`id`
                            LEFT JOIN dr_stock as stock
                            ON jb.stock = stock.`id`
                            LEFT JOIN dr_size as size
                            ON jb.size = size.`id`
                            LEFT JOIN dr_quantity as quan
                            ON jb.quantity = quan.`id`
                            LEFT JOIN dr_finishing as finish
                            ON jb.finishing = finish.`id`
                            LEFT JOIN jobstatus as dept
                            ON jb.department = dept.`id`
                            where jb.id  = ".$job_id.' order by jb.id desc';
        $result = $this->db->query($query);
        if($result->num_rows() > 0) {
            return $result->row();
        }
        return false;
    }
    
    public function jobShirtDetailById($job_id) {
        $query = "SELECT jb.id as job_id,jb.job_name,jb.quantity_size,jb.additional_notes,jb.others,
                            type.`name` as type_name,
                            fcol.`name` as fcolor_name,
                            bcol.`name` as bcolor_name,
                            dept.`name` as department_name,
                            dept.`id` as department_id
                            FROM dr_orders_shirts as jb
                            LEFT JOIN dr_shirttype as type
                            ON jb.type = type.`id`
                            LEFT JOIN dr_shirt_frontcolor as fcol
                            ON jb.front_color = fcol.`id`
                            LEFT JOIN dr_shirt_backcolor as bcol
                            ON jb.back_color = bcol.`id`
                            LEFT JOIN jobstatus as dept
                            ON jb.department = dept.`id`
                            where jb.id  = ".$job_id.' order by jb.id desc';
        $result = $this->db->query($query);
        if($result->num_rows() > 0) {
            return $result->row();
        }
        return false;
    }
    public function orderShirtJobDetails($order_id) {
        $query = "SELECT jb.id as job_id,jb.job_name,jb.quantity_size,jb.additional_notes,jb.others,
                            type.`name` as type_name,
                            fcol.`name` as fcolor_name,
                            bcol.`name` as bcolor_name,
                            dept.`name` as department_name,
                            dept.`id` as department_id
                            FROM dr_orders_shirts as jb
                            LEFT JOIN dr_shirttype as type
                            ON jb.type = type.`id`
                            LEFT JOIN dr_shirt_frontcolor as fcol
                            ON jb.front_color = fcol.`id`
                            LEFT JOIN dr_shirt_backcolor as bcol
                            ON jb.back_color = bcol.`id`
                            LEFT JOIN jobstatus as dept
                            ON jb.department = dept.`id`
                            where jb.order_id  = ".$order_id.' order by jb.id desc';
        $result = $this->db->query($query);
        if($result->num_rows() > 0) {
            return $result->result();
        }
        return false;
    }
    public function get_jobstatus_order($company_id) {
        $filter = new JobStatus();
        $filter->set_active(1);
        $filter->set_company_id($company_id);
        $filter->set_order_by(array('id'));
        $items = $this->get_object($filter);
        return $items;
    }

    public function get_jobstatusbyid($id) {
        $sqlquery = "SELECT id FROM jobstatus WHERE id IN (SELECT status FROM dr_orderss WHERE id=" . $id . ")";
        $result = $this->db->query($sqlquery);

        $return = array();
        foreach ($result->result() as $rows) {
            $return[$rows->id] = $rows->id;
        }
        return $return;
    }

    public function get_currentoperatorbyid($id) {
        $sqlquery = "SELECT id FROM operators WHERE id IN (SELECT operator_id FROM dr_orderss WHERE id=" . $id . ")";
        $result = $this->db->query($sqlquery);

        $return = array();
        foreach ($result->result() as $rows) {
            $return[$rows->id] = $rows->id;
        }
        return $return;
    }

    public function get_operatorlist($company_id) {
        $filter = new OperatorList();
        $filter->set_active(1);
        $filter->set_company_id($company_id);
        $filter->set_order_by(array('id'));
        $items = $this->get_object($filter);
        return $items;
    }
    
    public function getJobStatusOrderById($job_id,$company_id) {
        $this->db->select('*');
        $this->db->from('jobstatus_order');
        $this->db->where('job_id',$job_id);
        $this->db->where('company_id',$company_id);
        $result = $this->db->get();
        if($result->num_rows() > 0) {
            return $result->row();
        }
        return false;
    }

    public function get_jobstatus($company_id) {
        //$sqlquery = "SELECT id,name FROM jobstatus where active='1' and company_id=" . $company_id;
        $sqlquery = 'SELECT jb.*,jbo.orderBy
                FROM jobstatus jb
                LEFT JOIN jobstatus_order jbo
                ON jb.id = jbo.job_id
                WHERE active=1
                AND jb.company_id = '.$company_id.'
                ORDER BY orderBy,jb.name ASC;';
        $result = $this->db->query($sqlquery);

        $return = array();
        foreach ($result->result() as $rows) {
            $return[$rows->id] = $rows->name;
        }
        return $return;
    }
    
    public function defaultJobStatus() {
        //$sqlquery = "SELECT id,name FROM jobstatus where active='1' and company_id=0";
        $sqlquery = 'SELECT jb.*,jbo.orderBy
                FROM jobstatus jb
                LEFT JOIN jobstatus_order jbo
                ON jb.id = jbo.job_id
                WHERE active=1
                AND jb.company_id = 0
                ORDER BY orderBy,jb.name ASC;';
        $result = $this->db->query($sqlquery);

        $return = array();
        foreach ($result->result() as $rows) {
            $return[$rows->id] = $rows->name;
        }
        return $return;
    }
    
    public function getJobStatusWithDefault($company_id) {
        //$sqlquery = "SELECT id,name FROM jobstatus where active='1' and company_id IN (0,".$company_id.")";
        $sqlquery = 'SELECT jb.*,jbo.orderBy
                FROM jobstatus jb
                LEFT JOIN jobstatus_order jbo
                ON jb.id = jbo.job_id
                WHERE active=1
                AND jb.company_id IN (0,'.$company_id.')
                ORDER BY orderBy,jb.name ASC;';
        $result = $this->db->query($sqlquery);

        $return = array();
        foreach ($result->result() as $rows) {
            $return[$rows->id] = $rows->name;
        }
        return $return;
    }

    public function get_stock($company_id) {
        $sqlquery = "SELECT id,name FROM dr_stock where active='1' and company_id=" . $company_id;
        $result = $this->db->query($sqlquery);

        $return = array();
        foreach ($result->result() as $rows) {
            $return[$rows->id] = $rows->name;
        }

        return $return;
    }

    public function get_colors($company_id) {

        $sqlquery = "SELECT id,name FROM dr_colors where active='1' and company_id=" . $company_id;
        $result = $this->db->query($sqlquery);

        $return = array();
        foreach ($result->result() as $rows) {
            $return[$rows->id] = $rows->name;
        }

        return $return;
    }

    public function get_size($company_id) {

        $sqlquery = "SELECT id,name FROM dr_size where active='1' and company_id=" . $company_id;
        $result = $this->db->query($sqlquery);

        $return = array();
        foreach ($result->result() as $rows) {
            $return[$rows->id] = $rows->name;
        }

        return $return;
    }

    public function get_quantity($company_id) {

        $sqlquery = "SELECT id,name FROM dr_quantity where active='1' and company_id=" . $company_id . ' order by name asc';
        $result = $this->db->query($sqlquery);

        $return = array();
        foreach ($result->result() as $rows) {
            $return[$rows->id] = $rows->name;
        }

        return $return;
    }

    public function get_coating($company_id) {

        $sqlquery = "SELECT id,name FROM dr_coating where active='1' and company_id=" . $company_id;
        $result = $this->db->query($sqlquery);

        $return = array();
        foreach ($result->result() as $rows) {
            $return[$rows->id] = $rows->name;
        }

        return $return;
    }

    public function get_finishing($company_id) {

        $sqlquery = "SELECT id,name FROM dr_finishing where active='1' and company_id=" . $company_id;
        $result = $this->db->query($sqlquery);

        $return = array();
        foreach ($result->result() as $rows) {
            $return[$rows->id] = $rows->name;
        }

        return $return;
    }
    
    public function get_shirt_type($company_id) {

        $sqlquery = "SELECT id,name FROM dr_shirttype where active='1' and company_id=" . $company_id;
        $result = $this->db->query($sqlquery);

        $return = array();
        foreach ($result->result() as $rows) {
            $return[$rows->id] = $rows->name;
        }

        return $return;
    }
    
    public function get_shirt_front_color($company_id) {

        $sqlquery = "SELECT id,name FROM dr_shirt_frontcolor where active='1' and company_id=" . $company_id;
        $result = $this->db->query($sqlquery);

        $return = array();
        foreach ($result->result() as $rows) {
            $return[$rows->id] = $rows->name;
        }

        return $return;
    }
    
    public function get_shirt_back_color($company_id) {

        $sqlquery = "SELECT id,name FROM dr_shirt_backcolor where active='1' and company_id=" . $company_id;
        $result = $this->db->query($sqlquery);

        $return = array();
        foreach ($result->result() as $rows) {
            $return[$rows->id] = $rows->name;
        }

        return $return;
    }
    
    public function get_shirt_size($company_id) {

        $sqlquery = "SELECT id,name FROM dr_shirt_size where active='1' and company_id=" . $company_id;
        $result = $this->db->query($sqlquery);

        $return = array();
        foreach ($result->result() as $rows) {
            $return[$rows->id] = $rows->name;
        }

        return $return;
    }

    public function get_salesrep($company_id) {
        $sqlquery = "SELECT id,name FROM dt_salesrep where active ='1' and company_id=" . $company_id;
        $result = $this->db->query($sqlquery);

        $return = array();
        foreach ($result->result() as $rows) {
            $return[$rows->id] = $rows->name;
        }
        return $return;
    }

    public function get_shippingmethod($company_id) {
        $sqlquery = "SELECT id,name FROM shippingmethod where active ='1' and company_id=" . $company_id;
        $result = $this->db->query($sqlquery);

        $return = array();
        foreach ($result->result() as $rows) {
            $return[$rows->id] = $rows->name;
        }
        return $return;
    }

    public function get_terms($company_id) {
        $sqlquery = "SELECT id,name FROM terms where active ='1' and company_id=" . $company_id;
        $result = $this->db->query($sqlquery);

        $return = array();
        foreach ($result->result() as $rows) {
            $return[$rows->id] = $rows->name;
        }
        return $return;
    }

    public function get_company_list() {

        $sqlquery = "SELECT id,name FROM dr_companies where active ='1'";
        $result = $this->db->query($sqlquery);

        $return = array();
        foreach ($result->result() as $rows) {
            $return[$rows->id] = $rows->name;
        }
        return $return;
    }

    public function get_coatinglist($company_id) {
        $sqlquery = "SELECT id,name FROM coating where active ='1' AND company_id=" . $company_id;
        $result = $this->db->query($sqlquery);

        $return = array();
        foreach ($result->result() as $rows) {
            $return[$rows->id] = $rows->name;
        }
        return $return;
    }

    public function get_person_names($type = Person::TYPE_CLIENT, $company_id) {
        $filter = new Person();
        $filter->set_person_type($type);
        $filter->set_company_id($company_id);
        $filter->set_active(1);
        $filter->set_order_by(array('name'));

        $persons = $this->get_object($filter);
        $return = array();
        foreach ($persons as $person) {
            $return[$person->id] = $person->name;
        }
        return $return;
    }

    public function get_items($company_id) {

        $filter = new Base_item();
        $filter->set_company_id($company_id);
        $filter->set_active(1);
        $filter->set_order_by(array('name'));
        $items = $this->get_object($filter);

        $return = array();
        foreach ($items as $item) {
            $return[$item->id] = $item->name;
        }
        return $return;
    }

    public function get_filtereditems($id, $company_id) {

        $filter = new Base_item();
        $filter->set_company_id($company_id);
        $filter->set_id($id);
        $items = $this->get_object($filter);

        $return = array();
        foreach ($items as $item) {

            $return = $item;
        }
        return $return;
    }

    public function get_menuitems($company_id) {

        $filter = new MenuItem();
        $filter->set_active(1);
        $filter->set_company_id($company_id);
        $filter->set_order_by(array('id'));
        $items = $this->get_object($filter);
        return $items;
    }

    public function get_types($type, $company_id) {
        $filter = new Type();
        $filter->set_type($type);
        $filter->set_company_id($company_id);
        $filter->set_active(1);
        $filter->set_order_by(array('name'));
        $types = $this->get_object($filter);

        $return = array();
        foreach ($types as $type) {
            $return[$type->id] = $type->name;
        }
        return $return;
    }

    public function get_taxes($company_id) {
        $filter = new Tax();
        $filter->set_company_id($company_id);
        $filter->set_active(1);
        $taxes = $this->get_object($filter);

        $return = array();
        foreach ($taxes as $tax) {
            $return[$tax->id] = $tax->name;
        }
        return $return;
    }

    function countJobsStatistics($companyId, $status_id = 0, $start_date = '', $end_date = '') {
        
        
        $queryCount = 'SELECT COUNT(jobs.order_id) as total
                        FROM dr_orderss ordr 
                        RIGHT JOIN dr_orders_jobs jobs ON ordr.id = jobs.order_id 
                        WHERE ordr.company_id ='.$companyId.' 
                        AND ordr.is_quote=0';
        $where = '';
        if($status_id != 0 ){
            $where .= ' AND jobs.department = '.$status_id;
        }
        
        if ($start_date != '') {
            $st_d = explode('-', $start_date);
            $st_d = $st_d[2] . '-' . $st_d[0] . '-' . $st_d[1];
            $where .= ' AND ordr.order_date <="'.$st_d.'"';
        }
        
        if ($end_date != '') {
            $et_d = explode('-', $end_date);
            $et_d =  $et_d[2] . '-' . $et_d[0] . '-' . $et_d[1];
            $where .= ' AND ordr.order_date <="'.$et_d.'"';
        }
        
        $resultTotal    = $this->db->query( $queryCount.$where );
        $rowTotal       = $resultTotal->row();
        $rowTotal       = $rowTotal->total;
        

        return $rowTotal;
    }

    public function get_us_states() {
        $states = array(
            "Alabama",
            "Alaska",
            "Arizona",
            "Arkansas",
            "California",
            "Colorado",
            "Connecticut",
            "Delaware",
            "District of Columbia",
            "Florida",
            "Georgia",
            "Hawaii",
            "Idaho",
            "Illinois",
            "Indiana",
            "Iowa",
            "Kansas",
            "Kentucky",
            "Louisiana",
            "Maine",
            "Maryland",
            "Massachusetts",
            "Michigan",
            "Minnesota",
            "Mississippi",
            "Missouri",
            "Montana",
            "Nebraska",
            "Nevada",
            "New Hampshire",
            "New Jersey",
            "New Mexico",
            "New York",
            "North Carolina",
            "North Dakota",
            "Ohio",
            "Oklahoma",
            "Oregon",
            "Pennsylvania",
            "Rhode Island",
            "South Carolina",
            "South Dakota",
            "Tennessee",
            "Texas",
            "Utah",
            "Vermont",
            "Virginia",
            "Washington",
            "West Virginia",
            "Wisconsin",
            "Wyoming"
        );
        $ret = array();
        foreach ($states as $state) {
            $ret[$state] = $state;
        }
        return $ret;
    }

    public function get_companies(Person $person) {
        $return = array();
        if ($person->person_type == Person::TYPE_ADMIN) {
            $filter = new Company();
            $companies = $this->get_object($filter);
            foreach ($companies as $company) {
                $return[$company->id] = $company->name;
            }
        }
        return $return;
    }

    public function get_times() {
        return array(
            '07:00' => '7:00 AM',
            '07:30' => '7:30 AM',
            '08:00' => '8:00 AM',
            '08:30' => '8:30 AM',
            '09:00' => '9:00 AM',
            '09:30' => '9:30 AM',
            '10:00' => '10:00 AM',
            '10:30' => '10:30 AM',
            '11:00' => '11:00 AM',
            '11:30' => '11:30 AM',
            '12:00' => '12:00 PM',
            '12:30' => '12:30 PM',
            '13:00' => '1:00 PM',
            '13:30' => '1:30 PM',
            '14:00' => '2:00 PM',
            '14:30' => '2:30 PM',
            '15:00' => '3:00 PM',
            '15:30' => '3:30 PM',
            '16:00' => '4:00 PM',
            '16:30' => '4:30 PM',
            '17:00' => '5:00 PM',
            '17:30' => '5:30 PM',
            '18:00' => '6:00 PM',
            '18:30' => '6:30 PM',
            '19:00' => '7:00 PM',
            '19:30' => '7:30 PM',
            '20:00' => '8:00 PM'
        );
    }

    public function get_duration_times() {
        for ($i = 0.5; $i < 24; $i+=0.5) {
            $return["$i"] = "$i hours";
        }
        return $return;
    }

    public function get_item($item_id, $company_id = '') {

        $filter = new Base_item();
        //$filter->set_company_id( $company_id );
        $filter->set_id($item_id);

        $item = reset($this->get_object($filter));

        return $item;
    }

    public function get_salesrep_byid($item_id) {

        $filter = new SalesRep();
        $param = $item_id;
        $call = 'set_id';
        $filter->set_id($param);
        $item = reset($this->get_object($filter));
        return $item;
    }

    public function get_jobstatus_byid($item_id) {

        $filter = new JobStatus();
        $param = $item_id;
        $call = 'set_id';
        $filter->set_id($param);
        $item = reset($this->get_object($filter));
        return $item;
    }

    public function get_productiondetail_byid($item_id, $item_type) {

        if (class_exists($item_type)) {

            $filter = new $item_type();
            $param = $item_id;
            $call = 'set_id';
            $filter->set_id($param);
            $item = reset($this->get_object($filter));
            return $item;
        } else {
            return false;
        }
    }

    public function get_operator_byid($item_id) {

        $filter = new OperatorList();
        $param = $item_id;
        $call = 'set_id';
        $filter->set_id($param);
        $item = reset($this->get_object($filter));
        return $item;
    }

    public function get_shippingmethod_byid($item_id) {

        $filter = new ShippingMethod();
        $param = $item_id;
        $call = 'set_id';
        $filter->set_id($param);
        $item = reset($this->get_object($filter));
        return $item;
    }

    public function get_coating_byid($item_id) {

        $filter = new Coating();
        $param = $item_id;
        $call = 'set_id';
        $filter->set_id($param);
        $item = reset($this->get_object($filter));
        return $item;
    }

    public function get_term_byid($item_id) {

        $filter = new Terms();
        $param = $item_id;
        $call = 'set_id';
        $filter->set_id($param);
        $item = reset($this->get_object($filter));
        return $item;
    }

    public function get_estimated_ages() {
        $base_year = 1920;
        $current_year = gmdate('Y');
        $years = array();
        for ($i = 1; ($i + $base_year) < $current_year; $i++) {
            $year = $base_year + $i;
            $years[$year] = $year;
        }
        return $years;
    }

    /*     * **
     * put your comment there...
     * 
     * @param Order $order
     * @param Order $old_order
     */

    public function save_order(Order $order, Order $old_order = null) {
        /**
         * Validate order first
         */
        // if( !$order->validate()){
        //     throw new Order_model_exception( implode(',',$order->errors) );
        //}
        /**
         * Save/update all persons attached to this order

          foreach( $order->persons as $person=>$person_id ){

          $old_person = null;
          if( isset( $old_order->$person )){
          $old_person = $old_order->$person;
          }
          if( $order->$person->validate() ){
          $order->$person_id = $this->save_or_update_object( $order->$person,
          $old_person );
          }



          }
         * 



          $objectperson = new Person();
          $objectperson->set_name($this->input->post('client_name'));
          $objectperson->set_address($this->input->post('client_address'));
          $objectperson->set_address_2($this->input->post('client_address_2'));
          $objectperson->set_state($this->input->post('client_state'));
          $objectperson->set_city($this->input->post('client_city'));
          $objectperson->set_zip($this->input->post('client_zip'));
          $objectperson->set_phone_1($this->input->post('client_phone_1'));
          $objectperson->set_phone_2($this->input->post('client_phone_2'));
          $objectperson->set_email($this->input->post('client_email'));
          $objectperson->set_referred_by($this->input->post('client_referred_by'));
          $objectperson->set_company_id($order->company_id);
          $objectperson->set_person_type('1');
         * 
         */
        /**
         * Save this order, or update old one.
         */
        $order_id = $this->save_or_update_object($order, $old_order);
        /**
         * Delete old order items
         */
        $filter_item = new Item();
        $filter_item->set_order_id($order_id);
        $this->delete_object($filter_item);

        /**
         * Save items
         */
        if (is_array($order->items)) {
            foreach ($order->items as $item) {
                $item->set_order_id($order_id);
                $this->save_or_update_object($item);
            }
        }
        return $order_id;
    }

    public function save_or_update_object(Record $record, Record $old_record = null) {
        
        $record->sleep();

        if ($old_record) {
            if (!$record->equal($old_record)) {
                $type = self::QUERY_UPDATE;
                $record->id = $old_record->id;
            }
        } else {
            if (isset($record->id) && intval($record->id) && $old_record == null) {
                //throw new Order_model_exception('New person with id, withoud old person, sometnihg is  fishy!'. $record->__toString() );
                $type = self::QUERY_UPDATE;
            } else {
                $type = self::QUERY_INSERT;
            }
        }

        switch ($type) {
            case self::QUERY_INSERT:
            case self::QUERY_UPDATE:
                $query = $this->createquery($record, $type);
                if ($this->db->query($query)) {
                    $id = isset($record->id) && $record->id ? $record->id : $this->db->insert_id();
                } else {
                    throw new Order_model_exception('Database transaction failed');
                    
                }
                log_message('debug', 'Query  : ' . $query);
                break;
            default:
                $id = $record->id;
                break;
        }

        $record->wake_up();

        return $id;
    }

    public function get_max_runorderid() {
        /* $query="SELECT MAX(id)maxid FROM run_orders";
          $res=mysql_query($query);
          if(mysql_num_rows($res) > 0){
          $result= mysql_fetch_assoc($res);
          return $result["maxid"] +1;
          }else{
          return 1;
          } */
        $query = "SHOW TABLE STATUS WHERE name = 'run_orders'";
        $res = mysql_query($query);
        $result = mysql_fetch_assoc($res);
        return $result["Auto_increment"];
    }

    public function get_runlist($company_id) {
        $query = "SELECT id,order_ids,size,paperstock,colors,quantity,coating,due_date,proofs,printing_start_date,printing_start_time,printing_end_date,printing_end_time,printing_operator,printing_sheet_count,coating_start_date,coating_start_time,coating_end_date,coating_end_time,coating_operator,coating_sheet_count,cutting_start_date,cutting_start_time,cutting_end_date,cutting_end_time,cutting_operator,cutting_sheet_count,additional_info,upload_image FROM run_orders WHERE company_id=$company_id  ORDER BY id DESC";
        $res = mysql_query($query);
        $return = array();
        while ($results = mysql_fetch_assoc($res)) {
            $return [] = $results;
        }
        return $return;
    }

    public function save_run_order($file_name, $company_id) {
        if ($this->input->post('runid')) {
            $save_run = array(
                'size' => $this->input->post('Size'),
                'paperstock' => $this->input->post('PaperStock'),
                'colors' => $this->input->post('Colors'),
                'quantity' => $this->input->post('Quantity'),
                'coating' => $this->input->post('Coating'),
                'due_date' => date('Y-m-d', strtotime(str_replace('-', '/', $this->input->post('due_date')))),
                'proofs' => $this->input->post('Proofs'),
                'printing_start_date' => date('Y-m-d', strtotime(str_replace('-', '/', $this->input->post('printing_start_date')))),
                'printing_start_time' => $this->input->post('printing_start_time'),
                'printing_end_date' => date('Y-m-d', strtotime(str_replace('-', '/', $this->input->post('printing_end_date')))),
                'printing_end_time' => $this->input->post('printing_end_time'),
                'printing_operator' => $this->input->post('printing_operator'),
                'printing_sheet_count' => $this->input->post('printing_sheet_count'),
                'coating_start_date' => date('Y-m-d', strtotime(str_replace('-', '/', $this->input->post('coating_start_date')))),
                'coating_start_time' => $this->input->post('coating_start_time'),
                'coating_end_date' => date('Y-m-d', strtotime(str_replace('-', '/', $this->input->post('coating_end_date')))),
                'coating_end_time' => $this->input->post('coating_end_time'),
                'coating_operator' => $this->input->post('coating_operator'),
                'coating_sheet_count' => $this->input->post('coating_sheet_count'),
                'cutting_start_date' => date('Y-m-d', strtotime(str_replace('-', '/', $this->input->post('cutting_start_date')))),
                'cutting_start_time' => $this->input->post('cutting_start_time'),
                'cutting_end_date' => date('Y-m-d', strtotime(str_replace('-', '/', $this->input->post('cutting_end_date')))),
                'cutting_end_time' => $this->input->post('cutting_end_time'),
                'cutting_operator' => $this->input->post('cutting_operator'),
                'cutting_sheet_count' => $this->input->post('cutting_sheet_count'),
                'additional_info' => $this->input->post('AdditionalInfo'),
                'upload_image' => $file_name
            );
            $this->db->where('id', $this->input->post('runid'));
            $this->db->update('run_orders', $save_run);
            return true;
        } else {
            $this->update_multiple_order_jobstatus($this->input->post('oid'), $this->input->post('jid'));
            $save_run = array(
                'company_id' => $company_id,
                'size' => $this->input->post('Size'),
                'paperstock' => $this->input->post('PaperStock'),
                'colors' => $this->input->post('Colors'),
                'quantity' => $this->input->post('Quantity'),
                'coating' => $this->input->post('Coating'),
                'due_date' => date('Y-m-d', strtotime(str_replace('-', '/', $this->input->post('due_date')))),
                'proofs' => $this->input->post('Proofs'),
                'additional_info' => $this->input->post('AdditionalInfo'),
                'upload_image' => $file_name,
                'order_ids' => $this->input->post('oid')
            );
            $this->db->insert('run_orders', $save_run);
            $return_id = mysql_insert_id();
            $this->db->where('id', $return_id);
            $this->db->delete('run_orders_temp');
            return $return_id;
        }
    }

    public function save_run_order_temp($file_name) {
        $save_run = array(
            'id' => $this->input->post('hdn_max_run_id'),
            'size' => $this->input->post('Size'),
            'paperstock' => $this->input->post('PaperStock'),
            'colors' => $this->input->post('Colors'),
            'quantity' => $this->input->post('Quantity'),
            'coating' => $this->input->post('Coating'),
            'due_date' => date('Y-m-d', strtotime(str_replace('-', '/', $this->input->post('due_date')))),
            'proofs' => $this->input->post('Proofs'),
            'additional_info' => $this->input->post('AdditionalInfo'),
            'upload_image' => $file_name,
            'order_ids' => $this->input->post('oid')
        );
        $this->db->insert('run_orders_temp', $save_run);
        return mysql_insert_id();
    }

    public function get_rundetails_byid($run_id) {
        $query = "SELECT id,order_ids,size,paperstock,colors,quantity,coating,due_date,proofs,additional_info,upload_image FROM run_orders WHERE id=$run_id";
        $res = mysql_query($query);
        $result = mysql_fetch_assoc($res);
        return $result;
    }

    public function get_run_temp_details_byid($auto_id) {
        $query = "SELECT id,order_ids,size,paperstock,colors,quantity,coating,due_date,proofs,additional_info,upload_image FROM run_orders_temp WHERE auto_id=$auto_id";
        $res = mysql_query($query);
        $result = mysql_fetch_assoc($res);
        return $result;
    }

    public function delete_runorder($run_id) {
        $query = "DELETE FROM run_orders WHERE id=$run_id";
        $res = mysql_query($query);
        $result = mysql_fetch_assoc($res);
        return $result;
    }
    public function update_person_pwd($data,$id){
        
        $this->db->set('password',$data);
        $this->db->where('id',$id);
        $result = $this->db->update('dr_persons');
        if($result){
            return true;
        }
    }
    public function save_or_update_personobject(Record $record, Record $old_record = null) {

        $record->sleep();

        if ($old_record) {
            if (!$record->equal($old_record)) {
                $type = self::QUERY_UPDATE;
                $record->id = $old_record->id;
            }
        } else {
            if (isset($record->id) && intval($record->id) && $old_record == null && $record->name == $this->input->post('client_name')) {
                //throw new Order_model_exception('New person with id, withoud old person, sometnihg is  fishy!'. $record->__toString() );

                $type = self::QUERY_UPDATE;
            } else {
                $type = self::QUERY_INSERT;
            }
        }

        switch ($type) {
            case self::QUERY_INSERT:
            case self::QUERY_UPDATE:
                $query = $this->createquery($record, $type);
                if ($this->db->query($query)) {
                    $id = isset($record->id) && $record->id ? $record->id : mysql_insert_id();
                } else {
                    throw new Order_model_exception('Database transaction failed');
                }
                log_message('debug', 'Query  : ' . $query);
                break;
            default:
                $id = $record->id;
                break;
        }

        $record->wake_up();

        return $id;
    }

    public function delete_object(Record $object) {
        $filters = array();
        foreach ($object as $field => $value) {
            if ($value !== null && !is_array($value) && !is_object($value) && $field != 'table') {
                $filters[] = "$field = '".addslashes($value)."'";
            }
        }
        $filter = $filters ? 'WHERE ' . implode(' AND ', $filters) : '';
        if ($filter) {
            $query = "DELETE FROM $object->table $filter";
            $this->db->query($query);
        }
    }

    public function delete_order(Order $object) {
        if ($object->id) {
            $query = "DELETE FROM $object->table WHERE id = '$object->id'";
            $this->db->query($query);
        }
    }

    public function delete_order_all($orderidlist) {
        $query = "DELETE FROM dr_orderss WHERE id in (" . $orderidlist . ")";
        $this->db->query($query);

        $query = "DELETE FROM dr_order_items WHERE order_id in (" . $orderidlist . ")";
        $this->db->query($query);
    }

    public function update_order_jobstatus($orderid, $department_id) {
        $query = "update dr_orders_jobs set department='" . $department_id . "' WHERE id ='" . $orderid . "'";
        $this->db->query($query);
    }
    public function update_order_shirtstatus($orderid, $department_id) {
        $query = "update dr_orders_shirts set department='" . $department_id . "' WHERE id ='" . $orderid . "'";
        $this->db->query($query);
    }

    public function update_multiple_order_jobstatus($order_ids, $jobstatus_id) {
        $query = "update dr_orderss set department='" . $jobstatus_id . "' WHERE id IN(" . $order_ids . ")";
        $this->db->query($query);
    }
    public function update_multiple_order_shirtstatus($order_ids, $jobstatus_id) {
        $query = "update dr_orders_shirts set department='" . $jobstatus_id . "' WHERE id IN(" . $order_ids . ")";
        $this->db->query($query);
    }

    public function update_order_operator($orderid, $operator_id) {
        $query = "update dr_orderss set operator_id='" . $operator_id . "' WHERE id ='" . $orderid . "'";
        $this->db->query($query);
    }

    private function createquery(Record $object, $type) {
        //$type = self::QUERY_INSERT;	
        if (isset($object->id) && $object->id > 0) {
            //$type = self::QUERY_UPDATE;
        }
        switch ($type) {
            case self::QUERY_INSERT:
                foreach ($object as $column => $value) {

                    if ($value !== null &&
                            !is_object($value) &&
                            !is_array($value) &&
                            $column != 'id' &&
                            $column != 'table' &&
                            !preg_match('/^_/', $column)) {

                        $value = $this->db->escape($value);
                        $query[$column] = "$value";
                 
                    }
                }

                break;
            case self::QUERY_UPDATE:
                foreach ($object as $column => $value) {

                    if ($value !== null &&
                            !is_object($value) &&
                            !is_array($value) &&
                            $column != 'table' &&
                            $column != 'id' &&
                            !preg_match('/^_/', $column)) {

                        $value = $this->db->escape($value);
                        $query[] = "$column = $value";
                    }
                }
                break;
        }

        switch ($type) {
            case self::QUERY_INSERT:

                $columns = implode(',', array_keys($query));
                $values = implode(',', $query);
                $table = $object->table;

                $query = "INSERT INTO `$object->table` ($columns) VALUES ( $values )";
                break;
            case self::QUERY_UPDATE:

                $columns_values = implode(' , ', $query);
                $id = $object->id;
                $query = "UPDATE `$object->table` SET $columns_values WHERE id=$id";

                break;
        }
        return $query;
    }

    private function create_query(Record $object) {
        $type = self::QUERY_INSERT;

        if (isset($object->id) && $object->id > 0) {
            $type = self::QUERY_UPDATE;
        }
        switch ($type) {
            case self::QUERY_INSERT:
                foreach ($object as $column => $value) {

                    if ($value !== null &&
                            !is_object($value) &&
                            !is_array($value) &&
                            $column != 'id' &&
                            $column != 'table' &&
                            !preg_match('/^_/', $column)) {

                        $value = $this->db->escape($value);
                        $query[$column] = "$value";
                    }
                }
                break;
            case self::QUERY_UPDATE:
                foreach ($object as $column => $value) {

                    if ($value !== null &&
                            !is_object($value) &&
                            !is_array($value) &&
                            $column != 'table' &&
                            $column != 'id' &&
                            !preg_match('/^_/', $column)) {

                        $value = $this->db->escape($value);
                        $query[] = "$column = $value";
                    }
                }
                break;
        }

        switch ($type) {
            case self::QUERY_INSERT:

                $columns = implode(',', array_keys($query));
                $values = implode(',', $query);
                $table = $object->table;

                $query = "INSERT INTO `$object->table` ($columns) VALUES ( $values )";
                break;
            case self::QUERY_UPDATE:

                $columns_values = implode(' , ', $query);
                $id = $object->id;
                $query = "UPDATE `$object->table` SET $columns_values WHERE id=$id";

                break;
        }
        return $query;
    }

    public function get_orders(Filter $filter, $return_scalar = false) {


        $filters = array();
        foreach ($filter as $field => $value) {
            if ($value !== null && !in_array($field, array('count', 'offset'))) {
                if ($field == 'start_date') {
                    $field = 'report_due';
                    $delim = '>=';
                    $value = date('m-d-Y', strtotime($value));
                } else if ($field == 'end_date') {
                    $field = 'report_due';
                    $delim = '<=';
                    $value = date('m-d-Y', strtotime($value));
                }elseif ($field == 'order_start_date') {
                    $field = 'order_date';
                    $delim = '>=';
                    $value = date('Y-m-d', strtotime($value));
                } else if ($field == 'order_end_date') {
                    $field = 'order_date';
                    $delim = '<=';
                    $value = date('Y-m-d', strtotime($value));
                } else {
                    $delim = '=';
                }
                $filters[] = " $field $delim '$value'";
            }
        }
        $where = $filters ? ' WHERE ' . implode(' AND ', $filters) : '';
        
        $where .= ' order by id DESC';

        if ($filter->count) {
            $where .= " LIMIT $filter->count ";
        }
        if ($filter->offset) {
            $offset = $filter->count * $filter->offset;
            $where .= " OFFSET $offset ";
        }


        $query = "SELECT id,company_order_id,client_id,company_id,order_date,report_due,sales_rep,terms,order_notes,shipping_amount,shipping_method,subtotal,tax,total,paid,payment_n,notes_charge_date,amount_due,company,po_text,notes_date,notes_by FROM dr_orderss " . $where;
        $result = $this->db->query($query);
        
        $methods = get_class_methods('Order');
        
        $orders = array();

        foreach ($result->result() as $row_order) {
            
            $order = new Order();
            foreach ($row_order as $field => $value) {
                $call = "set_$field";
                if (in_array($call, $methods)) {
                    $order->$call($value);
                }
            }

            foreach ($order->persons as $person => $id) {
                $call = 'set_' . $person;

                $person = new Person();
                $person->set_id($order->$id);

                if ($persons = $this->get_object($person)) {
                    $order->$call(reset($persons));
                }
            }
            /*
              $filter = new Type();
              foreach( $order->types as $field=>$type_id ){
              $filter->set_id( $order->$type_id );
              if( $types = $this->get_object( $filter )){
              $type = reset( $types) ;
              } else {
              $type = new Type();
              }
              $order->set_type( $type, $field );
              }

             */
            $items = $this->get_order_items($order->id);
            foreach ($items as $item) {
                $order->add_item($item);
            }

            /*
              $filter_company = new Company();
              $filter_company->set_id( $order->company_id );

              $companies = $this->get_object( $filter_company );
              if( $companies ){
              $order->set_company( reset( $companies ));
              } */
            $orders[] = $order;
        }
        
        if ($return_scalar) {
            
            return $orders[0];
        } else {
            return $orders;
        }
    }

    public function get_orders_to_displayAll(Filter $filter, $company_id, $return_scalar = false) {
        $filters = array();
        foreach ($filter as $field => $value) {
            if ($value !== null && !in_array($field, array('count', 'offset'))) {
                if ($field == 'start_date') {
                    $field = 'inspection_date';
                    $delim = '>=';
                } else if ($field == 'end_date') {
                    $field = 'inspection_date';
                    $delim = '<=';
                } else {
                    $delim = '=';
                }
                $filters[] = " $field $delim '$value'";
            }
        }
        //$where = ' WHERE STATUS NOT IN (13,15) AND company_id='.$company_id;
        $where = ' WHERE STATUS NOT IN ((SELECT id FROM `jobstatus` WHERE name IN ("On Press","on press","onpress","Onpress","OnPress") AND company_id=' . $company_id . '),(SELECT id FROM  `jobstatus` WHERE name IN ("Completed",  "completed") AND company_id =' . $company_id . ')) AND company_id=' . $company_id;

        if ($filter->count) {
            $where .= " LIMIT $filter->count ";
        }
        if ($filter->offset) {
            $offset = $filter->count * $filter->offset;
            $where .= " OFFSET $offset ";
        }


        $query = "SELECT id,order_date,(select name from dr_persons where id=o.client_id)company_id,order_date,CONCAT('<p>- Job Name: ',job_name, '<p> - Stock: ', stock,'<p> - Colors: ',colors, '<p> - Qty: ', quantity, '<p> - Finishing: ',finishing , '<p> - Coating: ',coating,'<p> - Others: ',others)order_notes,report_due,(select name from dr_persons where id=o.sales_rep)sales_rep,(select name from jobstatus where id=o.status)status,id as client_id,operator_id,client_id as agent_id FROM dr_orderss o $where";
        $result = $this->db->query($query);

        $methods = get_class_methods('Order');

        $orders = array();

        foreach ($result->result() as $row_order) {
            $order = new Order();
            foreach ($row_order as $field => $value) {
                $call = "set_$field";
                if (in_array($call, $methods)) {
                    $order->$call($value);
                }
            }

            foreach ($order->persons as $person => $id) {
                $call = 'set_' . $person;

                $person = new Person();
                $person->set_id($order->$id);

                if ($persons = $this->get_object($person)) {
                    $order->$call(reset($persons));
                }
            }

            $filter = new Type();
            foreach ($order->types as $field => $type_id) {
                $filter->set_id($order->$type_id);
                if ($types = $this->get_object($filter)) {
                    $type = reset($types);
                } else {
                    $type = new Type();
                }
                $order->set_type($type, $field);
            }


            $items = $this->get_order_items($order->id);
            foreach ($items as $item) {
                $order->add_item($item);
            }

            $filter_company = new Company();
            $filter_company->set_id($order->company_id);

            $companies = $this->get_object($filter_company);
            if ($companies) {
                $order->set_company(reset($companies));
            }
            $orders[] = $order;
        }
        if ($return_scalar) {
            return $orders[0];
        } else {
            return $orders;
        }
    }

    public function get_orders_to_display(Filter $filter, $return_scalar = false) {


        $filters = array();
        foreach ($filter as $field => $value) {
            if ($value !== null && !in_array($field, array('count', 'offset'))) {
                if ($field == 'start_date') {
                    $field = 'inspection_date';
                    $delim = '>=';
                } else if ($field == 'end_date') {
                    $field = 'inspection_date';
                    $delim = '<=';
                } else {
                    $delim = '=';
                }
                $filters[] = " $field $delim '$value'";
            }
        }
        $where = $filters ? ' WHERE ' . implode(' AND ', $filters) : '';

        if ($filter->count) {
            $where .= " LIMIT $filter->count ";
        }
        if ($filter->offset) {
            $offset = $filter->count * $filter->offset;
            $where .= " OFFSET $offset ";
        }


        $query = "SELECT id,order_date,(select name from dr_persons where id=o.client_id)company_id,order_date,CONCAT('<p>- Job Name: ',job_name, '<p> - Stock: ', stock,'<p> - Colors: ',colors, '<p> - Qty: ', quantity, '<p> - Finishing: ',finishing , '<p> - Coating: ',coating,'<p> - Others: ',others)order_notes,report_due,(select name from dr_persons where id=o.sales_rep)sales_rep,(select name from jobstatus where id=o.status)status,id as client_id,operator_id,client_id as agent_id FROM dr_orderss o $where";
        $result = $this->db->query($query);

        $methods = get_class_methods('Order');

        $orders = array();

        foreach ($result->result() as $row_order) {
            $order = new Order();
            foreach ($row_order as $field => $value) {
                $call = "set_$field";
                if (in_array($call, $methods)) {
                    $order->$call($value);
                }
            }

            foreach ($order->persons as $person => $id) {
                $call = 'set_' . $person;

                $person = new Person();
                $person->set_id($order->$id);

                if ($persons = $this->get_object($person)) {
                    $order->$call(reset($persons));
                }
            }

            $filter = new Type();
            foreach ($order->types as $field => $type_id) {
                $filter->set_id($order->$type_id);
                if ($types = $this->get_object($filter)) {
                    $type = reset($types);
                } else {
                    $type = new Type();
                }
                $order->set_type($type, $field);
            }


            $items = $this->get_order_items($order->id);
            foreach ($items as $item) {
                $order->add_item($item);
            }

            $filter_company = new Company();
            $filter_company->set_id($order->company_id);

            $companies = $this->get_object($filter_company);
            if ($companies) {
                $order->set_company(reset($companies));
            }
            $orders[] = $order;
        }
        if ($return_scalar) {
            return $orders[0];
        } else {
            return $orders;
        }
    }

    public function get_run_orders($company_id, $order_ids, $return_scalar = false) {


        $query = "SELECT id,order_date,report_due,(select name from dr_persons where id=o.sales_rep)sales_rep,id as client_id,operator_id FROM dr_orderss o WHERE company_id=$company_id AND id IN($order_ids)";
        $result = $this->db->query($query);

        $methods = get_class_methods('Order');

        $orders = array();

        foreach ($result->result() as $row_order) {
            $order = new Order();
            foreach ($row_order as $field => $value) {
                $call = "set_$field";
                if (in_array($call, $methods)) {
                    $order->$call($value);
                }
            }

            foreach ($order->persons as $person => $id) {
                $call = 'set_' . $person;

                $person = new Person();
                $person->set_id($order->$id);

                if ($persons = $this->get_object($person)) {
                    $order->$call(reset($persons));
                }
            }


            $items = $this->get_order_items($order->id);
            foreach ($items as $item) {
                $order->add_item($item);
            }

            $filter_company = new Company();
            $filter_company->set_id($order->company_id);

            $companies = $this->get_object($filter_company);
            if ($companies) {
                $order->set_company(reset($companies));
            }
            $orders[] = $order;
        }
        if ($return_scalar) {
            return $orders[0];
        } else {
            return $orders;
        }
    }

    public function get_persons(Person $person) {
        return $this->get_object($person);
        /*
          $filters = array();
          foreach( $person as $field=>$value ){
          if( $value !== null && !is_array($value) && !is_object($value) && $field != 'table'){
          $filters[] = "$field = '$value'";
          }
          }

          $filter = $filters ? 'WHERE ' . implode(' AND ', $filters) : '';
          $query = "SELECT * FROM `dr_persons` $filter";

          $results = $this->db->query( $query );

          $methods = get_class_methods('Person');

          $persons = array();
          foreach( $results->result() as $row_person ){
          $person = new Person();
          foreach( $row_person as $field=>$value ){
          $call = 'set_' . $field;
          if( in_array( $call, $methods )){
          $person->$call( $value );
          }

          }
          $persons[] = $person;
          }
          return $persons; */
    }


    public function get_object(Record $object, $return_scalar = false) {

        if (!$objects = $this->get_from_cache($object)) {
            $filters = array();
            foreach ($object as $field => $value) {
                if ($value !== null && !is_array($value) && !is_object($value) && $field != 'table') {
                    $filters[] = "$field = '$value'";
                }
            }

            $order_by = '';
            if ($object->order_by) {
                $order_by = 'ORDER BY ' . implode(',', $object->order_by);
            }

            $filter = $filters ? 'WHERE ' . implode(' AND ', $filters) : '';
            $query = "SELECT * FROM `$object->table` $filter $order_by";

            $results = $this->db->query($query);

            $class = get_class($object);
            $methods = get_class_methods($class);

            $objects = array();
            foreach ($results->result() as $row_object) {
                $object = new $class;
                foreach ($row_object as $field => $value) {
                    $call = 'set_' . $field;
                    if (in_array($call, $methods)) {
                        $object->$call($value);
                    }
                }
                $object->wake_up();
                $objects[] = $object;
            }

            $this->cache_objects($objects);
        }


        if ($return_scalar) {
            if (isset($objects[0])) {
                return $objects[0];
            } else {
                return null;
            }
        } else {
            return $objects;
        }
    }

    private function cache_objects($objects) {
        foreach ($objects as $object) {
            $this->object_cache[get_class($object)][$object->id] = $object;
        }
    }

    private function get_from_cache(Record $object) {
        $class = get_class($object);
        $return = array();
        if (isset($this->object_cache[$class])) {
            if ($object->id !== null) {
                if (isset($this->object_cache[$class][$object->id])) {
                    $return[] = $this->object_cache[$class][$object->id];
                }
            }
            //TODO add cache search on non id fields.
        }
        return $return;
    }

    private function get_order_items($order_id) {
        $query = "SELECT * FROM `dr_order_items` WHERE order_id = '$order_id'";

        $results = $this->db->query($query);
        $methods = get_class_methods('Item');
        $items = array();
        foreach ($results->result() as $row_result) {
            $item = new Item();
            foreach ($row_result as $field => $value) {
                $call = 'set_' . $field;
                if (in_array($call, $methods)) {
                    $item->$call($value);
                    //throw new Order_model_exception('Invalid record call:' . $call);
                }
            }
            $items[] = $item;
        }
        return $items;
    }

    public function get_us_date_format($date) {
        if (preg_match('!(?P<year>\d\d\d\d)-(?P<month>\d\d)-(?P<day>\d\d)\s*(?P<rest>.*)!', $date, $matches)) {
            $date = $matches['month'] . '-' . $matches['day'] . '-' . $matches['year'] . ' ' . ( isset($matches['rest']) ? ' ' . $matches['rest'] : '' );
        }
        return $date;
    }

}

class Filter {

    public $start_date;
    public $end_date;
    public $agent_id;
    public $inspector_id;
    public $client_id;
    public $company_id;
    public $id;
    public $status;
    public $count;
    public $offset;
    public $buyer;
    public $pending_report;
    public $sales_rep;
    public $order_start_date;
    public $order_end_date;

    public function __construct($id = null, $company_id = null, $start_date = null, $end_date = null, $status = null) {
        $this->id = $id;
        $this->start_date = $start_date;
        $this->end_date = $end_date;
        $this->status = $status;
        $this->company_id = $company_id;
    }

    public function set_start_date($date) {
        $matches = array();
        if (preg_match('/(\d\d)-(\d\d)-(\d\d\d\d)/', $date, $matches)) {
            $date = $matches['3'] . '-' . $matches['1'] . '-' . $matches['2'];
        }
        $this->start_date = $date;
    }

    public function set_end_date($date) {
        $matches = array();
        if (preg_match('/(\d\d)-(\d\d)-(\d\d\d\d)/', $date, $matches)) {
            $date = $matches['3'] . '-' . $matches['1'] . '-' . $matches['2'];
        }
        $this->end_date = $date;
    }

    public function set_agent_id($agent_id) {
        $this->agent_id = $agent_id;
    }

    public function set_inspector_id($inspector_id) {
        $this->inspector_id = $inspector_id;
    }

    public function set_client_id($client_id) {
        $this->client_id = $client_id;
    }

    public function set_order_id($order_id) {
        $this->id = $order_id;
    }

    public function set_count($count) {
        $this->count = $count;
    }

    public function set_offset($offset) {
        $this->offset = $offset;
    }

    public function set_status($status) {
        $this->status = $status;
    }

    public function set_pending_report($status) {
        $this->pending_report = $status;
    }

    public function set_company_id($id) {
        $this->company_id = $id;
    }

    public function set_buyer($id) {
        $this->buyer = $id;
    }

    public function set_sales_rep($id) {
        $this->sales_rep = $id;
    }

    public function set_is_quote($is_quote) {
        $this->is_quote = $is_quote;
    }

    public function set_order_start_date($date) {
        $matches = array();
        if (preg_match('/(\d\d)-(\d\d)-(\d\d\d\d)/', $date, $matches)) {
            $date = $matches['3'] . '-' . $matches['1'] . '-' . $matches['2'];
        }
        $this->order_start_date = $date;
    }

    public function set_order_end_date($date) {
        $matches = array();
        if (preg_match('/(\d\d)-(\d\d)-(\d\d\d\d)/', $date, $matches)) {
            $date = $matches['3'] . '-' . $matches['1'] . '-' . $matches['2'];
        }
        $this->order_end_date = $date;
    }

}

abstract class Record {

    const ACTIVE_RECORD = 1;
    const NONACTIVE_RECORD = 0;
    const SYSTEM_COMPANY_ID = 0;

    /**
     * Used for validation.
     * Contains map fields and corresponding error messages
     * 
     * @var array
     */
    public $errors = array();

    /**
     * DB table this record refers to
     * 
     * @var string
     */
    public $table;

    /**
     * Used for validation. This record field need to be non null
     * 
     * @var array
     */
    protected $required = array();

    /**
     * Record validation function
     * 
     */
    public $active;
    public $id;
    public $company_id;
    public $order_by = array();

    public function __construct($id = null, $company_id = null) {
        if ($id) {
            $this->id = $id;
        }
        /**
         * Company id can be 0
         * - Common goods, shared by all companies 
         */
        $this->company_id = $company_id;
    }

    public function validate() {
        foreach ($this->required as $field) {
            if ($this->$field == null) {
                $this->errors[$field] = 'Must not be null';
            }
        }
        $valid = true;
        if ($this->errors) {
            $valid = false;
        }
        return $valid;
    }

    public function set_active($active) {
        $this->active = $active;
        return $this;
    }

    public function set_order_by(array $columns) {
        $this->order_by = (array) $columns;
        return $this;
    }

    /**
     * Record comparison function
     * 
     * @param Record $record
     */
    public function equal(Record $record) {
        return true;
    }

    public function sleep() {
        
    }

    public function wake_up() {
        
    }

    public function set_id($id) {
        $this->id = $id;
        return $this;
    }

    public function set_company_id($id) {
        $this->company_id = $id;
        return $this;
    }

}

//------------ Order Class ------------------------------

class Order extends Record {

    const ORDER_STATUS_PENDING = '7';
    const ORDER_STATUS_DISPATCHED = '10';
    const ORDER_STATUS_CLOSED = '8';
    const ORDER_STATUS_UNPAID = '9';
    const ORDER_STATUS_PENDING_REPORT = '11';
    const ORDER_STATUS_REPORT_STARTED = '12';
    const ORDER_STATUS_REPORT_COMPLETED = '13';

    public $table = 'dr_orderss';
    protected $required = array(
        //'agent',
        'client',
        'order_date',
        'inspection_date',
        'inspection_time',
        'terms',
        'status',
        'meet_with',
        'address',
        'city',
        'zip',
        'subtotal',
        'total',
        'meet_with',
        'address',
        'city',
        'inspector_id',
        'total',
        'amount_due'
    );
    public $persons = array(
        //'agent' => 'agent_id',
        'client' => 'client_id',
        'inspector' => 'sales_rep'
    );
    public $inspector;
    //public $agent;
    public $client;
    public $types = array(
        'term' => 'terms',
        'inspection' => 'type_of_inspection',
        'structure' => 'type_of_structure',
        'utilities' => 'type_of_utilities',
        'foundation' => 'type_of_foundation',
    );
    //TODO - fix this
    public $term = array();
    public $inspection = array();
    public $structure = array();
    public $utility = array();
    public $foundation = array();
    public $status_string = array();
    // Order Details
    public $items;
    public $order_date;
    public $due_by;
    public $sales_rep;
    public $terms;
    // Production details

    
    public $shipping_method;
    // Order Notes
    public $order_notes;
    public $inspection_date;
    public $inspection_time;
    public $phone_1;
    public $phone_2;
    public $fax;
    public $email;
    public $type_of_inspection;
    public $inspector_id;
    public $type_of_structure;
    public $estimated_age;
    public $estimated_inspection_time;
    public $type_of_foundation;
    public $square_footage;
    public $type_of_utilities;
    public $utility_status;
    public $report_due;
    //Meet with
    public $meet_with;
    public $address;
    public $city;
    public $zip;
    public $state;
    public $access;
    //Notes    
    public $notes_inspection_details;
    public $notes_agent;
    public $notes_order;
    public $notes_date;
    public $notes_by;
    public $utilities;
    //Order price
    public $subtotal = 0;
    public $tax = 0;
    public $shipping_amount = 0;
    public $tax_type;
    public $total = 0;
    public $paid = 0;
    public $payment_n;
    public $notes_charge_date;
    public $amount_due = 0;
    public $buyer;
    public $company;
    public $pool;
    public $spa;
    public $po_text;
    public $agent_id;
    public $client_id;
    public $order_id;
    public $company_id;
    public $id;
    public $operator_id;
    public $is_quote;
    public $company_order_id;

    public function __construct($id = null) {
        $this->id = $id;
        $this->client = new Person(Person::TYPE_CLIENT);
        //$this->agent  = new Person( Person::TYPE_AGENT);
        $this->inspector = new Person(Person::TYPE_INSPECTOR);
    }
    public function set_notes_charge_date($notes_charge_date){
        $this->notes_charge_date = $notes_charge_date;
        return $this;
    }
    public function set_payment_n($payment_n){
        $this->payment_n = $payment_n;
        return $this;
    }
    public function set_id($id) {
        $this->id = $id;
        return $this;
    }

    public function set_operator_id($operatorid) {
        $this->operator_id = $operatorid;
        return $this;
    }

    public function set_inspector(Person $inspector = null) {
        $this->inspector = $inspector;
        $this->inspector_id = $inspector->id;
        return $this;
    }

    public function set_agent(Person $agent) {
        $this->agent = $agent;
        $this->agent_id = $agent->id;
        return $this;
    }

    public function set_client(Person $client) {
        $this->client = $client;
        $this->client_id = $client->id;
        return $this;
    }

    public function set_type(Type $type, $type_name) {
        $this->$type_name = $type;
    }

    public function __set($var, $value) {
        throw new Order_model_exception('Use setter!');
    }

    public function add_item(Item $item) {
        $this->items[] = $item;
        return $this;
    }

    public function set_order_date($date) {
        $matches = array();
        if (preg_match('/(\d\d)-(\d\d)-(\d\d\d\d)/', $date, $matches)) {
            $date = $matches['3'] . '-' . $matches['1'] . '-' . $matches['2'];
        }
        if (!preg_match('/\d\d\d\d-\d\d-\d\d/', $date, $match)) {
            $this->errors['order_date'] = 'Invalid date format';
        }
        $this->order_date = $date;
        return $this;
    }

    public function set_due_by($duedate) {
        $matches = array();
        if (preg_match('/(\d\d)-(\d\d)-(\d\d\d\d)/', $duedate, $matches)) {
            $duedate = $matches['3'] . '-' . $matches['1'] . '-' . $matches['2'];
        }
        if (!preg_match('/\d\d\d\d-\d\d-\d\d/', $duedate, $match)) {
            $this->errors['order_date'] = 'Invalid date format';
        }
        $this->$due_by = $duedate;
        return $this;
    }

    public function set_sales_rep($sales_rep) {
        $this->sales_rep = $sales_rep;
        return $this;
    }

    public function set_shipping_method($shippingmethod) {
        $this->shipping_method = $shippingmethod;
        return $this;
    }
    // Order Notes    
    public function set_order_notes($ordernotes) {
        $this->order_notes = $ordernotes;
        return $this;
    }
    // Order date    
    public function set_notes_date($notes_date) {
        $this->notes_date = $notes_date;
        
        return $this;
    }
    // Order BY    
    public function set_notes_by($notes_by) {
        $this->notes_by = $notes_by;
        return $this;
    }

    public function set_inspection_date($date) {
        $matches = array();
        if (preg_match('/(\d\d)-(\d\d)-(\d\d\d\d)/', $date, $matches)) {
            $date = $matches['3'] . '-' . $matches['1'] . '-' . $matches['2'];
        }
        if (!preg_match('/\d\d\d\d-\d\d-\d\d/', $date, $match)) {
            $this->errors['inspection_date'] = 'Invalid date format';
        }
        $this->inspection_date = $date;
        return $this;
    }

    public function set_inspection_time($time, $front_end_display = false) {
        if (!preg_match('/\d{1,2}:\d{1,2}/', $time, $match)) {
            $this->errors['inspection_time'] = 'Invalid time format';
        }
        $this->inspection_time = $front_end_display ? $time : substr($time, 0, 5);
        return $this;
    }

    public function set_terms($terms) {
        $this->terms = $terms;
        return $this;
    }

    public function set_state($state) {
        $this->state = $state;
        return $this;
    }

    public function set_type_of_inspection($type) {
        $this->type_of_inspection = $type;
        return $this;
    }

    public function set_type_of_structure($type) {
        $this->type_of_structure = $type;
        return $this;
    }

    public function set_estimated_age($age) {
        $this->estimated_age = $age;
        return $this;
    }

    public function set_type_of_foundation($foundation) {
        $this->type_of_foundation = $foundation;
        return $this;
    }

    public function set_square_footage($footage) {
        $this->square_footage = $footage;
        return $this;
    }

    public function set_type_of_utilities($utilites) {
        $this->type_of_utilities = $utilites;
        return $this;
    }

    public function set_utility_status($status) {
        $this->utility_status = $status;
        return $this;
    }

    public function set_notes_inspection_details($notes) {
        $this->notes_inspection_details = $notes;
        return $this;
    }

    public function set_notes_agent($notes) {
        $this->notes_agent = $notes;
        return $this;
    }

    public function set_notes_order($notes) {
        $this->notes_order = $notes;
        return $this;
    }

    public function set_meet_with($meet) {
        $this->meet_with = $meet;
        return $this;
    }

    public function set_address($address) {
        $this->address = $address;
        return $this;
    }

    public function set_city($city) {
        $this->city = $city;
        return $this;
    }

    public function set_zip($zip) {
        $this->zip = $zip;
        return $this;
    }

    public function set_client_id($id) {
        $this->client_id = $id;
        return $this;
    }

    public function set_agent_id($id) {
        $this->agent_id = $id;
        return $this;
    }

    public function set_inspector_id($id) {
        $this->inspector_id = $id;
        return $this;
    }

    public function set_report_due($due) {
        $this->report_due = $due;
        return $this;
    }

    public function set_subtotal($subtotal) {
        if (!is_numeric($subtotal) || $subtotal <= 0) {
            $this->errors['subtotal'] = 'Must be numeric!';
        }
        $this->subtotal = $subtotal;
        return $this;
    }

    public function set_tax($tax) {
        if (!is_numeric($tax)) {
            $this->errors['tax'] = 'Must be numeric!';
        }
        $this->tax = $tax;
        return $this;
    }

    public function set_shipping_amount($shippingamount) {
        if (!is_numeric($shippingamount)) {
            $this->errors['shipping_amount'] = 'Must be numeric!';
        }
        $this->shipping_amount = $shippingamount;
        return $this;
    }

    public function set_tax_type($tax) {
        $this->tax_type = $tax;
        return $this;
    }

    public function set_total($total) {
        if (!is_numeric($total) || $total <= 0) {
            $this->errors['total'] = 'Must be numeric!';
        }
        $this->total = $total;
        return $this;
    }

    public function set_paid($paid) {
        if (!is_numeric($paid) || $paid < 0) {
            $this->errors['paid'] = 'Must be numeric!';
        }
        $this->paid = $paid;
        return $this;
    }

    public function set_amount_due($amount_due) {
        if (!is_numeric($amount_due)) {
            $this->errors['amount_due'] = 'Must be numeric!';
        }
        $this->amount_due = $amount_due;
        return $this;
    }

    public function set_company_id($company_id) {
        $this->company_id = $company_id;
        return $this;
    }

    public function set_estimated_inspection_time($time) {
        $this->estimated_inspection_time = $time;
        return $this;
    }

    public function set_phone_1($phone1) {
        $this->phone_1 = $phone1;
        return $this;
    }

    public function set_phone_2($phone2) {
        $this->phone_2 = $phone2;
        return $this;
    }

    public function set_email($email) {
        $this->email = $email;
        return $this;
    }

    public function set_fax($fax) {
        $this->fax = $fax;
        return $this;
    }

    public function set_buyer($buyer) {
        $this->buyer = $buyer;
        return $this;
    }

    public function set_company($company) {
        $this->company = $company;
        return $this;
    }

    public function set_pool($pool) {
        $this->pool = $pool;
        return $this;
    }

    public function set_spa($spa) {
        $this->spa = $spa;
        return $this;
    }

    public function set_access($access) {
        $this->access = $access;
        return $this;
    }

    public function set_po_text($po_text) {
        $this->po_text = $po_text;
        return $this;
    }

    public function set_is_quote($is_quote) {
        $this->is_quote = $is_quote;
        return $this;
    }

    public function set_company_order_id($company_order_id) {
        $this->company_order_id = $company_order_id;
        return $this;
    }

    public function validate() {
        /* foreach( $this->persons as $object=>$id ){
          if( !$this->$object->validate() ){
          //$this->error .= $this->$object->error;
          }
          } */
        if ($this->items) {
            foreach ($this->items as $item) {
                if (!$item->validate()) {
                    //$this->error .= $item->error;
                }
            }
        } else {
            //TODO order without items
        }
        foreach ($this->required as $field) {
            if ($this->$field === null) {
                $this->errors[$field] = 'Please enter valid value:' . $field;
            }
        }
        return $this->errors ? false : true;
    }

    public function __toString() {
        return '';
    }

    public function equal(Record $order) {
        
    }

}

class Ordersjob extends Record {

    public $table = 'dr_orders_jobs';
    public $id;
    public $order_id;
    public $job_name;
    public $stock;
    public $colors;
    public $size;
    public $quantity;
    public $finishing;
    public $coating;
    public $department;
    public $others;
    public $additional_notes;
    public $created_at;


    protected $required = array(
        'order_id',
        'job_name',
    );
    
    public function __construct($id = null, $order_id = null) {
        $this->id = $id;
        $this->order_id = $order_id;
    }

    public function set_job_name($jobname) {
        $this->job_name = $jobname;
        return $this;
    }

    public function set_stock($stock) {
        $this->stock = $stock;
        return $this;
    }

    public function set_colors($colors) {
        $this->colors = $colors;
        return $this;
    }

    public function set_size($size) {
        $this->size = $size;
        return $this;
    }

    public function set_quantity($quantity) {
        $this->quantity = $quantity;
        return $this;
    }

    public function set_finishing($finishing) {
        $this->finishing = $finishing;
        return $this;
    }

    public function set_coating($coating) {
        $this->coating = $coating;
        return $this;
    }
    
    public function set_department($department) {
        $this->department = $department;
        return $this;
    }

    public function set_others($others) {
        $this->others = $others;
        return $this;
    }

    public function set_additional_notes($additionalnotes) {
        $this->additional_notes = $additionalnotes;
        return $this;
    }
    
    public function set_created_at($created_at) {
        $this->created_at = $created_at;
        return $this;
    }

    public function validate() {

        foreach ($this->required as $field) {
            if ($this->$field === null) {
                $this->errors[$field] = 'Please enter valid value:' . $field;
            }
        }
        return $this->errors ? false : true;
    }

    public function equal(Record $record) {
        
    }

}

class Ordersshirt extends Record {

    public $table = 'dr_orders_shirts';
    public $id;
    public $order_id;
    public $job_name;
    public $type;
    public $front_color;
    public $back_color;
    public $quantity_size;
    public $department;
    public $others;
    public $additional_notes;
    public $created_at;
    
    protected $required = array(
        'order_id',
        'job_name',
    );
    
    public function __construct($id = null, $order_id = null) {
        $this->id = $id;
        $this->order_id = $order_id;
    }

    public function set_job_name($jobname) {
        $this->job_name = $jobname;
        return $this;
    }

    public function set_type($type) {
        $this->type = $type;
        return $this;
    }

    public function set_front_color($front_color) {
        $this->front_color = $front_color;
        return $this;
    }

    public function set_back_color($back_color) {
        $this->back_color = $back_color;
        return $this;
    }

    public function set_quantity_size($quantity_size) {
        $this->quantity_size = $quantity_size;
        return $this;
    }
    public function set_created_at($created_at) {
        $this->created_at = $created_at;
        return $this;
    }
    public function set_others($others) {
        $this->others = $others;
        return $this;
    }
    public function set_additional_notes($additional_notes) {
        $this->additional_notes = $additional_notes;
        return $this;
    }
    public function set_department($department) {
        $this->department = $department;
        return $this;
    }

    public function validate() {

        foreach ($this->required as $field) {
            if ($this->$field === null) {
                $this->errors[$field] = 'Please enter valid value:' . $field;
            }
        }
        return $this->errors ? false : true;
    }

    public function equal(Record $record) {
        
    }

}

// ----------- End Order Class --------------------------
//------------ Start Person Class -----------------------

class Person extends Record {

    public $table = 'dr_persons';

    const TYPE_AGENT = 1;
    const TYPE_USER = 4;
    const TYPE_ADMIN = 5;
    const TYPE_INSPECTOR = 2;
    const TYPE_CLIENT = 3;
    const TYPE_SYSTEM = 6;
    const TYPE_COMPANY = 7;

    public $name;
    public $contact;
    public $address;
    public $city;
    public $state;
    public $zip;
    public $phone_1;
    public $phone_2;
    public $email;
    public $email_2;
    public $email_3;
    public $fax;
    public $datetime;
    public $password;
    public $id;
    public $company_id;
    public $person_type;
    public $active;
    public $notes;
    public $company;
    public $how_did_you_hear_about_us;
    public $timeoff_date_start;
    public $timeoff_time_start;
    public $timeoff_date_end;
    public $timeoff_time_end;
    public $address_2;
    public $referred_by;
    public $required = array(
        'name',
        'person_type',
        'company_id',
        'email'
    );

    public function __construct($type = null, $id = null, $company_id = null) {
        if ($type) {
            $this->person_type = $type;
        }
        parent::__construct($id, $company_id);
    }

    public function __set($var, $value) {
        throw new Order_model_exception('Use setter!');
    }

    public function set_address_2($address2) {
        $this->address_2 = $address2;
        return $this;
    }

    public function set_referred_by($referred_by) {
        $this->referred_by = $referred_by;
        return $this;
    }

    public function set_id($id) {
        $this->id = $id;
        return $this;
    }

    public function set_name($name) {
        $this->name = $name;
        return $this;
    }
    
    public function set_contact($contact) {
        $this->contact = $contact;
        return $this;
    }

    public function set_city($city) {
        $this->city = $city;
        return $this;
    }

    public function set_address($address) {
        $this->address = $address;
        return $this;
    }

    public function set_zip($zip) {
        $this->zip = $zip;
        return $this;
    }

    public function set_state($state) {
        $this->state = $state;
        return $this;
    }

    public function set_phone_1($phone1) {
        if ($phone1) {
            if ($this->check_phone($phone1)) {
                $this->phone_1 = $phone1;
            } else {
                $this->errors['phone_1'] = 'Invalid phone format';
            }
        }
        return $this;
    }

    public function set_phone_2($phone2) {
        if ($phone2) {
            if ($this->check_phone($phone2)) {
                $this->phone_2 = $phone2;
            } else {
                $this->errors['phone_2'] = 'Invalid phone format';
            }
        }
        return $this;
    }

    public function set_email($email) {
        if ($email) {
            $email = strtolower($email);
            if (preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/", $email)) {
                $this->email = $email;
            } else {
                $this->errors['email'] = 'Invalid email format';
            }
        }
        return $this;
    }

    public function set_email_2($email2) {
        if ($email2) {
            $email2 = strtolower($email2);
            if (preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/", $email2)) {
                $this->email_2 = $email2;
            } else {
                $this->errors['email_2'] = 'Invalid email format';
            }
        }
        return $this;
    }

    public function set_email_3($email3) {
        if ($email3) {
            $email3 = strtolower($email3);
            if (preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/", $email3)) {
                $this->email_3 = $email3;
            } else {
                $this->errors['email_3'] = 'Invalid email format';
            }
        }
        return $this;
    }

    public function set_fax($fax) {
        if ($fax) {
            if ($this->check_phone($fax)) {
                $this->fax = $fax;
            } else {
                $this->errors['fax'] = 'Invalid phone format';
            }
            $this->fax = $fax;
        }
        return $this;
    }

    public function set_person_type($type) {
        $this->person_type = $type;
        return $this;
    }

    public function set_datetime($datetime) {
        $this->datetime = $datetime;
        return $this;
    }

    public function set_company_id($company_id) {
        $this->company_id = $company_id;
        return $this;
    }

    public function set_password($password) {
        $this->password = $password;
        return $this;
    }

    public function equal(Record $person) {
        return false;
    }

    public function set_active($active) {
        $this->active = $active;
        return $this;
    }

    public function set_notes($notes) {
        $this->notes = $notes;
        return $this;
    }

    public function set_company($active) {
        $this->company = $active;
        return $this;
    }

    public function set_how_did_you_hear_about_us($us) {
        $this->how_did_you_hear_about_us = $us;
        return $this;
    }

    public function set_timeoff_date_start($date) {
        if ($date) {
            $matches = array();
            if (preg_match('/(\d\d)-(\d\d)-(\d\d\d\d)/', $date, $matches)) {
                $date = $matches['3'] . '-' . $matches['1'] . '-' . $matches['2'];
            }
            if (!preg_match('/\d\d\d\d-\d\d-\d\d/', $date, $match)) {
                $this->errors['timeoff_date_start'] = 'Invalid date format';
            }
            $this->timeoff_date_start = $date;
        }
    }

    public function set_timeoff_date_end($date) {
        if ($date) {
            $matches = array();
            if (preg_match('/(\d\d)-(\d\d)-(\d\d\d\d)/', $date, $matches)) {
                $date = $matches['3'] . '-' . $matches['1'] . '-' . $matches['2'];
            }
            if (!preg_match('/\d\d\d\d-\d\d-\d\d/', $date, $match)) {
                $this->errors['timeoff_date_end'] = 'Invalid date format';
            }
            $this->timeoff_date_end = $date;
        }
    }

    public function set_timeoff_time_start($date) {
        $this->timeoff_time_start = $date;
    }

    public function set_timeoff_time_end($date) {
        $this->timeoff_time_end = $date;
    }

    public function __toString() {
        return '';
    }

    protected function check_phone($phone) {
        $valid = false;
        if (preg_match('/\d+(-|\s+\d+)*/', $phone)) {
            $valid = true;
        }
        return $valid;
    }

    public function validate() {
        foreach ($this->required as $field) {
            if ($this->$field == null) {

                if ($this->person_type == Person::TYPE_AGENT and $field == 'email') {
                    //unset($this->errors[ 'email' ]);
                } else {
                    $this->errors[$field] = 'Must not be null';
                }
            }
        }

        if (!isset($this->errors['email']) and $this->email != '') {
            $CI = & get_instance();

            if ($this->id != '') {
                $query = $CI->db->query("select * from dr_persons where id!=" . $this->id . " and email='" . $this->email . "'");
                if ($query->num_rows() > 0) {
                    $row = $query->row();
                    $this->errors['email'] = 'This email is already in our system under "' . $row->name . '", please use another email.';
                }
            } else {
                $query = $CI->db->query("select * from dr_persons where email='" . $this->email . "'");
                if ($query->num_rows() > 0) {
                    $row = $query->row();
                    $this->errors['email'] = 'This email is already in our system under "' . $row->name . '", please use another email.';
                }
            }
        }

        //if( $this->person_type == self::TYPE_CLIENT && isset($this->errors['name'])){
        //unset( $this->errors['name']);
        //}
        $valid = true;
        if ($this->errors) {
            $valid = false;
        }
        return $valid;
    }

}

//------------ End Person Class -----------------------



class Company extends Record {

    public $table = 'dr_companies';
    public $name;
    public $address;
    public $city;
    public $state;
    public $zip;
    public $phone_1;
    public $phone_2;
    public $email;
    public $fax;
    public $payment_gateway;
    public $merchant;
    public $transaction;
    public $active;
    public $datetime;
    public $id;
    public $time_zone;
    public $primary_contact;
    public $stripe_id;
    public $expire_company;
    public $subscription_id;
    public $required = array(
        'name',
        'city',
        'address',
        'state',
        'phone_1',
        'email',
    );

    public function __construct() {
        
    }

    public function __set($var, $value) {
        throw new Order_model_exception('Use setter!');
    }

    public function set_id($id) {
        $this->id = $id;
        return $this;
    }

    public function set_name($name) {
        $this->name = $name;
        return $this;
    }

    public function set_city($city) {
        $this->city = $city;
        return $this;
    }

    public function set_address($address) {
        $this->address = $address;
        return $this;
    }

    public function set_zip($zip) {
        $this->zip = $zip;
        return $this;
    }

    public function set_state($state) {
        $this->state = $state;
        return $this;
    }

    public function set_phone_1($phone1) {
        $this->phone_1 = $phone1;
        return $this;
    }

    public function set_phone_2($phone2) {
        $this->phone_2 = $phone2;
        return $this;
    }

    public function set_email($email) {
        $this->email = $email;
        return $this;
    }

    public function set_fax($fax) {
        $this->fax = $fax;
        return $this;
    }

    public function set_datetime($datetime) {
        $this->datetime = $datetime;
        return $this;
    }

    public function set_active($active) {
        $this->active = $active;
        return $this;
    }

    public function set_payment_gateway($active) {
        $this->payment_gateway = $active;
        return $this;
    }

    public function set_time_zone($time_zone) {
        $this->time_zone = $time_zone;
        return $this;
    }

    public function set_merchant($active, $enc = true) {
        $this->merchant = $active;
        return $this;
    }

    public function set_transaction($active, $enc = true) {
        $this->transaction = $active;
        return $this;
    }

    public function set_primary_contact($primary_contact) {
        $this->primary_contact = $primary_contact;
        return $this;
    }

    public function set_stripe_id($stripe_id) {
        $this->stripe_id = $stripe_id;
        return $this;
    }

    public function set_expire_company($expire_company) {
        $this->expire_company = $expire_company;
        return $this;
    }

    public function set_subscription_id($subscription_id) {
        $this->subscription_id = $subscription_id;
        return $this;
    }

    public function equal(Record $person) {
        return false;
    }

    public function __toString() {
        return '';
    }

}

class Item_order extends Record {

    public $table = 'dr_order_items';
    public $item_id;
    public $order_id;
    public $quantity;
    public $description;
    public $price;

    public function __set($var, $value) {
        throw new Order_model_exception('Use setter!' . $var);
    }

    public function set_id($id) {
        $this->id = $id;
        return $this;
    }

    public function set_order_id($id) {
        $this->order_id = $id;
        return $this;
    }

    public function set_quantity($quantity) {
        $this->quantity = $quantity;
        return $this;
    }

    public function set_price($price) {
        $this->price = $price;
        return $this;
    }

    public function set_description($description) {
        $this->description = $description;
        return $this;
    }

    public function __toString() {
        return '';
    }

    public function equal(Record $item) {
        return false;
    }

}

class Item extends Record {

    public $table = 'dr_order_items';
    public $description;
    public $price;
    public $order_id;
    public $item_id;
    public $name;
    public $required = array(
        'item_id',
        'description',
        'price'
    );

    public function set_item_id($id) {
        if (!is_numeric($id)) {
            $this->errors['id'] = 'Invalid id value';
        }
        $this->item_id = (int) $id;
        return $this;
    }

    public function set_order_id($id) {
        $this->order_id = (int) $id;
        return $this;
    }

    public function set_price($price) {
        if (!preg_match('/\d+(\.\d\d)?/', $price)) {
            $this->errors['price'] = 'Invalid price value';
        }
        $this->price = (float) $price;
        return $this;
    }

    public function set_description($description) {
        $this->description = $description;
        return $this;
    }

    public function set_company_id($company_id) {
        $this->company_id = (int) $company_id;
        return $this;
    }

    public function set_active($active) {
        $this->active = $active;
        return $this;
    }

    public function set_name($active) {
        $this->name = $active;
        return $this;
    }

    public function __toString() {
        return '';
    }

    public function equal(Record $item) {
        return false;
    }

}

class Base_item extends Record {

    public $table = 'dr_item';
    public $name;
    public $description;
    public $price;
    public $company_id;
    public $id;
    public $required = array(
        'description',
        'price'
    );

    public function set_id($id) {
        $this->id = (int) $id;
        return $this;
    }

    public function set_name($name) {
        $this->name = $name;
        return $this;
    }

    public function set_price($price) {
        if (!preg_match('/\d+(\.\d\d)?/', $price)) {
            $this->errors['price'] = 'Invalid price value';
        }
        $this->price = (float) $price;
        return $this;
    }

    public function set_description($description) {
        $this->description = $description;
        return $this;
    }

    public function set_company_id($company_id) {
        $this->company_id = (int) $company_id;
        return $this;
    }

    public function set_active($active) {
        $this->active = $active;
        return $this;
    }

    public function __toString() {
        return '';
    }

    public function equal(Record $item) {
        return false;
    }

}

class Message extends Record {

    const TYPE_MSG = 1;
    const TYPE_TODO = 2;

    public $table = 'dr_messages';
    public $required = array(
    );
    public $id;
    public $type;
    public $from_user_id;
    public $to_user_id;
    public $subject;
    public $message;
    public $datetime;

    public function set_id($id) {
        $this->id = $id;
    }

    public function set_from_user_id($id) {
        $this->from_user_id = $id;
    }

    public function set_to_user_id($id) {
        $this->to_user_id = $id;
    }

    public function set_subject($subject) {
        
    }

    public function set_type($type) {
        $this->type = $type;
    }

    public function set_message($message) {
        $this->message = $message;
        $this->subject = substr($message, 0, 20);
    }

    public function set_datetime($datetime) {
        $this->datetime = $datetime;
    }

    public function equal(Record $item) {
        return $true;
    }

}

class Tax extends Record {

    const TAX_TYPE_PERC = '1';
    const TAX_TYPE_FIX = '2';

    public $table = 'dr_taxes';
    public $name;
    public $description;
    public $value;
    //public $type;
    public $company_id;
    public $id;
    public $active;

    public function set_name($tax_name) {
        $this->name = $tax_name;
    }

    public function set_description($description) {
        $this->description = $description;
    }

    public function set_value($value) {
        $this->value = $value;
    }

    public function set_type($type) {
        $this->type = $type;
    }

    public function set_company_id($company_id) {
        $this->company_id = $company_id;
    }

    public function set_id($id) {
        $this->id = $id;
    }

    public function set_active($active) {
        $this->active = $active;
        return $this;
    }

    public function validate() {
        return true;
    }

    public function equal(Record $record) {
        return false;
    }

}

// Declare header menu item class
class Coating extends Record {

    public $table = 'coating';
    public $name;
    public $id;
    public $active;

    public function set_name($menu_name) {
        $this->name = $menu_name;
    }

    public function set_id($id) {
        $this->id = $id;
    }

    public function set_active($active) {
        $this->active = $active;
        return $this;
    }

    public function validate() {
        return true;
    }

    public function equal(Record $record) {
        return false;
    }

}

// Declare header menu item class
class MenuItem extends Record {

    public $table = 'jobstatus';
    public $name;
    public $id;
    public $active;
    public $company_id;

    public function set_name($menu_name) {
        $this->name = $menu_name;
    }

    public function set_id($id) {
        $this->id = $id;
    }

    public function set_company_id($company_id) {
        $this->company_id = $company_id;
    }

    public function set_active($active) {
        $this->active = $active;
        return $this;
    }

    public function validate() {
        return true;
    }

    public function equal(Record $record) {
        return false;
    }

}

// Declare Sales Representive class
class SalesRep extends Record {

    public $table = 'dt_salesrep';
    public $name;
    public $id;
    public $active;

    public function set_name($salesrep_name) {
        $this->name = $salesrep_name;
    }

    public function set_id($id) {
        $this->id = $id;
    }

    public function set_active($active) {
        $this->active = $active;
        return $this;
    }

    public function validate() {
        return true;
    }

    public function equal(Record $record) {
        return false;
    }

}

// Declare Terms class
class Terms extends Record {

    public $table = 'terms';
    public $name;
    public $id;
    public $active;
    public $required = array(
        'name'
    );

    public function set_name($jobstaus_name) {
        $this->name = $jobstaus_name;
    }

    public function set_id($id) {
        $this->id = $id;
    }

    public function set_active($active) {
        $this->active = $active;
        return $this;
    }

    public function validate() {
        foreach ($this->required as $field) {
            if ($this->$field == null) {
                $this->errors[$field] = 'Must not be null';
            }
        }

        $valid = true;
        if ($this->errors) {
            $valid = false;
        }
        return $valid;
    }

    public function equal(Record $record) {
        return false;
    }

}

// Declare Job Status class
class JobStatus extends Record {

    public $table = 'jobstatus';
    public $name;
    public $id;
    public $active;
    public $required = array(
        'name'
    );

    public function set_name($jobstaus_name) {
        $this->name = $jobstaus_name;
    }

    public function set_id($id) {
        $this->id = $id;
    }

    public function set_active($active) {
        $this->active = $active;
        return $this;
    }

    public function validate() {

        foreach ($this->required as $field) {
            if ($this->$field == null) {
                $this->errors[$field] = 'Must not be null';
            }
        }

        if (!isset($this->errors['name'])) {
            $CI = & get_instance();

            if ($this->id != '') {
                $query = $CI->db->query("select id from jobstatus where company_id = " . $this->company_id . " and id!=" . $this->id . " and name='" . $this->name . "'");
                if ($query->num_rows() > 0) {
                    $this->errors['name'] = 'This department is already in our system, please use another status';
                }
            } else {
                $query = $CI->db->query("select id from jobstatus where company_id = " . $this->company_id . " and name='" . $this->name . "'");
                if ($query->num_rows() > 0) {
                    $this->errors['name'] = 'This department is already in our system, please use another status';
                }
            }
        }


        $valid = true;
        if ($this->errors) {
            $valid = false;
        }
        return $valid;
    }

    public function equal(Record $record) {
        return false;
    }

}

// Declare Stock class
class Stock extends Record {

    public $table = 'dr_stock';
    public $name;
    public $id;
    public $active;
    public $required = array(
        'name'
    );

    public function set_name($jobstaus_name) {
        $this->name = $jobstaus_name;
    }

    public function set_id($id) {
        $this->id = $id;
    }

    public function set_active($active) {
        $this->active = $active;
        return $this;
    }

    public function validate() {

        foreach ($this->required as $field) {
            if ($this->$field == null) {
                $this->errors[$field] = 'Must not be null';
            }
        }

        $valid = true;
        if ($this->errors) {
            $valid = false;
        }
        return $valid;
    }

    public function equal(Record $record) {
        return false;
    }

}

// Declare Colors class
class Colors extends Record {

    public $table = 'dr_colors';
    public $name;
    public $id;
    public $active;
    public $required = array(
        'name'
    );

    public function set_name($jobstaus_name) {
        $this->name = $jobstaus_name;
    }

    public function set_id($id) {
        $this->id = $id;
    }

    public function set_active($active) {
        $this->active = $active;
        return $this;
    }

    public function validate() {

        foreach ($this->required as $field) {
            if ($this->$field == null) {
                $this->errors[$field] = 'Must not be null';
            }
        }

        $valid = true;
        if ($this->errors) {
            $valid = false;
        }
        return $valid;
    }

    public function equal(Record $record) {
        return false;
    }

}

// Declare Size class
class Size extends Record {

    public $table = 'dr_size';
    public $name;
    public $id;
    public $active;
    public $required = array(
        'name'
    );

    public function set_name($jobstaus_name) {
        $this->name = $jobstaus_name;
    }

    public function set_id($id) {
        $this->id = $id;
    }

    public function set_active($active) {
        $this->active = $active;
        return $this;
    }

    public function validate() {

        foreach ($this->required as $field) {
            if ($this->$field == null) {
                $this->errors[$field] = 'Must not be null';
            }
        }

        $valid = true;
        if ($this->errors) {
            $valid = false;
        }
        return $valid;
    }

    public function equal(Record $record) {
        return false;
    }

}

// Declare Quantity class
class Quantity extends Record {

    public $table = 'dr_quantity';
    public $name;
    public $id;
    public $active;
    public $required = array(
        'name'
    );

    public function set_name($jobstaus_name) {
        $this->name = $jobstaus_name;
    }

    public function set_id($id) {
        $this->id = $id;
    }

    public function set_active($active) {
        $this->active = $active;
        return $this;
    }

    public function validate() {

        foreach ($this->required as $field) {
            if ($this->$field == null) {
                $this->errors[$field] = 'Must not be null';
            }
        }

        $valid = true;
        if ($this->errors) {
            $valid = false;
        }
        return $valid;
    }

    public function equal(Record $record) {
        return false;
    }

}

// Declare Finishing class
class Finishing extends Record {

    public $table = 'dr_finishing';
    public $name;
    public $id;
    public $active;
    public $required = array(
        'name'
    );

    public function set_name($jobstaus_name) {
        $this->name = $jobstaus_name;
    }

    public function set_id($id) {
        $this->id = $id;
    }

    public function set_active($active) {
        $this->active = $active;
        return $this;
    }

    public function validate() {

        foreach ($this->required as $field) {
            if ($this->$field == null) {
                $this->errors[$field] = 'Must not be null';
            }
        }

        $valid = true;
        if ($this->errors) {
            $valid = false;
        }
        return $valid;
    }

    public function equal(Record $record) {
        return false;
    }

}

// Declare Coating class
class Coatings extends Record {

    public $table = 'dr_coating';
    public $name;
    public $id;
    public $active;
    public $required = array(
        'name'
    );

    public function set_name($jobstaus_name) {
        $this->name = $jobstaus_name;
    }

    public function set_id($id) {
        $this->id = $id;
    }

    public function set_active($active) {
        $this->active = $active;
        return $this;
    }

    public function validate() {

        foreach ($this->required as $field) {
            if ($this->$field == null) {
                $this->errors[$field] = 'Must not be null';
            }
        }

        $valid = true;
        if ($this->errors) {
            $valid = false;
        }
        return $valid;
    }

    public function equal(Record $record) {
        return false;
    }

}

class ShirtType extends Record {

    public $table = 'dr_shirttype';
    public $name;
    public $id;
    public $active;
    public $required = array(
        'name'
    );

    public function set_name($jobstaus_name) {
        $this->name = $jobstaus_name;
    }

    public function set_id($id) {
        $this->id = $id;
    }

    public function set_active($active) {
        $this->active = $active;
        return $this;
    }

    public function validate() {

        foreach ($this->required as $field) {
            if ($this->$field == null) {
                $this->errors[$field] = 'Must not be null';
            }
        }

        $valid = true;
        if ($this->errors) {
            $valid = false;
        }
        return $valid;
    }

    public function equal(Record $record) {
        return false;
    }

}

class ShirtBackColor extends Record {

    public $table = 'dr_shirt_backcolor';
    public $name;
    public $id;
    public $active;
    public $required = array(
        'name'
    );

    public function set_name($jobstaus_name) {
        $this->name = $jobstaus_name;
    }

    public function set_id($id) {
        $this->id = $id;
    }

    public function set_active($active) {
        $this->active = $active;
        return $this;
    }

    public function validate() {

        foreach ($this->required as $field) {
            if ($this->$field == null) {
                $this->errors[$field] = 'Must not be null';
            }
        }

        $valid = true;
        if ($this->errors) {
            $valid = false;
        }
        return $valid;
    }

    public function equal(Record $record) {
        return false;
    }

}

class ShirtFrontColor extends Record {

    public $table = 'dr_shirt_frontcolor';
    public $name;
    public $id;
    public $active;
    public $required = array(
        'name'
    );

    public function set_name($jobstaus_name) {
        $this->name = $jobstaus_name;
    }

    public function set_id($id) {
        $this->id = $id;
    }

    public function set_active($active) {
        $this->active = $active;
        return $this;
    }

    public function validate() {

        foreach ($this->required as $field) {
            if ($this->$field == null) {
                $this->errors[$field] = 'Must not be null';
            }
        }

        $valid = true;
        if ($this->errors) {
            $valid = false;
        }
        return $valid;
    }

    public function equal(Record $record) {
        return false;
    }

}

class ShirtSize extends Record {

    public $table = 'dr_shirt_size';
    public $name;
    public $id;
    public $active;
    public $required = array(
        'name'
    );

    public function set_name($jobstaus_name) {
        $this->name = $jobstaus_name;
    }

    public function set_id($id) {
        $this->id = $id;
    }

    public function set_active($active) {
        $this->active = $active;
        return $this;
    }

    public function validate() {

        foreach ($this->required as $field) {
            if ($this->$field == null) {
                $this->errors[$field] = 'Must not be null';
            }
        }

        $valid = true;
        if ($this->errors) {
            $valid = false;
        }
        return $valid;
    }

    public function equal(Record $record) {
        return false;
    }

}

// Declare Operator class
class OperatorList extends Record {

    public $table = 'operators';
    public $name;
    public $id;
    public $company_id;
    public $active;
    public $required = array(
        'name'
    );

    public function set_name($jobstaus_name) {
        $this->name = $jobstaus_name;
    }

    public function set_id($id) {
        $this->id = $id;
    }

    public function set_company_id($companyid) {
        $this->company_id = $companyid;
    }

    public function set_active($active) {
        $this->active = $active;
        return $this;
    }

    public function validate() {
        foreach ($this->required as $field) {
            if ($this->$field == null) {
                $this->errors[$field] = 'Must not be null';
            }
        }

        $valid = true;
        if ($this->errors) {
            $valid = false;
        }
        return $valid;
    }

    public function equal(Record $record) {
        return false;
    }

}

// Declare Shipping class
class ShippingMethod extends Record {

    public $table = 'shippingmethod';
    public $name;
    public $id;
    public $active;
    public $charges;

    public function set_name($shipping_name) {
        $this->name = $shipping_name;
    }

    public function set_charges($chargesamount) {
        $this->charges = $chargesamount;
    }

    public function set_active($activeitem) {
        $this->active = $activeitem;
    }

    public function set_id($id) {
        $this->id = $id;
    }

    public function validate() {
        return true;
    }

    public function equal(Record $record) {
        return false;
    }

}

class Type extends Record {

    const TYPE_TERM = '1';
    const TYPE_UTILITY = '2';
    const TYPE_FOUNDATION = '3';
    const TYPE_INSPECTION = '4';
    const TYPE_STRUCTURE = '5';
    const TYPE_UTILITY_STATUS = '16';
    const TYPE_GENERAL_REMARKS = '17';

    public $table = 'dr_types';
    public $type;
    public $name;
    public $description;
    public $company_id;
    public $id;
    public $active;

    public function __construct($id = null, $company_id = null, $name = null, $description = null, $type = null, $active = null) {
        $this->name = $name;
        $this->description = $description;
        $this->type = $type;
        $this->active = $active;
        parent::__construct($id, $company_id);
    }

    public function set_type($type) {
        $this->type = $type;
    }

    public function set_name($name) {
        $this->name = $name;
    }

    public function set_company_id($id) {
        $this->company_id = $id;
    }

    public function set_id($id) {
        $this->id = $id;
    }

    public function set_description($description) {
        $this->description = $description;
    }

    public function set_active($active) {
        $this->active = $active;
        return $this;
    }

    public function validate() {
        return true;
    }

    public function equal(Record $record) {
        
    }

}

class Transaction extends Record {

    public $table = 'dr_transactions';
    public $order_id;
    public $id;
    public $message;
    public $amount;
    public $request;
    public $datetime;
    public $response;
    public $status;

    public function set_id($order_id) {
        $this->id = $order_id;
        return $this;
    }

    public function set_order_id($order_id) {
        $this->order_id = $order_id;
        return $this;
    }

    public function set_amount($amount) {
        $this->amount = $amount;
        return $this;
    }

    public function set_request($request) {
        $this->request = $request;
        return $this;
    }

    public function set_response($response) {
        $this->response = $response;
        return $this;
    }

    public function set_datetime($datetime) {
        $this->datetime = $datetime;
        return $this;
    }

    public function set_status($status) {
        $this->status = $status;
        return $this;
    }

    public function set_message($status) {
        $this->message = $status;
        return $this;
    }

    public function equal(Record $record) {
        return true;
    }

}

/*
  CREATE TABLE `dr_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `address` tinytext,
  `inspector_id` int(11) DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `inspection_date` date DEFAULT NULL,
  `inspection_time` varchar(5) DEFAULT NULL,
  `property_status` varchar(10) DEFAULT NULL,
  `attendees` text,
  `comments` text,
  PRIMARY KEY (`id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1
 */

class Report extends Template {

    protected $required = array(
        'template_id',
        'property_id',
        'property_status',
        'inspection_date',
        'order_id',
    );

    const STATUS_GOOD = 'good';
    const STATUS_FAIR = 'fair';
    const STATUS_POOR = 'poor';

    public $table = 'dr_report';
    public $order_id;
    public $id;
    public $company_id;
    public $address;
    public $city;
    public $state;
    public $zip;
    public $inspector_id;
    public $agent_id;
    public $client_id;
    public $property_id;
    public $template_id;
    public $inspection_date;
    public $inspection_time;
    public $estimated_age;
    public $property_status;
    public $report_status;
    public $categories;
    public $attendees;
    public $comments;
    public $type_ids = array();

    public function __construct($id = null, $company_id = null, $categories = null, $order_id = null) {
        $this->company_id = $company_id;
        $this->id = $id;
        $this->_agent = new Person();
        $this->_client = new Person();
        $this->_inspector = new Person();
        $this->_order = new Order();
        $this->_property = new Property();
        $this->_template = new Template();
        $this->_address = null;
        $this->_types_selected = array();
        $this->_all_types = array();
        $this->_general_disclosures = array();
        $this->_general_selected = array();
        $this->_pictures = array();
        $this->order_id = $order_id;
        $this->description = null;
    }

    public function set_id($order_id) {
        $this->id = $order_id;
        return $this;
    }

    public function set_company_id($company_id) {
        $this->company_id = $company_id;
        return $this;
    }

    public function set_city($city) {
        $this->city = $city;
        return $this;
    }

    public function set_address($address) {
        $this->address = $address;
        return $this;
    }

    public function set_zip($zip) {
        $this->zip = $zip;
        return $this;
    }

    public function set_state($state) {
        $this->state = $state;
        return $this;
    }

    public function set_estimated_age($var) {
        $this->estimated_age = $var;
        return $this;
    }

    public function set_inspector_id($var) {
        if (is_numeric($var)) {
            $this->inspector_id = $var;
        } else {
            $this->errors['inspector_id'] = 'Must be numeric';
        }
        return $this;
    }

    public function set_agent_id($var) {
        if (is_numeric($var)) {
            $this->agent_id = $var;
        } else {
            $this->errors['agent_id'] = 'Must be numeric';
        }
        return $this;
    }

    public function set_client_id($var) {
        if (is_numeric($var)) {
            $this->client_id = $var;
        } else {
            $this->errors['client_id'] = 'Must be numeric';
        }
        $this->client_id = $var;
        return $this;
    }

    public function set_template_id($var) {
        if (is_numeric($var)) {
            $this->template_id = $var;
        } else {
            $this->errors['template_id'] = 'Must be numeric';
        }
        return $this;
    }

    public function set_property_id($var) {
        if (is_numeric($var)) {
            $this->property_id = $var;
        } else {
            $this->errors['property_id'] = 'Must be numeric';
        }
        return $this;
    }

    public function set_inspection_date($date) {
        $matches = array();
        if (preg_match('/(\d\d)-(\d\d)-(\d\d\d\d)/', $date, $matches)) {
            $date = $matches['3'] . '-' . $matches['1'] . '-' . $matches['2'];
        }
        $this->inspection_date = $date;
        if (!preg_match('/\d\d\d\d-\d\d-\d\d/', $this->inspection_date, $match)) {
            $this->errors['inspection_date'] = 'Invalid date format';
        }
        return $this;
    }

    public function set_inspection_time($var) {
        $this->inspection_time = $var;
        return $this;
    }

    public function set_property_status($var) {
        $this->property_status = $var;
        return $this;
    }

    public function set_attendees($var) {
        $this->attendees = $var;
        return $this;
    }

    public function set_comments($comments) {
        $this->comments = $comments;
        return $this;
    }

    public function set_categories($categories) {
        $this->categories = $categories;
        return $this;
    }

    public function set_type_ids($types) {
        $this->type_ids = $types;
        return $this;
    }

    public function set_order_id($order_id) {
        $this->order_id = $order_id;
        return $this;
    }

    public function add_utility_types($types) {
        $this->_types_selected = $types;
        return $this;
    }

    public function add_general_disclusures($types) {
        $this->_general_disclosures = $types;
        return $this;
    }

    public function add_agent($types) {
        if ($types) {
            $this->_agent = $types;
            $this->agent_id = $this->_agent->id;
        }
        return $this;
    }

    public function add_client($types) {
        if ($types) {
            $this->_client = $types;
            $this->client_id = $this->_client->id;
        }
        return $this;
    }

    public function add_inspector($types) {
        if ($types) {
            $this->_inspector = $types;
            $this->inspector_id = $this->_inspector->id;
        }
        return $this;
    }

    public function set_description($description) {
        $this->description = null;
    }

    public function add_order(Order $order) {
        if ($order) {
            //TODO Remove adding this fields if they are overriden
            $this->_order = $order;
            $this->order_id = $this->_order->id;
            $this->_agent = $order->agent;
            $this->agent_id = $order->agent_id;
            $this->client_id = $order->client_id;
            $this->_client = $order->client;
            $this->inspector_id = $order->inspector_id;
            $this->_inspector = $order->inspector;

            $array = array();
            $array[] = $order->address;
            $array[] = $order->city;
            $array[] = $order->zip;
            $array[] = $order->state;
            $this->_address = implode(', ', $array);
            $this->_address = implode(', ', $array);
            $this->inspection_date = $order->inspection_date;

            $this->address = $order->address;
            $this->city = $order->city;
            $this->zip = $order->zip;
            $this->state = $order->state;
        }
        return $this;
    }

    public function add_types_selected($types) {
        if (is_array($types)) {
            foreach ($types as $type) {
                $this->type_ids[] = $type->id;
                if ($type->type == Type::TYPE_UTILITY_STATUS) {
                    $this->_types_selected[] = $type;
                } else {
                    $this->_general_selected[] = $type;
                }
            }
        }
        return $this;
    }

    public function add_pictures(array $pictures) {
        foreach ($pictures as $picture) {
            if ($picture instanceof Picture) {
                $this->_pictures[$picture->subcategory_id][] = $picture;
            }
        }
        return $this;
    }

    public function add_property_type($type) {
        $this->_property_type = $type;
        return $this;
    }

    public function add_report_number($number) {
        $this->_report_number = $number;
        return $this;
    }

    public function add_general_selected($var) {
        $this->_general_selected = $var;
        return $this;
    }

    public function add_company_logo($path) {
        $this->_company_logo = $path;
        return $this;
    }

    public function equal(Record $record) {
        return true;
    }

    public function sleep() {
        $this->type_ids = serialize($this->type_ids);
        parent::sleep();
    }

    public function wake_up() {
        $this->type_ids = unserialize($this->type_ids);
        parent::wake_up();
    }

}

class Categories {

    public $categories = array();

    public function __construct($categories = array()) {
        $this->categories = $categories;
    }

    public function set_categories($categories) {
        DebugBreak();
        $this->categories = $categories;
    }

    public function add_value(Value $value, $index) {
        if ($category_id = $this->get_category_id($value->subcategory_id)) {
            if ($subcategory = $this->categories[$category_id][$index]->_subcategories[$value->subcategory_id]) {
                if (!$subcategory->_values) {
                    $subcategory->_values = array();
                }

                if ($value->id == null) {
                    $count = 0;
                    foreach ($subcategory->_values as $id => $sub_value) {
                        if (strpos($id, 'RS_') !== false) {
                            $count++;
                        }
                    }
                    $value->set_id('RS_' . $value->subcategory_id . '_' . $count);
                }

                $subcategory->_values[$value->id] = $value;
            }
        } else {
            throw new Exception('Cant find category for given value subcategory: ' . $value->subcategory_id);
        }

        return $value->id;
    }

    private function get_category_id($subcategory_id) {
        foreach ($this->categories as $category_id => $categories) {
            foreach ($categories as $index => $category) {
                foreach ($category->_subcategories as $subindex => $subcat) {
                    if ($subindex == $subcategory_id) {
                        return $category->id;
                    }
                }
            }
        }
        return null;
    }

    public function add_subcategory($subcategory, $index) {

        $values = $subcategory->_values ? $subcategory->_values : array();
        if (!$this->categories[$subcategory->category_id][$index]->_subcategories) {
            $this->categories[$subcategory->category_id][$index]->_subcategories = array();
        }

        if ($subcategory->id == null) {
            $count = 0;
            foreach ($this->categories[$subcategory->category_id][$index]->_subcategories as $sub_id => $value) {
                if (strpos($sub_id, 'RS_') !== false) {
                    $count++;
                }
            }
            $subcategory->set_id('RS_' . $subcategory->category_id . '_' . $index . '_' . $count);
        }

        if ($index !== null) {
            $this->categories[$subcategory->category_id][$index]->_subcategories[$subcategory->id] = $subcategory;
        } else {
            foreach ($this->categories[$subcategory->category_id] as $index => $category)
                $category->_subcategories[$subcategory->id] = $subcategory;
        }

        return $subcategory->id;
    }

    public function add_category(Category $category, $index = null) {
        if (!isset($this->categories[$category->id])) {
            $this->categories[$category->id] = array();
        }
        $index = $index === null ? count($this->categories[$category->id]) : $index;
        $this->categories[$category->id][$index] = $category;
        if (!$category->get_general_description_subcategory()) {
            $this->add_subcategory($this->create_general_description_subcategory($category->id), $index);
        }
        return $index;
    }

    public function add_concern($concern, $subcategory_id, $index) {
        $category_id = $this->get_category_id($subcategory_id);
        if ($value = $this->categories[$category_id][$index]->_subcategories[$subcategory_id]->_values[$concern->value_id]) {
            if ($concern->id == null) {
                $count = 0;
                foreach ($value->_concerns as $sub_id => $conc) {
                    if (strpos($sub_id, 'RS_') !== false) {
                        $count++;
                    }
                }
                $concern->set_id('RS_' . $category_id . '_' . $index . '_' . $subcategory_id . '_' . $value->id . '_' . $count);
            }
            $value->_concerns[$concern->id] = $concern;
        }
        return $concern->id;
    }

    public function get_category_index($category_id) {
        $index = 0;
        if (isset($this->categories[$category_id])) {
            $index = count($this->categories[$category_id]);
        }
        return $index;
    }

    public function get_category($category_id, $index = 0) {
        $category = null;
        if (isset($this->categories[$category_id][$index])) {
            $category = $this->categories[$category_id][$index];
        } else {
            throw new Exception('Category doesn exist in categories');
        }
        return $category;
    }

    public function create_general_description_subcategory($category_id) {
        $subcategory = new Subcategory();
        $subcategory
                ->set_name('General Details')
                ->set_category_id($category_id)
                ->set_general_description(true);
        return $subcategory;
    }

    public function remove_category($category_id, $index) {
        $object = $this->categories[$category_id][$index];
        unset($this->categories[$category_id][$index]);
        return $object;
    }

    public function remove_subcategory($category_id, $index, $subcategory_id) {
        $object = $this->categories[$category_id][$index]->_subcategories[$subcategory_id];
        unset($this->categories[$category_id][$index]->_subcategories[$subcategory_id]);
        return $object;
    }

    public function remove_value($category_id, $index, $subcategory_id, $value_id) {
        $object = $this->categories[$category_id][$index]->_subcategories[$subcategory_id]->_values[$value_id];
        unset($this->categories[$category_id][$index]->_subcategories[$subcategory_id]->_values[$value_id]);
        return $object;
    }

    public function remove_concern($category_id, $index, $subcategory_id, $value_id, $concern_id) {
        $object = $this->categories[$category_id][$index]->_subcategories[$subcategory_id]->_values[$value_id]->_concerns[$concern_id];
        unset($this->categories[$category_id][$index]->_subcategories[$subcategory_id]->_values[$value_id]->_concerns[$concern_id]);
        return $object;
    }

}

class Category extends Record {

    public $table = 'dr_report_categories';
    public $name;
    public $active;
    public $_index;
    public $_subcategories = array();
    public $_general_descriptions = array();

    public function __construct($id = null, $name = null, $company_id = null, $active = null) {
        $this->name = $name;
        $this->active = $active;
        parent::__construct($id, $company_id);
    }

    public function set_name($name) {
        $this->name = $name;
        return $this;
    }

    public function set_active($active) {
        $this->active = $active;
        return $this;
    }

    public function add_subcategories($_subcategories) {
        $this->_subcategories = $_subcategories;
    }

    public function add_subcategory($_subcategory) {
        $this->_subcategories[] = $_subcategory;
    }

    public function get_general_description_subcategory() {
        $subcat = false;
        foreach ($this->_subcategories as $subcategory) {
            if ($subcategory->general_description) {
                $subcat = $subcategory;
                break;
            }
        }
        return $subcat;
    }

    public function remove_general_description_subcategory_from_tree($index, Subcategory $subcategory = null) {
        $subcategory = $subcategory ? $subcategory : $this->get_general_description_subcategory();
        if (isset($this->_subcategories[$subcategory->id])) {
            unset($this->_subcategories[$subcategory->id]);
        }
        return $this;
    }

    public function set_index($index) {
        $this->_index = $index;
    }

    public function validate() {
        $valid = false;
        foreach ($this->_subcategories as $id => $subcategory) {
            if ($subcategory->validate()) {
                $valid = true;
                break;
            }
        }
        return $valid;
    }

}

class Subcategory extends Category {

    const RATING_GOOD = 'good';
    const RATING_FAIR = 'fair';
    const RATING_POOR = 'poor';
    const RATING_NA = 'n/a';

    public $table = 'dr_report_subcategories';
    public $category_id;
    public $general_description;
    public $_values;
    public $_value_ids = array();
    public $_rating;
    public $_comments;

    public function __construct($id = null, $name = null, $category_id = null, $company_id = null, $active = null, $general_description = false) {
        if ($category_id) {
            $this->category_id = $category_id;
        }
        $this->general_description = $general_description !== null ? ($general_description ? 1 : 0) : null;
        $this->id = $id;
        $this->active = $active;
        $this->company_id = $company_id;
        $this->name = $name;
        $this->_values = array();
    }

    public function set_category_id($id) {
        $this->category_id = $id;
        return $this;
    }

    public function set_general_description($desc) {
        $this->general_description = $desc;
        return $this;
    }

    public function add_rating($rating) {
        $this->_rating = $rating;
        return $this;
    }

    public function add_comments($comments) {
        $this->_comments = $comments;
        return $this;
    }

    public function add_selected_values($ids) {
        if (is_array($ids)) {
            $this->_value_ids = $ids;
        }
        return $this;
    }

    public function add_values($values) {
        $this->_values = $values;
        return $this;
    }

    public function add_value($value) {
        $this->_values[] = $value;
        return $this;
    }

    public function get_value($value_id) {
        if (isset($this->_values[$value_id])) {
            return $this->_values[$value_id];
        }
    }

    public function validate() {
        $valid = false;
        foreach ($this->_value_ids as $id) {
            if ($this->_values[$id]->validate()) {
                $valid = true;
                break;
            }
        }
        return $valid;
    }

}

class Value extends Subcategory {

    public $table = 'dr_report_categorie_values';
    public $subcategory_id;
    public $_concerns = array();
    public $_concern_ids = array();
    public $_selected;

    public function __construct($id = null, $name = null, $subcategory_id = null, $company_id = null, $active = null) {
        $this->subcategory_id = $subcategory_id;
        parent::__construct($id, $name, null, $company_id, $active, null);
    }

    public function set_subcategory_id($id) {
        $this->subcategory_id = $id;
        return $this;
    }

    public function add_concerns($concerns) {
        $this->_concerns = $concerns;
    }

    public function add_concern($concern) {
        $this->_concerns[] = $concern;
    }

    public function add_selected_concerns($ids) {
        $this->_concern_ids = $ids;
    }

    public function validate() {
        $valid = false;
        if ($this->name) {
            $valid = true;
        } else {
            foreach ($this->_concern_ids as $id) {
                if (is_object($this->_concerns[$id]) && $this->_concerns[$id]->validate()) {
                    $valid = true;
                    break;
                }
            }
        }
        return $valid;
    }

}

class Concern extends Value {

    public $table = 'dr_report_concerns';
    public $value_id;
    public $name;

    public function __construct($id = null, $name = null, $value_id = null, $company_id = null, $active = null) {
        $this->value_id = $value_id;
        parent::__construct($id, $name, null, $company_id, $active);
    }

    public function validate() {
        if ($this->name) {
            return true;
        }
        return false;
    }

}

class Picture extends Record {

    const PICTURE_PREFACE_ID = '0';

    public $table = 'dr_report_pictures';
    public $id;
    public $report_id;
    public $subcategory_id;
    public $path;

    public function __construct($id = null, $company_id = null, $subcategory_id = null, $path = null, $report_id = null) {
        $this->subcategory_id = $subcategory_id;
        $this->report_id = $report_id;
        $this->id = $id;
        $this->path = $path;
        parent::__construct($id, $company_id);
    }

    public function set_id($id) {
        $this->id = $id;
        return $this;
    }

    public function set_report_id($var) {
        $this->report_id = $var;
        return $this;
    }

    public function set_subcategory_id($var) {
        $this->subcategory_id = $var;
        return $this;
    }

    public function set_path($var) {
        $this->path = $var;
        return $this;
    }

    public function equal(Record $record) {
        
    }

}

class Property extends Record {

    public $table = 'dr_report_properties';
    public $name;
    public $description;
    public $image_filename;

    public function __construct($name = null, $description = null, $id = null, $company_id = null, $active = null, $image_filename = null) {
        $this->name = $name;
        $this->description = $description;
        $this->active = $active;
        $this->image_filename = $image_filename;
        parent::__construct($id, $company_id);
    }

    public function set_name($name) {
        $this->name = $name;
    }

    public function set_description($description) {
        $this->description = $description;
    }

    public function set_image_filename($image_filename) {
        $this->image_filename = $image_filename;
    }

    public function validate() {
        return true;
    }

    public function equal(Record $record) {
        
    }

}

class Template extends Record {

    public $table = 'dr_report_templates';
    public $property_id;
    public $name;
    public $description;

    /**
     * put your comment there...
     * 
     * @var Categories
     */
    public $categories;
    public $active;

    public function __construct($name = null, $description = null, $property_id = null, $categories = null, $id = null, $company_id = null, $active = null) {
        $this->name = $name;
        $this->description = $description;
        $this->property_id = $property_id;
        $this->active = $active;
        $this->categories = new Categories();
        parent::__construct($id, $company_id);
    }

    public function set_name($name) {
        $this->name = $name;
    }

    public function set_description($description) {
        $this->description = $description;
    }

    public function set_property_id($id) {
        $this->property_id = $id;
    }

    public function set_categories($categories) {
        $this->categories = $categories;
    }

    public function set_active($active) {
        $this->active = $active;
    }

    public function get_category_index($category_id) {
        return $this->categories->get_category_index($category_id);
    }

    public function add_category($category, $index = null) {
        return $this->categories->add_category($category, $index = null);
    }

    public function add_concern($concern, $subcategory_id, $index) {
        return $this->categories->add_concern($concern, $subcategory_id, $index);
    }

    public function add_subcategory($subcategory, $index) {
        return $this->categories->add_subcategory($subcategory, $index);
    }

    public function add_value($value, $index) {
        return $this->categories->add_value($value, $index);
    }

    public function remove_category($category_id, $index) {
        return $this->categories->remove_category($category_id, $index);
    }

    public function remove_subcategory($category_id, $index, $subcategory_id) {
        return $this->categories->remove_subcategory($category_id, $index, $subcategory_id);
    }

    public function remove_value($category_id, $index, $subcategory_id, $value_id) {
        return $this->categories->remove_value($category_id, $index, $subcategory_id, $value_id);
    }

    public function remove_concern($category_id, $index, $subcategory_id, $value_id, $concern_id) {
        return $this->categories->remove_concern($category_id, $index, $subcategory_id, $value_id, $concern_id);
    }

    public function get_categories() {
        return $this->categories;
    }

    public function is_category_set($category_id, $category_index) {
        if (isset($this->categories->categories[$category_id][$category_index])) {
            return true;
        }
        return false;
    }

    public function is_value_set($category_id, $category_index, $subcategory_id, $value_id) {
        if (isset($this->categories->categories[$category_id][$category_index]->_subcategories[$subcategory_id]->_values[$value_id])) {
            return true;
        }
        return false;
    }

    public function get_value($category_id, $category_index, $subcategory_id, $value_id) {
        if (isset($this->categories->categories[$category_id][$category_index]->_subcategories[$subcategory_id]->_values[$value_id])) {
            return $this->categories->categories[$category_id][$category_index]->_subcategories[$subcategory_id]->_values[$value_id];
        }
    }

    public function get_subcategory($category_id, $category_index, $subcategory_id) {
        if (isset($this->categories->categories[$category_id][$category_index]->_subcategories[$subcategory_id])) {
            return $this->categories->categories[$category_id][$category_index]->_subcategories[$subcategory_id];
        }
    }

    public function set_concern_selected($category_id, $index, $subcategory_id, $value_id, $concern_id) {
        if ($value = $this->get_value($category_id, $index, $subcategory_id, $value_id)) {
            if (isset($value->_concerns[$concern_id])) {
                $value->_concerns[$concern_id]->set_selected(true);
            }
        }
    }

    public function find_subcategory($subcategory_id, $return_scalar = false) {
        $found = array();
        foreach ($this->categories->categories as $category_id => $categories) {
            foreach ($categories as $index => $category) {
                if (isset($category->_subcategories[$subcategory_id])) {
                    if ($return_scalar) {
                        return $category->_subcategories[$subcategory_id];
                    } else {
                        $found[] = $category->_subcategories[$subcategory_id];
                    }
                }
            }
        }
        return $found;
    }

    public function wake_up() {
        $this->categories = is_object($array = @unserialize(@gzuncompress($this->categories))) ? $array : new Categories();
    }

    public function sleep() {
        $this->categories = $this->categories ? gzcompress(serialize($this->categories)) : null;
    }

}

class Undo extends Record {

    const TYPE_CONCERN = 'concern';
    const TYPE_SUBCATEGORY = 'subcategory';
    const TYPE_VALUE = 'value';
    const TYPE_CATEGORY = 'category';

    public $table = 'dr_report_undo';
    public $id;
    public $object_id;
    public $type;
    public $category_id;
    public $category_index;
    public $subcategory_id;
    public $value_id;
    public $concern_id;
    public $object;

    public function __construct($object_id = null, $type = null, $category_id = null, $category_index = null, $subcategory_id = null, $value_id = null, $concern_id = null, $object = null, $id = null) {
        $this->object = $object;
        $this->object_id = $object_id;
        $this->category_id = $category_id;
        $this->category_index = $category_index;
        $this->subcategory_id = $subcategory_id;
        $this->value_id = $value_id;
        $this->concern_id = $concern_id;
        $this->type = $type;
        $this->id = $id;
    }

    public function set_category_id($val) {
        $this->category_id = $val;
    }

    public function set_category_index($val) {
        $this->category_index = $val;
    }

    public function set_subcategory_id($val) {
        $this->subcategory_id = $val;
    }

    public function set_value_id($val) {
        $this->value_id = $val;
    }

    public function set_concern_id($val) {
        $this->concern_id = $val;
    }

    public function set_object($val) {
        $this->object = $val;
    }

    public function set_type($val) {
        $this->type = $val;
    }

    public function set_object_id($val) {
        $this->object_id = $val;
    }

    public function wake_up() {
        $this->object = unserialize(gzuncompress($this->object));
    }

    public function sleep() {
        $this->object = gzcompress(serialize($this->object));
    }

}

###############################################
###############################################

class Order_model_exception extends Exception {
    
}

//End of model
