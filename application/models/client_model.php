<?php
class Client_model extends Order_model{
    public function get_client_data( Person $person ){
        $person->set_person_type( Person::TYPE_AGENT );
        $persons = $this->get_persons( $person );
        
        $person->set_person_type( Person::TYPE_CLIENT );
        $persons = $persons + $this->get_persons( $person );
         
        $clients = array();
        foreach( $persons as $person ){
            $client = new Client( $person );
            $client->set_amount_due( $this->get_client_amount_due( $client ));
            $clients[] = $client;
        }
        
        /*
        $filter = new Filter();
        $filter->set_company_id( $person->company_id );
        $orders = $this->get_orders( $filter );
        foreach( $orders as $order ){
            $_person = new Person();
            $_person->set_name( $order->buyer );
            $client = new Client( $_person );
            $client->set_amount_due( $order->amount_due );
            $clients[] = $client;
        }
        */
        
        return $clients;
    }    
    public function get_client_amount_due( Client $client ){
        $query = "SELECT SUM(amount_due) AS amount_due FROM `dr_orderss` WHERE client_id = '$client->id' and is_quote=0";
        
        $result = $this->db->query( $query );
        $amount_due = 0;
        if( $result->num_rows() ){
            foreach( $result->result() as $row ){
                $amount_due = $row->amount_due;
                break;
            }            
        }

        return round($amount_due, 2);
    }
}

class Client extends Person{
    public $amount_due;
    
    public function __construct( Person $person = null ){
        if( $person ){
            foreach( $person as $field=>$value){
                $this->$field = $value;
            }
        }
    }

    public function set_amount_due( $amount_due ){
        if( !is_numeric( $amount_due )){
            $this->errors['amount_due'] = 'Due amount must be numeric';
        }
        $this->amount_due = $amount_due;
        return $this;
    }
}
