<?php
class Schedule_model extends Order_model{
    
    public function get_messages( Message $filter ){
        $messages = $this->get_object( $filter );
        
        $user_filter = new Person();
        foreach( $messages as $message ){
             
             $user_filter->set_id( $message->from_user_id );
             $message->_user_from = reset($this->get_object( $user_filter ));
             $user_filter->set_id( $message->to_user_id );
             $message->_user_to   = reset($this->get_object( $user_filter ));
        }
        return $messages;
    }
    public function get_recipients( Person $filter ){
        
        $persons = $this->get_object( $filter );
        $recipients = array();
        foreach( $persons as $person ){
            $recipients[ $person->id ] = $person->name;
        }
        return $recipients;
    }
}
