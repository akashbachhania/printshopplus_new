<?php
class Home_model extends Order_model{
    
	 public function __construct( ){
        parent::__construct();
		$this->load->database();
    }
	
	public function get_jobs_bystatus($jobstatus)
	{		
		$sqlquery="SELECT JobId, JobDate,IFNULL(DATEDIFF(CURDATE(), jobs.ReceivableDate)+1, '-1') AS daysOut, JobName, Description, Date, DueDate, Rep, Press, JobNumber, Status, ETA, InvoiceId, operator, tag, States FROM jobs where States='".$jobstatus."'";		    
        $results = $this->db->query($sqlquery);
        return $results->result();			
	}  
	
	public function get_jobs_byid($jobid)
	{		
		$sqlquery="SELECT JobId, JobDate,IFNULL(DATEDIFF(CURDATE(), jobs.ReceivableDate)+1, '-1') AS daysOut, JobName, Description, Date, DueDate, Rep, Press, JobNumber, Status, ETA, InvoiceId, operator, tag, States FROM jobs where jobid='".$jobid."'";		    
        $results = $this->db->query($sqlquery);
        return $results->result();			
	} 
	
	public function updateoperator($jobid, $operatorname)
	{
		$sqlquery="update jobs set operator='".$operatorname."' where jobid='".$jobid."'";		    
        $results = $this->db->query($sqlquery);
	}
	
	public function updatetag($jobid, $tagname)
	{
		$sqlquery="update jobs set tag='".$tagname."' where jobid='".$jobid."'";		    
        $results = $this->db->query($sqlquery);
	}
	
	public function updatestate($jobid, $statename)
	{
		$sqlquery="update jobs set States='".$statename."' where jobid='".$jobid."'";		    
        $results = $this->db->query($sqlquery);
	}
	
	public function deletejob($jobid)
	{
		$sqlquery="delete FROM jobs where jobid='".$jobid."'";		    
        $results = $this->db->query($sqlquery);
	}    
	
	public function get_operators()
	{
		$sqlquery="SELECT * FROM Operators";		    
        $results = $this->db->query($sqlquery);
        return $results->result();			
	}    
	public function get_tags()
	{
		$sqlquery="SELECT * FROM tags";		    
        $results = $this->db->query($sqlquery);
        return $results->result();			
	}  
	
	public function get_jobstatus()
	{
		$sqlquery="SELECT * FROM jobstatus";		    
        $results = $this->db->query($sqlquery);
        return $results->result();			
	}      
	
	
	
    public function get_messages( Message $filter, Person $user ){
        
        $messages = $this->get_object( $filter );
        
        $user_filter = new Person();
        foreach( $messages as $message ){
             
             $user_filter->set_id( $message->from_user_id );
             $message->_user_from = reset($this->get_object( $user_filter ));
             $user_filter->set_id( $message->to_user_id );
             $message->_user_to   = reset($this->get_object( $user_filter ));
             $message->datetime   = $this->get_us_date_format( $message->datetime );
        }
        return $messages;
    }
    public function get_recipients( Person $filter, Person $user ){
        
        $myself    = $user->id;
        $mycompany = $filter->company_id;
        if( $user->person_type == Person::TYPE_ADMIN){
            $filter    = new Person();
            $myself    = $user->id;
            $mycompany = 'AdminStaff';
        }
        
                   
        $persons = $this->get_object( $filter );
        $cached_companies = array();
        $cached_companies['0'] = 'AdminsStaff';
        
        $companies = array();
        $recipients = array();
        foreach( $persons as $person ){
            if( !isset($cached_companies[$person->company_id])){
                $company_filter = new Company();
                $company_filter->set_id( $person->company_id );
                $company = $this->get_object( $company_filter );
                $cached_companies[ $person->company_id ] = $company['0']->name;
            }
            if( $user->person_type == Person::TYPE_ADMIN ){
                if( $person->person_type == Person::TYPE_USER){                                        
                    $companies[ $cached_companies[$person->company_id]][ $person->id ] = $person->name;    
                }                     
            } else {
                if( $name = $this->get_person_type( $person  )){
                    $companies[ $cached_companies[$person->company_id]][ $person->id ] = $name . $person->name;     
                }                   
            }         
        } 
        $companies[ $mycompany ][ $myself ] = 'Myself'; 
        return $companies;
    }
    public function get_person_type( Person $person ){
        $name = '';
        switch( $person->person_type ){
            case Person::TYPE_INSPECTOR:
                $name = '[Inspector]';
            break;
            case Person::TYPE_USER:
                $name = '[Coordinator]';
            break;            
        }
        return $name;
    }
}  
?>
