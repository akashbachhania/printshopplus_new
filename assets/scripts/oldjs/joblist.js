function cleanURL(strurl)
{
	var index = strurl.indexOf("/1");	
	if(index > 0) 
	{
		strurl= strurl.substr(0,index);
		strurl = strurl + "/1";
	}	
	index = strurl.indexOf("/2");	
	if(index > 0) 
	{
		strurl= strurl.substr(0,index);
		strurl = strurl + "/2";
	}		
	return strurl;
//	alert( "Final URL - " +strurl);	
}

function edit_job(jobid)
{
	var loc=location.href + "/e/" + jobid ;
	alert(loc);
	//window.location.assign(loc);
}

function delete_job(jobid)
{
	var getconfirmation=confirm("Do you want to delete this job ?");
	if(getconfirmation)
	{
		var loc=location.href;
		loc =  cleanURL(loc) + "/d/" + jobid;
		window.location.assign(loc);
	}
}

function changeoperator(operatorlist, jobid)
{
	var operatorname = operatorlist.options[operatorlist.selectedIndex].text;
	var loc=location.href;
	cleanURL(loc);
	loc =  cleanURL(loc) + "/op/" + jobid +"/"+operatorname;	
	window.location.assign(loc);
}

function changetagname(taglist, jobid)
{
	var tagname = taglist.options[taglist.selectedIndex].text;
	var loc=location.href;
	loc = cleanURL(loc) + "/t/" + jobid +"/"+tagname;
	window.location.assign(loc);
}

function changestate(statelist, jobid)
{
	var statename = statelist.options[statelist.selectedIndex].text;
	var loc=location.href;
	loc =  cleanURL(loc) + "/s/" + jobid +"/"+statename;
	window.location.assign(loc);
}
