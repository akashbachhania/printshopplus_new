jQuery(document).ready(function(){
	$("#iconbar li").hover(
		function(){
			var iconName = $(this).find("img").attr("src");
			var origen = iconName.split("x.")[0];
			$(this).find("img").attr({src: "" + origen + "o.gif"});
			$(this).find("h5").attr({
				"style": 'display:inline'
			});
			$(this).find("h5").animate({opacity: 1, top: "-170"}, {queue:false, duration:100});
		}, 
		function(){
			var iconName = $(this).find("img").attr("src");
			var origen = iconName.split("o.")[0];
			$(this).find("img").attr({src: "" + origen + "x.gif"});
			$(this).find("h5").animate({opacity: 0, top: "-170"}, {queue:false, duration:100}, "linear",
			$("h5").hide(),
				function(){
					$(this).find("h5").attr({"style": 'display:none'});			
				}
			);
		});
	$("#matte li").hover(
		function(){
			var iconName = $(this).find("img").attr("src");
			var origen = iconName.split("x.")[0];
			$(this).find("img").attr({src: "" + origen + "o.gif"});
			$(this).find("h5").attr({
				"style": 'display:inline'
			});
			$(this).find("h5").animate({opacity: 1, top: "-170"}, {queue:false, duration:100});
		}, 
		function(){
			var iconName = $(this).find("img").attr("src");
			var origen = iconName.split("o.")[0];
			$(this).find("img").attr({src: "" + origen + "x.gif"});
			$(this).find("h5").animate({opacity: 0, top: "-170"}, {queue:false, duration:100}, "linear",
			$("h5").hide(),
				function(){
					$(this).find("h5").attr({"style": 'display:none'});			
				}
			);
		});
});