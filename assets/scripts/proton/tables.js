$(document).ready(function() {
    !verboseBuild || console.log('-- starting proton.tables build');
    
    proton.tables.build();
});

proton.tables = {
	build: function () {
		// Data Tables
		$('#tableSortable,#tble2').dataTable({
				responsive: true,
				"order": [[ 0, "desc" ]]
			}); 
		$('#tble22').dataTable({
			responsive:true,
			"order": [[ 0, "desc" ]]
		});
		$('.dataTables_wrapper').find('input, select').addClass('form-control');
		$('.dataTables_wrapper').find('input').attr('placeholder', 'Quick Search');

		$('.dataTables_wrapper select').select2();
	}
}